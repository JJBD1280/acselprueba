CREATE OR REPLACE PACKAGE BODY ACSELPRB.PR_POLIZA AS
/*
  REVISIONS:

  Ver          Fecha      Autor        Descripcion
  1.34        05/02/2019  JR           #22484 Coaseguro Merge Produccion 1.0.1.20 con 1.28 y 1.31 de Calidad.
  1.0.1.20    14/01/2019  SII          #24317 Calculo de prima al anular una prorroga
  1.0.1.19    01/11/2018  COTECSA      #23911 Indicador para Anulacion de Productos Solo a Inicio de Vigencia.
  1.0.1.18    03/08/2018  COTECSA      Redmine #23771  Proyecto Suplementos WEB.
  1.0.1.17    04/09/2018  COTECSA-FM   REDMINE 23718  SE COLOCA LA MODIFICACI�N DEL PROCESO DE PRORROGA EL CUAL NO SE VERSIONO EN LA 1.0.1.14
  1.0.1.16    23/08/2018  SII-GROUP   Redmine 23825   Producci�n - (Aranda XXXX) Error al activar prorroga (SE AGREGA UNA VALIDACION EN EL PROCEDIMIENTO ES_MOVIMIENTO_DEL_PERIODO QUE NO TOME LAS PRORROGAS)
  1.0.1.15    10/08/2018  COTECSA-FM   REDMINE 23718   se modifica el n�mero de versi�n de la 1.0.1.14
  1.0.1.14    10/08/2018  COTECSA-FM   REDMINE 23718   Modificaci�n de los procesos ANULAR_SIS, PRORROGA, ACTIVAR para dejar una huella al momento de realizar 
                                                       una prorroga y as� poder devolver correctamente la prorroga cuando anulen la p�liza 
  1.0.1.13    16/07/2018  Consis-ATCP  Redmine 21858   RQ23708 Modifica el mov_definitivo del comprobante 025 generado con la trm de la fecha Emi  
  1.0.1.12    05/06/2018  COTECSA-MX   Redmine #23453  Se Elmina la validacion de Saldos Pendientes por Casos Especiales en Prorrata 8.  
  1.0.1.11    18/05/2018  COTECSA-FM   REDMINE *23432* Se modifica el proceso FACT_COMPLEMENTO_RAMO para que solo se haga la modificaci�n a la cobertura a la cual se le esta colocando
                                                       el valor a facturar y Se comenta esta linea debido a que esta mal el if IF nMontoApl > 0 THEN.
  1.0.1.10    10.05.2018  CONSIS JD   MERGE 21858 con las siguientes versiones:
  --                                    1.4         22/11/2017  CONSIS - EFVC  REDMINE 22583- 21858. Redondeo Moneda Extranjera
  1.0.1.9     14/03/2018  SII-GROUP   Redmine #23276   Producci�n - (Aranda 204242) Error al emitir suplementos. Se Agrego un Cursor para que valide varias operaciones
  1.0.1.8     11/03/2018  BBVA-JJBD    Se agrega validacion de fechas en la funcion ANULAR_SIS.
  1.0.1.7     05/03/2018  COTECSA-MX   Redmine #23183 Nueva Validacion para Renovar polizas, para validar movimientos pendientes (MOD_COBERT) antes de Renovar.
  1.0.1.6     05/03/2018  COTECSA-MX   Merge de la Version v 1.0.1.3.1.2 con la Version v 1.0.1.4. (Se Nivelan las Versiones de Produccion, las 1.0.1.5 ya esta inmersa en este cambio).
  1.0.1.3.1.2 09/02/2018  COTECSA-FM   REDMINE *23135* - Se le realiza un cambio al proceso FACT_COMPLEMENTO_RAMO para buscar el IDECOBERT y el NUMMOD que tenga PRIMAMONEDA > 0
  1.0.1.3.1.1 22/01/2018  TECNOCOM     REDMINE 22981     Se valida Bloquear los movimientos del Accionista que se encuentre ACT en la lista BAEP.
  1.0.1.3.1.0 26/01/2018  TECNOCOM     REDMINE 23070     Se realiza ajuste en renovaciones para realizar el copiado de tarificacion, antes de generacion de coberturas de asegurado. (Merge Ver 1.10)
  1.0.1.3     26/12/2017  BBVA-RGS     REDMINE 22954     Solo se valida que la POLIZA tenga siniestro en estado activo o modificado
  1.0.1.2     14/12/2017  TECNOCOM     REDMINE 22964   - Revocar Poliza Tipo Prorrata II (ANULAR_SIS).
  1.0.1.1     09/11/2017  4SIGHT-EFBC  REDMINE 22491   - Se agrega filtro en procedimiento ANULAR_SIS.
  1.0.1.0     18/10/2017  COTECSA-FM   Merge 1.88 REDMINE *22327* - Se comenta el llamdo del pck pr_factura.anular en el proceso anula_sis debido a que no debe anular factura.
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------
    1.57.1.17  19/07/2017  OJRG    CC 22292. Se utiliza nueva variable en RENOVAR_CERTIFICADO, la cual almacena el resultado de PR.SALUD; as� se reduce el numero de concurrencias al mismo.
    1.57.1.16  14/07/2017 OGQB  Merge 1.83 COTECSA-MX Redmine #22049 - Validar Estatus de Siniestros (ANU y DEC) para las Anulaciones.
    1.57.1.15  07/07/2017 4SIGHT-EFBC  Redmine 21875 - Cambios para ANULACION de p�lizas PRORRATA 8
    1.57.1.14  23/06/2017 YEIK - SETI  Optimizacion query en linea 9036
  D 1.57.1.13  03/04/2017          JC.DuqueL Mto fijo para polizas WEB
  D 1.57.1.12  04/04/2017  EFBC    4SIGHT-RM21556 Se agrega llamado a la funcion ACTUALIZA_CONTROL_POLIZA en RENOVAR_CERTIFICADO
  D 1.57.1.11  22/03/2017  EFBC    4SIGHT-RM21556 Se agrega funcion para actualizar CONTROL_POLIZA y llamado en RENOVAR.
  D 1.57.1.10  22/03/2017  EFBC    4SIGHT-RM21556 Se soluciona Concurrencia con version de ATCP.
  D 1.57.1.9  22/03/2017  EFBC    4SIGHT-RM21556 Se soluciona Concurrencia con version de ATCP.
  D 1.57.1.8  22/03/2017  EFBC    4SIGHT-RM21556 Se agrega funcion para actualizar CONTROL_POLIZA y llamado en RENOVAR.
  D 1.57.1.7  10/11/2016  ATCP    RC20548 Se adiciona validaci�n para que no intente insertar varias veces en t$_factura a la misma factura y numseq
  D 1.57.1.6  29/09/2016  EDMV     CC Redmine # 20756, No realiza el cruce de polizas HTWB, se le agrego el 'X' en el query detectado por Carlos Mora linea 5651
  D 1.57.1.5  22/08/2016  EDMV     #20477 Vigencia 10 meses
  D 1.57.1.4  28/04/2016  CAMM     19800 Correccion fechas de movimiento en facturacion complementaria
  D 1.57.1.3  30/03/2016  FDCR     19526 Llamado al Procedimiento PR_DATOS_PARTICULARES_VIDA.INCLUIR.
  D 1.57.1.2  03/03/2016  RRR      18607-Performance Activaciones P�lizas Colectivas Interfaz 697. Merge v.1.61
  D 1.57.1.1  10.02.2015 JD merge de 1.57 con 1.60
  D 1.57.1  10.02.2015 JD merge de 1.57 con 1.60
  D 1.33 - Miguel Portillo - RM14272 - 16/06/2015
  D 1.35 - Miguel Portillo - RM14272 - 18/06/2015
  D 1.37   22.05.15    KMAYEN   Incidencia #13581 Exequias TLMK ACSELWEB
                                Cancelaciones con suplementos (1era Entrega)
  1.49   - 03/09/2015  Luz Aida Se inserta en la tabla texto_endoso en el
                                procedimiento ANULAR_SIN_CALC
  1.50     07/09/2015  KMAYEN   Performance #15692 se modifica CARGAR
  1.51     23/09/2015  RRR      Se modifica CARGAR, Incidencia # 16328, se deshace 1.50
  1.52     24/09/2015  Luz Aida Se realiza modificaci�n en FACT_COMPLEMENTO_RAMO para la mejora 8114
  1.53     30/09/2015  KMAYEN   #15672 Se modifica COMP_SUMAASEG se elimina llamada
                                del cursor de SUMA_ANT.
  1.54     29/10/2015  EFVC     Se cambia temporalmente indicado para que permita hacer la rehabilitacion de polizas
  1.55     30/10/2015  EFVC     Control para cambia temporalmente indicado para que permita hacer la rehabilitacion de polizas
  1.56     09/11/2015  EFVC     Se cambia Condicion de Control para proceso rehabilitacion de polizas se atualice el monto de la operacion
  1.57     17/11/2015  FDCR     Body con procedimiento para actualizar campos nuevos en MOD_COBERT y llamado de la funcionalidad  los comentarios tiene fecha 30/09/2015.
  --------- ---------- --------------- ------------------------------------
*/

   FUNCTION ASIG_IDEPOL
      RETURN NUMBER IS
      nIdePol   POLIZA.IdePol%TYPE;
   BEGIN
      SELECT SQ_IDEPOL.NEXTVAL
      INTO   nIdePol
      FROM   SYS.DUAL;
      RETURN (nIdePol);
   END ASIG_IDEPOL;
   --ACTUALIZA_CONTROL_POLIZA
   --<<BBVA 22/03/2017 #21556 4Sight-EFBC Procedimiento creado para la actualizacion de la tabla
   --CONTROL_POLIZA. Por ahora s�lo actualiza IndRecaudoOnline ya que esto soluciona el RM.
   --Si se requiere actualizar alg�n otro campo, se debe adicionar en este procedimiento.
   PROCEDURE ACTUALIZA_CONTROL_POLIZA(rControlPoliza CONTROL_POLIZA%ROWTYPE) IS
   BEGIN
     UPDATE CONTROL_POLIZA
        SET indrecauonline = rControlPoliza.Indrecauonline
      WHERE idecotiza = rControlPoliza.Idecotiza;
   EXCEPTION
     WHEN OTHERS THEN
       RAISE_APPLICATION_ERROR(-50054,
                               PR.MENSAJE('ABD',
                                          50054,
                                          'CONTROL_POLIZA',
                                          rControlPoliza.Idecotiza,
                                          NULL));
   END ACTUALIZA_CONTROL_POLIZA;
   -->>BBVA 22/03/2017 #21556 4Sight-EFBC
   --RENOVACION_MASIVA
   PROCEDURE RENOVACION_MASIVA (cIndTodas VARCHAR2, dFecFin DATE, cCodOfi VARCHAR2, cIndTOficina VARCHAR2, cCodProd VARCHAR2, cCodRamo VARCHAR2) IS
      cUsr           VARCHAR2 (10);
      cUsrImp        VARCHAR2 (10);
      nIdePolRen     POLIZA.IdePol%TYPE;
      nNumOper       OPER_POL.NumOper%TYPE;
      cIndRenPend    PRODUCTO.IndRenPend%TYPE;
      nDiasRenPend   PRODUCTO.DiasRenPend%TYPE;
      nExisteRec     NUMBER (3)                  := 0;
      dFecRen        POLIZA.FecRen%TYPE;
      --<I ESVI081> Javier Asmat/07.12.2004
      --Variable para capturar el Idepol del cursor pues no existia y tomaba la variable del package
      nIdePol        POLIZA.IdePol%TYPE;
      --<F ESVI081>
      cIndRen        VARCHAR2 (1);
      CURSOR POL_RENOV_TODAS IS
         SELECT IdePol
         FROM   POLIZA
         WHERE  IndRenAuto = 'S'
         AND    StsPol = 'ACT'
         AND    FecRen <= dFecFin;
      CURSOR POL_RENOV_OFICINA IS
         SELECT IdePol
         FROM   POLIZA
         WHERE  IndRenAuto = 'S'
         AND    StsPol = 'ACT'
         AND    FecRen <= dFecFin
         AND    CodOfiSusc = cCodOfi
         AND    (   CodProd = cCodProd
                 OR cCodProd IS NULL)
         AND    (   CodProd IN (SELECT CodProd
                                FROM   RAMO_PROD
                                WHERE  CodRamo = cCodRamo)
                 OR cCodRamo IS NULL)
         ORDER BY IdePol;
      CURSOR POL_RENOV_PROD IS
         SELECT IdePol
         FROM   POLIZA
         WHERE  IndRenAuto = 'S'
         AND    StsPol = 'ACT'
         AND    FecRen <= dFecFin
         AND    (   CodOfiSusc = cCodOfi
                 OR cCodOfi IS NULL)
         AND    (   CodProd = cCodProd
                 OR cCodProd IS NULL)
         AND    (   CodProd IN (SELECT CodProd
                                FROM   RAMO_PROD
                                WHERE  CodRamo = cCodRamo)
                 OR cCodRamo IS NULL)
         ORDER BY IdePol;
      ---Recibo pendientes---
      CURSOR RECIBO_Q IS
         SELECT COUNT (F.IdeFact)
         FROM   RECIBO R, FACTURA F
         WHERE  R.IdePol = nIdePol
         AND    R.StsRec = 'ACT'
         AND    F.NumOper = R.NumOper
         AND    ((dFecRen - F.FecVencFact) - nDiasRenPend) >= 0;   --<Criterio para renovaci�n con cartera pendiente, tomando en cuenta el n�mero de dias m�ximo para la renovaci�n con pendientes. Rodolfo Mel�ndez <08/04/2005>
   BEGIN
      BEGIN
         SELECT SUBSTR (RTRIM (USER, ' '), 1, 8), 'XX' || SUBSTR (RTRIM (USER, ' '), 3, 6)
         INTO   cUsr, cUsrImp
         FROM   SYS.DUAL;
      END;
      BEGIN
         SELECT NVL (IndRenPend, 'N'), NVL (DiasRenPend, 0)
         INTO   cIndRenPend, nDiasRenPend
         FROM   PRODUCTO
         WHERE  CodProd = cCodProd;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            RAISE_APPLICATION_ERROR (-20220, PR.MENSAJE ('ABD', 20220, 'No existe informaci�n ' || cIndRenPend, NULL, NULL));
      END;
      --<I ESVI081> Javier Asmat/07.12.2004
      --Se activa la variable a S indicando que es Renovacion Masiva PR_POLIZA.cIndRenovMasiva  := 'S';
      --<F ESVI081>
      IF cIndTodas = 'S' THEN
         FOR PR IN POL_RENOV_TODAS LOOP
            --<I ESVI081> Javier Asmat/07.12.2004
            --Se asigna el Idepol del cursor a la variable para que el cursor de recibos levante lo correcto
            nIdePol  := PR.IdePol;
            --<F ESVI081>
            OPEN RECIBO_Q;
            FETCH  RECIBO_Q
            INTO   nExisteRec;
            CLOSE RECIBO_Q;
            --
            IF     nExisteRec > 0
               AND cIndRenPend = 'N' THEN
               cIndRen  := 'N';
            ELSE
               cIndRen  := 'S';
            END IF;
            IF cIndRen = 'S' THEN
               nIdePolRen  := PR_POLIZA.RENOVAR (PR.IdePol, NULL, NULL);
               --
               UPDATE POLIZA
               SET IndRenovLote = cUsr
               WHERE  IdePol = nIdePolRen;
               --
               IF nIdePolRen IS NOT NULL THEN
                  nNumOper  := PR_POLIZA.ACTIVAR (nIdePolRen, 'D');
                  --
                  UPDATE POLIZA
                  SET IndFact = 'N'
                  WHERE  IdePol = nIdePolRen;
                  --
                  PR_GEN_REA.GENERAR_REDISTRIBUIR (nNumOper);
                  PR_ACREENCIA.GENERAR (nNumOper);
                  PR_FRACCIONAMIENTO.GENERAR (nNumOper);
                  PR_MOV_PRIMA.T$_MOV_ELIMINAR (nIdePolRen);
               END IF;
            END IF;
         END LOOP;
      ELSE
         IF cCodOfi IS NOT NULL THEN
            FOR PR IN POL_RENOV_OFICINA LOOP
               --<I ESVI081> Javier Asmat/07.12.2004
               --Se asigna el Idepol del cursor a la variable para que el cursor de recibos levante lo correcto
               nIdePol  := PR.IdePol;
               OPEN RECIBO_Q;
               FETCH  RECIBO_Q
               INTO   nExisteRec;
               CLOSE RECIBO_Q;
               --
               IF     nExisteRec > 0
                  AND cIndRenPend = 'N' THEN
                  cIndRen  := 'N';
               ELSE
                  cIndRen  := 'S';
               END IF;
               IF cIndRen = 'S' THEN
                  nIdePolRen  := PR_POLIZA.RENOVAR (PR.IdePol, NULL, NULL);
                  --
                  UPDATE POLIZA
                  SET IndRenovLote = cUsr
                  WHERE  IdePol = nIdePolRen;
                  --
                  IF nIdePolRen IS NOT NULL THEN
                     nNumOper  := PR_POLIZA.ACTIVAR (nIdePolRen, 'D');
                     --
                     UPDATE POLIZA
                     SET IndFact = 'N'
                     WHERE  IdePol = nIdePolRen;
                     --
                     PR_GEN_REA.GENERAR_REDISTRIBUIR (nNumOper);
                     PR_ACREENCIA.GENERAR (nNumOper);
                     PR_FRACCIONAMIENTO.GENERAR (nNumOper);
                     PR_MOV_PRIMA.T$_MOV_ELIMINAR (nIdePolRen);
                  END IF;
               END IF;
            END LOOP;
         ELSIF cCodProd IS NOT NULL THEN
            FOR PR IN POL_RENOV_PROD LOOP
               --<I ESVI081> Javier Asmat/07.12.2004
               --Se asigna el Idepol del cursor a la variable para que el cursor de recibos levante lo correcto
               nIdePol  := PR.IdePol;
               OPEN RECIBO_Q;
               FETCH  RECIBO_Q
               INTO   nExisteRec;
               CLOSE RECIBO_Q;
               --
               IF     nExisteRec > 0
                  AND cIndRenPend = 'N' THEN
                  cIndRen  := 'N';
               ELSE
                  cIndRen  := 'S';
               END IF;
               IF cIndRen = 'S' THEN
                  nIdePolRen  := PR_POLIZA.RENOVAR (PR.IdePol, NULL, NULL);
                  --
                  UPDATE POLIZA
                  SET IndRenovLote = cUsrImp
                  WHERE  IdePol = nIdePolRen;
                  --
                  IF nIdePolRen IS NOT NULL THEN
                     nNumOper  := PR_POLIZA.ACTIVAR (nIdePolRen, 'D');
                     --
                     UPDATE POLIZA
                     SET IndFact = 'N'
                     WHERE  IdePol = nIdePolRen;
                     PR_GEN_REA.GENERAR_REDISTRIBUIR (nNumOper);
                     PR_ACREENCIA.GENERAR (nNumOper);
                     PR_FRACCIONAMIENTO.GENERAR (nNumOper);
                     PR_MOV_PRIMA.T$_MOV_ELIMINAR (nIdePolRen);
                  END IF;
               END IF;
            END LOOP;
         END IF;
      END IF;
      --<I ESVI081> Javier Asmat/07.12.2004
      --Finalizado el proceso, apagamos el indicador
      PR_POLIZA.cIndRenovMasiva  := 'N';
   --<F ESVI081>
   END;
   --//
   -- CERRAR_MOV
   PROCEDURE CERRAR_MOV (nIdePol POLIZA.IdePol%TYPE) IS
      --
      dFecOper                DATE;
      cDescReng               VARCHAR2 (50);
      cCodAcepRiesgoBase      VARCHAR2 (20);
      cTipoOp                 VARCHAR2 (3);
      nTotMtoMoneda           NUMBER (22, 2);
      nNumOper                OPER_POL.NumOPER%TYPE;
      nNumCert                CERTIFICADO.NumCert%TYPE;
      nNumDetOper             NUMBER (2);
      nExiste                 NUMBER (1);
      nInd_Responsable_Pago   NUMBER (1)                 := 0;
      nNumENDoso              NUMBER (10);
      cIndRespPago            POLIZA.IndRESPPAGO%TYPE;
      cTipoRamo               RAMO.TipoRamo%TYPE;
      cTipoOpA                VARCHAR2 (3);
      nNumOperAnu             NUMBER (14);
      nMtoOper                NUMBER (22, 2);
      nMtoOperAnuPro          NUMBER (22, 2);
      nMto                    NUMBER (22, 2);
      nMtoAjusteExc           NUMBER (22, 2);
      --
      cCodProd                POLIZA.CodProd%TYPE;
      --
      nApPriMinZonSis    NUMBER (1);   --<<BBVA - Consis 13/09/2012 - Valida Prima Minima por Zona Sisimica
      cIndPrimMinPlan    VARCHAR2(1);  --<<BBVA - Consis 09/02/2015
      --
      nIdeClau           NUMBER (3);
      nExistC            NUMBER (1) := 0;
      nIdeClauCert       NUMBER (20);

      nRegMovPrima       NUMBER (10):=0;
      nPrimaAnual        oper_pol.MTOPRIMAANUALDEV%type;
      nPrimaAnualRef     oper_pol.MTOPRIMAANUALDEV%type;

      CURSOR REG_MOV_PRIMA IS
         SELECT IdeMovPrima, MtoLocal
         FROM   MOV_PRIMA
         WHERE  IdePol = nIdePol
         AND    StsMovPrima = 'GEN';
         TYPE TABLE_MOVPRIMA IS TABLE OF REG_MOV_PRIMA%ROWTYPE INDEX BY PLS_INTEGER;
    MOVPRIMA TABLE_MOVPRIMA;

    CURSOR OPER_POL_GEN IS
         SELECT IdePol, NumCert, SUM (MtoMoneda) TotMtoMoneda, MIN (FecIniVig) dFecMinRec
         FROM   RECIBO
         WHERE  NumOper = nNumOper
         GROUP BY IdePol, NumCert;
         TYPE TABLE_OPERPOL IS TABLE OF OPER_POL_GEN%ROWTYPE INDEX BY PLS_INTEGER;
    OPERPOL TABLE_OPERPOL;

    CURSOR RECA_DCTO_CERTIF_C IS
         SELECT NumRecaDcto, R.IdeRecaDcto, CodRecaDcto, TipoRecaDcto, MtoRecaDctoFact, 'C' OrigModRecaDcto
         FROM   MOD_RECA_DCTO_CERTIF M, RECA_DCTO_CERTIF R
         WHERE  M.IdeRecaDcto = R.IdeRecaDcto
         AND    M.IdePol = nIdePol
         AND    M.NumCert = nNumCert
         AND    M.NumOper = nNumOper
         UNION
         SELECT NumRecaDcto, R.IdeRecaDcto, CodRecaDcto, TipoRecaDcto, MtoRecaDctoFact, 'A' OrigModRecaDcto
         FROM   MOD_RECA_DCTO_CERTIF_ASEG M, RECA_DCTO_CERTIF_ASEG R, ASEGURADO A
         WHERE  A.IdeAseg = M.IdeAseg
         AND    M.IdeRecaDcto = R.IdeRecaDcto
         AND    A.IdePol = nIdePol
         AND    A.NumCert = nNumCert
         AND    M.NumOper = nNumOper
         UNION
         SELECT NumRecaDcto, R.IdeRecaDcto, CodRecaDcto, TipoRecaDcto, MtoRecaDctoFact, 'B' OrigModRecaDcto
         FROM   MOD_RECA_DCTO_CERTIF_BIEN M, RECA_DCTO_CERTIF_BIEN R, BIEN_CERT A
         WHERE  A.IdeBien = M.IdeBien
         AND    M.IdeRecaDcto = R.IdeRecaDcto
         AND    A.IdePol = nIdePol
         AND    A.NumCert = nNumCert
         AND    M.NumOper = nNumOper
         ORDER BY 1;
      CURSOR RESP_PAGO_C IS
         SELECT CodCli, PorcPago, CodPlanFracc, NumModPlanFracc, Direc, CodPais, CodEstado, CodCiudad, CodMunicipio, Telex, Fax, Zip, Telef1, Telef2,
                Telef3, CodCobrador, CodViaCobro
         FROM   RESP_PAGO
         WHERE  IdePol = nIdePol
         AND    NumCert = nNumCert;
      CURSOR RESP_PAGO_P IS
         SELECT CodCli, PorcPago, CodPlanFracc, NumModPlanFracc, Direc, CodPais, CodEstado, CodCiudad, CodMunicipio, Telex, Fax, Zip, Telef1, Telef2,
                Telef3, CodCobrador, CodViaCobro
         FROM   RESP_PAGO_POL
         WHERE  IdePol = nIdePol;
      CURSOR REC_REASEGURO_Q IS
         SELECT IdeRec, CodRamoCert
         FROM   RECIBO
         WHERE  NumOper = nNumOper
         ORDER BY NUMCERT, IDEREC;
      CURSOR DIST_COA_C IS
         SELECT CodAcepRiEsgo
         FROM   DIST_COA
         WHERE  IdePol = nIdePol
         AND    StsCoa = 'ACT'
         AND    CodAcepRiesgo <> cCodAcepRiesgoBase;
      CURSOR MOV_CERT_EXC IS
         SELECT MtoAjusteExc
         FROM   RECIBO R, MOV_PRIMA M
         WHERE  M.IdeMovPrima = R.IdeMovPrima
         AND    R.NumCert = nNumCert
         AND    R.NumOper = nNumOper;
   BEGIN
      nNumOper              := PR_OPERACION.nNumOper;
      cCodAcepRiesgoBase    := PR_ACEP_RIESGO.ACEP_RIESGO_BASE (PR_POLIZA.cCodcia);


      /*  consis 07/10/2014 pedro pineda
      FOR M IN REG_MOV_PRIMA LOOP
         PR_MOV_PRIMA.CERRAR (M.IdeMovPrima);
      END LOOP;
      FOR M IN REG_MOV_PRIMA LOOP
         PR_MOV_PRIMA.ACTIVAR (M.IdeMovPrima);
      END LOOP;
      */
     ---------------- consis 07/10/2014
      OPEN REG_MOV_PRIMA;
    LOOP
       FETCH REG_MOV_PRIMA BULK COLLECT
       INTO MOVPRIMA LIMIT 1000;
       EXIT WHEN MOVPRIMA.COUNT = 0;

       FOR i IN 1 .. MOVPRIMA.COUNT LOOP
         PR_MOV_PRIMA.CERRAR(MOVPRIMA(i).IdeMovPrima);
         PR_MOV_PRIMA.ACTIVAR(MOVPRIMA(i).IdeMovPrima);
         nRegMovPrima := nRegMovPrima+1;
       END LOOP;
      --COMMIT;
    END LOOP;
    CLOSE REG_MOV_PRIMA;

      nRegMovPrima := 0;
      ---------------- fin consis 07/10/2014
      PR_POLIZA.CARGAR (nIdePol);
      nNumENDoso            := PR_POLIZA.NumERO_ENDOSO (PR_POLIZA.cCodProd, PR_POLIZA.cCodOfiemi, 'EC');
      cIndRespPago          := PR_POLIZA.cIndRespPago;
      cCodProd              := PR_POLIZA.cCodProd;
      -----
 ----     FOR OP IN OPER_POL_GEN LOOP
 --------- consis  07/10/2014
       OPEN OPER_POL_GEN;
    LOOP
       FETCH OPER_POL_GEN BULK COLLECT
       INTO OPERPOL LIMIT 1000;
       EXIT WHEN OPERPOL.COUNT = 0;

       FOR i IN 1 .. OPERPOL.COUNT LOOP
         nRegMovPrima:= nRegMovPrima+1;
 --------- consis  07/10/2014
         nTotMtoMoneda          := OPERPOL(i).TotMtoMoneda;
         nNumCert               := OPERPOL(i).NumCert;

----          nTotMtoMoneda          := OP.TotMtoMoneda;
    ----     nNumCert               := OP.NumCert;
         --Erick Zoque 20/05/2013 E_CON_20120821_1_7
         dFecOper               := PR_FECHAS_EQUIVALENTES.FECHA_EMISION (SYSDATE, PR_POLIZA.cCodcia,'001');
         --dFecOper               := PR_FECHAS_EQUIVALENTES.FECHA_EMISION (SYSDATE, PR_POLIZA.cCodcia);
         IF NVL (cIndRespPago, 'N') = 'N' THEN
            BEGIN
               SELECT 'MOD'
               INTO   cTipoOp
               FROM   OPER_POL
               WHERE  IndAnul = 'N'
               AND    NumOper =
                         (SELECT MAX (NumOper)
                          FROM   OPER_POL
                          WHERE  IdePol = nIdePol
                         ---- AND    NumCert = OP.NumCert
                          AND    NumCert = OPERPOL(i).NumCert
                          AND    NumOPER < nNumOper
                          AND    FecAnul IS NULL
                          AND    IndAnul = 'N'
                          AND    TipoOP = 'EMI');
            EXCEPTION
               WHEN NO_DATA_FOUND THEN
                  cTipoOp  := 'EMI';
               WHEN TOO_MANY_ROWS THEN
                  cTipoOp  := 'MOD';
            END;
         ELSE
            BEGIN
               SELECT 'MOD'
               INTO   cTipoOp
               FROM   OPER_POL
               WHERE  IndAnul = 'N'
               AND    NumOper =
                         (SELECT MAX (NumOper)
                          FROM   OPER_POL
                          WHERE  IdePol = nIdePol
                          AND    NumCert = OPERPOL(i).NumCert
                        ----  AND    NumCert = OP.NumCert
                          AND    NumOPER < nNumOper
                          AND    FecAnul IS NULL
                          AND    IndAnul = 'N'
                          AND    TipoOP = 'EMI');
            EXCEPTION
               WHEN NO_DATA_FOUND THEN
                  cTipoOp  := 'EMI';
               WHEN TOO_MANY_ROWS THEN
                  cTipoOp  := 'MOD';
            END;
         END IF;
         IF nTotMtoMoneda = 0 THEN
            cTipoOp  := 'ESM';
         END IF;
         -- Rehabilitacion de Anulaciones por monto pENDiente
         BEGIN
            SELECT OPE.TipoOp, OPE.NumOper, NVL (ABS (OPE.MtoOper), 0),
                   DECODE (NVL (OPE.MtoOperAnuPro, 0), 0, NVL (OPE.MtoOper, 0), ABS (OPE.MtoOperAnuPro))
            INTO   cTipoOpA, nNumOperAnu, nMtoOper, nMtoOperAnuPro
            FROM   OPER_POL OPE
            WHERE  OPE.NumOper = (SELECT MAX (O.NumOper)
                                  FROM   OPER_POL O
                                  WHERE  O.IdePol  = OPERPOL(i).IdePol
                                  AND    O.NumCert = OPERPOL(i).NumCert
                                  AND    O.MtoOper <> 0
                                  AND    O.IndAnul = 'N')
            AND    OPE.NumCert = OPERPOL(i).NumCert;
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               cTipoOpA        := NULL;
               nNumOperAnu     := 0;
               nMtoOper        := 0;
               nMtoOperAnuPro  := 0;
         END;
         IF     cTipoOpA = 'ANU'
            AND nMtoOper != nMtoOperAnuPro THEN
            nMto  := nMtoOperAnuPro;
         ELSE
            nMto  := 0;
         END IF;
         --
         -- Exclusion por Monto PENDiente
         IF     NVL (PR_CERTIFICADO.cIndExcluir, 'N') IN ('S', 'R')
            AND NVL (PR_CERTIFICADO.cTipoAnul, ' ') = 'M' THEN
            BEGIN
               SELECT nTotMtoMoneda - NVL (SUM (NVL (MtoAjusteExc, 0)), 0)
               INTO   nMto
               FROM   RECIBO R, MOV_PRIMA M
               WHERE  M.IdeMovPrima = R.IdeMovPrima
               AND    R.NumCert = nNumCert
               AND    R.NumOper = nNumOper;
            EXCEPTION
               WHEN NO_DATA_FOUND THEN
                  RAISE_APPLICATION_ERROR (-20100, 'No Existe Informacion de nNumCert- nNumOper ' || nNumCert || ' = ' || nNumOper);
            END;
         END IF;

         BEGIN
             SELECT SUM(NVL(mc.PRIMA,0))
             INTO nPrimaAnual
             FROM MOD_COBERT MC ,RECIBO R, MOV_PRIMA M
             WHERE MC.IDEMOVPRIMA = M.IDEMOVPRIMA
              AND M.IdeMovPrima = R.IdeMovPrima
               AND    R.NumCert = nNumCert
               AND    R.IdePol  = nIdePol
               AND    R.NumOper = nNumOper;
         END;

         BEGIN
             SELECT SUM(NVL(mc.PRIMA,0))
             INTO nPrimaAnualRef
             FROM MOD_COBERT MC ,RECIBO R, MOV_PRIMA M
             WHERE MC.IDEMOVPRIMA = M.IDEMOVPRIMA
              AND M.IdeMovPrima = R.IdeMovPrima
               AND    R.NumCert = nNumCert
               AND    R.IdePol  = nIdePol
               AND    R.NumOper = (Select MAX(OP.NumOper)
                                   FROM MOD_COBERT MC2 ,RECIBO R2, MOV_PRIMA M2, OPER_POL OP
                                   WHERE MC2.IDEMOVPRIMA = M2.IDEMOVPRIMA
                                    AND M2.IdeMovPrima = R2.IdeMovPrima
                                     AND    R2.NumCert = nNumCert
                                     AND    R2.IdePol  = nIdePol
                                     AND    R2.NumOper = OP.NumOper
                                     AND    OP.TipoOp != 'ESM'
                                     AND    OP.NumCert = nNumCert
                                     AND    OP.IdePol  = nIdePol
                                     AND    R2.NumOper != nNumOper);
         END;

                   /*
                   IF OP.dFecMinRec < PR_FECHAS_EQUIVALENTES.FECHA_EMISION (SYSDATE, PR_POLIZA.cCodCia) THEN
                      PR_OPER_USUARIO.AUTORIZAR ('127', NULL, NULL, NULL, NULL, NULL, 0, NULL, cCodProd, cTipoOp, OP.dFecMinRec);
                   END IF;*/
         --
         INSERT INTO OPER_POL
                     (IdePol, NumCert, NumOper, FecMov, MtoOper, TipoOp, NumEndoso, IndAnul, MtoOperAnuPro,MtoPrimaAnualDev)
         VALUES (OPERPOL(i).IdePol, OPERPOL(i).NumCert, nNumOper, dFecOper, nTotMtoMoneda, cTipoOp, nNumENDoso, 'N', nMto, (nvl(nPrimaAnual,0) - nvl(nPrimaAnualRef,0)));

         UPDATE RECIBO SET TIPOOPE = cTipoOp
         WHERE  IdePol  = OPERPOL(i).IdePol
         AND    NumCert = OPERPOL(i).NumCert
         AND    NumOper = nNumOper;

         -- Exclusion por Monto PENDiente
         IF     NVL (PR_CERTIFICADO.cIndExcluir, 'N') IN ('S', 'R')
            AND NVL (PR_CERTIFICADO.cTipoAnul, ' ') = 'M' THEN
            BEGIN
               SELECT NVL (SUM (MtoAjusteExc), 0)
               INTO   nMtoAjusteExc
               FROM   RECIBO R, MOV_PRIMA M
               WHERE  M.IdeMovPrima = R.IdeMovPrima
               AND    R.NumCert = nNumCert
               AND    R.NumOper = nNumOper;
            END;
            IF nMtoAjusteExc != 0 THEN
               BEGIN
                  SELECT NVL (MAX (NumDetOper), 0) + 1
                  INTO   nNumDetOper
                  FROM   DET_OPER_POL
                  WHERE  IdePol = nIdePol
                  AND    NumCert = nNumCert
                  AND    NumOper = nNumOper;
               EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                     nNumDetOper  := 1;
               END;
               INSERT INTO DET_OPER_POL
                           (IdePol, NumCert, NumOper, NumDetOper,
                            DescDet,
                            MtoDetOper)
                    VALUES (nIdePol, nNumCert, nNumOper, nNumDetOper,
                            DECODE (PR_CERTIFICADO.cIndExcluir, 'S', 'EXCLUSION POR MONTO PENDIENTE', 'R', 'REVERSION EXCLUSION POR MONTO PENDIENTE'),
                            nMtoAjusteExc);
            END IF;
         END IF;
         --
         -- Inserta El Recargo Y Descuento Al Detalle De La Operacion --
         FOR X IN RECA_DCTO_CERTIF_C LOOP
            IF X.MtoRecaDctoFact <> 0 THEN
               BEGIN
                  SELECT DescRecaDcto
                  INTO   cDescReng
                  FROM   RECA_DCTO
                  WHERE  CodRecaDcto = X.CodRecaDcto
                  AND    TipoRecaDcto = X.TipoRecaDcto;
               EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                     RAISE_APPLICATION_ERROR (-20100,
                                              'No se a encontrado el recargo o descuento en RECA_DCTO ' || X.CodRecaDcto || '-' || X.TipoRecaDcto);
                  WHEN TOO_MANY_ROWS THEN
                     RAISE_APPLICATION_ERROR (-20100,
                                              'Existe mas de un  recargo o descuento en RECA_DCTO ' || X.CodRecaDcto || '-' || X.TipoRecaDcto);
                  WHEN OTHERS THEN
                     RAISE_APPLICATION_ERROR (-20100, 'Error al seleccionar desde tabla RECA_DCTO ' || SQLERRM);
               END;
               BEGIN
                  SELECT NVL (MAX (NumDetOper), 0) + 1
                  INTO   nNumDetOper
                  FROM   DET_OPER_POL
                  WHERE  IdePol = nIdePol
                  AND    NumCert = nNumCert
                  AND    NumOper = nNumOper;
               EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                     nNumDetOper  := 1;
               END;
               INSERT INTO DET_OPER_POL
                           (IdePol, NumCert, NumOper, NumDetOper, DescDet, MtoDetOper, CodRecaDcto, TipoRecaDcto, IdeRecaDcto,
                            OrigModRecaDcto)
                    VALUES (OPERPOL(i).IdePol, OPERPOL(i).NumCert, nNumOper, nNumDetOper, cDescReng, X.MtoRecaDctoFact, X.CodRecaDcto, X.TipoRecaDcto, X.IdeRecaDcto,
                            X.OrigModRecaDcto);
            END IF;
         END LOOP;
         -- INSERTA LOS RESPONSABLES DE PAGO
         nInd_Responsable_Pago  := 0;
         FOR RP IN RESP_PAGO_C LOOP
            BEGIN
               SELECT 1
               INTO   nExiste
               FROM   RESP_PAGO_MOV
               WHERE  IdePol  = nIdePol
               AND    NumCert = nNumCert
               AND    NumOper = nNumOper
               AND    CodCli  = RP.CodCli;
            EXCEPTION
               WHEN NO_DATA_FOUND THEN
                  nExiste  := 0;
               WHEN TOO_MANY_ROWS THEN
                  nExiste  := 1;
            END;
            IF nExiste = 0 THEN
               INSERT INTO RESP_PAGO_MOV
                           (IdePol, NumCert, NumOper, CodCli, PorcPago, CodPlanFracc, NumModPlanFracc, Direc, CodPais,
                            CodEstado, CodCiudad, CodMunicipio, Telex, Fax, Zip, Telef1, Telef2, Telef3, CodCobrador,
                            CodViaCobro)
                    VALUES (nIdePol, nNumCert, nNumOper, RP.CodCli, RP.PorcPago, RP.CodPlanFracc, RP.NumModPlanFracc, RP.Direc, RP.CodPais,
                            RP.CodEstado, RP.CodCiudad, RP.CodMunicipio, RP.Telex, RP.Fax, RP.Zip, RP.Telef1, RP.Telef2, RP.Telef3, RP.CodCobrador,
                            RP.CodViaCobro);
            END IF;
            nInd_Responsable_Pago  := 1;
         END LOOP;
         --
         IF nInd_Responsable_Pago = 0 THEN
            FOR RP IN RESP_PAGO_P LOOP
               INSERT INTO RESP_PAGO_MOV
                           (IdePol, NumCert, NumOper, CodCli, PorcPago, CodPlanFracc, NumModPlanFracc, Direc, CodPais,
                            CodEstado, CodCiudad, CodMunicipio, Telex, Fax, Zip, Telef1, Telef2, Telef3, CodCobrador,
                            CodViaCobro)
                    VALUES (nIdePol, nNumCert, nNumOper, RP.CodCli, RP.PorcPago, RP.CodPlanFracc, RP.NumModPlanFracc, RP.Direc, RP.CodPais,
                            RP.CodEstado, RP.CodCiudad, RP.CodMunicipio, RP.Telex, RP.Fax, RP.Zip, RP.Telef1, RP.Telef2, RP.Telef3, RP.CodCobrador,
                            RP.CodViaCobro);
            END LOOP;
         END IF;

      END LOOP; -- DE BULK DE OPER_POL GEN
       --COMMIT; -- CADA MIL
    END LOOP; -- DE FETCH DE OPER POL GEN

      PR_POLIZA.ACTUALIZA_ENDOSO (PR_POLIZA.cCodProd, PR_POLIZA.cCodOfiemi, 'EC');
      -- GENERA LOS ENDOSOS DE COSSEGURO --
      IF PR_POLIZA.cTipoPdcion = 'C' THEN
         FOR X IN DIST_COA_C LOOP
            PR_DIST_COA.GENERAR_MOV (nIdePol, X.CodAcepRiesgo, nNumOper);
            --<<PARA CLAUSULA DE COASEGURO CEDIDO - Miguel Portillo - Consis Int - 30/08/2013
              nIdeClau := PR.BUSCA_LVAL('CLAUCOAS', 'C');
              SELECT MAX(IdeClauCert)
                INTO nIdeClauCert
                FROM CLAU_POL
               WHERE IdeClau = IdeClau
               and Idepol =  nIdePol;

              SELECT COUNT(*)
                INTO nExistC
                FROM TEXTO_CLAU_POL
               WHERE IdeClauCert = nIdeClauCert;

              IF nExistC = 0 then
                 PR_CLAU_POL.TEXT_CLAU_COA(nIdePol, nNumOper);
              END IF;
            --PARA CLAUSULA DE COASEGURO CEDIDO - Miguel Portillo - Consis Int - 30/08/2013>>
            PR_POLIZA.ORDEN_COA (nIdePol, nNumOper, 'EC', 'S');
         END LOOP;
      ELSIF PR_POLIZA.cTipoPdcion = 'A' THEN
         PR_DIST_COA.GENERAR_MOV_ACEPTADO (nIdePol);
      END IF;
      IF NVL (PR_POLIZA.cOperRev, 'ACT') <> 'REV' THEN   -- No debe valida Prima Minima con reversos. Anusky Gonzalez.
        --<<BBVA - Consis 13/09/2012 - Valida Prima Minima por Zona Sisimica
        nApPriMinZonSis := pr_poliza.VALAPLIC_PRIMIN_ZONSIS(nIdePol);
        --<<BBVA -  Consis 09/02/2015 - Indicador (cIndPrimMinPlan) Aplica Prima Minima a nivel de Plan
        cIndPrimMinPlan := PR_PLAN_PROD.VALAPLIC_PRIMIN_PLAN(cCodProd,nIdePol);

        IF nApPriMinZonSis = 1 AND cIndPrimMinPlan = 'S' THEN
              PR_POLIZA.VALIDA_PRIMA_MINIMA_ZONASIS (nIdePol, nNumOper);
        ELSIF cIndPrimMinPlan = 'S' THEN
              PR_POLIZA.VALIDA_PRIMA_MINIMA (nIdePol, nNumOper);
        END IF;
        --<<BBVA -  Consis 09/02/2015
        --<<BBVA - Consis 13/09/2012
      END IF;


      -- REASEGURO --
      IF PR.BUSCA_PARAMETRO ('034') = 'NO' THEN
         FOR RT IN REC_REASEGURO_Q LOOP
            PR_GEN_REA.SUMA_RAMO_REA (RT.IdeRec);
         END LOOP;
         PR_GEN_REA.generar_cta_tec (nNumOper);
         PR_DIST_REA.COPIA_FACULT (nIdePol, nIdePol);
      END IF;
      --//
      PR_POLIZA.nOper_Cerr  := nNumOper;
   END;
   --//
   --T$_CERRAR_MOV
   PROCEDURE T$_CERRAR_MOV (nIdePol POLIZA.IdePol%TYPE) IS
      --
      nTotMtoMoneda   T$_OPER_POL.MtoOper%TYPE;
      nNumOper        T$_OPER_POL.NumOper%TYPE;
      nNumCert        T$_OPER_POL.NumCert%TYPE;
      cTipoOp         T$_OPER_POL.TipoOp%TYPE;
      nMtoAjusteExc   T$_OPER_POL.MtoOper%TYPE;
      dFecOper        DATE;
      cDescReng       VARCHAR2 (50);
      nNumDetOper     NUMBER (2);
      nDias           NUMBER (3);
      --
      nApPriMinZonSis NUMBER(1); --<<BBVA - Consis 13/09/2012 - Valida Prima Minima por Zona Sisimica
      nPrimaAnual        oper_pol.MTOPRIMAANUALDEV%type;
      --
      CURSOR REG_MOV_PRIMA IS
         SELECT IdeMovPrima, MtoLocal
         FROM   T$_MOV_PRIMA
         WHERE  IdePol = nIdePol
         AND    StsMovPrima = 'GEN';
      CURSOR OPER_POL_GEN IS
         SELECT IdePol, NumCert, SUM (NVL (MtoMoneda, 0)) TotMtoMoneda
         FROM   T$_RECIBO
         WHERE  NumOper = nNumOper
         GROUP BY IdePol, NumCert;
      CURSOR RECA_DCTO_CERTIF_C IS
         SELECT R.NumRecaDcto, R.CodRecaDcto, R.TipoRecaDcto, M.MtoRecaDctoFact, M.IdeRecaDcto, R.IdeRecaDcto IDERECADCTO2
         FROM   T$_MOD_RECA_DCTO_CERTIF M, RECA_DCTO_CERTIF R
         WHERE  M.IdeRecaDcto = R.IdeRecaDcto
         AND    M.IdePol = nIdePol
         AND    M.NumCert = nNumCert
         AND    M.NumOper = nNumOper
         UNION
         SELECT R.NumRecaDcto, R.CodRecaDcto, R.TipoRecaDcto, M.MtoRecaDctoFact, M.IdeRecaDcto, R.IdeRecaDcto IDERECADCTO2
         FROM   T$_MOD_RECA_DCTO_CERTIF_ASEG M, RECA_DCTO_CERTIF_ASEG R
         WHERE  M.IdeRecaDcto = R.IdeRecaDcto
         AND    M.IdePol = nIdePol
         AND    M.NumCert = nNumCert
         AND    M.NumOper = nNumOper
         UNION
         SELECT R.NumRecaDcto, R.CodRecaDcto, R.TipoRecaDcto, M.MtoRecaDctoFact, M.IdeRecaDcto, R.IdeRecaDcto IDERECADCTO2
         FROM   T$_MOD_RECA_DCTO_CERTIF_BIEN M, RECA_DCTO_CERTIF_BIEN R
         WHERE  M.IdeRecaDcto = R.IdeRecaDcto
         AND    M.IdePol = nIdePol
         AND    M.NumCert = nNumCert
         AND    M.NumOper = nNumOper
         ORDER BY 1;
   BEGIN
      nNumOper  := PR_OPERACION.nNumOper;
      FOR M IN REG_MOV_PRIMA LOOP
         PR_T$_MOV_PRIMA.CERRAR (M.IdeMovPrima);
      END LOOP;
      --PRIMA_MINIMA (nIdePol, nNumOper, 'T');
      IF PR_POLIZA.cTipoPdcion = 'A' THEN
         PR_DIST_COA.T$_GENERAR_MOV_ACEPTADO (nIdePol);
      END IF;
      PR_POLIZA.CARGAR (nIdepol);
      FOR OP IN OPER_POL_GEN LOOP
         nTotMtoMoneda  := OP.TotMtoMoneda;
         nNumCert       := OP.NumCert;
         --Erick Zoque 20/05/2013 E_CON_20120821_1_7
         dFecOper       := PR_FecHAS_EQUIVALENTES.FecHA_EMISION (SYSDATE, PR_POLIZA.cCodcia,'001');
         BEGIN
            SELECT 'MOD'
            INTO   cTipoOp
            FROM   OPER_POL
            WHERE  IndAnul = 'N'
            AND    NumOper =
                      (SELECT MAX (NumOper)
                       FROM   OPER_POL
                       WHERE  IdePol = nIdePol
                       AND    NumCert = OP.NumCert
                       AND    NumOPER < nNumOper
                       AND    FecAnul IS NULL
                       AND    IndAnul = 'N'
                       AND    TipoOP = 'EMI');
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               cTipoOp  := 'EMI';
            WHEN TOO_MANY_ROWS THEN
               cTipoOp  := 'MOD';
         END;
         IF nTotMtoMoneda = 0 THEN
            cTipoOp  := 'ESM';
         END IF;

         /*BEGIN
             SELECT SUM(NVL(mc.PRIMA,0))
             INTO nPrimaAnual
             FROM MOD_COBERT MC ,t$_RECIBO R, t$_MOV_PRIMA M
             WHERE MC.IDEMOVPRIMAT = M.IDEMOVPRIMA
              AND M.IdeMovPrima = R.IdeMovPrima
               AND    R.NumCert = nNumCert
               AND    R.NumOper = nNumOper;
         END;*/
         INSERT INTO T$_OPER_POL
                     (IdePol, NumCert, NumOper, FecMov, TipoOp, MtoOper/* ,MtoPrimaAnualDev*/ )
              VALUES (OP.IdePol, OP.NumCert, nNumOper, dFecOper, cTipoOp, nTotMtoMoneda/*,nPrimaAnual*/);
         IF     NVL (PR_CERTIFICADO.cIndExcluir, 'N') = 'S'
            AND NVL (PR_CERTIFICADO.cTipoAnul, ' ') = 'M' THEN
            BEGIN
               SELECT NVL (SUM (NVL (MtoAjusteExc, 0)), 0)
               INTO   nMtoAjusteExc
               FROM   T$_RECIBO R, T$_MOV_PRIMA M
               WHERE  M.IdeMovPrima = R.IdeMovPrima
               AND    R.NumCert = nNumCert
               AND    R.NumOper = nNumOper;
            END;
            IF nMtoAjusteExc != 0 THEN
               BEGIN
                  SELECT NVL (MAX (NumDetOper), 0) + 1
                  INTO   nNumDetOper
                  FROM   T$_DET_OPER_POL
                  WHERE  IdePol = nIdePol
                  AND    NumCert = nNumCert
                  AND    NumOper = nNumOper;
               EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                     nNumDetOper  := 1;
               END;
               INSERT INTO T$_DET_OPER_POL
                           (IdePol, NumCert, NumOper, NumDetOper, DescDet, MtoDetOper)
                    VALUES (nIdePol, nNumCert, nNumOper, nNumDetOper, 'EXCLUSION POR MONTO PENDIENTE', nMtoAjusteExc);
            END IF;
         END IF;
         --
         FOR X IN RECA_DCTO_CERTIF_C LOOP
            IF X.MtoRecaDctoFact <> 0 THEN
               BEGIN
                  SELECT DescRecaDcto
                  INTO   cDescReng
                  FROM   RECA_DCTO
                  WHERE  CodRecaDcto = X.CodRecaDcto
                  AND    TipoRecaDcto = X.TipoRecaDcto;
               EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                     RAISE_APPLICATION_ERROR (-20100,
                                              'No se a encontrado el recargo o descuento en RECA_DCTO ' || X.CodRecaDcto || '-' || X.TipoRecaDcto);
                  WHEN TOO_MANY_ROWS THEN
                     RAISE_APPLICATION_ERROR (-20100,
                                              'Existe mas de un  recargo o descuento en RECA_DCTO ' || X.CodRecaDcto || '-' || X.TipoRecaDcto);
                  WHEN OTHERS THEN
                     RAISE_APPLICATION_ERROR (-20100, 'Error al seleccionar desde tabla RECA_DCTO ' || SQLERRM);
               END;
               BEGIN
                  SELECT NVL (MAX (NumDetOper), 0) + 1
                  INTO   nNumDetOper
                  FROM   T$_DET_OPER_POL
                  WHERE  IdePol = nIdePol
                  AND    NumCert = nNumCert
                  AND    NumOper = nNumOper;
               EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                     nNumDetOper  := 1;
               END;
               --
               INSERT INTO T$_DET_OPER_POL
                           (IdePol, NumCert, NumOper, NumDetOper, DescDet, MtoDetOper)
                    VALUES (OP.IdePol, OP.NumCert, nNumOper, nNumDetOper, cDescReng, X.MtoRecaDctoFact);
            END IF;
         END LOOP;
      END LOOP;
      --<<BBVA - Consis 13/09/2012 - Valida Prima Minima por Zona Sisimica
      nApPriMinZonSis := pr_poliza.VALAPLIC_PRIMIN_ZONSIS(nIdePol);
      if nApPriMinZonSis = 1 then
            PR_POLIZA.T$_VALIDA_PRIMA_MINIMA_ZONASIS (nIdePol, nNumOper);
      else
            PR_POLIZA.T$_VALIDA_PRIMA_MINIMA (nIdePol, nNumOper);
      end if;
      --<<BBVA - Consis 13/09/2012


   END;   --///
   --
     /*
    Nombre     : GENERAR_MOV
    Objetivo   : GENERA LOS MOVIMIENTO SEGUN LOS DATOS MODIFICADOS EN LA POLIZA/CERTIFICADO
    Log de Cambios:
    Fecha        Autor             Descripci�n
    --------------------------------------------------------------------------------------------------------------------------------
    20/02/2005   Sergio Sotelo   Agragada Llamada a Funci�n PR_POLIZA.TIPO_OPER y que evalue
                                 La Base del C�lculo del Recargo y o Descuento
    --------------------------------------------------------------------------------------------------------------------------------
   */
   PROCEDURE GENERAR_MOV (nIdePol NUMBER, cTipoMov VARCHAR2) IS
      nEmision           NUMBER                        := 0;
      cIndMovPorCobert   VARCHAR2 (1)                  := 'N';
      cTipo              VARCHAR2 (1)                  := NULL;
      dFecIniMaseg       DATE;
      dFecFinMaseg       DATE;
      dFecIniValid       DATE;
      dFecFinValid       DATE;
      dFecIniMcobert     MOD_COBERT.FecIniValid%TYPE;
      dFecFinMcobert     MOD_COBERT.FecFinValid%TYPE;
      nNumCert           CERTIFICADO.NumCert%TYPE;
      cCodOfiemi         CERTIFICADO.CodOfiemi%TYPE;
      cCodMoneda         POLIZA.CodMoneda%TYPE;
      cCodPlan           CERT_RAMO.CodPLAN%TYPE;
      cRevPlan           CERT_RAMO.REVPLAN%TYPE;
      cCodRamoCert       CERT_RAMO.CodRAMOCERT%TYPE;
      nNumRen            POLIZA.NumRen%TYPE; --<Migraci�n/Javier Asmat/01.11.2005> Se cambio tipo de dato de NUMBER(1) al de la columna en la tabla
      cBaseRecaDcto      VARCHAR (1);
      cStsFact           FACTURA.STSFACT%TYPE;
      nNumOperFact       OPER_POL.NumOPER%TYPE;
      --
      -- REG_CERTIFICADOS -- (Certificados de la Poliza)
      CURSOR REG_CERTIFICADOS IS
         SELECT NumCert
         FROM   CERTIFICADO
         WHERE  IdePol = nIdePol
         AND    StsCert IN ('INC', 'MOD');

         TYPE TABLE_UNIFICACION IS TABLE OF REG_CERTIFICADOS%ROWTYPE INDEX BY PLS_INTEGER;
    CERT TABLE_UNIFICACION;
      --
      -- RECA_DCTO_C -- (Recargos y Descuentos)
      CURSOR RECA_DCTO_C IS
         SELECT NumRecaDcto, CodRecaDcto, TipoRecaDcto, StsRecaDcto, IdeRecaDcto, BaseRecaDcto, 'C' Origen, 0 Ide
         FROM   RECA_DCTO_CERTIF
         WHERE  IdePol = nIdePol
         AND    NumCert = nNumCert
         AND    StsRecaDcto IN ('INC', 'MOD', 'ACT')
         UNION
         SELECT NumRecaDcto, CodRecaDcto, TipoRecaDcto, StsRecaDcto, IdeRecaDcto, BaseRecaDcto, 'A' Origen, A.IdeAseg Ide
         FROM   RECA_DCTO_CERTIF_ASEG RD, ASEGURADO A
         WHERE  RD.IdeAseg = A.IdeAseg
         AND    A.IdePol = nIdePol
         AND    A.NumCert = nNumCert
         AND    RD.StsRecaDcto IN ('INC', 'MOD', 'ACT')
         UNION
         SELECT NumRecaDcto, CodRecaDcto, TipoRecaDcto, StsRecaDcto, IdeRecaDcto, BaseRecaDcto, 'B' Origen, A.IdeBien Ide
         FROM   RECA_DCTO_CERTIF_BIEN RD, BIEN_CERT A
         WHERE  RD.IdeBien = A.IdeBien
         AND    A.IdePol = nIdePol
         AND    A.NumCert = nNumCert
         AND    RD.StsRecaDcto IN ('INC', 'MOD', 'ACT')
         ORDER BY 1;
      --
      -- Determina La Cantidad De Movimientos De Prima A Generar Dependiendo De Los Ramos Y Monedas)
      CURSOR REG_RECIBOS IS
        SELECT MC.CodMoneda, MC.NumCert, MC.CodRamoCert, CC.CodPlan, CC.RevPlan
        FROM   MOD_COBERT MC, COBERT_CERT CC
        WHERE  CC.IdePol = nIdePol
        AND    MC.IDEPOL = CC.IDEPOL --<<BBVA Consis EFVC 24/11/2014 se agrega condicion para mejorar performance consulta
        AND    MC.IdeCobert = CC.IdeCobert
        AND    MC.StsModCobert IN ('INC', 'MOD')
        UNION
        SELECT MC.CodMoneda, MC.NumCert, MC.CodRamoCert, CC.CodPlan, CC.RevPlan
        FROM   MOD_COBERT MC, COBERT_ASEG CC
        WHERE  MC.IdePol = nIdePol
        AND    MC.StsModCobert IN ('INC', 'MOD')
        AND    MC.IdeCobert = CC.IdeCobert
        UNION
        SELECT MC.CodMoneda, MC.NumCert, MC.CodRamoCert, CC.CodPlan, CC.RevPlan
        FROM   MOD_COBERT MC, COBERT_BIEN CC
        WHERE  MC.IdePol = nIdePol
        AND    MC.StsModCobert IN ('INC', 'MOD')
        AND    MC.IdeCobert = CC.IdeCobert
        UNION
        SELECT CodMoneda, NumCert, CodRamoCert, CodPlan, RevPlan
        FROM   MOD_ASEG
        WHERE  IdePol = nIdePol
        AND    StsModAseg IN ('INC', 'MOD')
        GROUP BY CodMoneda, NumCert, CodRamoCert, CodPlan, RevPlan;
         TYPE TABLE_REGRECIBOS IS TABLE OF REG_RECIBOS%ROWTYPE INDEX BY PLS_INTEGER;
    R_REC TABLE_REGRECIBOS;
      --
      -- Solo Para Recargos y Descuentos
      CURSOR SOLO_RECA_DCTO_C IS
         SELECT RF.NumCert, RDP.CodRamoPlan, RF.FecIniValid, RF.FecFinValid, CR.CodPlan, CR.RevPlan
         FROM   RECA_DCTO_CERTIF RF, CONF_RECA_DCTO_PROD RDP, CERT_RAMO CR
         WHERE  RDP.CodProd = cCodProd
         AND    RDP.CodRecaDcto = RF.CodRecaDcto
         AND    RDP.TipoRecaDcto = RF.TipoRecaDcto
         AND    RF.IdePol = CR.IdePol
         AND    RF.NumCert = CR.NumCert
         AND    CR.CodRamoCert = RDP.CodRamoPlan
         AND    RF.IdePol = nIdePol
         AND    StsRecaDcto IN ('INC', 'MOD')
         GROUP BY RF.NumCert, RDP.CodRamoPlan, RF.FecIniValid, RF.FecFinValid, CR.CodPlan, CR.RevPlan
         UNION
         SELECT A.NumCert, RDP.CodRamoPlan, RF.FecIniValid, RF.FecFinValid, CR.CodPlan, CR.RevPlan
         FROM   RECA_DCTO_CERTIF_ASEG RF, CONF_RECA_DCTO_PROD RDP, CERT_RAMO CR, ASEGURADO A
         WHERE  RDP.CodProd = cCodProd
         AND    RDP.CodRecaDcto = RF.CodRecaDcto
         AND    RDP.TipoRecaDcto = RF.TipoRecaDcto
         AND    RF.IdeAseg = A.IdeAseg
         AND    A.IdePol = CR.IdePol
         AND    A.NumCert = CR.NumCert
         AND    CR.CodRamoCert = RDP.CodRamoPlan
         AND    A.IdePol = nIdePol
         AND    StsRecaDcto IN ('INC', 'MOD')
         GROUP BY A.NumCert, RDP.CodRamoPlan, RF.FecIniValid, RF.FecFinValid, CR.CodPlan, CR.RevPlan
         UNION
         SELECT A.NumCert, RDP.CodRamoPlan, RF.FecIniValid, RF.FecFinValid, CR.CodPlan, CR.RevPlan
         FROM   RECA_DCTO_CERTIF_BIEN RF, CONF_RECA_DCTO_PROD RDP, CERT_RAMO CR, BIEN_CERT A
         WHERE  RDP.CodProd = cCodProd
         AND    RDP.CodRecaDcto = RF.CodRecaDcto
         AND    RDP.TipoRecaDcto = RF.TipoRecaDcto
         AND    RF.IdeBien = A.IdeBien
         AND    A.IdePol = CR.IdePol
         AND    A.NumCert = CR.NumCert
         AND    CR.CodRamoCert = RDP.CodRamoPlan
         AND    A.IdePol = nIdePol
         AND    StsRecaDcto IN ('INC', 'MOD')
         GROUP BY A.NumCert, RDP.CodRamoPlan, RF.FecIniValid, RF.FecFinValid, CR.CodPlan, CR.RevPlan;
   --
   BEGIN
      PR_POLIZA.CARGAR (nIdePol);
      cCodProd          := PR_POLIZA.cCodProd;   -- DATOS DE LA POLIZA.
      cIndMovPorCobert  := 'N';
      --
   -----   FOR R_REC IN REG_RECIBOS LOOP     consis  07/10/2014 pedro pineda
       OPEN REG_RECIBOS;
       LOOP
       FETCH REG_RECIBOS BULK COLLECT
       INTO R_REC LIMIT 10000;
       EXIT WHEN R_REC.COUNT = 0;
       FOR i IN 1 .. R_REC.COUNT LOOP
         cCodMoneda        := R_REC(i).CodMoneda;
         nNumCert          := R_REC(i).NumCert;
         cCodRamoCert      := R_REC(i).CodRamoCert;
         cCodPlan          := R_REC(i).CodPlan;
         cRevPlan          := R_REC(i).RevPlan;
         cTipo             := 'P';
         --
         -- Determina La Fecha Minima Y Maxima De La ModIFicacion O Movimiento De Prima
         BEGIN
            SELECT MIN (FecIniValid), MAX (FecFinValid)
            INTO   dFecIniMcobert, dFecFinMcobert
            FROM   MOD_COBERT
            WHERE  IdePol = nIdePol
            AND    NumCert = nNumCert
            AND    CodRamoCert = cCodRamoCert
            AND    StsModCobert IN ('INC', 'MOD');
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               RAISE_APPLICATION_ERROR (-20100, 'No Se A Podido Encontrar el MOD_COBERT ' || nIdePol || '-' || nNumCert || '-' || cCodRamoCert);
         END;
         -- Comentado por Eduardo Ocando 04/08/2005. Defecto Nro. 3511 Autorizado por Javier Asmat.

         /*         BEGIN
            SELECT MIN (FecIniValid), MAX (FecFinValid)
            INTO   dFecIniMaseg, dFecFinMaseg
            FROM   MOD_ASEG
            WHERE  IdePol = nIdePol
            AND    NumCert = nNumCert
            AND    CodRamoCert = cCodRamoCert
            AND    StsModAseg IN ('INC', 'MOD');
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               RAISE_APPLICATION_ERROR (-20100, 'No Se A Podido Encontrar el MOD_ASEG ' || nIdePol || '-' || nNumCert || '-' || cCodRamoCert);
                  END;*/
         --       Hasta Aqui la modificaci�n y/o comentario
         --
         IF     dFecIniMcobert IS NOT NULL
            AND dFecIniMaseg IS NOT NULL THEN
            BEGIN
               SELECT LEAST (dFecIniMcobert, dFecIniMaseg)
               INTO   dFecIniValid
               FROM   SYS.DUAL;
            END;
         ELSE
            IF dFecIniMcobert IS NOT NULL THEN
               dFecIniValid  := dFecIniMcobert;
            ELSIF dFecIniMaseg IS NOT NULL THEN
               dFecIniValid  := dFecIniMaseg;
            END IF;
         END IF;
         --
         IF     dFecFinMcobert IS NOT NULL
            AND dFecFinMaseg IS NOT NULL THEN
            BEGIN
               SELECT GREATEST (dFecFinMcobert, dFecFinMaseg)
               INTO   dFecFinValid
               FROM   SYS.DUAL;
            END;
         ELSE
            IF dFecFinMcobert IS NOT NULL THEN
               dFecFinValid  := dFecFinMcobert;
            ELSIF dFecFinMaseg IS NOT NULL THEN
               dFecFinValid  := dFecFinMaseg;
            END IF;
         END IF;
         --
         BEGIN
            SELECT CodOfiemi
            INTO   cCodOfiemi
            FROM   CERTIFICADO
            WHERE  IdePol = nIdePol
            AND    NumCert = nNumCert;
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               RAISE_APPLICATION_ERROR (-20220, PR.MENSAJE ('ABD', 20220, 'Certificado (' || TO_CHAR (nNumCert) || ')', NULL, NULL));
            WHEN TOO_MANY_ROWS THEN
               RAISE_APPLICATION_ERROR (-20100, 'Existe mas de un  CertIFicado' || nIdePol || '-' || nNumCert);
            WHEN OTHERS THEN
               RAISE_APPLICATION_ERROR (-20100, 'Error al seleccionar desde tabla CertIFicado ' || SQLERRM);
         END;
         -- GENERA EL MOVIMIENTO DE PRIMA EN STATUS 'GEN'
         PR_MOV_PRIMA.GENERAR (nIdePol,
                               nNumCert,
                               cCodRamoCert,
                               cCodMoneda,
                               dFecIniValid,
                               dFecFinValid,
                               cCodOfiemi,
                               cTipoMov,
                               cCodPlan,
                               cRevPlan,
                               cTipo);
         cIndMovPorCobert  := 'S';
               END LOOP;
     --COMMIT;
      END LOOP;
     CLOSE REG_RECIBOS;
      --
      --
      IF cIndMovPorCobert = 'N' THEN
         FOR X IN SOLO_RECA_DCTO_C LOOP
            cTipo             := 'R';
            cCodMoneda        := PR_POLIZA.cCodMoneda;
            nNumCert          := X.NumCert;
            dFecIniValid      := X.FecIniValid;
            dFecFinValid      := X.FecFinValid;
            cCodRamoCert      := X.CodRamoPlan;
            cCodPlan          := X.CodPlan;
            cRevPlan          := X.RevPlan;
            --
            BEGIN
               SELECT CodOfiemi
               INTO   cCodOfiemi
               FROM   CERTIFICADO
               WHERE  IdePol = nIdePol
               AND    NumCert = nNumCert;
            EXCEPTION
               WHEN NO_DATA_FOUND THEN
                  RAISE_APPLICATION_ERROR (-20220, PR.MENSAJE ('ABD', 20220, 'CertIFicado (' || TO_CHAR (nNumCert) || ')', NULL, NULL));
               WHEN TOO_MANY_ROWS THEN
                  RAISE_APPLICATION_ERROR (-20100, 'Existe mas de un  CertIFicado' || nIdePol || '-' || nNumCert);
               WHEN OTHERS THEN
                  RAISE_APPLICATION_ERROR (-20100, 'Error al seleccionar desde tabla CertIFicado ' || SQLERRM);
            END;
            PR_MOV_PRIMA.GENERAR (nIdePol,
                                  nNumCert,
                                  cCodRamoCert,
                                  cCodMoneda,
                                  dFecIniValid,
                                  dFecFinValid,
                                  cCodOfiemi,
                                  cTipoMov,
                                  cCodPlan,
                                  cRevPlan,
                                  cTipo);
            -- Esta tomando en cuenta los movimiento de prima a pesar de ser solo para reca y dcto
            cIndMovPorCobert  := 'S';
         END LOOP;
      END IF;
      --
      IF cIndMovPorCobert = 'S' THEN
       OPEN REG_CERTIFICADOS;
        LOOP
           FETCH REG_CERTIFICADOS BULK COLLECT
           INTO CERT LIMIT 1000;
           EXIT WHEN CERT.COUNT = 0;
           FOR i IN 1 .. CERT.COUNT LOOP
       ----  FOR X IN REG_CERTIFICADOS LOOP     consis  07/10/2014 pedro pineda

            nNumCert       := CERT(i).NumCert;
            cBaseRecaDcto  := TIPO_OPER (nIdePol, nNumCert, PR_OPERACION.nNumOper);
            PR_RECA_DCTO_CERTIF.OBLIGATORIO (nIdePol, NULL, nNumCert, dFecIniValid, dFecFinValid, cCodProd, cBaseRecaDcto);
            IF cTipo = 'P' THEN
               FOR Y IN RECA_DCTO_C LOOP
                  IF Y.StsRecaDcto IN ('INC', 'MOD', 'ACT') THEN
                     BEGIN
                        SELECT MAX (NumOper)
                        INTO   nNumOperFact
                        FROM   OPER_POL
                        WHERE  IdePol = nIdePol
                        AND    NumCert = nNumCert
                        AND    TipoOp IN ('EMI');
                     EXCEPTION
                        WHEN NO_DATA_FOUND THEN
                           nEmision  := 0;
                     END;
                     --
                     BEGIN
                        SELECT DISTINCT STSFACT
                        INTO   cStsFact
                        FROM   FACTURA
                        WHERE  NumOPER = NVL (nNumOperFact, 0)
                        AND    StsFact = 'ANU';
                     EXCEPTION
                        WHEN NO_DATA_FOUND THEN
                           nEmision  := 0;
                        WHEN TOO_MANY_ROWS THEN
                           nEmision  := 1;
                     END;
                     IF NVL (cStsFact, 'ANU') = 'ANU' THEN
                        nEmision  := 0;
                     ELSE
                        nEmision  := 1;
                     END IF;
                     -- Fin de la modificacion.
                     BEGIN
                        SELECT NVL (NumRen, 0)
                        INTO   nNumRen
                        FROM   POLIZA
                        WHERE  IdePol = nIdePol;
                     EXCEPTION
                        WHEN NO_DATA_FOUND THEN
                           nNumRen  := 0;
                     END;
                     IF Y.BaseRecaDcto = 'T' THEN
                        IF Y.Origen = 'C' THEN
                           PR_RECA_DCTO_CERTIF.GENERAR_MOV (nIdePol,
                                                            nNumCert,
                                                            PR_OPERACION.nNumOper,
                                                            Y.IdeRecaDcto,
                                                            dFecIniValid,
                                                            dFecFinValid,
                                                            cTipoMov);
                        ELSIF Y.Origen = 'A' THEN
                           PR_RECA_DCTO_CERTIF_ASEG.GENERAR_MOV (nIdePol,
                                                                 nNumCert,
                                                                 Y.Ide,
                                                                 PR_OPERACION.nNumOper,
                                                                 Y.IdeRecaDcto,
                                                                 dFecIniValid,
                                                                 dFecFinValid,
                                                                 cTipoMov);
                        ELSIF Y.Origen = 'B' THEN
                           PR_RECA_DCTO_CERTIF_BIEN.GENERAR_MOV (nIdePol,
                                                                 nNumCert,
                                                                 Y.Ide,
                                                                 PR_OPERACION.nNumOper,
                                                                 Y.IdeRecaDcto,
                                                                 dFecIniValid,
                                                                 dFecFinValid,
                                                                 cTipoMov);
                        ELSE
                           RAISE_APPLICATION_ERROR (-20100,
                                                    'Error en la Configuracion De Recargo y Descuento. 1' || '-' || Y.BaseRecaDcto || '-' || SQLERRM);
                        END IF;
                     ELSIF Y.BaseRecaDcto = 'E' THEN
                        IF nEmision = 0 THEN
                           IF Y.Origen = 'C' THEN
                              PR_RECA_DCTO_CERTIF.GENERAR_MOV (nIdePol,
                                                               nNumCert,
                                                               PR_OPERACION.nNumOper,
                                                               Y.IdeRecaDcto,
                                                               dFecIniValid,
                                                               dFecFinValid,
                                                               cTipoMov);
                           ELSIF Y.Origen = 'A' THEN
                              PR_RECA_DCTO_CERTIF_ASEG.GENERAR_MOV (nIdePol,
                                                                    nNumCert,
                                                                    Y.Ide,
                                                                    PR_OPERACION.nNumOper,
                                                                    Y.IdeRecaDcto,
                                                                    dFecIniValid,
                                                                    dFecFinValid,
                                                                    cTipoMov);
                           ELSIF Y.Origen = 'B' THEN
                              PR_RECA_DCTO_CERTIF_BIEN.GENERAR_MOV (nIdePol,
                                                                    nNumCert,
                                                                    Y.Ide,
                                                                    PR_OPERACION.nNumOper,
                                                                    Y.IdeRecaDcto,
                                                                    dFecIniValid,
                                                                    dFecFinValid,
                                                                    cTipoMov);
                           ELSE
                              RAISE_APPLICATION_ERROR (-20100,
                                                       'Error en la Configuracion De Recargo y Descuento. 2' || '-' || Y.BaseRecaDcto || '-'
                                                       || SQLERRM);
                           END IF;
                        END IF;
                     ELSIF Y.BaseRecaDcto = 'M' THEN
                        IF nEmision > 0 THEN
                           IF Y.Origen = 'C' THEN
                              PR_RECA_DCTO_CERTIF.GENERAR_MOV (nIdePol,
                                                               nNumCert,
                                                               PR_OPERACION.nNumOper,
                                                               Y.IdeRecaDcto,
                                                               dFecIniValid,
                                                               dFecFinValid,
                                                               cTipoMov);
                           ELSIF Y.Origen = 'A' THEN
                              PR_RECA_DCTO_CERTIF_ASEG.GENERAR_MOV (nIdePol,
                                                                    nNumCert,
                                                                    Y.Ide,
                                                                    PR_OPERACION.nNumOper,
                                                                    Y.IdeRecaDcto,
                                                                    dFecIniValid,
                                                                    dFecFinValid,
                                                                    cTipoMov);
                           ELSIF Y.Origen = 'B' THEN
                              PR_RECA_DCTO_CERTIF_BIEN.GENERAR_MOV (nIdePol,
                                                                    nNumCert,
                                                                    Y.Ide,
                                                                    PR_OPERACION.nNumOper,
                                                                    Y.IdeRecaDcto,
                                                                    dFecIniValid,
                                                                    dFecFinValid,
                                                                    cTipoMov);
                           ELSE
                              RAISE_APPLICATION_ERROR (-20100,
                                                       'Error en la Configuracion De Recargo y Descuento 3' || '-' || Y.BaseRecaDcto || '-' || SQLERRM);
                           END IF;
                        END IF;
                     ELSIF Y.BaseRecaDcto = 'R' THEN   -- Solo renovacion
                        IF     NVL (nNumRen, 0) > 0
                           AND nEmision = 0 THEN
                           IF Y.Origen = 'C' THEN
                              PR_RECA_DCTO_CERTIF.GENERAR_MOV (nIdePol,
                                                               nNumCert,
                                                               PR_OPERACION.nNumOper,
                                                               Y.IdeRecaDcto,
                                                               dFecIniValid,
                                                               dFecFinValid,
                                                               cTipoMov);
                           ELSIF Y.Origen = 'A' THEN
                              PR_RECA_DCTO_CERTIF_ASEG.GENERAR_MOV (nIdePol,
                                                                    nNumCert,
                                                                    Y.Ide,
                                                                    PR_OPERACION.nNumOper,
                                                                    Y.IdeRecaDcto,
                                                                    dFecIniValid,
                                                                    dFecFinValid,
                                                                    cTipoMov);
                           ELSIF Y.Origen = 'B' THEN
                              PR_RECA_DCTO_CERTIF_BIEN.GENERAR_MOV (nIdePol,
                                                                    nNumCert,
                                                                    Y.Ide,
                                                                    PR_OPERACION.nNumOper,
                                                                    Y.IdeRecaDcto,
                                                                    dFecIniValid,
                                                                    dFecFinValid,
                                                                    cTipoMov);
                           ELSE
                              RAISE_APPLICATION_ERROR (-20100,
                                                       'Error en la Configuracion De Recargo y Descuento 3' || '-' || Y.BaseRecaDcto || '-' || SQLERRM);
                           END IF;
                        END IF;
                     ELSIF Y.BaseRecaDcto = 'V' THEN   -- Solo modificaciones despues de renovada
                        IF     NVL (nNumRen, 0) > 0
                           AND nEmision > 0 THEN
                           IF Y.Origen = 'C' THEN
                              PR_RECA_DCTO_CERTIF.GENERAR_MOV (nIdePol,
                                                               nNumCert,
                                                               PR_OPERACION.nNumOper,
                                                               Y.IdeRecaDcto,
                                                               dFecIniValid,
                                                               dFecFinValid,
                                                               cTipoMov);
                           ELSIF Y.Origen = 'A' THEN
                              PR_RECA_DCTO_CERTIF_ASEG.GENERAR_MOV (nIdePol,
                                                                    nNumCert,
                                                                    Y.Ide,
                                                                    PR_OPERACION.nNumOper,
                                                                    Y.IdeRecaDcto,
                                                                    dFecIniValid,
                                                                    dFecFinValid,
                                                                    cTipoMov);
                           ELSIF Y.Origen = 'B' THEN
                              PR_RECA_DCTO_CERTIF_BIEN.GENERAR_MOV (nIdePol,
                                                                    nNumCert,
                                                                    Y.Ide,
                                                                    PR_OPERACION.nNumOper,
                                                                    Y.IdeRecaDcto,
                                                                    dFecIniValid,
                                                                    dFecFinValid,
                                                                    cTipoMov);
                           ELSE
                              RAISE_APPLICATION_ERROR (-20100,
                                                       'Error en la Configuracion De Recargo y Descuento 3' || '-' || Y.BaseRecaDcto || '-' || SQLERRM);
                           END IF;
                        END IF;
                     ELSIF Y.BaseRecaDcto = 'A' THEN   -- las dos anteriores
                        IF NVL (nNumRen, 0) > 0 THEN
                           IF Y.Origen = 'C' THEN
                              PR_RECA_DCTO_CERTIF.GENERAR_MOV (nIdePol,
                                                               nNumCert,
                                                               PR_OPERACION.nNumOper,
                                                               Y.IdeRecaDcto,
                                                               dFecIniValid,
                                                               dFecFinValid,
                                                               cTipoMov);
                           ELSIF Y.Origen = 'A' THEN
                              PR_RECA_DCTO_CERTIF_ASEG.GENERAR_MOV (nIdePol,
                                                                    nNumCert,
                                                                    Y.Ide,
                                                                    PR_OPERACION.nNumOper,
                                                                    Y.IdeRecaDcto,
                                                                    dFecIniValid,
                                                                    dFecFinValid,
                                                                    cTipoMov);
                           ELSIF Y.Origen = 'B' THEN
                              PR_RECA_DCTO_CERTIF_BIEN.GENERAR_MOV (nIdePol,
                                                                    nNumCert,
                                                                    Y.Ide,
                                                                    PR_OPERACION.nNumOper,
                                                                    Y.IdeRecaDcto,
                                                                    dFecIniValid,
                                                                    dFecFinValid,
                                                                    cTipoMov);
                           ELSE
                              RAISE_APPLICATION_ERROR (-20100,
                                                       'Error en la Configuracion De Recargo y Descuento 3' || '-' || Y.BaseRecaDcto || '-' || SQLERRM);
                           END IF;
                        END IF;
                     ELSE
                        RAISE_APPLICATION_ERROR (-20100,
                                                 'Error en la Configuracion De Recargo y Descuento 4' || '-' || Y.BaseRecaDcto || '-' || SQLERRM);
                     END IF;
                  END IF;
               END LOOP;
            ELSE
               --
               FOR Y IN RECA_DCTO_C LOOP
                  IF Y.StsRecaDcto IN ('INC', 'MOD') THEN
                     BEGIN
                        SELECT COUNT (*)
                        INTO   nEmision
                        FROM   OPER_POL
                        WHERE  IdePol = nIdePol
                        AND    NumCert = nNumCert
                        AND    TipoOp IN ('EMI', 'MOD');
                     EXCEPTION
                        WHEN NO_DATA_FOUND THEN
                           nEmision  := 0;
                     END;
                     IF Y.BaseRecaDcto = 'T' THEN
                        IF Y.Origen = 'C' THEN
                           PR_RECA_DCTO_CERTIF.GENERAR_MOV (nIdePol,
                                                            nNumCert,
                                                            PR_OPERACION.nNumOper,
                                                            Y.IdeRecaDcto,
                                                            dFecIniValid,
                                                            dFecFinValid,
                                                            cTipoMov);
                        ELSIF Y.Origen = 'A' THEN
                           PR_RECA_DCTO_CERTIF_ASEG.GENERAR_MOV (nIdePol,
                                                                 nNumCert,
                                                                 Y.Ide,
                                                                 PR_OPERACION.nNumOper,
                                                                 Y.IdeRecaDcto,
                                                                 dFecIniValid,
                                                                 dFecFinValid,
                                                                 cTipoMov);
                        ELSIF Y.Origen = 'B' THEN
                           PR_RECA_DCTO_CERTIF_BIEN.GENERAR_MOV (nIdePol,
                                                                 nNumCert,
                                                                 Y.Ide,
                                                                 PR_OPERACION.nNumOper,
                                                                 Y.IdeRecaDcto,
                                                                 dFecIniValid,
                                                                 dFecFinValid,
                                                                 cTipoMov);
                        ELSE
                           RAISE_APPLICATION_ERROR (-20100,
                                                    'Error en la Configuracion De Recargo y Descuento 5' || '-' || Y.BaseRecaDcto || '-' || SQLERRM);
                        END IF;
                     ELSIF Y.BaseRecaDcto = 'E' THEN
                        IF nEmision = 0 THEN
                           IF Y.Origen = 'C' THEN
                              PR_RECA_DCTO_CERTIF.GENERAR_MOV (nIdePol,
                                                               nNumCert,
                                                               PR_OPERACION.nNumOper,
                                                               Y.IdeRecaDcto,
                                                               dFecIniValid,
                                                               dFecFinValid,
                                                               cTipoMov);
                           ELSIF Y.Origen = 'A' THEN
                              PR_RECA_DCTO_CERTIF_ASEG.GENERAR_MOV (nIdePol,
                                                                    nNumCert,
                                                                    Y.Ide,
                                                                    PR_OPERACION.nNumOper,
                                                                    Y.IdeRecaDcto,
                                                                    dFecIniValid,
                                                                    dFecFinValid,
                                                                    cTipoMov);
                           ELSIF Y.Origen = 'B' THEN
                              PR_RECA_DCTO_CERTIF_BIEN.GENERAR_MOV (nIdePol,
                                                                    nNumCert,
                                                                    Y.Ide,
                                                                    PR_OPERACION.nNumOper,
                                                                    Y.IdeRecaDcto,
                                                                    dFecIniValid,
                                                                    dFecFinValid,
                                                                    cTipoMov);
                           ELSE
                              RAISE_APPLICATION_ERROR (-20100,
                                                       'Error en la Configuracion De Recargo y Descuento 6' || '-' || Y.BaseRecaDcto || '-' || SQLERRM);
                           END IF;
                        END IF;
                     ELSIF Y.BaseRecaDcto = 'M' THEN
                        IF nEmision > 0 THEN
                           IF Y.Origen = 'C' THEN
                              PR_RECA_DCTO_CERTIF.GENERAR_MOV (nIdePol,
                                                               nNumCert,
                                                               PR_OPERACION.nNumOper,
                                                               Y.IdeRecaDcto,
                                                               dFecIniValid,
                                                               dFecFinValid,
                                                               cTipoMov);
                           ELSIF Y.Origen = 'A' THEN
                              PR_RECA_DCTO_CERTIF_ASEG.GENERAR_MOV (nIdePol,
                                                                    nNumCert,
                                                                    Y.Ide,
                                                                    PR_OPERACION.nNumOper,
                                                                    Y.IdeRecaDcto,
                                                                    dFecIniValid,
                                                                    dFecFinValid,
                                                                    cTipoMov);
                           ELSIF Y.Origen = 'B' THEN
                              PR_RECA_DCTO_CERTIF_BIEN.GENERAR_MOV (nIdePol,
                                                                    nNumCert,
                                                                    Y.Ide,
                                                                    PR_OPERACION.nNumOper,
                                                                    Y.IdeRecaDcto,
                                                                    dFecIniValid,
                                                                    dFecFinValid,
                                                                    cTipoMov);
                           ELSE
                              RAISE_APPLICATION_ERROR (-20100,
                                                       'Error en la Configuracion De Recargo y Descuento 7' || '*' || Y.BaseRecaDcto || '*' || SQLERRM);
                           END IF;
                        END IF;
                     ELSE
                        RAISE_APPLICATION_ERROR (-20100,
                                                 'Error en la Configuracion De Recargo y Descuento 8' || '-' || Y.BaseRecaDcto || '-' || SQLERRM);
                     END IF;
                  END IF;
               END LOOP;
            END IF;
        END LOOP;   --- de bulk de reg_certificado
     --COMMIT;        --- de bulk de reg_certificado
         END LOOP;
     CLOSE REG_CERTIFICADOS;
      ELSE
         NULL;
      --  RAISE_APPLICATION_ERROR(-20220,'No existe movimiento de prima. '||SQLERRM);
      END IF;
   END;
FUNCTION ACTIVAR (nIdePol POLIZA.IdePol%TYPE, cTipoMov VARCHAR2)  RETURN NUMBER IS
      nExiste               NUMBER (1)                     := 0;
      dFecExc               DATE;
      nNumCert              CERTIFICADO.NumCert%TYPE;
      nExcCert              NUMBER (1)                     := 0;
      nNumOper              OPER_POL.NumOPER%TYPE;
      nNumOperFact          OPER_POL.NumOPER%TYPE;
      nTasaInteres          NUMBER (9, 6);
      nPorcCtp              NUMBER (8, 4);
      nMeses                NUMBER;
      nNumLiq               NUMBER;
      nMesesFactor          NUMBER (3);
      nDiasFacturar         NUMBER (3);
      cCodRamoCert          VARCHAR2 (4);
      cStsPol               POLIZA.STSPOL%TYPE;
      cTipoFact             VARCHAR2 (1);
      cStsCert              VARCHAR2 (3);
      cIndTipoPro           VARCHAR2 (1);
      cIndGenFracc          VARCHAR2 (1)                   := 'S';
      cCodProd              PRODUCTO.CodPROD%TYPE;
      cClasePol             POLIZA.CLASEPOL%TYPE;
      dFecUltFact           POLIZA.FecULTFACT%TYPE;
      dFecFactAnt           DATE;
      dFecIniPol            DATE;
      nIdeCot               COTIZACIOND.IDECOT%TYPE;
      nNumFinanc            NUMBER                         := 0;
      nGiros_Cobrados       NUMBER                         := 0;
      cHayGirosActivos      VARCHAR2 (1);
      cExisOperPrev         VARCHAR2 (1);
      nSumaCedida           NUMBER (14, 2);
      cIndEmitirPEND        VARCHAR2 (1);
      cExistePEND           VARCHAR2 (1);
      cTipoSuscProd         PRODUCTO.tiposuscprod%TYPE;   --<Javier Asmat/25.06.2004/Capitalizacion>
      cIndObligInspec       PRODUCTO.IndObligInspec%TYPE;
      nExisInspec           NUMBER (1)                     := 0;
      nIdeDeduc             NUMBER (14);
      cValidaSumaAseg       VARCHAR2 (100)                 := 'N';
      nValAsegurable        NUMBER (22, 2);
      nValAsegurado         NUMBER (22, 2);
      nValAsegurableLucro   NUMBER (22, 2);
      nValAseguradoLucro    NUMBER (22, 2);
      nTotValAsegurado      NUMBER (22, 2);
      nTotSumaAsegurada     NUMBER (22, 2);
      nSumaAsegurada        NUMBER (22, 2);
      nReqPen               NUMBER (6):=0;
      nNumCert1             NUMBER (14);
      n                     NUMBER;
      nSumaAsegAnt          NUMBER;
      nDias                 NUMBER;
      nValDeclaTrans    NUMBER;
      nExCatrVid        NUMBER;
      nSumaRecibo       NUMBER (22, 2) := 0;
      nExisteMvtben     NUMBER         :=0 ;

    TYPE TABLE_UNIFICACION IS TABLE OF REG_CERTIFICADO_INC%ROWTYPE INDEX BY PLS_INTEGER;
    CERT TABLE_UNIFICACION;


  CURSOR D IS
      SELECT CE.IdePol, CE.NumCert
      FROM   CERT_VEH CE
      WHERE  CE.IdePol = nIdePol;
  CURSOR C IS
      SELECT C.CodCobert, C.SumaAsegMoneda
      FROM   COBERT_CERT C
      WHERE  C.IdePol = nIdePol
        AND  C.NumCert = nNumCert1;
  CURSOR REC_FRACC IS
    SELECT NumCert, MtoOper, TipoOp, FecMov
    FROM   OPER_POL
    WHERE  NumOper = nNumOper;
    TYPE TABLE_OPERPOL IS TABLE OF REC_FRACC%ROWTYPE INDEX BY PLS_INTEGER;
    OPPO TABLE_OPERPOL;
  CURSOR CERTIF_EXCLUIDOS IS
    SELECT C.IdePol, C.NumCert, C.StsCert, C.FecExc
    FROM   OPER_POL OP, CERTIFICADO C
    WHERE  OP.IdePol = C.IdePol
      AND  OP.NumCert = C.NumCert
      AND  C.StsCert = 'EXC'
      AND  OP.TipoOp = 'MOD'
      AND  OP.NumOper = nNumOper;
  CURSOR C_VER_DIST_FACULT IS
    SELECT GR.IdePol, GR.NumCert
    FROM   GEN_REA GR, DIST_REA_MOD DM
    WHERE  GR.IdePol = nIdePol
      AND  DM.IdeGenRea = GR.IdeGenRea
      AND  DM.IdeRec = GR.IdeRec
      AND  DM.IdeCttoRea = 99999999999999
    GROUP BY GR.IdePol, GR.NumCert;
  CURSOR VERIFICA_CONFIG_ENDOSOS IS
    SELECT NumCert, CodPlan, RevPlan, CodRamoCert
    FROM   CERT_RAMO
    WHERE  IdePol = nIdePol
      AND  StsCertRamo IN ('INC', 'MOD')
    ORDER BY NumCert;
  CURSOR CERTIFICADO_C IS
    SELECT NumCert
    FROM   CERTIFICADO
    WHERE  IdePol = nIdePol;

  --<<BBVA Consis EFVC  21/11/2014. Se crea cursor local para manejo eliminado desarrollo BUlCOlect de Pedro
   CURSOR CERTIFICADO_INC IS
      SELECT NumCert, FecIng
      FROM   CERTIFICADO
      WHERE  IdePol = nIdePol
      AND    StsCert IN ('INC', 'MOD')
      ORDER BY NumCert;


BEGIN
  --Erick Zoque 20/05/2013 E_CON_20120821_1_7
  PR_FECHAS_EQUIVALENTES.CEVENTO := '001';
  PR_POLIZA.CARGAR (nIdePol);
  nSumaAsegAnt := SUMA_ASEGURADA_POL(nIdePol);
  --DV. 20/11/2012. Validar Requisitos por P�liza
  BEGIN
      SELECT NVL (COUNT (*), 0)
      INTO   nReqPen
      FROM   REQ_POL
      WHERE  IdePol  = nIdePol
      AND    StsReq  = 'PEN';
  END;
  IF nReqPen > 0 THEN
      RAISE_APPLICATION_ERROR (-20100,'La P�liza Tiene requisitos Pendientes.');
  END IF;

  --<<Consis EFVC. 20/01/2015. Valida Estado Caratula de Vida
  BEGIN
      SELECT 1
      INTO   nExCatrVid
      FROM   DAT_PART_POL_PERSO
      WHERE  IdePol  = nIdePol
      AND    NVL(StsDat,'MOD')  IN ('MOD');
  EXCEPTION
      WHEN NO_DATA_FOUND THEN
           nExCatrVid := 0;
      WHEN TOO_MANY_ROWS THEN
           nExCatrVid := 1;
  END;
  IF nExCatrVid > 0 THEN
      RAISE_APPLICATION_ERROR (-20100,'La Caratula de la P�liza no esta Activa. revise...');
  END IF;
  -->>Consis EFVC. 20/01/2015.
  --
  IF PR.SALUD (nIdePol) = 'S' AND PR_POLIZA.cStsPol = 'INC' AND PR_POLIZA.nNumRen > 0 THEN
    FOR A IN (SELECT NumCert
              FROM   CERTIFICADO
              WHERE  IdePol = nIdePol
                AND  StsCert IN ('INC', 'MOD')) LOOP
      PR_RECA_DCTO_CERTIF.VERIFICA_RECADCTO_RENOV (nIdePol, a.NumCert);
    END LOOP;
    FOR A IN (SELECT IdeAseg
              FROM   ASEGURADO
              WHERE  IdePol = nIdePol
                AND  StsAseg IN ('INC', 'MOD')) LOOP
      PR_RECA_DCTO_CERTIF_ASEG.VERIFICA_RECADCTO_RENOV (a.IdeAseg);
    END LOOP;
  END IF;
  BEGIN
    SELECT CodProd
    INTO   cCodProd
    FROM   POLIZA
    WHERE  IdePol = nIdePol;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      cCodProd  := PR_POLIZA.cCodProd;
  END;
  cValidaSumaAseg  := BUSCA_LVAL ('SUMASEG', cCodProd);
  IF cValidaSumaAseg = 'S' THEN
    PR_POLIZA.TOT_POLIZA (nIdePol, nValAsegurable, nValAsegurado, nValAsegurableLucro, nValAseguradoLucro);
    nTotValAsegurado   := nValAsegurado + nValAseguradoLucro;
    nTotSumaAsegurada  := 0;
    FOR X IN CERTIFICADO_C LOOP
      nSumaAsegurada     := 0;
      nSumaAsegurada     := PR_CERTIFICADO.MONTO_CAP_ASEG (cCodProd, nIdePol, X.NumCert);
      nTotSumaAsegurada  := nTotSumaAsegurada + nSumaAsegurada;
    END LOOP;
    IF nTotValAsegurado != nTotSumaAsegurada THEN
      RAISE_APPLICATION_ERROR(-20220, PR.MENSAJE('ABD', 20220,
                               'La Sumatoria de las coberturas que acumula Suma Asegurada en toda la P�liza debe ser igual al Valor Total Asegurado en Datos Particulares de la P�liza',
                               NULL,
                               NULL));
    END IF;
  END IF;
  BEGIN
    SELECT IndObligInspec
    INTO   cIndObligInspec
    FROM   PRODUCTO
    WHERE  CodProd = cCodProd;
  EXCEPTION
    WHEN NO_DATA_FOUND OR TOO_MANY_ROWS THEN
      cIndObligInspec  := NULL;
  END;
  IF cIndObligInspec = 'S' THEN
    BEGIN
      SELECT 1
      INTO   nExisInspec
      FROM   POLIZA_INSPECCION
      WHERE  IdePol = nIdePol;
    EXCEPTION
      WHEN NO_DATA_FOUND OR TOO_MANY_ROWS THEN
        nExisInspec  := 0;
    END;
    IF nExisInspec = 0 THEN
      RAISE_APPLICATION_ERROR (-20220,PR.MENSAJE ('ABD', 20220,
                               'Falta colocar los datos de la inspecci�n son obligatorios para este producto',
                               NULL,
                               NULL));
    END IF;
  END IF;
  dFecUltFact      := PR_POLIZA.dFecUltFact;
  IF PR_POLIZA.cStsPol IN ('VAL', 'INC') THEN
    --Erick Zoque 20/05/2013 E_CON_20120821_1_7
    dFecIniPol  := PR_FECHAS_EQUIVALENTES.FECHA_EMISION (SYSDATE, PR_POLIZA.cCodcia,'001');
    UPDATE POLIZA
    SET    FecIniPol = dFecIniPol
    WHERE  IdePol = nIdePol;
  END IF;
  IF PR_POLIZA.VALIDAR (nIdePol, 'D') = 0 THEN
    RAISE_APPLICATION_ERROR (-20220, PR.MENSAJE ('ABD', 20220, 'Poliza', NULL, NULL));
  END IF;
  FOR VCE IN VERIFICA_CONFIG_ENDOSOS LOOP
    BEGIN
      SELECT IndEmitirPend
      INTO   cIndEmitirPend
      FROM   RAMO_PLAN_PROD
      WHERE  CodProd = PR_POLIZA.cCodProd
        AND  CodPlan = VCE.CodPlan
        AND  RevPlan = VCE.RevPlan
        AND  CodRamoPlan = VCE.CodRamoCert;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        cIndEmitirPEND  := 'S';
    END;
    IF NVL (cIndEmitirPend, 'N') != 'S' THEN
      BEGIN
        SELECT UNIQUE 'S'
        INTO   cExistePend
        FROM   GIROS_FINANCIAMIENTO GF, COND_FINANCIAMIENTO CF
        WHERE  GF.NumFinanc = CF.NumFinanc
          AND  CF.IdePol = nIdePol
          AND  CF.NumCert = VCE.NumCert
          AND  GF.StsGiro NOT IN ('COB', 'ANU');
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          cExistePend  := 'N';
      END;
      /*IF NVL (cExistePend, 'N') = 'S' THEN
        RAISE_APPLICATION_ERROR (-20220, PR.MENSAJE ('AIF', 0022041, VCE.NumCert, NULL, NULL));
      END IF;*/
      --DV. 12/08/2013. Se controla por nivel de autoridad.
    END IF;
  END LOOP;
  FOR X IN C_VER_DIST_FACULT LOOP
    IF NOT PR_DIST_REA.EXISTE_DIST_FACULT (X.IdePol, X.NumCert, nNumOper) THEN
      RAISE_APPLICATION_ERROR (-20220, PR.MENSAJE ('ABD',20220,
                               'Para el IdePol ' || TO_CHAR (x.Idepol) || ' Operaci�n # ' || TO_CHAR (nNumOper)
                               || ' falta colocar Facultativo en el  Certificado ' || x.NumCert,
                               NULL,
                               NULL));
    END IF;
  END LOOP;
  IF cTipoMov = 'D' THEN
    PR_OPERACION.INICIAR ('ACTIV', 'POLIZA', TO_CHAR (nIdePol));
    IF PR_POLIZA.cIndCobProv = 'S' THEN
      PR_COBERT_PROVI.GENERAR (PR_POLIZA.cCodProd, PR_POLIZA.nNumPol, nIdePol);
    END IF;
    BEGIN
      SELECT P.IdeCot
      INTO   nIdeCot
      FROM   POLIZA P, COTIZACIOND C
      WHERE  C.STSCOT NOT IN ('ACT', 'ANU')
        AND  C.IdeCot = P.IdeCot
        AND  P.IdePol = nIdePol
        AND  P.IdeCot IS NOT NULL;
      PR_COTIZACION.ACTIVAR (nIdeCot);
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        nIdeCot  := NULL;
      WHEN TOO_MANY_ROWS THEN
        PR_COTIZACION.ACTIVAR (nIdeCot);
    END;
    PR_B$_POLIZA.PUSH (nIdePol, 'ACTIV', 'PRC');
    PR_POLIZA.GENERAR_MOV (nIdePol, 'D');
  ELSE
    --<<BBVA Consis EFVC 27/08/2014 - Validacion Datos Particulares Transporte si existenten trayectos pendientes de Declarar
    --BBVA EFVC 15/09/2014 Se agrga parametro
    nValDeclaTrans := PR_DAT_PART_MERCANCIA.VALIDA_TRAYEC_NO_DECLARADO('A',nIdePol);
    IF  nValDeclaTrans = 1 THEN
        RAISE_APPLICATION_ERROR (-20100, 'EL proceso de Activacion de Polizas de Declaraciones tiene trayectos de Transporte sin Declarar. Revise... ' || SQLERRM);
    END IF;
    -->>BBVA Consis EFVC 27/08/2014
    PR_BENEF_ASEG.VALIDA_PART_BENEF (nIdePol);
    IF PR_T$_MOV_PRIMA.VALIDAR (nIdePol) = 1 THEN
      PR_OPERACION.INICIAR ('T$MOV', 'POLIZA', TO_CHAR (nIdePol));
      PR_POLIZA.GENERAR_MOV (nIdePol, 'T');
    ELSE
      RAISE_APPLICATION_ERROR (-20220, PR.MENSAJE ('ABD', 20224, NULL, NULL, NULL));
    END IF;
  END IF;
  PR_POLIZA.GENERA_ARTICULO_41 (nIdePol);
  DELETE MESES_LIQ
  WHERE  IdePol = nIdePol;
  nMesesFactor     := PR.MESES_FORMA_PAGO (PR_POLIZA.cCodFormPago);
  --<<BBVA Consis 16/07/2013 - Se cambia posicion linea de numoper
  nNumOper  := PR_OPERACION.nNumOper; --<Defect 7022> O.Marin/06-01-2006/No generaci�n de documentos
  --<<BBVA Consis EFVC 21/11/2014. Se crea cursor local para manejo eliminado desarrollo BUlCOlect de Pedro
-----  FOR C IN REG_CERTIFICADO_INC LOOP    CONSIS 07/10/2014
 /*BEGIN
    cLOSE  REG_CERTIFICADO_INC;
   EXCEPTION WHEN OTHERS THEN NULL;
   END;
    OPEN REG_CERTIFICADO_INC;
    LOOP
       FETCH REG_CERTIFICADO_INC BULK COLLECT
       INTO CERT LIMIT 10000;
       EXIT WHEN CERT.COUNT = 0;

       FOR i IN 1 .. CERT.COUNT LOOP*/
  ---- nNumCert  := C.NumCert;
  FOR C IN CERTIFICADO_INC LOOP
   nNumCert  := C.NumCert;
    IF PR.BUSCA_LVAL ('CANTREC', PR_POLIZA.cCodFormPago) IN ('INVALIDO', '1') THEN
      PR_CERTIFICADO.ACTIVAR (nIdePol, nNumCert, cTipoMov);
    ELSE
      IF TO_CHAR(C.FecIng, 'YYYYMM') <= TO_CHAR (ADD_MONTHS (dFecUltFact, nMesesFactor * -1), 'YYYYMM') THEN
        nMeses         := 1;
        nDiasFacturar  := 0;
      END IF;
      INSERT INTO MESES_LIQ
      VALUES (nIdePol, nNumCert, nMeses, nDiasFacturar);
      PR_CERTIFICADO.ACTIVAR (nIdePol, nNumCert, cTipoMov);
    END IF;
    --<<BBVA Consis 16/07/2013 Cuando es la activacion Temporal se activan los datos del vehiuclo y se actualiza ccon la operacion del mvto
    IF cTipoMov = 'D' THEN
        PR_CAMBIO_CERT_VEH.ACTIVAR_EMI (nIdePol, nNumCert,nNumOper);
    END IF;
    --<<BBVA Consis 16/07/2013
  END LOOP;
   ------   consis 07/10/2014
    --COMMIT;
  /*END LOOP;
  CLOSE REG_CERTIFICADO_INC;*/
   ------   consis 07/10/2014
  -->>BBVA Consis EFVC 21/11/2014.

  IF cTipoMov = 'D' THEN
    PR_CLAU_POL.ACTIVAR (nIdePol);
    PR_ANEXO_POL.ACTIVAR (nIdePol);
    PR_RECA_DCTO_POL.ACTIVAR (nIdePol);
    PR_POLIZA_CLIENTE.ACTIVAR (nIdePol);
    PR_DIST_COA.ACTIVAR (nIdePol,nNumOper);
    PR_DEDUCIBLE_POLIZA.ACTIVAR (nIdePol);
    PR_POLIZA.CERRAR_MOV (nIdePol);
    --<<BBVA Consis 02/12/2014 Se actualiza operacion para el detalle de bienes que se incluieron
    PR_BIEN_CERT.CREA_MOD_DETALEBIEN(nIdePol,nNumOper);
    --<<BBVA EFVC Consis 08/01/2015 - Se actualiza Numero de operacion generada en los datos particulares para majeo de histroico
    PR_DATOS_PART_TR_MONTAJE.ACTIVAR(nIdePol,nNumOper);
    PR_DAT_PART_RCE.ACTIVAR(nIdePol,nNumOper);
    PR_DAT_PART_TRANSVALORES.ACTIVAR(nIdePol,nNumOper);
    PR_DATOS_PART_BANCARIO.ACTIVAR(nIdePol,nNumOper);
    PR_DAT_PART_PERSONAL.ACTIVAR(nIdePol,nNumOper);
    PR_DAT_PART_RC_PROFESIONAL.ACTIVAR(nIdePol,nNumOper);
    PR_DATOS_PARTICULARES_VIDA.ACTIVAR(nIdePol,nNumOper);
    PR_DAT_PART_RENTA.ACTIVAR(nIdePol,nNumOper);
    PR_DATOS_PART_HURT_TAR.ACTIVAR(nIdePol,nNumOper);
--Erick Zoque 11/26/2012
--Se cabia el lugar de PR_DIST_COA.ACTIVAR para que funcione correctamente la clausula de coaseguro.
--Erick Zoque 24/05/2013
--Se revierte el caqmbio ya que no afecta la generacion de la clausula pero si afecta la generacion
-- de movivientos de coaseguro cedido.
--    PR_DIST_COA.ACTIVAR (nIdePol);
    BEGIN
      SELECT TipoSuscProd
      INTO   cTipoSuscProd
      FROM   PRODUCTO
      WHERE  CodProd = PR_POLIZA.cCodProd;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        cTipoSuscProd  := NULL;
    END;
    IF cTipoSuscProd = 'A' THEN
      BEGIN
        UPDATE POLIZA
        SET    NumPol = PR_POLIZA.NUMERO_ENDOSO (PR_POLIZA.cCodProd, PR_POLIZA.cCodOfiEmi, NULL)
        WHERE  IdePol = nIdePol;
      EXCEPTION
        WHEN OTHERS THEN
          RAISE_APPLICATION_ERROR (-20290, PR.MENSAJE ('ABD', 20290, 'NumPol en POLIZA:' || SQLERRM, NULL, NULL));
      END;
    END IF;
    PR_POLIZA.GENERAR_TRANSACCION (nIdePol, 'ACTIV');

  -----  FOR C IN REC_FRACC LOOP     consis  07/10/2014
     OPEN REC_FRACC;
    LOOP
       FETCH REC_FRACC BULK COLLECT
       INTO OPPO LIMIT 10000;
       EXIT WHEN OPPO.COUNT = 0;

       FOR i IN 1 .. OPPO.COUNT LOOP
      BEGIN
        SELECT MAX (NumFinanc)
        INTO   nNumFinanc
        FROM   COND_FINANCIAMIENTO
        WHERE  StsFin IN ('FRA', 'COB', 'ACT')
     ----     AND  NumCert = C.NumCert         consis  07/10/2014   pp
          AND  NumCert = OPPO(i).NumCert
          AND  IdePol = nIdePol;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          nNumFinanc  := 0;
      END;
      BEGIN
        SELECT 'S'
        INTO   cExisOperPrev
        FROM   OPER_POL
        WHERE  IdePol = nIdePol
          AND  NumOper <> nNumOper;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          cExisOperPrev  := 'N';
        WHEN TOO_MANY_ROWS THEN
          cExisOperPrev  := 'S';
      END;
      IF NVL (PR_POLIZA.cOperRev, 'XXX') = 'REV' THEN
        UPDATE OPER_POL
        SET    NumRever = nNumOper
        WHERE  NumOper = PR_POLIZA.nNumOperRev;
        --
        UPDATE OPER_POL
        SET    TipoOp = 'REV'
        WHERE  NumOper = nNumOper;
        --
        UPDATE RECIBO SET TIPOOPE = 'REV'
        WHERE  IdePol = nIdePol
        AND    NumOper = nNumOper;
      ELSE
        IF OPPO(i).MtoOper > 0 AND NVL (cExisOperPrev, 'N') = 'N' THEN
       ----   PR_FRACCIONAMIENTO.EMITIR (nIdePol, C.NumCert, nNumOper);    consis   07/10/2014
           PR_FRACCIONAMIENTO.EMITIR(nIdePol, OPPO(i).NumCert, nNumOper);
        ELSIF OPPO(i).MtoOper > 0 AND NVL (cExisOperPrev, 'N') = 'S' THEN
         ---- PR_FRACCIONAMIENTO.EMITIR_ALTAS (nIdePol, C.NumCert, nNumOper, 'P');     consis   07/10/2014
          PR_FRACCIONAMIENTO.EMITIR_ALTAS(nIdePol, OPPO(i).NumCert, nNumOper, 'P');
        ELSIF OPPO(i).MtoOper < 0 THEN
       ----   PR_FRACCIONAMIENTO.EMITIR_ALTAS (nIdePol, C.NumCert, nNumOper, 'N');       consis   07/10/2014
           PR_FRACCIONAMIENTO.EMITIR_ALTAS(nIdePol, OPPO(i).NumCert, nNumOper, 'N');
        END IF;
      END IF;
    END LOOP;
    -------consis   07/10/2014
        --COMMIT;
    END LOOP;
  CLOSE REC_FRACC;
  -------consis   07/10/2014

  ELSE
    PR_POLIZA.T$_CERRAR_MOV (nIdePol);
  END IF;
  IF cTipoMov = 'D' THEN
    PR_AGENCIAMIENTO_POL.ACTIVAR (nIdePol);
    UPDATE POLIZA
    SET    IndFact = 'N'
    WHERE  IdePol = nIdePol
      AND  IndFact = 'R';
    IF PR.BUSCA_LVAL ('CANTREC', PR_POLIZA.cCodFormPago) IN ('INVALIDO', '1') THEN
      INSERT INTO FECHAS_LIQ
      (IdePol, NumOper, FecLiq, IndOk)
      VALUES
      (nIdePol, nNumOper, dFecUltFact, 'N');
    ELSE
      INSERT INTO FECHAS_LIQ
      (IdePol, NumOper, FecLiq, IndOk)
      VALUES
      (nIdePol, nNumOper, ADD_MONTHS (dFecUltFact, nMesesFactor * -1), 'N');
    END IF;
    IF PR_COTIZACION.COTIZACION (nIdePol, 'C') = 'N' AND NVL (PR_POLIZA.cOperRev, 'XXX') != 'REV' THEN
      FOR R IN (SELECT SUM (MtoOper) MtoOper
                FROM   OPER_POL
                WHERE  NumOper = nNumOper) LOOP
        IF R.MtoOper > 0 THEN
          PR_FRACCIONAMIENTO.OBLG_RETADM (nIdePol, nNumOper);
        ELSIF R.MtoOper < 0 THEN
          PR_FRACCIONAMIENTO.ACRE_RETADM (nIdePol, nNumOper);
        END IF;
      END LOOP;
    END IF;
    IF NVL (PR_POLIZA.cIndMovPolRen, 'N') = 'S' THEN
      PR_POLIZA.ACTIVA_RENOV (nIdePol);
    END IF;

    --<<BBVA EFVC Consis 30/09/2015 - Se atuliza numoper en mvto de mod_cobert
    BEGIN
        UPDATE MOD_COBERT
        SET NUMOPERMVTO = nNumOper
        WHERE IDEPOL=nIdePol
        AND  NUMOPERMVTO IS NULL
        AND  IDEMOVPRIMA IN (SELECT IDEMOVPRIMA
                            FROM RECIBO
                            WHERE IDEPOL = nIdePol);

    END;
    --<< COTECSA FM *23718* 04/08/2018 SE CREA ESTE UPDATE PARA QUE CUANDO EXISTA UNA PRORROGA COLOQUE LA OPERACI�N CON QUE SE HIZO
    UPDATE POLIZA_PRORROGA
    SET    NUMOPER = nNumOper
    WHERE  IDEPOL  = nIdePol
    AND    NUMOPER IS NULL;
    -->> COTECSA FM *23718* 04/08/2018 SE CREA ESTE UPATE PARA QUE CUANDO EXISTA UNA PRORROGA COLOQUE LA OPERACI�N CON QUE SE HIZO
  END IF;
  --nNumOper         := PR_OPERACION.nNumOper; DV. 03/10/2013
  --<<BBVA Consis EFVC 14/10/2014 - #4162 Se comenta proceso el cual cuando hay excluiones y para polizas colectiva con una unica factura esta anulando la factura generada en la poliza
  /*
  nExcCert         := 0;
  FOR CEXC IN CERTIF_EXCLUIDOS LOOP
    BEGIN
      SELECT 1
      INTO   nExiste
      FROM   ACREENCIA A, COND_FINANCIAMIENTO CF, GIROS_FINANCIAMIENTO GF
      WHERE  CF.IdePol = CEXC.IdePol
        AND  CF.NumCert = CEXC.NumCert
        AND  GF.NumFinanc = CF.NumFinanc
        AND  A.NumAcre = GF.NumAcre
        AND  A.StsAcre = 'ACT'
        AND  A.IdeFact IS NOT NULL;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        nExiste  := 0;
      WHEN TOO_MANY_ROWS THEN
        nExiste  := 1;
    END;
    IF nExiste = 1 THEN
      FOR FAAN IN (SELECT DISTINCT A.IDEFACT
                   FROM   ACREENCIA A, COND_FINANCIAMIENTO CF, GIROS_FINANCIAMIENTO GF
                   WHERE  CF.IdePol = CEXC.IdePol
                     AND  CF.NumCert = CEXC.NumCert
                     AND  GF.NumFinanc = CF.NumFinanc
                     AND  A.NumAcre = GF.NumAcre
                     AND  A.StsAcre = 'ACT'
                     AND  A.IdeFact IS NOT NULL) LOOP
        PR_FACTURA.AnulAR (FAAN.IDEFACT);
      END LOOP;
    END IF;
    IF PR.BUSCA_PARAMETRO ('055') = 'SI' THEN
      FOR X IN (SELECT A.NumAcre
                FROM   ACREENCIA A, COND_FINANCIAMIENTO CF, GIROS_FINANCIAMIENTO GF
                WHERE  CF.IdePol = CEXC.IdePol
                  AND  CF.NumCert = CEXC.NumCert
                  AND  GF.NumFinanc = CF.NumFinanc
                  AND  A.NumAcre = GF.NumAcre
                  AND  A.StsAcre = 'ACT'
                  AND  A.IdeFact IS NULL) LOOP
        PR_ACREENCIA.Anular_Sis (X.NumAcre);
        UPDATE ACREENCIA
        SET    FecSts = CEXC.FecExc
        WHERE  NumAcre = X.NumAcre;
        PR_GIROS_FINANCIAMIENTO.GENERAR_TRANSACCION (X.NumAcre, 'ANULA');
      END LOOP;
    END IF;
    FOR Y IN (SELECT DISTINCT O.NumOblig
              FROM   OBLIGACION O, PRE_OBLIG PO, REL_OBLACR_FRAC ROF, ACREENCIA A
              WHERE  PO.IdePol = CEXC.IdePol
                AND  PO.NumCert = CEXC.NumCert
                AND  PO.NumOBLIG = O.NumOblig
                AND  O.StsOblig = 'ACT'
                AND  ROF.NumOblig = PO.NumOblig
                AND  ROF.IdePol = PO.IdePol
                AND  ROF.NumCert = PO.NumCert
                AND  ROF.NumAcre = A.NumAcre
                AND  A.StsAcre NOT IN ('COB')
                AND  O.FecGtiaPago >= CEXC.FecExc) LOOP
      PR_OBLIGACION.Anular_Sis ('PRC', Y.NumOblig);
    END LOOP;
    nExcCert  := 1;
    dFecExc   := CEXC.FecExc;
  END LOOP;
  IF nExcCert = 1 THEN
    PR_FACTURA.GEN_FACT_EXC (nIdePol, dFecExc);
  END IF;
  BEGIN
    SELECT 1
    INTO   nExiste
    FROM   OPER_POL OP, CERTIFICADO C
    WHERE  NumOper = nNumOper
      AND  TipoOp = 'MOD'
      AND  OP.IdePol = C.IdePol
      AND  C.IdePol = OP.IdePol
      AND  C.StsCert <> 'EXC'
      AND  MtoOper < 0;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      nExiste  := 0;
    WHEN TOO_MANY_ROWS THEN
      nExiste  := 1;
  END;
  */
  --<<BBVA Consis EFVC 14/10/2014

  PR_POLIZA.ACTUALIZA_MIGRA_REASEGURO (nIdepol, nNumOper);
  --Generacion de comunicaci�n y documentaci�n para el cliente de la p�liza
  --PR_COMUNICA_POST_VENTA.CREA_COMUNICACION (nIdePol, nNumOper);
  PR_OPERACION.TERMINAR;
  IF cTipoMov = 'D' THEN
    PR_POLIZA.FACT_DIF_RSRVA (nIdePol, nNumOper);
    PR_VIOLA_AUTORIZA_NIVEL.ACTIVAR (nIdePol);
  END IF;
  BEGIN
    DELETE GARANTIAS_VEHICULO
    WHERE  IdePol = nIdePol;
    FOR B IN D LOOP
      nNumCert1  := b.NumCert;
      INSERT INTO GARANTIAS_VEHICULO
      VALUES (nIdePol, nNumCert1, ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
              ' ', ' ', ' ', ' ', ' ', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
      n          := 0;
      FOR A IN C LOOP
        n  := n + 1;
        IF n = 1 THEN
          UPDATE GARANTIAS_VEHICULO
          SET    Garantia1 = a.CodCobert,
                 Valor_Garantia1 = a.SumaAsegMoneda * 100
          WHERE  IdePol = nIdePol
            AND  NumCert = nNumCert1;
        ELSIF n = 2 THEN
          UPDATE GARANTIAS_VEHICULO
          SET    Garantia2 = a.CodCobert,
                 Valor_Garantia2 = a.SumaAsegMoneda * 100
          WHERE  IdePol = nIdePol
            AND  NumCert = nNumCert1;
        ELSIF n = 3 THEN
          UPDATE GARANTIAS_VEHICULO
          SET    Garantia3 = a.CodCobert,
                 Valor_Garantia3 = a.SumaAsegMoneda * 100
          WHERE  IdePol = nIdePol
            AND  NumCert = nNumCert1;
        ELSIF n = 4 THEN
          UPDATE GARANTIAS_VEHICULO
          SET    Garantia4 = a.CodCobert,
                 Valor_Garantia4 = a.SumaAsegMoneda * 100
          WHERE  IdePol = nIdePol
            AND  NumCert = nNumCert1;
        ELSIF n = 5 THEN
          UPDATE GARANTIAS_VEHICULO
          SET    Garantia5 = a.CodCobert,
                 Valor_Garantia5 = a.SumaAsegMoneda * 100
          WHERE  IdePol = nIdePol
            AND  NumCert = nNumCert1;
        ELSIF n = 6 THEN
          UPDATE GARANTIAS_VEHICULO
          SET    Garantia6 = a.CodCobert,
                 Valor_Garantia6 = a.SumaAsegMoneda * 100
          WHERE  IdePol = nIdePol
            AND  NumCert = nNumCert1;
        ELSIF n = 7 THEN
          UPDATE GARANTIAS_VEHICULO
          SET    Garantia7 = a.CodCobert,
                 Valor_Garantia7 = a.SumaAsegMoneda * 100
          WHERE  IdePol = nIdePol
            AND  NumCert = nNumCert1;
        ELSIF n = 8 THEN
          UPDATE GARANTIAS_VEHICULO
          SET    Garantia8 = a.CodCobert,
                 Valor_Garantia8 = a.SumaAsegMoneda * 100
           WHERE IdePol = nIdePol
             AND NumCert = nNumCert1;
        ELSIF n = 9 THEN
          UPDATE GARANTIAS_VEHICULO
          SET    Garantia9 = a.CodCobert,
                 Valor_Garantia9 = a.SumaAsegMoneda * 100
          WHERE  IdePol = nIdePol
            AND  NumCert = nNumCert1;
        ELSIF n = 10 THEN
          UPDATE GARANTIAS_VEHICULO
          SET    Garantia10 = a.CodCobert,
                 Valor_Garantia10 = a.SumaAsegMoneda * 100
          WHERE  IdePol = nIdePol
            AND  NumCert = nNumCert1;
        ELSIF n = 11 THEN
          UPDATE GARANTIAS_VEHICULO
          SET    Garantia11 = a.CodCobert,
                 Valor_Garantia11 = a.SumaAsegMoneda * 100
          WHERE  IdePol = nIdePol
            AND  NumCert = nNumCert1;
        ELSIF n = 12 THEN
          UPDATE GARANTIAS_VEHICULO
          SET    Garantia12 = a.CodCobert,
                 Valor_Garantia12 = a.SumaAsegMoneda * 100
          WHERE  IdePol = nIdePol
            AND  NumCert = nNumCert1;
        ELSIF n = 13 THEN
          UPDATE GARANTIAS_VEHICULO
          SET    Garantia13 = a.CodCobert,
                 Valor_Garantia13 = a.SumaAsegMoneda * 100
          WHERE  IdePol = nIdePol
            AND  NumCert = nNumCert1;
        ELSIF n = 14 THEN
          UPDATE GARANTIAS_VEHICULO
          SET    Garantia14 = a.CodCobert,
                 Valor_Garantia14 = a.SumaAsegMoneda * 100
          WHERE  IdePol = nIdePol
            AND  NumCert = nNumCert1;
        END IF;
      END LOOP;
    END LOOP;
  END;
  ---- Erick Zoque 04/09/2013 E_CON_20120815_1_1
  --GENERACION DE COMPROBANTE SUMAASEG
  IF cTipoMov = 'D' THEN
     /*IF NVL (PR_POLIZA.cOperRev, 'XXX') = 'REV' THEN
        COMP_SUMAASEG(nIdePol,nNumOper,'REV',nSumaAsegAnt);
     ELSE*/
        COMP_SUMAASEG(nIdePol,nNumOper,'ACT',nSumaAsegAnt);
     --END IF;
  END IF;
  --CONSIS(ER) 18/07/2013 Generar estimado de Retribuci�n en canal_poliza
  PR_CANAL_POLIZA.CALCULA_RET_ESTIMADA(nIdePol, nNumOper);
  --Erick Zoque Se agrega parte primas diferidas
  /*IF cTipoMov = 'D' THEN
     nDias := PR_POLIZA.DFECFINVIG - PR_POLIZA.DFECINIVIG;
     IF nDias > 366 THEN
        PR_ACREENCIA.VALIDA_DIFERIDA(nNumOper);
     END IF;
  END IF;*/
  PR_FECHAS_EQUIVALENTES.CEVENTO := NULL;
----- Vicente Lira VELM. Se agrega control para comisiones por monto, segun acuerdo con plinio y Edwin Romero
----- Se unifican los c�lculos para que todos los procesos relacionados con comisiones se realicen por porcentaje
-- << 21650 - JC.DuqueL MtoFijo (Esta actualizaci�n no debe existir ya que esta anulando la cofiguraci�n ya heredada)
-- Se elimina seccion 21650
-- <<21650 - JC.DuqueL MtoFijo (Esta actualizaci�n no debe existir ya que esta anulando la cofiguraci�n ya heredada)
-----
  --<<Consis 13/05/2014 EFVC Borrado de Mov_prima generado para proceso de exclusion beneficiarios por acsel web , registros que generan error en la generacion de facturas
  --<<Consis EFVC 09/11/2015    Se cambia Condicion de Control para proceso rehabilitacion de polizas se atualice el monto de la operacion

      BEGIN
             select 1 INTO nExisteMvtben
             from mov_prima where idepol=nIdePol AND tipo='B';
      EXCEPTION
             WHEN NO_DATA_FOUND THEN
                  nExisteMvtben := 0;
             WHEN TOO_MANY_ROWS THEN
                  nExisteMvtben := 1;
      END;

     IF cTipoMov = 'D'  AND nExisteMvtben = 1 THEN
     BEGIN

      delete REC_FINANCIAMIENTO
      WHERE IDEREC IN (SELECT IDEREC FROM recibo
      where idemovprima in (select idemovprima from mov_prima where idepol=nIdePol and tipo='B'));

      delete recibo
      where idemovprima in (select idemovprima from mov_prima where idepol=nIdePol and tipo='B');

      DELETE PART_INTER_MOV
      WHERE idemovprima in (select idemovprima from mov_prima where idepol=nIdePol AND tipo='B');

      DELETE RENG_MP
      WHERE idemovprima in (select idemovprima from mov_prima where idepol=nIdePol AND tipo='B');

      FOR X IN (SELECT SUM(MTOMONEDA) nSumaRecibo, numcert
            FROM recibo
            where NUMOPER= nNumOper
            group by numcert) LOOP
            IF nSumaRecibo IS NOT NULL THEN
               UPDATE OPER_POL
               SET MTOOPER = X.nSumaRecibo
               WHERE NUMOPER= nNumOper
               AND NUMCERT = X.NUMCERT;
            END IF;
      END LOOP;


      delete mov_prima
      where idepol=nIdePol and tipo='B';
     END;
   END IF;
   --<<Consis 13/05/2014 EFVC
--
-----
  --Validamos los Cruces Automaticos de la WEB, deben Cumplir Ciertas Condiciones--
  PR_CANCEL_SUPLE.VALIDAR_CRUCE_SUPLEMENTOS (nIDePol,nNumOper); --COTECSA #23771--
  RETURN (nNumOper);
END ACTIVAR;
   --  REFLEJAR MODIFICACION
   PROCEDURE REFLEJAR_MODIFICACION (nIdePol POLIZA.IdePol%TYPE) IS
      cStsPol   POLIZA.STSPOL%TYPE;
   BEGIN
      BEGIN
         SELECT StsPol
         INTO   cStsPol
         FROM   POLIZA
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            RAISE_APPLICATION_ERROR (-20100, 'No Existe EL idepol = ' || nIdePol);
      END;
      IF cStsPol = 'ACT' THEN
         PR_POLIZA.GENERAR_TRANSACCION (nIdePol, 'MODIF');
      END IF;
   END;
   --//
   -- GENERAR TRANSACCION
   PROCEDURE GENERAR_TRANSACCION (nIdePol IN NUMBER, cCodProceso IN VARCHAR2) IS
      cStsFinal   VARCHAR2 (3);
      cStsPol     VARCHAR2 (3);
   BEGIN
      SELECT StsPol
      INTO   cStsPol
      FROM   POLIZA
      WHERE  IdePol = nIdePol
      FOR UPDATE;
      cStsFinal  := PR.REGLA_STS_FINAL ('POLIZA', cCodProceso, cStsPol);
      UPDATE POLIZA
      SET StsPol = cStsFinal
      WHERE  IdePol = nIdePol;
   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         RAISE_APPLICATION_ERROR (-20205, PR.MENSAJE ('ABD', 20205, NULL, TO_CHAR (PR_POLIZA.nNumPol) || ' - ' || cStsPol, 'POLIZA'));
   END;
   --//
   -- INCLUIR
   PROCEDURE INCLUIR (nIdePol POLIZA.IdePol%TYPE) IS
      --
      nNumCert       CERTIFICADO.NumCert%TYPE               := NULL;
      cIndAutri      PROD_PLAN_FRAC.IndAutri%TYPE           := NULL;
      cIndLocRef     DATOS_PART_POLIZA.INDLOCREF%TYPE       := NULL;
      cTipoCliente   TERCERO_DATOS_TRIBU.TIPOCLIENTE%TYPE   := NULL;
      --
      cIndReqObl     VARCHAR2 (1)                           := 'S';
      --
      CURSOR ANEXO_POL_C IS
         SELECT IdeAnexo
         FROM   ANEXO_POL
         WHERE  IdePol = nIdePol
         AND    StsAnexo = 'VAL';
      CURSOR DIST_COA_C IS
         SELECT CodAcepRiesgo
         FROM   DIST_COA
         WHERE  IdePol = nIdePol
         AND    StsCoa = 'VAL';
      CURSOR RECA_DCTO_POL_C IS
         SELECT IDERECADCTO
         FROM   RECA_DCTO_POL
         WHERE  IdePol = nIdePol
         AND    StsRecaDcto = 'VAL';
      CURSOR POLIZA_CLIENTE_C IS
         SELECT CodCli
         FROM   POLIZA_CLIENTE
         WHERE  IdePol = nIdePol
         AND    StsPolCli = 'VAL';
      CURSOR AGENCIAMIENTO_POL_C IS
         SELECT TipoId, NumId, DvId, NumParcelaPago
         FROM   AGENCIAMIENTO_POL
         WHERE  IdePol = nIdePol
         AND    StsAgen = 'VAL';
      CURSOR DEDUC_C IS
         SELECT IdeDeduc
         FROM   DEDUCIBLE_POLIZA
         WHERE  IdePol = nIdePol
         AND    StsDeduc = 'VAL';
      CURSOR C_PROD_PLAN_FRAC IS
         SELECT INDAUTRI
         FROM   PROD_PLAN_FRAC
         WHERE  CODPROD = PR_POLIZA.cCodProd
         AND    CODPLANFRACC = PR_POLIZA.cCodPlanFrac
         AND    MODPLANFRACC = PR_POLIZA.cModPlanFrac;
      CURSOR C_VALIDA IS
         SELECT DPP.INDLOCREF, TDT.TIPOCLIENTE
         FROM   POLIZA P, CLIENTE C, TERCERO_DATOS_TRIBU TDT, DATOS_PART_POLIZA DPP
         WHERE  TDT.TIPOID = C.TIPOID
         AND    TDT.NUMID = C.NUMID
         AND    TDT.DVID = C.DVID
         AND    C.CODCLI = P.CODCLI
         AND    DPP.IDEPOL(+) = P.IDEPOL
         AND    P.IDEPOL = nIdePol;
      CURSOR CLAU_POL_C IS
         SELECT IdeClauCert
         FROM   CLAU_POL
         WHERE  IdePol = nIdePol
         AND    StsClau = 'VAL';
      CURSOR CERTIFICADO_VAL IS
         SELECT NumCert
         FROM   CERTIFICADO
         WHERE  IdePol = nIdePol
         AND    StsCert = 'VAL';
TYPE REG_CERTIFICADO  IS TABLE OF REG_CERTIFICADO_VAL%ROWTYPE  INDEX BY PLS_INTEGER;
    CERT REG_CERTIFICADO;

   BEGIN
     --Erick Zoque 20/05/2013 E_CON_20120821_1_7
     PR_FECHAS_EQUIVALENTES.CEVENTO := '001';
     -- GMoreno 12/04/2013 validaci�n de comisiones de actores de canales de ventas
     IF PR_COMISIONES_CANALES.VALIDA_CANAL_POLIZA(nIdePol) = 'N' THEN
        RAISE_APPLICATION_ERROR (-20220, PR.MENSAJE ('ABD', 20193, 'datos de comisiones/Retribuciones para el canal de venta ' , NULL, NULL));
     END IF;
      -- Cargar Los Datos De La Poliza
      PR_POLIZA.CARGAR (nIdePol);
      IF PR_POLIZA.VALIDAR (nIdePol, 'T') = 1 THEN
         PR_OPERACION.INICIAR ('INCLU', 'POLIZA', TO_CHAR (nIdePol));
         --DV. 13/11/2012. Requisitos por P�liza
         PR_REQ_POL.GENERAR_REQ_OBLIG(nIdePol);
         IF (MONTHS_BETWEEN (TRUNC (PR_POLIZA.dFecFinVig), TRUNC (PR_POLIZA.dFecIniVig))) > 12 THEN
            PR_VIOLA_AUTORIZA_NIVEL.VALIDAR_OPER (nIdePol, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0015');
         END IF;
         BEGIN
            OPEN C_PROD_PLAN_FRAC;
            FETCH  C_PROD_PLAN_FRAC
            INTO   cIndAutri;
            IF C_PROD_PLAN_FRAC%NOTFOUND THEN
               cIndAutri  := 'N';
            ELSE
               cIndAutri  := 'S';
            END IF;
            CLOSE C_PROD_PLAN_FRAC;
         END;
         IF NVL (cIndAutri, 'N') = 'S' THEN   -- Nivel de Autorizaciones.
            PR_VIOLA_AUTORIZA_NIVEL.VALIDAR_OPER (nIdePol, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0016');
         END IF;
         BEGIN
            OPEN C_VALIDA;
            FETCH  C_VALIDA
            INTO   cIndLocRef, cTipoCliente;
            IF C_VALIDA%NOTFOUND THEN
               cIndLocRef    := NULL;
               cTipoCliente  := NULL;
            END IF;
            CLOSE C_VALIDA;
         END;
         IF     NVL (cIndLocRef, 'N') = 'R'
            AND NVL (cTipoCliente, '01') = '05' THEN   -- Nivel de Autorizaciones.
            PR_VIOLA_AUTORIZA_NIVEL.VALIDAR_OPER (nIdePol, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0018');
         END IF;
         PR_B$_POLIZA.PUSH (nIdePol, 'INCLU', 'USR');   -- Guardar Reflejo de La Poliza
         -----------------  consis 07/10/2014
          /*BEGIN
            CLOSE  REG_CERTIFICADO_VAL;
            EXCEPTION WHEN OTHERS THEN NULL;
        END;*/
    /*OPEN REG_CERTIFICADO_VAL;
    LOOP
       FETCH REG_CERTIFICADO_VAL BULK COLLECT
       INTO CERT LIMIT 1000;
       EXIT WHEN CERT.COUNT = 0;
       FOR i IN 1 .. CERT.COUNT LOOP */
       -----------------  consis 07/10/2014

         FOR R_CERT IN CERTIFICADO_VAL LOOP   -- Incluir Las Tablas Asociadas A Poliza
           -- IF PR_CERTIFICADO.VALIDAR (nIdePol, R_CERT.NumCert) = 1 THEN   pp consis
                --PR_CERTIFICADO.INCLUIR_SIS ('PRC', nIdePol, cert(i).NumCert);
                PR_CERTIFICADO.INCLUIR_SIS ('PRC', nIdePol, R_CERT.NumCert);
           ---- END IF;   pp consis
       -----------------  consis 07/10/2014
         END LOOP;
       --COMMIT;
       --  END LOOP;
    --CLOSE REG_CERTIFICADO_VAL;
    -----------------  consis 07/10/2014

         PR_DATOS_PARTICULARES_VIDA.INCLUIR(nIdePol);

         FOR X IN ANEXO_POL_C LOOP
            PR_ANEXO_POL.INCLUIR_SIS ('PRC', nIdePol, X.IdeAnexo);
         END LOOP;
         FOR X IN DIST_COA_C LOOP
            PR_DIST_COA.INCLUIR_SIS ('PRC', nIdePol, X.CodAcepRiesgo);
         END LOOP;
         FOR X IN POLIZA_CLIENTE_C LOOP
            PR_POLIZA_CLIENTE.INCLUIR_SIS ('PRC', nIdePol, X.CodCli);
         END LOOP;
         FOR X IN RECA_DCTO_POL_C LOOP
            PR_RECA_DCTO_POL.INCLUIR_SIS ('PRC', X.IdeRecaDcto);
         END LOOP;
         FOR X IN AGENCIAMIENTO_POL_C LOOP
            PR_AGENCIAMIENTO_POL.INCLUIR_SIS ('PRC', nIdePol, X.TipoId, X.NumId, X.DvId, X.NumParcelaPago);
         END LOOP;
         FOR X IN DEDUC_C LOOP
            PR_DEDUCIBLE_POLIZA.INCLUIR_SIS ('PRC', nIdePol, X.IdeDeduc);
         END LOOP;
         FOR X IN CLAU_POL_C LOOP
            PR_CLAU_POL.INCLUIR_SIS ('PRC', X.IdeClauCert);
         END LOOP;
         PR_POLIZA.GENERA_ARTICULO_41 (nIdePol);
         PR_POLIZA.GENERAR_TRANSACCION (nIdePol, 'INCLU');
         PR_OPERACION.TERMINAR;
      ELSE
         RAISE_APPLICATION_ERROR (-20220, PR.MENSAJE ('ABD', 20220, 'Poliza: ' || TO_CHAR (PR_POLIZA.nNumPol), NULL, NULL));
      END IF;
      --Erick Zoque 20/05/2013 E_CON_20120821_1_7
      PR_FECHAS_EQUIVALENTES.CEVENTO := NULL;
   END INCLUIR;
   --//
   -- CARGAR
PROCEDURE CARGAR (nIdePol POLIZA.IdePol%TYPE)
IS
  cCodProd          POLIZA.CodProd%TYPE;
  cTipoSusc         POLIZA.TipoSusc%TYPE;
  cIndMultiRiesgo   POLIZA.IndMultiRiesgo%TYPE;
  cIndCobProv       POLIZA.IndCobProv%TYPE;
  nNumPol           POLIZA.NumPol%TYPE;
  cStsPol           POLIZA.StsPol%TYPE;
  dFecIniVig        POLIZA.FecIniVig%TYPE;
  dFecFinVig        POLIZA.FecFinVig%TYPE;
  cIndTipoPro       POLIZA.IndTipoPro%TYPE;
  nPorcCtp          POLIZA.PorcCtp%TYPE;
  cClasePol         POLIZA.ClasePol%TYPE;
  cTipoFact         POLIZA.TipoFact%TYPE;
  cTipoPdcion       POLIZA.TipoPdcion%TYPE;
  dFecUltFact       POLIZA.FecUltFact%TYPE;
  cCodFormPago      POLIZA.CodFormPago%TYPE;
  cCodMoneda        POLIZA.CodMoneda%TYPE;
  cCodOfiemi        POLIZA.CodOfiemi%TYPE;
  cCodOfiSusc       POLIZA.CodOfiSusc%TYPE;
  cCodFormFcion     POLIZA.CodFormFcion%TYPE;
  cCodCli           POLIZA.CodCli%TYPE;
  cIndMovPolRen     POLIZA.IndMovPolRen%TYPE;
  cTipoCobro        POLIZA.TipoCobro%TYPE;
  cIndFact          POLIZA.IndFact%TYPE;
  nNumRen           POLIZA.NumRen%TYPE;
  cIndRespPago      POLIZA.IndRespPago%TYPE;
  cCodCia           POLIZA.CodCia%TYPE;
  cTipoVig          POLIZA.TipoVig%TYPE;
  cIndRecupRea      POLIZA.IndRecupRea%TYPE;
  cModPlanFrac      POLIZA.ModPlanFrac%TYPE;
  cCodPlanFrac      POLIZA.CodPlanFrac%TYPE;
  cCodMotvAnul      POLIZA.CodMotvAnul%TYPE;
  dFecAnul          POLIZA.FecAnul%TYPE;
  cNumCotiFlex      POLIZA.NumCotiFlex%TYPE;
  --
  cFlagCarga        VARCHAR2(1) := 'S';--RRR_18607
BEGIN
  IF PR_POLIZA_MAS.cIndPolMas = 'S' THEN--RRR_18607
    IF nIdePol = NVL(PR_POLIZA.nIdePol, 0) AND PR_POLIZA_MAS.cIndCargaPol = 'N' THEN
      cFlagCarga := 'N';
    ELSIF PR_POLIZA_MAS.cIndCargaPol = 'S' THEN
      PR_POLIZA_MAS.cIndCargaPol := 'N';
    END IF;
  END IF;
  --
  IF cFlagCarga = 'S' THEN--RRR_18607
      BEGIN
         SELECT CodProd, TipoSusc, IndMultiRiesgo, IndCobProv, NumPol, StsPol, FecIniVig, FecFinVig, IndTipoPro, PorcCtp, ClasePol, TipoFact,
                FecUltFact, TipoPdcion, CodFormPago, CodMoneda, CodOfiemi, CodFormFcion, IndMovPolRen, TipoCobro, IndFact, NumRen, IndRespPago,
                CodOfiSusc, CodCia, TipoVig, IndRecupRea, CodCli, CodPlanFrac, ModPlanFrac, CodMotvAnul, FecAnul, NumCotiFlex
         INTO   cCodProd, cTipoSusc, cIndMultiRiesgo, cIndCobProv, nNumPol, cStsPol, dFecIniVig, dFecFinVig, cIndTipoPro, nPorcCtp, cClasePol,
                cTipoFact, dFecUltFact, cTipoPdcion, cCodFormPago, cCodMoneda, cCodOfiemi, cCodFormFcion, cIndMovPolRen, cTipoCobro, cIndFact,
                nNumRen, cIndRespPago, cCodOfiSusc, cCodCia, cTipoVig, cIndRecupRea, cCodCli, cCodPlanFrac, cModPlanFrac, cCodMotvAnul, dFecAnul,
                cNumCotiFlex
         FROM   POLIZA
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            RAISE_APPLICATION_ERROR (-20100, 'No Existe EL Idepol = ' || nIdePol);
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, 'Error al seleccionar desde tabla POLIZA ' || SQLERRM);
      END;
      ---  Se incluye una rutina especial para el c�lculo del porcentaje de Corto Plazo  ---
--      nPorcCtp                 := PR_POLIZA.CALC_CORTO_PLAZO (nPorcCtp, cIndTipoPro, dFecIniVig, dFecFinVig);
--------------------------------------------------------------------------------------
      PR_POLIZA.cCodProd         := cCodProd;
      PR_POLIZA.cTipoSusc        := cTipoSusc;
      PR_POLIZA.cIndMultiRiesgo  := cIndMultiRiesgo;
      PR_POLIZA.cIndCobProv      := cIndCobProv;
      PR_POLIZA.nNumPol          := nNumPol;
      PR_POLIZA.cStsPol          := cStsPol;
      PR_POLIZA.nIdePol          := nIdePol;
      PR_POLIZA.dFecIniVig       := dFecIniVig;
      PR_POLIZA.dFecFinVig       := dFecFinVig;
      PR_POLIZA.cIndTipoPro      := cIndTipoPro;
      PR_POLIZA.nPorcCtp         := nPorcCtp;
      PR_POLIZA.cClasePol        := cClasePol;
      PR_POLIZA.cTipoFact        := cTipoFact;
      PR_POLIZA.dFecUltFact      := dFecUltFact;
      PR_POLIZA.cTipoPdcion      := cTipoPdcion;
      PR_POLIZA.cCodFormPago     := cCodFormPago;
      PR_POLIZA.cCodMoneda       := cCodMoneda;
      PR_POLIZA.cCodOfiemi       := cCodOfiemi;
      PR_POLIZA.cCodOfiSusc      := cCodOfiSusc;
      PR_POLIZA.cCodFormFcion    := cCodFormFcion;
      PR_POLIZA.cCodCli          := cCodCli;
      PR_POLIZA.cIndMovPolRen    := cIndMovPolRen;
      PR_POLIZA.cTipoCobro       := cTipoCobro;
      PR_POLIZA.cIndFact         := cIndFact;
      PR_POLIZA.nNumRen          := nNumRen;
      PR_POLIZA.cIndRespPago     := cIndRespPago;
      PR_POLIZA.cCodCia          := cCodCia;
      PR_POLIZA.cTipoVig         := cTipoVig;
      -- Mod 11/03/2005
      PR_POLIZA.cModPlanFrac     := cModPlanFrac;
      PR_POLIZA.cCodPlanFrac     := cCodPlanFrac;
      PR_POLIZA.cCodMotvAnul     := cCodMotvAnul;
      PR_POLIZA.dFecAnul         := dFecAnul;
      PR_POLIZA.cNumCotiFlex     := cNumCotiFlex;
  END IF;
END CARGAR;
   --//
   -- VALIDAR
   FUNCTION VALIDAR (nIdePol POLIZA.IdePol%TYPE, cTipoMov VARCHAR2)
      RETURN NUMBER IS
      --
      nFlgReg            NUMBER (1);
      nNroPersMinCert    RAMO_PLAN_PROD.NroPersMin%TYPE;
      nNumAseg           NUMBER (10);
      cValCant           VARCHAR2 (1);
      -- Realizado por         Eduardo Ocando
      -- Fecha                 11/01/2005
      -- Descripci�n    ver Anexo 1
      cIndContributiva   POLIZA.IndContributiva%TYPE;
      nCantAseg          DAT_PART_AP.CantaSeg%TYPE;
      nCantAsegPol       DAT_PART_POL_VIDA.CantPersonas%TYPE;
      nLimaGreResPPol    DAT_PART_POL_VIDA.LimaGreResPPol%TYPE;
      cIndPorcLar        RAMO_PLAN_PROD.IndPorcLar%TYPE;
      --
      nExistePartU       NUMBER (10)                             := 0;
      nIndFactG          POLIZA.IndFactG%TYPE;
      cStsPol            POLIZA.STSPOL%TYPE;
      nCantCertRamo      NUMBER (10)                             := 0;
      nCertMayRiesgo     NUMBER (10)                             := 0;
      cCertificado       VARCHAR2 (200);
      cTextoMayRiesgo    VARCHAR2 (150);
      cModo              USUARIO.Modo%TYPE;
      cCodUsr            USUARIO.CodUsr%TYPE;
      cClaseComp         CPTO_CONTABLE.ClaseComp%TYPE;
      dFecCierre         DATE;
      cExisteResp        VARCHAR2(1);
      cTipoInter         VARCHAR2(1);
      cTomadorColseg     POLIZA.CodCli%TYPE;
      cFlgExist          VARCHAR2(1);
      nCodError          NUMBER;
      --<Miguel Portillo - Consis Int - 16/06/2015 - Valida Plan - Cumplimiento
      cValidCumpl        VARCHAR2(1);
      cCodPlan           CERT_RAMO.CodPlan%TYPE;
      cRevPlan           CERT_RAMO.RevPlan%TYPE;
      cCodRamoCert       CERT_RAMO.CodRamoCert%TYPE;
      --
      CURSOR C_CERTIFICADO_VAL IS
         SELECT NumCert, StsCert,IndMayRiesgo
         FROM   CERTIFICADO
         WHERE  IdePol = nIdePol;
      CURSOR C_CERTIFICADO_INC_MOD IS
         SELECT NumCert
         FROM   CERTIFICADO
         WHERE  IdePol = nIdePol
         AND    StsCert IN ('INC', 'MOD');
    TYPE REG_CERT_INC IS TABLE OF C_CERTIFICADO_INC_MOD%ROWTYPE INDEX BY PLS_INTEGER;
    CERT_INC REG_CERT_INC;

      CURSOR C_CLIENTE_VAL IS
         SELECT CV.CodVeto
         FROM   CLIENTE C, CLIENTE_VETOS CV
         WHERE  C.CodCli = PR_POLIZA.cCodCli
         AND   (CV.TipoId = C.TipoId OR CV.TipoId = C.TipoIdRepLegal) -->>  -- BBVA 11/01/2017 TECNOCOM -- REDMINE 22981
         AND   (CV.NumId  = C.NumId OR CV.NumId = C.NumIdRepLegal)
         AND   (CV.DvId   = C.DvId OR CV.DvId = C.DvidRepLegal)       --<<  -- BBVA 11/01/2017 TECNOCOM -- REDMINE 22981
         AND    CV.StatusVeto = 'ACT'
         AND    CV.IndVetoBoletin = 'V'
         UNION ALL                                                       -->>  -- BBVA 22/01/2017 TECNOCOM -- REDMINE 22981
         SELECT DISTINCT CV.CodVeto
         FROM   TERCERO T,
                CLIENTE C,
                TERCERO_SOCIOS TS,
                CLIENTE_VETOS CV
         WHERE T.TipoId   = C.TipoId
         AND   T.NumId    = C.NumId
         AND   T.DvId     = C.DvId
         AND   T.TipoId   = TS.TipoId
         AND   T.NumId    = TS.NumId
         AND   T.DvId     = TS.DvId
         AND   TS.TipoIdSoc = CV.TipoId
         AND   TS.NumIdSoc  = CV.NumId
         AND   TS.DvIdSoc   = CV.DvId
         AND   EXISTS (SELECT 1
                       FROM TERCERO_ROL TR
                       WHERE TR.TipoId = TS.TipoIdSoc
                       AND   TR.NumId  = TS.NumIdSoc
                       AND   TR.DvId   = TS.DvIdSoc
                       AND   TR.CodRol = '71'
                       AND   TR.StsRol = 'ACT')
         AND   CV.StatusVeto = 'ACT'
         AND   CV.IndVetoBoletin = 'V'
         AND   C.CodCli = PR_POLIZA.cCodCli;                               --<<  -- BBVA 22/01/2017 TECNOCOM -- REDMINE 22981

      CURSOR C_CERT_RAMO_POLIZA IS
         SELECT CodPlan, RevPlan, CodRamoCert, COUNT (*) CantCertRamo
         FROM   CERT_RAMO
         WHERE  IDEPOL = nIdePol
         AND    FecExc IS NULL
         GROUP BY CodPlan, RevPlan, CodRamoCert;

         TYPE REG_CERT_RAMO IS TABLE OF C_CERT_RAMO_POLIZA%ROWTYPE INDEX BY PLS_INTEGER;
    CERT_RA REG_CERT_RAMO;

      CURSOR C_RESP_PAGO IS
         SELECT R.CodCli, R.CodViaCobro, V.IndTipoViaCob
         FROM   RESP_PAGO_POL R, VIA_COBRO V
         WHERE  R.IdePol = nIdePol
         AND    V.CodViaCobro = R.CodViaCobro
         UNION
         SELECT R.CodCli, R.CodViaCobro, V.IndTipoViaCob
         FROM   RESP_PAGO R, VIA_COBRO V
         WHERE  R.IdePol = nIdePol
         AND    V.CodViaCobro = R.CodViaCobro;

     TYPE REG_RESPP IS TABLE OF C_RESP_PAGO%ROWTYPE INDEX BY PLS_INTEGER;
    RESPP REG_RESPP;

BEGIN
  PR_POLIZA.CARGAR (nIdePol);   -- DATOS DE LA POLIZA --
  IF PR_POLIZA.cStsPol NOT IN ('ANU', 'CAD') THEN
  --<Miguel Portillo - Consis Int - 16/06/2015 - Valida Plan - Cumplimiento
    BEGIN
      IF PR_POLIZA.cCodProd IS NOT NULL THEN
        BEGIN
          SELECT CodPlan, RevPlan, CodRamoCert
            INTO cCodPlan, cRevPlan, cCodRamoCert
            FROM CERT_RAMO
           WHERE IDEPOL = nIdePol
             AND RowNum = 1
           GROUP BY CodPlan, RevPlan, CodRamoCert;
        EXCEPTION WHEN OTHERS THEN
          cValidCumpl := 'S';
        END;
        BEGIN
          SELECT ValidCumpl
            INTO cValidCumpl
            FROM PLAN_PROD
           WHERE CodProd = PR_POLIZA.cCodProd
             AND CodPlan = cCodPlan
             AND RevPlan = cRevPlan;
        EXCEPTION WHEN NO_DATA_FOUND THEN
          cValidCumpl := 'S';
        END;
      ELSE
        cValidCumpl := 'S';
      END IF;

      IF cValidCumpl = 'S' THEN
        FOR X IN C_CLIENTE_VAL LOOP   -- CLIENTE CON VETOS --
          RAISE_APPLICATION_ERROR (-20353,
                                   PR.MENSAJE ('ABD', 20353, PR.NOMBRE_CLIENTE(PR_POLIZA.cCodCli) , PR_POLIZA.cCodCli, NULL));
        END LOOP;
      END IF;
    END;
  END IF;
      --Miguel Portillo - Consis Int - 16/06/2015>
      IF PR_POLIZA.cStsPol IN ('VAL', 'INC', 'MOD') THEN
         IF PR_POLIZA_CLIENTE.VALIDAR (nIdePol) = 0 THEN   -- NO POSEE CONTRATANTE --
            RAISE_APPLICATION_ERROR (-20214, PR.MENSAJE ('ABD', 20214, TO_CHAR (PR_POLIZA.nNumPol), NULL, NULL));
         END IF;
         IF PR_PART_INTER_POL.VALIDAR (nIdePol) = 0 THEN   -- NO POSEE INTERMEDIARIO --
            RAISE_APPLICATION_ERROR (-20213, PR.MENSAJE ('ABD', 20213, TO_CHAR (PR_POLIZA.nNumPol), NULL, NULL));
         END IF;
         -- Validacion de Vias de Cobro
         cExisteResp  := 'N';
    ------     FOR X IN C_RESP_PAGO LOOP
         OPEN C_RESP_PAGO;
        LOOP
           FETCH C_RESP_PAGO BULK COLLECT
           INTO RESPP LIMIT 1000;
           EXIT WHEN RESPP.COUNT = 0;
           FOR i IN 1 .. RESPP.COUNT LOOP
            cExisteResp  := 'S';
            IF RESPP(i).CodViaCobro IS NULL THEN
               RAISE_APPLICATION_ERROR (-20100,
                                        PR.MENSAJE ('ABD',
                                                    22052,
                                                    'Debe Ingresar la Via de Cobro para el Responsable de Pago ' || RESPP(i).CodCli || ' de la P�liza',
                                                    NULL,
                                                    NULL));
            END IF;
            -- Validacion para no permitir Plan de Financiamiento con Via de cobro diferente de Financiacion de Primas
            -- Defect #4761 - GZuazo - 31/10/2005
            /*
            BEGIN                         --Comentario TD
               SELECT '1'
               INTO   cValCant
               FROM   PLAN_FINANCIAMIENTO
               WHERE  CodPlan = PR_POLIZA.cCodPlanFrac
               AND    ModPlan = PR_POLIZA.cModPlanFrac
               AND    IndFracc = 'N';
            EXCEPTION
               WHEN NO_DATA_FOUND THEN
                  cValCant  := '0';
               WHEN TOO_MANY_ROWS THEN
                  cValCant  := '1';
            END;
            IF     cValCant = '1'
               AND X.IndTipoViaCob != 'F' THEN
               RAISE_APPLICATION_ERROR (-20100,
                                        'El Plan de la P�liza ' || PR_POLIZA.cCodPlanFrac || '-' || PR_POLIZA.cModPlanFrac
                                        || ' Corresponde con la V�a de Cobro de Financiaci�n de Primas. Verifique');
            END IF;
            --
            */
            IF RESPP(i).IndTipoViaCob = 'I' THEN   -- Nomina de Intermediarios
               BEGIN
                  SELECT '1'
                  INTO   cValCant
                  FROM   CLIENTE C, INTERMEDIARIO I
                  WHERE  C.CodCli = RESPP(i).CodCli
                  AND    I.TipoId = C.TipoId
                  AND    I.NumId = C.NumId
                  AND    I.DvId = C.DvId
                  AND    I.StsInter IN ('VAL', 'ACT');
               EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                     RAISE_APPLICATION_ERROR (-20100, 'El Cliente ' || RESPP(i).CodCli || ' No Pertenece a la N�mina de Intermediarios');
                  WHEN TOO_MANY_ROWS THEN
                     cValCant  := '1';
               END;
            --
            ELSIF RESPP(i).IndTipoViaCob = 'E' THEN   -- Nomina de Empleados
               -- TD 6594 - EC - 27/01/2006
               -- Se Busca si Intermedario es Clave Directa
               BEGIN
                  SELECT 'S'
                    INTO cTipoInter
                    FROM INTERMEDIARIO I, PART_INTER_POL P
                   WHERE I.TipoInter = 'D'
                     AND I.CodInter  = P.CodInter
                     AND P.IdePol    = nIdePol;
               EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                     cTipoInter := 'N';
                  WHEN TOO_MANY_ROWS THEN
                     cTipoInter := 'S';
               END;
               IF PR_POLIZA.cTipoSusc = 'I' THEN
                  -- Se busca si Tomador tiene C�digo de Empleado Activo
                  BEGIN
                     SELECT '1'
                     INTO   cValCant
                     FROM   CLIENTE C, TERCERO_EMPLEADO T
                     WHERE  C.CodCli = PR_POLIZA.cCodCli
                     AND    T.TipoId = C.TipoId
                     AND    T.NumId = C.NumId
                     AND    T.DvId = C.DvId
                     AND    T.Estado = '1';
                  EXCEPTION
                     WHEN NO_DATA_FOUND THEN
                        cValCant  := '0';
                     WHEN TOO_MANY_ROWS THEN
                        cValCant  := '1';
                  END;
                  --  Para P�lizas Individuales se deben cumplir las condiciones:
                  --  1.- Tomador con C�digo de Empleado Activo
                  --  2.- Intermediario con Clave Directa
                  --  3.- Responsable de Pago igual al Tomador
                  IF cValCant = '0' OR cTipoInter = 'N' OR PR_POLIZA.cCodCli != RESPP(i).CodCli THEN
                     RAISE_APPLICATION_ERROR (-20100, 'El Cliente ' || PR_POLIZA.cCodCli || ' No Pertenece a la N�mina de Empleados');
                  END IF;
               ELSIF PR_POLIZA.cTipoSusc = 'C' THEN
                  -- Se busca si Responsable de Pago tiene C�digo de Empleado Activo
                  BEGIN
                     SELECT '1'
                     INTO   cValCant
                     FROM   CLIENTE C, TERCERO_EMPLEADO T
                     WHERE  C.CodCli = RESPP(i).CodCli
                     AND    T.TipoId = C.TipoId
                     AND    T.NumId = C.NumId
                     AND    T.DvId = C.DvId
                     AND    T.Estado = '1';
                  EXCEPTION
                     WHEN NO_DATA_FOUND THEN
                        cValCant  := '0';
                     WHEN TOO_MANY_ROWS THEN
                        cValCant  := '1';
                  END;
                  -- Busca Cliente de Compa��a
                  BEGIN
                     SELECT C.CodCli
                       INTO cTomadorColseg
                       FROM MAESTRO_CIA M, CLIENTE C
                      WHERE C.TipoId = M.TipoId
                        AND C.NumId  = M.NumId
                        AND C.DvId   = M.DvId
                        AND M.CodCia = PR_POLIZA.cCodCia;
                  EXCEPTION
                     WHEN NO_DATA_FOUND THEN
                        cTomadorColseg := NULL;
                     WHEN TOO_MANY_ROWS THEN
                        RAISE_APPLICATION_ERROR (-20100, 'Erros en MAESTRO_CIA, existe Tercero Duplicado');
                  END;
                  --  Para P�lizas Colectivas se deben cumplir las condiciones:
                  --  1.- Responsable de Pago con C�digo de Empleado Activo
                  --  2.- Intermediario con Clave Directa
                  --  3.- Tipo de facturaci�n debe ser por Certificado
                  --  4.- Tomador debe ser siempre Colseguros
                  IF cValCant = '0' OR cTipoInter = 'N' OR
                     PR_POLIZA.cTipoFact != 'C' OR
                     PR_POLIZA.cCodCli != cTomadorColseg THEN
                     RAISE_APPLICATION_ERROR (-20100, 'El Cliente ' || RESPP(i).CodCli || ' No Pertenece a la N�mina de Empleados');
                  END IF;
               END IF;
            --
            ELSIF RESPP(i).IndTipoViaCob = 'F' THEN   -- Financiacion de Primas
               BEGIN
                  SELECT '1'
                  INTO   cValCant
                  FROM   PLAN_FINANCIAMIENTO
                  WHERE  CodPlan = PR_POLIZA.cCodPlanFrac
                  AND    ModPlan = PR_POLIZA.cModPlanFrac
                  AND    IndFracc = 'N'
                  AND    StsPlan = 'ACT';
               EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                     RAISE_APPLICATION_ERROR (-20100,
                                              'El Plan de la P�liza ' || PR_POLIZA.cCodPlanFrac || '-' || PR_POLIZA.cModPlanFrac
                                              || ' No es de Financiamiento');
                  WHEN TOO_MANY_ROWS THEN
                     cValCant  := '1';
               END;
            END IF;
         END LOOP;
         ------------ consis 07/10/2014
         --COMMIT;
         END LOOP;
         CLOSE C_RESP_PAGO;
         ------------ consis 07/10/2014

         -- EC - 23/01/2006
         IF NVL(cExisteResp,'N') = 'N' THEN
            RAISE_APPLICATION_ERROR (-20100, 'P�liza/Certificado NO tiene Responsables de Pago');
         END IF;
         -- Valida La Distribucion De Los Beneficiarios Del Asegurado --
         PR_BENEF_ASEG.VALIDA_PART_BENEF (nIdePol);
         nFlgReg  := 0;
         --
         FOR X IN C_CERTIFICADO_VAL LOOP   -- NO POSEE CERTIFICADO --
            nFlgReg  := 1;
            EXIT;
         END LOOP;
         IF nFlgReg = 0 THEN
            RAISE_APPLICATION_ERROR (-20211, PR.MENSAJE ('ABD', 20211, TO_CHAR (PR_POLIZA.nNumPol), NULL, NULL));
         END IF;
         -- Realizado por         Eduardo Ocando
         -- Fecha                 11/01/2005
         -- Descripci�n    Validaci�n de las p�lizas sin detalle de Asegurados, la cual si en la poliza
         -- se le indica que es contributiva sin detalle 'D' este valida el n�mero de asegurados con
         -- la cantidad de personas que se configura en los datos particulares de la poliza, caso contrario
         -- incluyes todos los certificados sin limites o asegurados.
         BEGIN
            SELECT NVL (IndContributiva, ''), NumPol, NVL (IndFactG, 'N'), StsPol
            INTO   cIndContributiva, nNumPol, nIndFactG, cStsPol
            FROM   POLIZA
            WHERE  Idepol = nIdePol;
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               cIndContributiva  := '';
               nIndFactG         := 'N';
            WHEN TOO_MANY_ROWS THEN
               cIndContributiva  := '';
               nIndFactG         := 'N';
         END;
         -- Fin del Proceso
         -- Realizado       Eduardo Ocando
         -- Fecha           13/03/2005.
         -- Descripci�n.    Validaci�n de los datos de Factor G.
         IF nIndFactG = 'S' THEN
            -- Se Busca si se registraron los datos del FACTOR G.
            BEGIN
               SELECT 1
               INTO   nExistePartU
               FROM   POLIZA_FACTORG
               WHERE  Idepol = nIdePol; --DV. 16/10/2012.  Se cambi� validaci�n.
             EXCEPTION
               WHEN NO_DATA_FOUND THEN
                  nExistePartU  := 0;
               WHEN TOO_MANY_ROWS THEN
                  nExistePartU  := 1;
            END;
            --
            IF NVL (nExistePartU, 0) = 0 THEN
               RAISE_APPLICATION_ERROR
                                    (-20211,
                                     PR.MENSAJE ('ABD',
                                                 20220,
                                                 'P�liza se le indico que posee FACTOR G, debe indicar los valores para los C�lculos, revise..! '
                                                 || PR_POLIZA.cCodProd || '-' || TO_CHAR (PR_POLIZA.nNumPol),
                                                 NULL,
                                                 NULL));
            END IF;
         END IF;
         -- Validacion de Mayor Riesgo que exista solo un Certificado marcado como mayo riesgo.
           -- Angelica Domenack 21/04/2005
         BEGIN
            SELECT COUNT (*)
            INTO   nCertMayRiesgo
            FROM   CERTIFICADO
            WHERE  IdePol = nIdePol
            AND    IndMayRiesgo = 'M';
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               nCertMayRiesgo  := 0;
         END;
         IF nCertMayRiesgo > 1 THEN
            FOR X IN C_CERTIFICADO_VAL LOOP   -- NO POSEE CERTIFICADO --
         --i--OTS 19928  Se deben Mostrar unicamente los certificados  con mayor riesgo.
         -- Lucia Mendez
           IF X.IndMayRiesgo = 'M' THEN
                cCertificado  := cCertificado || x.NumCert || ' , ';
           END IF;
          --f--OTS 19928
            END LOOP;
            RAISE_APPLICATION_ERROR
                                  (-20100,
                                   PR.MENSAJE ('AIF',
                                               22052,
                                               'La Poliza solo debe tener un certificado con mayor riesgo. Tiene mas de un certificado marcado : '
                                               || cCertificado || '. revise..! ',
                                               NULL,
                                               NULL));
         END IF;
         -- Fin de Validacion Mayor Concentracion

         -- Fin del Proceso.
         ---- Correccion de validacion de datos de p�liza contra datos de certificado (NRO DE ASEGURADOS) JLM
         ---- Se busca la cantidad de personas y el Monto maximo LAR que fue configurado en los datos
         ---- particulares de la poliza
         BEGIN
            SELECT NVL (CantPersonas, 0), NVL (LimAgreRespPol, 0)
            INTO   nCantAsegPol, nLimAgreRespPol
            FROM   DAT_PART_POL_VIDA
            WHERE  Idepol = nIdePol;
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               nCantAsegPol     := 0;
               nLimaGreResPPol  := 0;
            WHEN TOO_MANY_ROWS THEN
               nCantAsegPol     := 0;
               nLimaGreResPPol  := 0;
         END;
         ---- SE BUSCA EL TOTAL DE LA CANTIDAD DE PERSONAS DEL CERTIFICADO
         IF PR_POLIZA.cStsPol IN ('VAL', 'INC') THEN
            BEGIN
               SELECT NVL (SUM (CantaSeg), 0)
               INTO   nCantAseg
               FROM   DAT_PART_AP DP, CERTIFICADO C
               WHERE  DP.Idepol = C.Idepol
               AND    DP.NumCert = C.NumCert
               AND    C.Idepol = nIdepol
               AND    C.StsCert = PR_POLIZA.cStsPol;
            END;
         ELSIF PR_POLIZA.cStsPol = 'MOD' THEN
            BEGIN
               SELECT NVL (SUM (CantaSeg), 0)
               INTO   nCantAseg
               FROM   DAT_PART_AP DP, CERTIFICADO C
               WHERE  DP.Idepol = C.Idepol
               AND    DP.NumCert = C.NumCert
               AND    C.Idepol = nIdepol
               AND    C.StsCert IN ('ACT', 'MOD', 'INC');
            END;
         END IF;
         ---- SE VALIDA la cantidad de personas de los datos de la p�liza vs los datos del certificado
         IF     nCantAseg != 0
            AND nCantAsegPol != 0 THEN
            IF nCantAseg < nCantAsegPol THEN
               RAISE_APPLICATION_ERROR (-20211,
                                        PR.MENSAJE ('ABD',
                                                    20220,
                                                    'El Nro. de personas en el Certificado es inferior al Nro. de Personas en Datos Particulares ',
                                                    TO_CHAR (nCantAsegPol),
                                                    NULL));
            END IF;
         END IF;
         ---- Fin correcci�n de validacion de datos de p�liza contra datos de certificado (NRO DE ASEGURADOS) JLM
         ----
      -----   FOR X IN C_CERT_RAMO_POLIZA LOOP           consis  07/10/2014  pedro pineda
          OPEN C_CERT_RAMO_POLIZA;
        LOOP
           FETCH C_CERT_RAMO_POLIZA BULK COLLECT
           INTO CERT_RA LIMIT 1000;
           EXIT WHEN CERT_RA.COUNT = 0;
           FOR i IN 1 .. CERT_RA.COUNT LOOP
            ---- BUSCA LOS DATOS DE LAR OBLIGATORIO Y NUMERO DE PERSONAS
            BEGIN
               SELECT NVL (IndPorcLar, 'N'), NVL (NroPersMin, 0)
               INTO   cIndPorcLar, nNroPersMinCert
               FROM   RAMO_PLAN_PROD
               WHERE  CodProd = PR_POLIZA.cCodProd
               AND    CodPlan = CERT_RA(i).CodPlan
               AND    RevPlan = CERT_RA(i).RevPlan
               AND    CodRamoPlan = CERT_RA(i).CodRamoCert;
            EXCEPTION
               WHEN NO_DATA_FOUND THEN
                  cIndPorcLar      := 'N';
                  nNroPersMinCert  := 0;
               WHEN TOO_MANY_ROWS THEN
                  cIndPorcLar      := 'N';
                  nNroPersMinCert  := 0;
            END;
            ---- SE VALIDA LA OBLIGATORIEDAD DEL LAR.
            IF     cIndPorcLar = 'S'
               AND nLimAgreRespPol = 0 THEN
               RAISE_APPLICATION_ERROR
                      (-20211,
                       PR.MENSAJE ('ABD',
                                   20220,
                                   'En los Datos particulares de la p�liza, no fue ingresado el monto del Max. L.A.R. correspondiente, revise....! ',
                                   TO_CHAR (nLimaGreResPPol),
                                   NULL));
            END IF;
            ---- SE BUSCA LA CANTIDAD DE PERSONAS DEL CERTIFICADO
            BEGIN
               SELECT NVL (SUM (CantaSeg), 0)
               INTO   nCantAseg
               FROM   DAT_PART_AP
               WHERE  Idepol = nIdepol
               AND    CodPlan = CERT_RA(i).CodPlan
               AND    RevPlan = CERT_RA(i).RevPlan
               AND    CodRamoCert = CERT_RA(i).CodRamoCert;
            END;
            IF nCantAseg != 0 THEN
               IF nCantAseg < nNroPersMinCert THEN
                  RAISE_APPLICATION_ERROR (-20211,
                                           PR.MENSAJE ('ABD', 20300, CERT_RA(i).CodPlan || '-' || CERT_RA(i).RevPlan || '-' || CERT_RA(i).CodRamoCert, nNroPersMinCert, NULL));
               END IF;
            ELSE
               IF PR_POLIZA.cStsPol IN ('VAL', 'INC') THEN
                  IF CERT_RA(i).CantCertRamo < nNroPersMinCert THEN
                     RAISE_APPLICATION_ERROR (-20211,
                                              PR.MENSAJE ('ABD', 20300, CERT_RA(i).CodPlan || '-' || CERT_RA(i).RevPlan || '-' || CERT_RA(i).CodRamoCert, nNroPersMinCert, NULL));
                  END IF;
               ELSE
                  BEGIN
                     SELECT COUNT (*)
                     INTO   nCantCertRamo
                     FROM   CERT_RAMO C, POLIZA P
                     WHERE  P.IdePol = nIdePol
                     AND    C.IdePol = P.IdePol
                     AND    C.CodPlan = CERT_RA(i).CodPlan
                     AND    C.RevPlan = CERT_RA(i).RevPlan
                     AND    C.CodRamoCert = CERT_RA(i).CodRamoCert
                     AND    C.StsCertRamo IN ('INC', 'ACT', 'MOD')
                     AND    C.fecexc IS NULL
                     GROUP BY P.CodProd, P.IdePol, C.CodPlan, C.RevPlan, C.CodRamoCert;
                  EXCEPTION
                     WHEN OTHERS THEN
                        nCantCertRamo  := 0;
                  END;
                  IF nCantCertRamo < nNroPersMinCert THEN
                     RAISE_APPLICATION_ERROR (-20211,
                                              PR.MENSAJE ('ABD', 20300, CERT_RA(i).CodPlan || '-' || CERT_RA(i).RevPlan || '-' || CERT_RA(i).CodRamoCert, nNroPersMinCert, NULL));
                  END IF;
               END IF;
            END IF;
         END LOOP;
         ----------- consis 07/10/2014  pedro pineda
          --COMMIT;
         END LOOP;
         CLOSE C_CERT_RAMO_POLIZA;
         ----------- consis 07/10/2014
         --
         -- Verifico que tenga Correlativo Contable para Emitir si el Usuario Contabiliza
         -- EC - 25/11/2005
         BEGIN
            SELECT RTRIM(USER,' ')
              INTO cCodUsr
              FROM SYS.DUAL;
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               NULL;
         END;
         BEGIN
            SELECT Modo
              INTO cModo
              FROM USUARIO
             WHERE CodUsr = cCodUsr;
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               NULL;
         END;
         IF cModo  <> '2' THEN
            BEGIN
               SELECT RTRIM(LTRIM(MAX(ClaseComp)))
                 INTO cClaseComp
                 FROM CPTO_CONTABLE
                WHERE CodOper = '001';
            EXCEPTION
               WHEN NO_DATA_FOUND THEN
                  RAISE_APPLICATION_ERROR(-20205,PR.MENSAJE('ABD',20205,'001','CPTO_CONTABLE',NULL));
            END;
            IF NVL(cClaseComp,'XX') = 'XX' THEN
               RAISE_APPLICATION_ERROR(-20236,'No se consigue la clase de comprobante para el tipo: 001');
            END IF;
            --ERICK FABIAN ZOQUE 12/03/2013 CAMBIO E_CON_20120822_1_4
            --Se usa el tipcomp 001 de acuerdo al select anterior
            BEGIN
               SELECT FecCierre
                 INTO dFecCierre
                 FROM CORRELATIVO_COMP
                WHERE ClaseComp = RTRIM(LTRIM(cClaseComp))
                  --AND CodOfi    = RTRIM(LTRIM(PR_POLIZA.cCodOfiSusc))
                  AND tipcomp   = '001'
                  AND AnoComp   = TO_CHAR(SYSDATE,'YYYY');
            EXCEPTION
               WHEN NO_DATA_FOUND THEN
                  RAISE_APPLICATION_ERROR(-20205,PR.MENSAJE('ABD',20205,'Comprobante Contable Clase '||cClaseComp||' Oficina '||PR_POLIZA.cCodOfiSusc||' A�o '||
                                    TO_CHAR(SYSDATE,'YYYY'),'CORRELATIVO_COMP',NULL));
               WHEN TOO_MANY_ROWS THEN
                  RAISE_APPLICATION_ERROR(-20341,PR.MENSAJE('ABD',20341,'Comprobante Contable Clase '||cClaseComp||' Oficina '||PR_POLIZA.cCodOfiSusc||' A�o '||
                                    TO_CHAR(SYSDATE,'YYYY'),'CORRELATIVO_COMP',NULL));
            END;
         END IF;
         --<<BBVA Consis 25/04/2013 - Valida si obligatorio Datos de Placa, Serie , Chasis y motor
         IF cTipoMov in('D','T') THEN
                  IF PR.AUTO (nIdePol) = 'S' THEN
                     FOR X IN C_CERTIFICADO_INC_MOD LOOP
                     PR_AUTOS.VALIDA_DATOS_OBLIG (nIdepol, X.NumCert, cFlgExist, nCodError);
                     IF cFlgExist = 'S' THEN
                        RAISE_APPLICATION_ERROR (-20211, PR.MENSAJE ('ABD', nCodError, NULL, NULL, NULL));
                     END IF;
                     END LOOP;
                  END IF;
         END IF;
          -->>BBVA Consis 25/04/2013


         IF cTipoMov = 'D' THEN
            IF PR_POLIZA.cTipoSusc = 'C' THEN
         ------      FOR X IN C_CERTIFICADO_INC_MOD LOOP    consis 07/10/2014  pedro
               OPEN C_CERTIFICADO_INC_MOD;
            LOOP
               FETCH C_CERTIFICADO_INC_MOD BULK COLLECT
               INTO CERT_INC LIMIT 1000;
               EXIT WHEN CERT_INC.COUNT = 0;

               FOR i IN 1 .. CERT_INC.COUNT LOOP
                  --VALIDA TODOS LOS CERTIFICADOS
                  IF PR_CERTIFICADO.VALIDAR (nIdePol, CERT_INC(i).NumCert) = 1 THEN
                     RETURN (1);
                  END IF;
                  --<<BBVA Consis 25/04/2013 - Valida si es poliza de autos y si tiene inspeccion asociada
                  IF PR.AUTO (nIdePol) = 'S' THEN
                     IF PR_AUTOS.VALIDA_INSP_OBLIG (nIdepol, CERT_INC(i).NumCert) = 'N' THEN
                        RAISE_APPLICATION_ERROR (-20211, PR.MENSAJE ('ABD', 22065, NULL, NULL, NULL));
                     ELSE
                       RETURN (1);
                     END IF;
                  END IF;
                  -->>BBVA Consis 25/04/2013
               END LOOP;
               ------------------ consis 07/10/2014
                --COMMIT;
               END LOOP;
               CLOSE C_CERTIFICADO_INC_MOD;
               -------------------
            ----   RAISE_APPLICATION_ERROR (-20211, PR.MENSAJE ('ABD', 20211, TO_CHAR (PR_POLIZA.nNumPol), NULL, NULL));
               IF nFlgReg = 1 THEN
                  RETURN (1);
               ELSE
               RAISE_APPLICATION_ERROR (-20211, PR.MENSAJE ('ABD', 20211, TO_CHAR (PR_POLIZA.nNumPol), NULL, NULL));
               END IF;
               ------------------ consis 07/10/2014
            ELSE
               nFlgReg  := 0;
           ------    FOR X IN C_CERTIFICADO_INC_MOD LOOP       consis 07/10/2014
               OPEN C_CERTIFICADO_INC_MOD;
            LOOP
               FETCH C_CERTIFICADO_INC_MOD BULK COLLECT
               INTO CERT_INC LIMIT 1000;
               EXIT WHEN CERT_INC.COUNT = 0;

               FOR i IN 1 .. CERT_INC.COUNT LOOP
                  nFlgReg  := 1;
                  -- VALIDA TODOS LOS CERTIFICADOS
                  IF PR_CERTIFICADO.VALIDAR (nIdePol, CERT_INC(i).NumCert) = 0 THEN
                     RETURN (0);
                  END IF;
                  --<<BBVA Consis 25/04/2013 - Valida si es poliza de autos y si tiene inspeccion asociada
                  IF PR.AUTO (nIdePol) = 'S' THEN
                     IF PR_AUTOS.VALIDA_INSP_OBLIG (nIdepol, CERT_INC(i).NumCert) = 'N' THEN
                        RAISE_APPLICATION_ERROR (-20211, PR.MENSAJE ('ABD', 22065, NULL, NULL, NULL));
                     ELSE
                       RETURN (1);
                     END IF;
                  END IF;
                  -->>BBVA Consis 25/04/2013
               END LOOP;
               ---------- consis 07/10/2014
               --COMMIT;
               END LOOP;
               CLOSE C_CERTIFICADO_INC_MOD;
               ---------- consis 07/10/2014
               --
               IF nFlgReg = 1 THEN
                  RETURN (1);
               ELSE
                  RAISE_APPLICATION_ERROR (-20211, PR.MENSAJE ('ABD', 20211, TO_CHAR (PR_POLIZA.nNumPol), NULL, NULL));
               END IF;
            --
            END IF;
         ELSE
            RETURN (1);
         END IF;
         RETURN (1);
      END IF;
      RETURN (1);
   END VALIDAR;
   --//
   --
   PROCEDURE FACTURACION (nIdePol NUMBER, cTipFact VARCHAR2, dFecUltFac DATE) IS
      --
      dFecFinVig         DATE                                 := NULL;
      dFecFact           DATE                                 := NULL;
      nNumMod            NUMBER                               := NULL;
      nMeses             NUMBER (3)                           := NULL;
      --<I Defect 2343> Javier Asmat/05.05.2005
      --Se cambia el tipo de dato para las variables de Suma, Tasa y Prima para evitar error de value error
      nSumaAsegMoneda    MOD_COBERT.SumaAsegMoneda%TYPE       := NULL;
      nPrimaMoneda       MOD_COBERT.PrimaMoneda%TYPE          := NULL;
      nTasa              MOD_COBERT.Tasa%TYPE                 := NULL;
      --<F Defect 2343>
      cStsModCobert      VARCHAR2 (3)                         := NULL;
      cOrigModCobert     VARCHAR2 (1)                         := NULL;
      cStsCobert         VARCHAR2 (3)                         := NULL;
      cStsModAseg        VARCHAR2 (3)                         := NULL;
      cStsAseg           VARCHAR2 (3)                         := NULL;
      cIndTipoPro        VARCHAR2 (1)                         := NULL;
      nActivacion        NUMBER                               := NULL;
      cActivar           VARCHAR2 (1)                         := NULL;
      cIndRenPend        PRODUCTO.IndRenPend%TYPE             := NULL;
      cIndEmitirPend     RAMO_PLAN_PROD.IndEmitirPend%TYPE    := NULL;
      cDiasEmitirPend    RAMO_PLAN_PROD.DiasEmitirPend%TYPE   := NULL;
      nExisteRec         NUMBER (1)                           := 0;
      dFecRen            POLIZA.FecRen%TYPE                   := NULL;
      nIdeAseg           NUMBER (14)                          := NULL;
      cCodPlan           VARCHAR2 (3)                         := NULL;
      cRevPlan           VARCHAR2 (3)                         := NULL;
      cCodRamoCert       VARCHAR2 (4)                         := NULL;
      cCodCobert         VARCHAR2 (4)                         := NULL;
      cIndRetarifaFact   VARCHAR2 (1)                         := NULL;
      --<I Defect 1168> Javier Asmat/08.02.2005
      --Variables para determinar si la facturaci�n es vencida o no y adecuar el comportamiento para el primer recibo
      dFecIniVig         POLIZA.FecIniVig%TYPE                := NULL;
      nNumCert           CERTIFICADO.NumCert%TYPE             := NULL;
      nCantRec           NUMBER                               := NULL;
      cStsPol            POLIZA.STSPOL%TYPE                   := NULL;
      --<F Defect 1168>
      --  Modificado   Eduardo Ocando 13/07/2005
      nNumModAjuste      MOD_COBERT.NumMod%TYPE;
      -- Fin de la Modificaci�n.
      --<I Defect 3382> Javier Asmat/25.08.2005
      --Variables para atender el Defect
      nNumModMax         MOD_COBERT.NumMod%TYPE;
      cEsRever           VARCHAR2 (2);
      --<F Defect 3382>
      CURSOR COB_POL_Q IS
         SELECT NumCert, IdeCobert, MAX (NumMod) NumModCod
         FROM   MOD_COBERT
         WHERE  IdePol = nIdePol
         AND    IndCobEsp = 'N'
         AND    StsModCobert <> 'REV'
         GROUP BY NumCert, IdeCobert;
      --
      CURSOR COB_POL_ASEG_Q IS
         SELECT IdeAseg, MAX (NumMod) NumModCod
         FROM   MOD_ASEG
         WHERE  IdePol = nIdePol
         AND    StsModAseg <> 'REV'
         GROUP BY IdeAseg;
      --
      CURSOR c_CERTIFICADO IS
         SELECT NumCert
         FROM   CERTIFICADO
         WHERE  IdePol = nIdePol
         AND    StsCert <> ' REV';
      -- Re-organizaci�n del grupo familiar al momento de la facturaci�n, Pedro Ferreira, JUNIO/2004
      CURSOR c_COB_GRUPO_FAM IS
         SELECT POL.CodProd, AG.CodRamoCert, CA.CodPlan, CA.RevPlan, AG.Idepol, AG.IdeAseg, AG.NumCert, CA.CodCobert, CA.IdeCobert
         FROM   POLIZA POL, COBERT_PLAN_PROD CP, ASEGURADO AG, COBERT_ASEG CA
         WHERE  POL.IdePol = nIdePol
         AND    AG.Idepol = POL.IdePol
         AND    CA.IdeAseg = AG.IdeAseg
         AND    CP.CodProd = POL.CodProd
         AND    CP.CodPlan = CA.CodPlan
         AND    CP.RevPlan = CA.RevPlan
         AND    CP.CodRamoPlan = AG.CodRamoCert
         AND    CP.CodCobert = CA.CodCobert
         AND    CP.ParamPreCobert = 'TARFAMI';
      --
      CURSOR RECIBO_Q IS
         SELECT COUNT (F.IdeFact)
         FROM   RECIBO R, FACTURA F
         WHERE  R.IdePol = nIdePol
         AND    R.StsRec = 'ACT'
         AND    F.NumOper = R.NumOper
         AND    ((dFecRen - F.FecVencFact) - cDiasEmitirPend) >= 0;   --<Criterio para renovaci�n con cartera pendiente, tomando en cuenta el n�mero de dias m�ximo para la renovaci�n con pendientes. Rodolfo Mel�ndez <08/04/2005>
      --
      CURSOR C_ASEG IS
         SELECT IdeAseg
         FROM   ASEGURADO
         WHERE  IdePol = nIdePol
         AND    StsAseg = 'MOD';
      --Obtiene los par�metros para validar el indicador de emitir con pendientes<11/04/2005> Rodolfo Mel�ndez
      CURSOR VALIDA_PEND IS
         SELECT DISTINCT AG.CodRamoCert, CA.CodPlan, CA.RevPlan
         FROM   POLIZA POL, COBERT_PLAN_PROD CP, ASEGURADO AG, COBERT_ASEG CA
         WHERE  POL.IdePol = nIdePol
         AND    AG.Idepol = POL.IdePol
         AND    CA.IdeAseg = AG.IdeAseg
         AND    CP.CodProd = POL.CodProd
         AND    CP.CodPlan = CA.CodPlan
         AND    CP.RevPlan = CA.RevPlan
         AND    CP.CodRamoPlan = AG.CodRamoCert
         AND    CP.CodCobert = CA.CodCobert;
   --<11/04/2005>
   BEGIN
      PR_POLIZA.CARGAR (nIdepol);
      BEGIN
         SELECT FecFinVig, IndTipoPro, CodProd, FecIniVig, StsPol, NumRen   --<Defect 1168/Javier Asmat/08.02.2005> Se recupera la columna FecIniVig
         INTO   dFecFinVig, cIndTipoPro, cCodProd, dFecIniVig, cStsPol, nNumRen
         FROM   POLIZA
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            RAISE_APPLICATION_ERROR (-20220, PR.MENSAJE ('ABD', 20220, 'No se encontraron los datos de la p�liza: ' || nIdePol, NULL, NULL));
         WHEN TOO_MANY_ROWS THEN
            RAISE_APPLICATION_ERROR (-20220, PR.MENSAJE ('ABD', 20220, 'Existe Mas De Un Dato En La Poliza ' || nIdePol, NULL, NULL));
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20220, PR.MENSAJE ('ABD', 20220, 'Error al seleccionar desde tabla RECA_DCTO ' || SQLERRM, NULL, NULL));
      END;
      --Valida el indicador de Emitir con pendientes y si est� en el rango establecido de d�as.
      --<11/04/2005> Rodolfo Mel�ndez
      FOR A IN VALIDA_PEND LOOP
         BEGIN
            SELECT NVL (IndEmitirPend, 'N'), NVL (DiasEmitirPend, 0)
            INTO   cIndEmitirPend, cDiasEmitirPend
            FROM   RAMO_PLAN_PROD
            WHERE  CodProd = cCodProd
            AND    CodPlan = A.CodPlan
            AND    RevPlan = A.RevPlan
            AND    CodRamoPlan = A.CodRamoCert;
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               RAISE_APPLICATION_ERROR (-20220, PR.MENSAJE ('ABD', 20220, 'No Existe Informaci�n = ' || cIndEmitirPend, NULL, NULL));
            WHEN TOO_MANY_ROWS THEN
               RAISE_APPLICATION_ERROR (-20220, PR.MENSAJE ('ABD', 20220, 'Existe Mas De Un Dato En RAMO_PLAN_PROD ' || cCodProd, NULL, NULL));
            WHEN OTHERS THEN
               RAISE_APPLICATION_ERROR (-20220, PR.MENSAJE ('ABD', 20220, 'Error al seleccionar desde tabla RAMO_PLAN_PROD ' || SQLERRM, NULL, NULL));
         END;
         BEGIN
            OPEN RECIBO_Q;
            FETCH  RECIBO_Q
            INTO   nExisteRec;
            CLOSE RECIBO_Q;
         END;
         --
         IF     NVL (nExisteRec, 0) > 0
            AND cIndEmitirPend = 'N' THEN
            RAISE_APPLICATION_ERROR (-20100,
                                     PR.MENSAJE ('FRM', 50097, 'Existe ' || nExisteRec || ' Recibo Pendiente Vencido...' || nIdePol, NULL, NULL));
         END IF;
      END LOOP;
      --<11/04/2005>
      BEGIN
         cActivar  := 'N';
         FOR a IN (SELECT RP.ParaCalculo, RA.IdeRecaDcto
                   FROM   RECA_DCTO_CERTIF_ASEG RA, RECA_DCTO_PROD RP
                   WHERE  RA.IdeAseg IN (SELECT IdeAseg
                                         FROM   ASEGURADO
                                         WHERE  IdePol = nIdePol)
                   AND    RP.CodProd = cCodProd
                   AND    RP.TipoRecaDcto = RA.TipoRecaDcto
                   AND    RP.CodRecaDcto = RA.CodRecaDcto
                   AND    NVL (RP.IndVeriFact, 'N') = 'S') LOOP
            PR_RECA_DCTO_CERTIF_ASEG.EXCLUIR_FACTURACION (nIdePol, a.IdeRecaDcto, a.ParaCalculo, dFecUltFact, cActivar);
         END LOOP;
         --
         IF NVL (cActivar, 'N') = 'S' THEN
            nActivacion  := PR_POLIZA.ACTIVAR (nIdepol, 'D');
         END IF;
      END;
      -- Meses de c�lculo para �ltima Facturaci�n...
      nMeses  := PR.MESES_FORMA_PAGO (cTipFact);
      IF cIndTipoPro = '1' THEN
         dFecFact  := ADD_MONTHS (dFecUltFac, nMeses) - 1;
      ELSE
         dFecFact  := ADD_MONTHS (dFecUltFac, nMeses);
      END IF;
      --
      IF TRUNC (dFecFact) > TRUNC (dFecFinVig) THEN
         --<I Defect 3243> Javier Asmat/06.07.2005
         --Se considerara un margen de error de 3 dias para los casos en que algun periodo facturado tenga d�a 28 o 29 de febrero
         --lo cual origina que el ultimo periodo de facturaci�n se distorsione en esa cantidad de d�as aproximadamente
         IF (TRUNC (dFecFact) - TRUNC (dFecFinVig)) > 3 THEN
            RAISE_APPLICATION_ERROR (-20937,
                                     PR.MENSAJE ('AIF',
                                                 20937,
                                                 TO_CHAR (dFecFact) || ', Fin Poliza ' || TO_CHAR (dFecFinVig) || '-' || cIndTipoPro || '-'
                                                 || dFecUltFac,
                                                 NULL,
                                                 NULL));
         ELSE
            dFecFact  := TRUNC (dFecFinVig);
         END IF;
      --<F Defect 3243>
      END IF;
      --
      FOR A IN COB_POL_Q LOOP
         nNumMod  := A.NumModCod;
         --<I Defect 3382> Javier Asmat/25.08.2005
         --Verificamos que no hayan movimientos posteriores al m�ximo, si los hubiera y tiene estado REV, significa que borraron
         --el movimiento en estado INC que permitia activar nuevamente la p�liza. En ese caso buscamos el m�ximo valor de modificaci�n
         BEGIN
            SELECT MAX (NumMod)
            INTO   nNumModMax
            FROM   MOD_COBERT
            WHERE  IdeCobert = A.IdeCobert;
         END;
         IF nNumMod < nNumModMax THEN
            BEGIN
               SELECT 'SI'
               INTO   cEsRever
               FROM   MOD_COBERT
               WHERE  IdeCobert = A.IdeCobert
               AND    NumMod = nNumModMax
               AND    StsModCobert = 'REV';
            EXCEPTION
               WHEN NO_DATA_FOUND THEN
                  cEsRever  := 'NO';
               WHEN TOO_MANY_ROWS THEN
                  cEsRever  := 'SI';
            END;
            --
            IF cEsRever = 'SI' THEN
               nNumMod  := nNumModMax;
            END IF;
         ELSE
            nNumModMax  := NULL;
         END IF;
         --<F Defect 3382>
         BEGIN
            SELECT StsModCobert, OrigModCobert, CodRamoCert
            INTO   cStsModCobert, cOrigModCobert, cCodRamoCert
            FROM   MOD_COBERT
            WHERE  IdeCobert = A.IdeCobert
            AND    NumMod = A.NumModCod;
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               RAISE_APPLICATION_ERROR (-20220, PR.MENSAJE ('ABD', 20220, 'No Existe MOD_COBERT  IdeCobert:' || A.IdeCobert, NULL, NULL));
            WHEN TOO_MANY_ROWS THEN
               RAISE_APPLICATION_ERROR (-20220, PR.MENSAJE ('ABD', 20220, 'Existe Mas De Un Dato En MOD_COBERT ' || cCodProd, NULL, NULL));
            WHEN OTHERS THEN
               RAISE_APPLICATION_ERROR (-20220, PR.MENSAJE ('ABD', 20220, 'Error al seleccionar desde tabla MOD_COBERT ' || SQLERRM, NULL, NULL));
         END;
         --
         IF cOrigModCobert = 'A' THEN
            BEGIN
               SELECT StsCobert, IdeAseg, CodPlan, RevPlan, CodCobert
               INTO   cStsCobert, nIdeAseg, cCodPlan, cRevPlan, cCodCobert
               FROM   COBERT_ASEG
               WHERE  IdeCobert = A.IdeCobert;
            EXCEPTION
               WHEN NO_DATA_FOUND THEN
                  RAISE_APPLICATION_ERROR (-20220, PR.MENSAJE ('ABD', 20220, 'No Existe EL IdeCobert (A) = ' || A.IdeCobert, NULL, NULL));
               WHEN TOO_MANY_ROWS THEN
                  RAISE_APPLICATION_ERROR (-20220, PR.MENSAJE ('ABD', 20220, 'Existe Mas De Un Dato En COBERT_ASEG ' || cCodProd, NULL, NULL));
               WHEN OTHERS THEN
                  RAISE_APPLICATION_ERROR (-20220, PR.MENSAJE ('ABD', 20220, 'Error al seleccionar desde tabla COBERT_ASEG ' || SQLERRM, NULL, NULL));
            END;
         ELSIF cOrigModCobert = 'B' THEN
            BEGIN
               SELECT StsCobert, CodPlan, RevPlan,
                      CodCobert   --<Defect 1168/Javier Asmat/08.02.2005> Se recuperan CodPlan, RevPlan y CodCobert para evitar error de NO_DATA_FOUND
               INTO   cStsCobert, cCodPlan, cRevPlan, cCodCobert
               FROM   COBERT_BIEN
               WHERE  IdeCobert = A.IdeCobert;
            EXCEPTION
               WHEN NO_DATA_FOUND THEN
                  RAISE_APPLICATION_ERROR (-20220, PR.MENSAJE ('ABD', 20220, 'No Existe EL IdeCobert (B) = ' || A.IdeCobert, NULL, NULL));
               WHEN TOO_MANY_ROWS THEN
                  RAISE_APPLICATION_ERROR (-20220, PR.MENSAJE ('ABD', 20220, 'Existe Mas De Un Dato En COBERT_BIEN ' || cCodProd, NULL, NULL));
               WHEN OTHERS THEN
                  RAISE_APPLICATION_ERROR (-20220, PR.MENSAJE ('ABD', 20220, 'Error al seleccionar desde tabla COBERT_BIEN ' || SQLERRM, NULL, NULL));
            END;
         ELSIF cOrigModCobert = 'C' THEN
            BEGIN
               SELECT StsCobert, CodPlan, RevPlan,
                      CodCobert   --<Defect 1168/Javier Asmat/08.02.2005> Se recuperan CodPlan, RevPlan y CodCobert para evitar error de NO_DATA_FOUND
               INTO   cStsCobert, cCodPlan, cRevPlan, cCodCobert
               FROM   COBERT_CERT
               WHERE  IdeCobert = A.IdeCobert;
            EXCEPTION
               WHEN NO_DATA_FOUND THEN
                  RAISE_APPLICATION_ERROR (-20220, PR.MENSAJE ('ABD', 20220, 'No Existe EL IdeCobert (C) = ' || A.IdeCobert, NULL, NULL));
               WHEN TOO_MANY_ROWS THEN
                  RAISE_APPLICATION_ERROR (-20220, PR.MENSAJE ('ABD', 20220, 'Existe Mas De Un Dato En COBERT_CERT ' || cCodProd, NULL, NULL));
               WHEN OTHERS THEN
                  RAISE_APPLICATION_ERROR (-20220, PR.MENSAJE ('ABD', 20220, 'Error al seleccionar desde tabla COBERT_CERT ' || SQLERRM, NULL, NULL));
            END;
         END IF;
         --
         IF cStsCobert NOT IN ('EXC', 'ANU', 'CAD') THEN
            IF cStsModCobert IN ('ACT') THEN   --<Defect 1168/Javier Asmat/10.02.2005> Se elimino de la condici�n el status INC. No era necesario en este punto
               PR_MOD_COBERT.GENERAR_LIQUIDACION ('USR', A.IdeCobert, nNumMod + 1, cOrigModCobert);
               --<I Defect 1168> Javier Asmat/08.02.2005
               --Condicionamos, si es facturaci�n vencida y es el primer movimiento, la vigencia del MOD_COBERT es la misma que el inicio de la poliza
               IF     PR_POLIZA.ES_FACTURACION_VENCIDA (nIdePol)
                  AND PR_POLIZA.ES_PRIMER_MOVIMIENTO (nIdePol) THEN
                  BEGIN
                     UPDATE MOD_COBERT
                     SET FecIniValid = dFecIniVig,
                         FecFinValid = dFecIniVig
                     WHERE  IdeCobert = A.IdeCobert
                     AND    NumMod = nNumMod + 1;
                  EXCEPTION
                     WHEN OTHERS THEN
                        RAISE_APPLICATION_ERROR (-20290, PR.MENSAJE ('ABD', 20290, 'MOD_COBERT:' || SQLERRM, NULL, NULL));
                  END;
               ELSE
                  --<Defect 3243/Javier Asmat/06.07.2005> Se debe actualizar FecFinValid con dFecFact pues esta ya tiene valor validado
                  BEGIN
                     UPDATE MOD_COBERT
                     SET FecIniValid = dFecUltFac,
                         FecFinValid = dFecFact
                     WHERE  IdeCobert = A.IdeCobert
                     AND    NumMod = nNumMod + 1;
                  EXCEPTION
                     WHEN OTHERS THEN
                        RAISE_APPLICATION_ERROR (-20290, PR.MENSAJE ('ABD', 20290, 'MOD_COBERT(2):' || SQLERRM, NULL, NULL));
                  END;
                  /*
                  IF cIndTipoPro = '1' THEN
                     BEGIN
                        UPDATE MOD_COBERT
                        SET FecIniValid = dFecUltFac,
                            FecFinValid = ADD_MONTHS (dFecUltFac, nMeses) - 1
                        WHERE  IdeCobert = A.IdeCobert
                        AND    NumMod = nNumMod + 1;
                     EXCEPTION
                        WHEN OTHERS THEN
                           RAISE_APPLICATION_ERROR (-20290, PR.MENSAJE ('ABD', 20290, 'MOD_COBERT(2):' || SQLERRM, NULL, NULL));
                     END;
                  ELSE
                     BEGIN
                        UPDATE MOD_COBERT
                        SET FecIniValid = dFecUltFac,
                            FecFinValid = ADD_MONTHS (dFecUltFac, nMeses)
                        WHERE  IdeCobert = A.IdeCobert
                        AND    NumMod = nNumMod + 1;
                     EXCEPTION
                        WHEN OTHERS THEN
                           RAISE_APPLICATION_ERROR (-20290, PR.MENSAJE ('ABD', 20290, 'MOD_COBERT(3):' || SQLERRM, NULL, NULL));
                     END;
                  END IF;
               */
               END IF;
               --<F Defect 1168>
               PR_MOD_COBERT.INCLUIR (A.IdeCobert, nNumMod + 1);
               BEGIN
                  SELECT NVL (IndRetarifaFact, 'N')
                  INTO   cIndRetarifaFact
                  FROM   COBERT_PLAN_PROD
                  WHERE  CodProd = cCodProd
                  AND    CodPlan = cCodPlan
                  AND    RevPlan = cRevPlan
                  AND    CodRamoPlan = cCodRamoCert
                  AND    CodCobert = cCodCobert;
               EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                     RAISE_APPLICATION_ERROR (-20100, PR.MENSAJE ('ABD', 22052, SQLERRM, NULL, NULL));
                  WHEN TOO_MANY_ROWS THEN
                     RAISE_APPLICATION_ERROR (-20220,
                                              PR.MENSAJE ('ABD', 20220, 'Existe Mas De Un Dato En COBERT_PLAN_PROD ' || cCodProd, NULL, NULL));
                  WHEN OTHERS THEN
                     RAISE_APPLICATION_ERROR (-20220,
                                              PR.MENSAJE ('ABD', 20220, 'Error al seleccionar desde tabla COBERT_PLAN_PROD ' || SQLERRM, NULL, NULL));
               END;   --
               -- Re-Tarificaci�n de Coberturas
               IF     cOrigModCobert = 'A'
                  AND cIndRetarifaFact = 'S' THEN
                  PR_COBERT_ASEG.ACTUALIZAR_COBERTURA (nIdePol, a.NumCert, cCodPlan, cRevPlan, cCodRamoCert, a.IdeCobert, cCodCobert, NULL, nIdeAseg);
               END IF;
               --
               BEGIN
                  SELECT SumaAsegMoneda, PrimaMoneda, Tasa
                  INTO   nSumaAsegMoneda, nPrimaMoneda, nTasa
                  FROM   MOD_COBERT
                  WHERE  IdeCobert = A.IdeCobert
                  AND    NumMod = A.NumModCod;
               EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                     RAISE_APPLICATION_ERROR (-20220,
                                              PR.MENSAJE ('ABD',
                                                          20220,
                                                          'No Existe (A.IdeCobert=A.NumModCod)= ' || A.IdeCobert || ' - ' || A.NumModCod,
                                                          NULL,
                                                          NULL));
                  WHEN TOO_MANY_ROWS THEN
                     RAISE_APPLICATION_ERROR (-20220, PR.MENSAJE ('ABD', 20220, 'Existe Mas De Un Dato En MOD_COBERT ' || cCodProd, NULL, NULL));
                  WHEN OTHERS THEN
                     RAISE_APPLICATION_ERROR (-20220,
                                              PR.MENSAJE ('ABD', 20220, 'Error al seleccionar desde tabla MOD_COBERT ' || SQLERRM, NULL, NULL));
               END;
               -- Modificado por   Eduardo Ocando 13/07/2005
               nNumModAjuste  := PR_MOD_COBERT.NUMMOD_AJUSTE (A.IdeCobert, nIdePol, 'M');   -- Busca NumMod
               IF NVL (nNumModAjuste, 0) <> 0 THEN
                  nNumMod  := nNumModAjuste;
               ELSE
                  --<I Defect 3382> Javier Asmat/25.08.2005
                  -- Se asigna nuevamente el valor del cursor sino uno menor por ser el movimiento reversado
                  IF nNumModMax IS NULL THEN
                     nNumMod  := A.NumModCod;
                  ELSE
                     nNumMod  := A.NumModCod - 1;
                  END IF;
               --<F Defect 3382>
               END IF;
               -- Fin de la Modificaci�n para el Ajuste Blanket.
               --
               BEGIN
                  UPDATE MOD_COBERT
                  SET StsModCobert = 'LIQ'
                  WHERE  IdeCobert = A.IdeCobert
                  AND    NumMod = nNumMod;
               EXCEPTION
                  WHEN OTHERS THEN
                     RAISE_APPLICATION_ERROR (-20220, PR.MENSAJE ('ABD', 20220, 'Error actualizando MOD_COBERT: ' || SQLERRM, NULL, NULL));
               END;
            ELSE
               -- Modificado por   Eduardo Ocando 13/07/2005
               nNumModAjuste  := PR_MOD_COBERT.NUMMOD_AJUSTE (A.IdeCobert, nIdePol, 'M');   -- Busca NumMod donde la m es una constatante que va determinar que es lo que voy a retornar
               IF NVL (nNumModAjuste, 0) <> 0 THEN
                  nNumMod  := nNumModAjuste;
               ELSE
                  --<I Defect 3382> Javier Asmat/25.08.2005
                  -- Se asigna nuevamente el valor del cursor sino uno menor por ser el movimiento reversado
                  IF nNumModMax IS NULL THEN
                     nNumMod  := A.NumModCod;
                  ELSE
                     nNumMod  := A.NumModCod - 1;
                  END IF;
               --<F Defect 3382>
               END IF;
               -- Fin de la Modificaci�n para el Ajuste Blanket.
               IF     cStsModCobert = 'INC'
                  AND nNumMod > 0 THEN
                  BEGIN
                     UPDATE MOD_COBERT
                     SET StsModCobert = 'LIQ'
                     WHERE  IdeCobert = A.IdeCobert
                     AND    NumMod = nNumMod - 1
                     AND    StsModCobert <> 'REV';
                  EXCEPTION
                     WHEN OTHERS THEN
                        RAISE_APPLICATION_ERROR (-20220, PR.MENSAJE ('ABD', 20220, 'Error actualizando MOD_COBERT: ' || SQLERRM, NULL, NULL));
                  END;
               END IF;
            END IF;
         END IF;
      END LOOP;
      --
      FOR B IN COB_POL_ASEG_Q LOOP
         nNumMod  := B.NumModCod;
         --<I Defect 3382> Javier Asmat/25.08.2005
         --Verificamos que no hayan movimientos posteriores al m�ximo, si los hubiera y tiene estado REV, significa que borraron
         --el movimiento en estado INC que permitia activar nuevamente la p�liza. En ese caso buscamos el m�ximo valor de modificaci�n
         BEGIN
            SELECT MAX (NumMod)
            INTO   nNumModMax
            FROM   MOD_ASEG
            WHERE  Ideaseg = B.Ideaseg;
         END;
         IF nNumMod < nNumModMax THEN
            BEGIN
               SELECT 'SI'
               INTO   cEsRever
               FROM   MOD_ASEG
               WHERE  IdeAseg = B.IdeAseg
               AND    NumMod = nNumModMax
               AND    StsModAseg = 'REV';
            EXCEPTION
               WHEN NO_DATA_FOUND THEN
                  cEsRever  := 'NO';
               WHEN TOO_MANY_ROWS THEN
                  cEsRever  := 'SI';
            END;
            --
            IF cEsRever = 'SI' THEN
               nNumMod  := nNumModMax;
            END IF;
         ELSE
            nNumModMax  := NULL;
         END IF;
         --<F Defect 3382>
         BEGIN
            SELECT StsModAseg, NumCert   --<Defect 1168/Javier Asmat/08.02.2005> Recuperamos el numero de certificado
            INTO   cStsModAseg, nNumCert
            FROM   MOD_ASEG
            WHERE  IdeAseg = B.IdeAseg
            AND    NumMod = B.NumModCod;
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               RAISE_APPLICATION_ERROR (-20220,
                                        PR.MENSAJE ('ABD',
                                                    20220,
                                                    'No Existe (B.IdeAseg= B.NumModCod)=' || B.IdeAseg || ' -' || B.NumModCod,
                                                    NULL,
                                                    NULL));
            WHEN TOO_MANY_ROWS THEN
               RAISE_APPLICATION_ERROR (-20220, PR.MENSAJE ('ABD', 20220, 'Existe Mas De Un Dato En MOD_ASEG ' || cCodProd, NULL, NULL));
            WHEN OTHERS THEN
               RAISE_APPLICATION_ERROR (-20220, PR.MENSAJE ('ABD', 20220, 'Error al seleccionar desde tabla MOD_ASEG ' || SQLERRM, NULL, NULL));
         END;
         --
         BEGIN
            SELECT StsAseg
            INTO   cStsAseg
            FROM   ASEGURADO
            WHERE  IdeAseg = B.IdeAseg;
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               RAISE_APPLICATION_ERROR (-20220, PR.MENSAJE ('ABD', 20220, 'No Existe (B.IdeAseg= B.NumModCod)=' || B.IdeAseg, NULL, NULL));
            WHEN TOO_MANY_ROWS THEN
               RAISE_APPLICATION_ERROR (-20220, PR.MENSAJE ('ABD', 20220, 'Existe Mas De Un Dato En ASEGURADO ' || cCodProd, NULL, NULL));
            WHEN OTHERS THEN
               RAISE_APPLICATION_ERROR (-20220, PR.MENSAJE ('ABD', 20220, 'Error al seleccionar desde tabla ASEGURADO ' || SQLERRM, NULL, NULL));
         END;
         --
         IF cStsAseg NOT IN ('EXC', 'ANU', 'CAD') THEN
            IF cStsModAseg = 'ACT' THEN
               PR_MOD_ASEG.GENERAR_LIQUIDACION ('USR', B.IdeAseg, nNumMod);
               --<I Defect 1168> Javier Asmat/08.02.2005
               --Condicionamos, si es facturaci�n vencida y es el primer movimiento, la vigencia del MOD_COBERT es la misma que el inicio de la poliza
               IF     PR_POLIZA.ES_FACTURACION_VENCIDA (nIdePol)
                  AND PR_POLIZA.ES_PRIMER_MOVIMIENTO (nIdePol) THEN
                  BEGIN
                     UPDATE MOD_ASEG
                     SET FecIniValid = dFecIniVig,
                         FecFinValid = dFecIniVig
                     WHERE  IdeAseg = B.IdeAseg
                     AND    NumMod = nNumMod + 1;
                  EXCEPTION
                     WHEN OTHERS THEN
                        RAISE_APPLICATION_ERROR (-20290, PR.MENSAJE ('ABD', 20290, 'MOD_ASEG:' || SQLERRM, NULL, NULL));
                  END;
               ELSE
                  --<Defect 3243/Javier Asmat/06.07.2005> Se debe actualizar FecFinValid con dFecFact pues esta ya tiene valor validado
                  BEGIN
                     UPDATE MOD_ASEG
                     SET FecIniValid = dFecUltFac,
                         FecFinValid = dFecFact
                     WHERE  IdeAseg = B.IdeAseg
                     AND    NumMod = nNumMod + 1;
                  EXCEPTION
                     WHEN OTHERS THEN
                        RAISE_APPLICATION_ERROR (-20290, PR.MENSAJE ('ABD', 20290, 'MOD_ASEG(2):' || SQLERRM, NULL, NULL));
                  END;
                  /*
                  IF cIndTipoPro = '1' THEN
                     BEGIN
                        UPDATE MOD_ASEG
                        SET FecIniValid = dFecUltFac,
                            FecFinValid = ADD_MONTHS (dFecUltFac, nMeses) - 1
                        WHERE  IdeAseg = B.IdeAseg
                        AND    NumMod = nNumMod + 1;
                     EXCEPTION
                        WHEN OTHERS THEN
                           RAISE_APPLICATION_ERROR (-20290, PR.MENSAJE ('ABD', 20290, 'MOD_ASEG(2):' || SQLERRM, NULL, NULL));
                     END;
                  ELSE
                     BEGIN
                        UPDATE MOD_ASEG
                        SET FecIniValid = dFecUltFac,
                            FecFinValid = ADD_MONTHS (dFecUltFac, nMeses)
                        WHERE  IdeAseg = B.IdeAseg
                        AND    NumMod = nNumMod + 1;
                     EXCEPTION
                        WHEN OTHERS THEN
                           RAISE_APPLICATION_ERROR (-20290, PR.MENSAJE ('ABD', 20290, 'MOD_ASEG(3):' || SQLERRM, NULL, NULL));
                     END;
                  END IF;
               */
               END IF;
			   
			   PR_MOD_ASEG.INCLUIR(B.IdeAseg, nNumMod + 1);
			   
               --<F Defect 1168>
               --<I Defect 3382> Javier Asmat/25.08.2005
               -- Se asigna nuevamente el valor del cursor sino uno menor por ser el movimiento reversado
               IF nNumModMax IS NULL THEN
                  nNumMod  := B.NumModCod;
               ELSE
                  nNumMod  := B.NumModCod - 1;
               END IF;
               --<F Defect 3382>
               BEGIN
                  UPDATE MOD_ASEG
                  SET StsModAseg = 'LIQ'
                  WHERE  IdeAseg = B.IdeAseg
                  AND    NumMod = nNumMod;
               EXCEPTION
                  WHEN OTHERS THEN
                     RAISE_APPLICATION_ERROR (-20220, PR.MENSAJE ('ABD', 20220, 'Error actualizando MOD_ASEG: ' || SQLERRM, NULL, NULL));
               END;
            ELSE
               --<I Defect 3382> Javier Asmat/25.08.2005
               -- Se asigna nuevamente el valor del cursor sino uno menor por ser el movimiento reversado
               IF nNumModMax IS NULL THEN
                  nNumMod  := B.NumModCod;
               ELSE
                  nNumMod  := B.NumModCod - 1;
               END IF;
               --<F Defect 3382>
               IF     cStsModAseg = 'INC'
                  AND nNumMod > 0 THEN
                  BEGIN
                     UPDATE MOD_ASEG
                     SET StsModAseg = 'LIQ'
                     WHERE  IdeAseg = B.IdeAseg
                     AND    NumMod = nNumMod - 1
                     AND    StsModASeg <> 'REV';
                  EXCEPTION
                     WHEN OTHERS THEN
                        RAISE_APPLICATION_ERROR (-20220, PR.MENSAJE ('ABD', 20220, 'Error actualizando MOD_ASEG: ' || SQLERRM, NULL, NULL));
                  END;
               END IF;
            END IF;
         END IF;
      END LOOP;
      --
      -- Re-organizaci�n del grupo familiar al momento de la facturaci�n, Pedro Ferreira, JUNIO/2004
      FOR a IN c_COB_GRUPO_FAM LOOP
         PR_COBERT_ASEG.ACTUALIZAR_COBERTURA (a.IdePol, a.NumCert, a.CodPlan, a.RevPlan, a.CodRamoCert, a.IdeCobert, a.CodCobert, NULL, a.IdeAseg);
      END LOOP;
      -- Inclusi�n del descuento de EPS. Raiza Rodr�guez
      FOR I IN C_ASEG LOOP
         PR_RECA_DCTO_CERTIF_ASEG.FACTURACION (I.IdeAseg, dFecUltFact, dFecFinVig, cCodProd);
      END LOOP;
      --<I Defect 1168> Javier Asmat/08.02.2005
      --Condicionamos, si es facturaci�n vencida y es el primer movimiento, no actualizamos la fecha de facturaci�n
      IF     PR_POLIZA.ES_FACTURACION_VENCIDA (nIdePol)
         AND PR_POLIZA.ES_PRIMER_MOVIMIENTO (nIdePol) THEN
         NULL;
      ELSE
         BEGIN
            SELECT COUNT (*)
            INTO   nCantRec
            FROM   RECIBO
            WHERE  IdePol = nIdePol;
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               nCantRec  := 999;
            WHEN TOO_MANY_ROWS THEN
               nCantRec  := 0;
         END;
         -- Si se trata de una modificaci�n de una poliza renovada y sin recibos generados
         IF     NVL (nNumRen, 0) > 0
            AND nCantRec = 0
            AND cStsPol = 'INC' THEN
            NULL;
         ELSE
            BEGIN
               UPDATE POLIZA
               SET FecUltFact = dFecFact
               WHERE  IdePol = nIdePol;
            EXCEPTION
               WHEN OTHERS THEN
                  RAISE_APPLICATION_ERROR (-20220, PR.MENSAJE ('ABD', 20220, 'Error actualizando POLIZA: ' || SQLERRM, NULL, NULL));
            END;
         END IF;
      END IF;
     --<F Defect 1168>
   --  END IF;
   END FACTURACION;
   --//
   --RENOVAR
FUNCTION RENOVAR (nIdePol POLIZA.IdePol%TYPE, cCodPlan CERT_RAMO.CodPLAN%TYPE, cRevPlan CERT_RAMO.REVPLAN%TYPE)
      RETURN NUMBER IS
      --
      dFecRen             POLIZA.FecRen%TYPE;
      dFecIniVig          POLIZA.FecIniVig%TYPE;
      dFecFinVig          POLIZA.FecFinVig%TYPE;
      nIndRenAuto         POLIZA.IndRenAuto%TYPE;
      nIdePolRen          POLIZA.IdePol%TYPE;
      cCodFormPago        POLIZA.CodFormPago%TYPE;
      cTipoVig            POLIZA.TipoVig%TYPE;
      dFecUltFact         POLIZA.FecUltFact%TYPE;
      cIndTipoPro         POLIZA.IndTipoPro%TYPE;
      nNumRen             POLIZA.NumRen%TYPE;
      cIndRenGar          POLIZA.IndRenGar%TYPE;
      cVigProxRen         POLIZA.VigProxRen%TYPE;
         cCodPlanFrac        POLIZA.Codplanfrac%TYPE;
      --
      dFecFact            DATE;
      dFecIniVigRen       DATE;
      dFecFinVigRen       DATE;
      nMeses              NUMBER;
      nDiasRetro          NUMBER;
      cCodProd            POLIZA.CodProd%TYPE;
      nIndPolVida         NUMBER (1);
      cIPlazoEdad         CARACTERISTICA_PLAN_VIDA.IPlazoEdad%TYPE;
      nAnosPlazoEdad      CARACTERISTICA_PLAN_VIDA.AnosPlazoEdad%TYPE;
      nDuracionPlan       CARACTERISTICA_PLAN_VIDA.DuracionPlan%TYPE;
      nIdeAseg            ASEGURADO.IdeAseg%TYPE;
      nAnosVigPlan        NUMBER (3);
      nAltura             NUMBER (3);
      nNumOperAnu         OPER_POL.NumOper%TYPE;
      --
      cVTAMAS             VARCHAR2 (1);
      cCodGRPPOL          POLIZA.CodGRPPOL%TYPE;
      nExisteRec          NUMBER (3)                                    := 0;
      cIndRenPend         PRODUCTO.IndRenPend%TYPE;
      nDiasRenPend        PRODUCTO.DiasRenPend%TYPE;
      ---
      cIndRenCaduca       CARACTERISTICA_PLAN_VIDA.IndRenCaduca%TYPE;
      nVecesRenPlazo      CARACTERISTICA_PLAN_VIDA.VecesRenPlazo%TYPE;
      dFecIniPol          POLIZA.FecIniPol%TYPE;
      nVenRenPlazo        NUMBER (6, 3)                                 := 0;
      --
      nNumPol             POLIZA.NumPol%TYPE;   --<Cotizacion/Javier Asmat/21.01.2005> Variable solo para mostrar la cotizaci�n que se esta tratando de renovar
      --
      cValidaDed          VARCHAR2 (1);
      cIndRen             VARCHAR2 (1);
      cCodPlanRamo        CERT_RAMO.CodPlan%TYPE;
      cRevPlanRamo        CERT_RAMO.RevPlan%TYPE;
      cCodRamoCert        CERT_RAMO.CodRamoCert%TYPE;
      cCodCli             POLIZA.CodCli%TYPE;
      cIndEdadActuarial   RAMO_PLAN_PROD.IndEdadActuarial%TYPE;
      cIndBaseCalcEdad    RAMO_PLAN_PROD.IndBaseCalcEdad%TYPE;
      cIndEdadCroExacta   RAMO_PLAN_PROD.IndEdadCroExacta%TYPE;
      nNumCert            CERTIFICADO.NumCert%TYPE;
      dFecNac             CLIENTE.FecNac%TYPE;
      nEdad               NUMBER (3);
      nEdadEgre           NUMBER (3);
      cVencioPol          VARCHAR2 (1)                                  := 'N';
      cStsAsegANU         ASEGURADO.StsAseg%TYPE;
         nCount              NUMBER(1);
      CURSOR RECIBO_Q IS
         SELECT COUNT (F.IdeFact)
         FROM   RECIBO R, FACTURA F
         WHERE  R.IdePol = nIdePol
         AND    R.StsRec = 'ACT'
         AND    F.NumOper = R.NumOper
         AND    ((dFecRen - F.FecVencFact) - nDiasRenPend) >= 0;   --<Criterio para renovaci�n con cartera pendiente, tomando en cuenta el n�mero de dias m�ximo para la renovaci�n con pendientes. Rodolfo Mel�ndez <08/04/2005>
      CURSOR VALIDA_EDAD IS
         SELECT CodCobert
         FROM   COBERT_PLAN_PROD
         WHERE  CodProd = cCodProd
         AND    CodPlan = cCodPlanRamo
         AND    RevPlan = cRevPlanRamo
         AND    CodRamoPlan = cCodRamoCert
         AND    IndCobertOblig = 'S';
      --
      rFilaPoliza         POLIZA%ROWTYPE;   --<Defect 3349/Javier Asmat/14.07.2005> Variable fila para recuperar todos los datos de la poliza a ser renovada
      rControlPoliza      CONTROL_POLIZA%ROWTYPE; --<<BBVA 22/03/2017 #21556 4sight-EFBC Variable record para recuperar el estado actual de CONTROL_POLIZA
   BEGIN
      VALIDAR_MOVIMIENTO (nIdePol); --COTECSA-MX #23183--
      --Erick Zoque 20/05/2013 E_CON_20120821_1_7
      PR_FECHAS_EQUIVALENTES.CEVENTO := '001';
      --<I Cotizacion> Javier Asmat/21.01.2005
      --Validamos si es que la poliza es una cotizaci�n o no. Si no lo es, se procede con la renovaci�n, en caso contrario se muestra error
      IF PR_COTIZACION.COTIZACION (nIdePol, 'C') = 'N' THEN
         BEGIN
            SELECT FecRen, FecIniVig, FecFinVig, NVL (IndRenAuto, 'N'), TipoVig, CodFormPago, FecUltFact, IndTipoPro, NumRen + 1,
                   NVL (IndRenGar, 'N'), VigProxRen, CodProd, FecIniPol, CodCli,CodPlanFrac
            INTO   dFecRen, dFecIniVig, dFecFinVig, nIndRenAuto, cTipoVig, cCodFormPago, dFecUltFact, cIndTipoPro, nNumRen, cIndRengar, cVigProxRen,
                   cCodProd, dFecIniPol, cCodCli,cCodPlanFrac
            FROM   POLIZA
            WHERE  IdePol = nIdePol;
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               RAISE_APPLICATION_ERROR (-20220,
                                        PR.MENSAJE ('ABD', 20220, 'No se encontro datos de Poliza con identIFicador ' || nIdePol, NULL, NULL));
            WHEN TOO_MANY_ROWS THEN
               RAISE_APPLICATION_ERROR (-20220,
                                        PR.MENSAJE ('ABD',
                                                    20220,
                                                    'Datos de poliza se encuentran duplicados para identIFicador ' || nIdePol,
                                                    NULL,
                                                    NULL));
            WHEN OTHERS THEN
               RAISE_APPLICATION_ERROR (-20220, PR.MENSAJE ('ABD', 20220, 'Error al Selecionar datos de Poliza. ' || SQLERRM, NULL, NULL));
         END;
         BEGIN
            SELECT FecNac
            INTO   dFecNac
            FROM   CLIENTE
            WHERE  CodCli = cCodCli;
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               RAISE_APPLICATION_ERROR (-20100, 'RENOVAR No se ha encontrado el cliente ' || cCodCli);
            WHEN TOO_MANY_ROWS THEN
               RAISE_APPLICATION_ERROR (-20100, 'RENOVAR Error datos de Cliente se encuentran duplicados ' || cCodCli);
            WHEN OTHERS THEN
               RAISE_APPLICATION_ERROR (-20100, 'RENOVAR Error al seleccionar desde tabla Cliente ' || SQLERRM);
         END;
         --
         -- Valida la Fecha de Renovacion con los dias de Retoractividad
         BEGIN
            SELECT DiasPreEmision, NVL (IndRenPend, 'N'), NVL (DiasRenPend, 0)
            INTO   nDiasRetro, cIndRenPend, nDiasRenPend
            FROM   PRODUCTO
            WHERE  CodProd = cCodProd;
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               RAISE_APPLICATION_ERROR (-20100, PR.MENSAJE ('FRM', 50097, 'No Existe DiasPreEmision = ' || nDiasRetro, NULL, NULL));
         END;
         ---->>BBVA Consis 03/09/2014 /// M.B.  Ya se controla a trav�s de un nivel de autoridad. Bloqueos por autorizaci�n (0014)
      /*IF     (TRUNC (SYSDATE) - dFecRen) > nDiasRetro
            AND nDiasRetro != 999 THEN
            RAISE_APPLICATION_ERROR (-20100, PR.MENSAJE ('FRM', 50097, 'Excede los dias de Retroactividad ... ' || nIdePol, NULL, NULL));
         END IF;*/

         --
         --<I ESVI081> Javier Asmat/07.12.2004
         --Preguntamos por el indicador de Renovaci�n Masiva, si esta encendido, esta validaci�n ya fue realizada,
         --en caso contrario, si debe realizarse
        -- FFVV
        /*IF NVL (PR_POLIZA.cIndRenovMasiva, 'N') = 'N' THEN
            OPEN RECIBO_Q;
            FETCH  RECIBO_Q
            INTO   nExisteRec;
            CLOSE RECIBO_Q;
            --
            IF     nExisteRec > 0
               AND cIndRenPend = 'N' THEN
               cIndRen  := 'N';
            ELSE
               cIndRen  := 'S';
            END IF;
            IF cIndRen = 'N' THEN
               IF nExisteRec = 1 THEN
                  RAISE_APPLICATION_ERROR (-20100,
                                           PR.MENSAJE ('FRM', 50097, 'Existe ' || nExisteRec || ' Recibo Pendiente Vencido...' || nIdePol, NULL, NULL));
               ELSE
                  RAISE_APPLICATION_ERROR (-20100,
                                           PR.MENSAJE ('FRM',
                                                       50097,
                                                       'Existen ' || nExisteRec || ' Recibos Pendientes Vencidos...' || nIdePol,
                                                       NULL,
                                                       NULL));
               END IF;
            END IF;
         END IF;*/
         --<F ESVI081>
         --
         --<<BBVA Consis  EFVC  29/07/2015 - Validacion para edad de egreso en renovaciones polizas si el titular supera la edad que no haga la renovacion
         PR_ASEGURADO.VALIDA_EDAD_EGRE(nIdePol);

         BEGIN
            SELECT 1, IdeAseg, IPlazoEdad, AnosPlazoEdad, DuracionPlan, NVL (CPV.IndRenCaduca, 'S'), NVL (CPV.VecesRenPlazo, 99), CR.CodPlan,
                   CR.RevPlan, CR.CodRamoCert, CR.NumCert
            INTO   nIndPolVida, nIdeAseg, cIPlazoEdad, nAnosPlazoEdad, nDuracionPlan, cIndRenCaduca, nVecesRenPlazo, cCodPlanRamo, cRevPlanRamo,
                   cCodRamoCert, nNumCert
            FROM   POLIZA P, CERT_RAMO CR, ASEGURADO A, CARACTERISTICA_PLAN_VIDA CPV
            WHERE  P.IdePol = nIdePol
            AND    P.TipoSusc = 'I'
            AND    CR.IdePol = P.IdePol
            AND    A.IdePol = CR.IdePol
            AND    A.NumCert = CR.NumCert
            AND    A.CodPlan = CR.CodPlan
            AND    A.RevPlan = CR.RevPlan
            AND    A.CodRamoCert = CR.CodRamoCert
            AND    CPV.CodProd = P.CodProd
            AND    CPV.CodPlan = CR.CodPlan
            AND    CPV.RevPlan = CR.RevPlan
            AND    CPV.CodRamo = CR.CodRamoCert
            AND    CPV.DuracionPlan > 1;
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               nIndPolVida     := 0;
               nAnosVigPlan    := 99;
               cIndRenCaduca   := 'S';
               nVecesRenPlazo  := 99;
            WHEN TOO_MANY_ROWS THEN
               nIndPolVida     := 0;
               nAnosVigPlan    := 99;
               cIndRenCaduca   := 'S';
               nVecesRenPlazo  := 99;
         END;
         IF nIndPolVida = 1 THEN
            PR.TIPOEDAD (cCodProd, cCodPlanRamo, cRevPlanRamo, cCodRamoCert, cIndEdadActuarial, cIndBaseCalcEdad, cIndEdadCroExacta);
            IF cIndBaseCalcEdad = 'I' THEN
               nEdad  := PR.CALC_EDAD (dFecRen, dFecNac, cIndEdadActuarial, cIndEdadCroExacta);
            ELSE
               nEdad  := PR_VIDA.CALC_EDAD_VIDA (nIdeAseg);
            END IF;
            FOR X IN VALIDA_EDAD LOOP
               BEGIN
                  SELECT PV.EdadEgre
                  INTO   nEdadEgre
                  FROM   POLIZA P, ASEGURADO A, PARENT_VAL_PLAN_PROD PV
                  WHERE  P.IdePol = nIdePol
                  AND    A.IdeASeg = nIdeAseg
                  AND    PV.CodProd = P.CodProd
                  AND    PV.CodPlan = cCodPlanRamo
                  AND    PV.RevPlan = cRevPlanRamo
                  AND    PV.CodRamoPlan = cCodRamoCert
                  AND    PV.CodCobert = X.CodCobert
                  AND    PV.CodParent = A.CodParent;
               EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                     nEdadEgre  := NULL;
               END;
               BEGIN
                  cVencioPol  := 'N';
                  IF nEdad >= nEdadEgre THEN
                     PR_POLIZA.VENCE_POLIZA (nIdePol, dFecFinVig, '0011', 'POR EDAD DE EGRESO DEL ASEGURADO DE LA POLIZA');
                     cVencioPol  := 'S';
                  END IF;
               EXCEPTION
                  WHEN OTHERS THEN
                     RAISE_APPLICATION_ERROR (-20100, PR.MENSAJE ('ABD', 20052, SQLERRM, NULL, NULL));
               END;
            END LOOP;
            IF cIPlazoEdad = 'E' THEN
               nAnosVigPlan  := (nAnosPlazoEdad - PR_VIDA.CALC_EDAD_VIDA (nIdeAseg));
            ELSE
               nAnosVigPlan  := nAnosPlazoEdad;
            END IF;
            nAltura       := PR.ALTURA_POLIZA (nIdePol);
            -- Realizado     Eduardo Ocando
            -- Fecha         03/12/2004
            -- Descripci�n   Especificaci�n ESVI060, N�mero de veces que una poliza a plazos
            -- puede ser renovada.
            nVenRenPlazo  := ((MONTHS_BETWEEN (dFecRen, dFecIniPol) / 12) / nAnosVigPlan);
            IF nVenRenPlazo = nVecesRenPlazo THEN
               nAltura        := 999;
               cIndRenCaduca  := 'N';
            ELSE
               IF nAltura = nAnosVigPlan THEN
                  nNumRen  := 0;
               END IF;
            END IF;
         END IF;

             -- SE VALIDA SI LA POLIZA ES DE ACSELWEB
             -- SE CAMBIA EL VALOR DE cTipoVig de H a A
             -- ESTO PARA CUMPLIR CON EL REQUERIMIENTO
             -- DONDE LA VIGENCIA ES DE 10 MESES
             -- Y LA RENOVACION QUEDE A 1 ANO
             BEGIN

                SELECT COUNT(*)
                INTO   nCount
                FROM   Poliza
                WHERE  Idepol = nIdePol
                AND    Numcotiflex IS NOT NULL
                AND    TipoVig = 'H';

             IF  nCount = 1 THEN

                cTipoVig     := PR.BUSCA_LVAL('HURTOWEB','03');

                cIndTipoPro  := PR.BUSCA_LVAL('HURTOWEB','02');

                cCodPlanFrac := PR.BUSCA_LVAL('HURTOWEB','04');

             END IF;

             END;


         -- Realizado por Eduardo Ocando
         -- Fecha         31/11/2004
         -- descripci�n   ESVI048, indica si la renovaci�n va ser autom�tica para los temporales
         -- se anexo la validaci�n si Caduca o No cuando la Altura es mayor a la vigencia de la
         -- poliza. el Indicador si es N caduca la p�liza.
         IF     nAltura >= nAnosVigPlan
            AND cIndRenCaduca = 'N' THEN
            PR_POLIZA.VENCE_POLIZA (nIdePol, dFecFinVig, '0011', 'POR TERMINACION DEL PLAN DE LA POLIZA');
         ELSIF cVencioPol = 'N' THEN
            IF nVenRenPlazo <= nVecesRenPlazo THEN
               BEGIN
                  SELECT SQ_IdePol.NEXTVAL
                  INTO   nIdePolRen
                  FROM   DUAL;
               END;
               dFecIniVigRen             := dFecRen;
               nMeses                    := PR.MESES_FORMA_PAGO (cTipoVig);
               IF cVigProxRen = 'S' THEN
                  nMeses    := 6;
                  cTipoVig  := 'S';
               END IF;
               -- 08/09/2005 Fernando Franklin. Si es autos la nueva vigencia siempre sera anual
               IF PR.AUTO (nIdePol) = 'S' THEN
                  nMeses    := 12;
                  cTipoVig  := 'A';
               END IF;
               IF nMeses IS NOT NULL THEN
                  dFecFinVigRen  := ADD_MONTHS (TRUNC (dFecIniVigRen), nMeses);
               ELSE
                  dFecFinVigRen  := (TRUNC (dFecIniVigRen) + (TRUNC (dFecFinVig) - TRUNC (dFecIniVig)));
               END IF;
               -- INSERTAR --
               --<I Defect 3349> Javier Asmat/14.07.2005
               --Cambiamos la forma de insertar la renovaci�n para que cuando se agreguen nuevas columnas estas tambien sean renovadas
               BEGIN
                  SELECT *
                  INTO   rFilaPoliza
                  FROM   POLIZA
                  WHERE  IdePol = nIdePol;
               EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                     RAISE_APPLICATION_ERROR (-20052, PR.MENSAJE ('ABD', 20052, 'No existe p�liza a renovar con Idepol ' || nIdePol, NULL, NULL));
               END;
               --Asignamos valores fijos o calculados previamente antes de insertar la nueva poliza
               rFilaPoliza.IdePol        := nIdePolRen;
               rFilaPoliza.NumRen        := nNumRen;
               rFilaPoliza.StsPol        := 'INC';
               rFilaPoliza.FecRen        := dFecFinVigRen;
               rFilaPoliza.FecIniVig     := dFecIniVigRen;
               rFilaPoliza.FecFinVig     := dFecFinVigRen;
               rFilaPoliza.TipoVig       := cTipoVig;
               rFilaPoliza.FecUltFact    := dFecIniVigRen;
               rFilaPoliza.IndMovPolRen  := 'N';
                      rFilaPoliza.Indtipopro    := cIndTipoPro;
                      rFilaPoliza.Codplanfrac   := cCodPlanFrac;
               --<I Defect 3690> Javier Asmat/25.08.2005
               --Si la forma de pago no es anual ni especial, entonces el IndFact debe ser 'S' y adelantamos la fecha de ultima facturaci�n si es anticipada
               IF PR.BUSCA_LVAL ('CANTREC', rFilaPoliza.CodFormPago) NOT IN ('INVALIDO', '1') THEN
                  rFilaPoliza.IndFact  := 'S';
                  IF rFilaPoliza.IndPolAntVenc = 'A' THEN
                     rFilaPoliza.FecUltFact  := ADD_MONTHS (rFilaPoliza.FecUltFact, PR.MESES_FORMA_PAGO (rFilaPoliza.CodFormPago));
                  END IF;
               END IF;
               --<F Defect 3690>
               --Insertamos la nueva poliza
               BEGIN
                  INSERT INTO POLIZA
                       VALUES rFilaPoliza;
               EXCEPTION
                  WHEN OTHERS THEN
                     RAISE_APPLICATION_ERROR (-20289, PR.MENSAJE ('ABD', 20289, ' POLIZA:' || SQLERRM, NULL, NULL));
               END;
               --<F Defect 3349>
               /*
               BEGIN
                  INSERT INTO POLIZA
                              (IDEPOL, CODPOL, NUMPOL, CODCLI, TIPOCOTPOL, FECOPER, OBSERVA, NUMREN, INDNUMPOL, CODPROD, STSPOL, FECREN, FECINIVIG,
                               FECFINVIG, TIPOVIG, TIPOPDCION, TIPOFACT, INDCOA, CODFORMFCION, CODOFIEMI, CODOFISUSC, CODMONEDA, INDMULTIINTER,
                               INDPOLADHESION, INDRENAUTO, FECANUL, CODMOTVANUL, TEXTMOTVANUL, TIPOSUSC, CODFORMPAGO, TIPOANUL, FECULTFACT,
                               CODPLANFRAC, MODPLANFRAC, INDCOBPROV, INDTIPOPRO, CLASEPOL, PORCCTP, INDFACT, INDMOVPOLREN, INDRESPPAGO, FECINIPOL,
                               IDECOT, INDRECUPREA, TIPOCOBRO, INDRENGAR, VIGPROXREN, NUMSOLIC, INDCANCELA, INDIMPFACT, CODGRPPOL, EXPDIVS,
                               INDDEVCONSIN, INDRENOVLOTE, NUMVECESIMP, CODFORMCAN, FECINIVIG1ERANO, CODCIA, FECRESOLUCION, TIPTRM, FECTRMCONVENIDA,
                               INDFACTG, INDDEUDORES, INDAJUSTEBLANKET, INDCONTRIBUTIVA, INDPOLANTVENC, NumPolMig)
                     SELECT nIdePolRen, CodPOL, NumPol, CodCli, TipoCOTPOL, FecOper, Observa, nNumRen, IndNumPol, CodPROD, 'INC', dFecFinVigRen,
                            dFecIniVigRen, dFecFinVigRen, cTipoVig, TipoPdcion, TipoFact, IndCoa, CodFormfCion, CodOfiemi, CodOfiSusc, CodMoneda,
                            IndMultiInter, IndPolAdhesion, IndRenAuto, FecAnul, CodMotvAnul, TextMotvAnul, TipoSusc, CodFormPago, TipoAnul,
                            dFecIniVigRen, CodPlanFrac, ModPlanFrac, IndCobProv, IndTipoPRO, ClasePol, Porcctp, IndFact, 'N', IndRespPago, FecIniPol,
                            IdeCot, IndRecupRea, TipoCobro, IndRenGar, VigProxRen, NumSolic, IndCancela, IndImpFact, CodGrpPol, ExpDivs,
                            IndDevConSin, IndRenovLote, NumVecesImp, CodFormCan, FecIniVig1erAno, CodCia, FecResolucion, TipTRM, FecTRMConvenida,
                            INDFACTG, INDDEUDORES, INDAJUSTEBLANKET, INDCONTRIBUTIVA, INDPOLANTVENC, NumPolMig
                     FROM   POLIZA
                     WHERE  IdePol = nIdePol;
               EXCEPTION
                  WHEN OTHERS THEN
                     RAISE_APPLICATION_ERROR (-20289, PR.MENSAJE ('ABD', 20289, ' POLIZA:' || SQLERRM, NULL, NULL));
               END;
               */
               --<<BBVA Consis 19/07/2013 - Se Agrga Funcionalidad para Copiar el factorg de la poliza que se esta Renovando
               -- Renovacion Factor-G
               PR_FACTORG.RENOVAR(nIdePol, nIdePolRen);
               -->>BBVA Consis 19/07/2013

               -- Renovacion de deducibles
               cValidaDed                := PR.BUSCA_PARAMETRO ('124');
               IF cValidaDed = 'S' THEN
                  PR_DEDUCIBLE_POLIZA.RENOVAR_NEW (nIdePol, nIdePolRen, dFecIniVigRen);
               ELSE
                  PR_DEDUCIBLE_POLIZA.RENOVAR (nIdePol, nIdePolRen, dFecIniVigRen);
               END IF;
               --
               --<< BBVA 26/01/2018 TECNOCOM INC 23070 - Se reliza proceso de tarificacion antes de generacion de coberturas de asegurado
               PR_TARIFA_PERSONA.RENOVAR(nIdePol, nIdePolRen);
               -->> BBVA 26/01/2018 TECNOCOM INC 23070
               --
               -- Renovar datos particulares de SALUD
               --PR_DP_POL_SALUD.RENOVAR(nIdePol,nIdePolRen);
               --
               -- Realizado         Eduardo Ocando
               -- Fecha             30/01/2005
               -- Descripci�n       Renovaci�n de los datos particulares
               -- de vida grupo.
               PR_DATOS_PART_VIDAG.RENOVAR (nIdePol, nIdePolRen);
               --
               PR_DATOS_PART_POLIZA.RENOVAR (nIdePol, nIdePolRen);
               IF rFilaPoliza.CODGRPPOL IS NULL THEN
                  PR_PART_INTER_POL.RENOVAR (nIdePol, nIdePolRen);
               END IF;
               --PR_CANAL_POLIZA.RENOVAR(nIdePol, nIdePolRen );
               PR_POLIZA.RENOVAR_CERTIFICADO (nIdePol, nIdePolRen, cCodPlan, cRevPlan, dFecIniVigRen);
               PR_POLIZA_CLIENTE.RENOVAR (nIdePol, nIdePolRen);
               PR_DIST_COA.RENOVAR (nIdePol, nIdePolRen, dFecIniVigRen, dFecFinVigRen);
               PR_RECA_DCTO_POL.RENOVAR (nIdePol, nIdePolRen, dFecIniVigRen, dFecFinVigRen);
               PR_ANEXO_POL.RENOVAR (nIdePol, nIdePolRen, dFecIniVigRen, dFecFinVigRen);
               --<<BBVA EFVC Consis 31/07/2014 - Se hace llamdo a proceso de cre clauslas en la renovaciond e poliza
               PR_CLAU_POL.RENOVAR(nIdePol, nIdePolRen, dFecIniVigRen, dFecFinVigRen);
               --<<BBVA EFVC Consis 31/07/2014
               PR_POLIZA_INSPECCION.RENOVAR (nIdePol, nIdePolRen);
               --PR_TARIFA_PERSONA.RENOVAR(nIdePol, nIdePolRen);
               PR_POLIZA.GENERAR_TRANSACCION (nIdePol, 'RENOV');

               --<<BBVA 22/03/2017 #21556 4Sight-EFBC Se actualiza el Indrecauonline a N en la renovacion
               --para habilitar la poliza para el envio a cobro
               BEGIN
                 SELECT *
                   INTO rControlPoliza
                   FROM CONTROL_POLIZA CP
                  WHERE CP.Idecotiza = rFilaPoliza.NumCotiFlex;
               EXCEPTION
                 WHEN NO_DATA_FOUND THEN
                   rControlPoliza.Idecotiza := NULL;
                 WHEN TOO_MANY_ROWS THEN
                   rControlPoliza.Idecotiza := rFilaPoliza.NumCotiFlex;
                 WHEN OTHERS THEN
                   rControlPoliza.Idecotiza := NULL;
               END;
               IF rControlPoliza.Idecotiza IS NOT NULL THEN
                 rControlPoliza.Indrecauonline := 'N';
                 ACTUALIZA_CONTROL_POLIZA(rControlPoliza);
               END IF;
               -->>BBVA 22/03/2017 #21556 4Sight-EFBC

            ELSE
               -- Se envia un mensaje de si es manual la renovaci�n si es autom�tica si
               -- no se envia nada.
               IF     cCodPlan IS NULL
                  AND cRevPlan IS NULL THEN
                  RAISE_APPLICATION_ERROR
                                   (-20100,
                                    PR.MENSAJE ('AIF',
                                                22052,
                                                ' Ha excedido el L�mite de veces que una p�liza puede ser renovada, Revise... Nro. Renovaciones '
                                                || TO_CHAR (nVenRenPlazo),
                                                NULL,
                                                NULL));
               END IF;
            END IF;
         END IF;
         BEGIN
            SELECT 'S', CodGrpPol
            INTO   cVtaMas, cCodGrpPol
            FROM   POLIZAS_VENTA_MASIVA
            WHERE  IdePol = nIdePol;
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               cVTAMAS  := 'N';
         END;
         IF cVTAMAS = 'S' THEN
            INSERT INTO POLIZAS_VENTA_MASIVA
                 VALUES (nIdePolREN, cCodGrpPol);
         END IF;
         --<<BBVA Consis 23/07/2013 - Proceso que genera los push para revertir incluison de la renovacion a VAL
         PR_B$_POLIZA.PUSH (nIdePolRen, 'INCLU', 'USR');
         FOR X IN (SELECT IDEPOL,NUMCERT FROM CERTIFICADO
                    WHERE STSCERT='INC' AND IDEPOL=nIdePolRen)LOOP
                    PR_B$_CERTIFICADO.PUSH (nIdePolRen, X.NUMCERT, 'INCLU', 'USR');
                    FOR Y IN (SELECT CODPLAN,REVPLAN,CODRAMOCERT
                             FROM CERT_RAMO
                             WHERE IDEPOL = X.IDEPOL AND NUMCERT = X.NUMCERT
                             AND STSCERTRAMO='INC') LOOP
                              PR_B$_CERT_RAMO.PUSH(X.IdePol,X.NumCert,Y.CodPlan,Y.RevPlan,Y.CodRamoCert,'INCLU','USR');
                    END LOOP;

         END LOOP;
         --Revierte el estado INC en que queda la poliza despues de la RENOVACION  a VAL
         PR_POLIZA.REVERTIR(nIdePolRen);
         -->>BBVA Consis 23/07/2013
         --DV. 10/04/2014
         BEGIN
           SELECT DISTINCT StsAseg
           INTO   cStsAsegANU
           FROM   ASEGURADO
           WHERE  IdePol  = nIdePolRen;
         EXCEPTION
           WHEN NO_DATA_FOUND OR TOO_MANY_ROWS THEN
              cStsAsegANU := NULL;
         END;
         IF cStsAsegANU = 'ANU' THEN
            UPDATE POLIZA
            SET    StsPol    = 'ANU',
                   FecAnul     = dFecIniVigRen,
                   CodMotvAnul = '0131'
            WHERE  IdePol = nIdePolRen;
            UPDATE CERTIFICADO
            SET    StsCert    = 'ANU',
                   FecExc     = dFecIniVigRen,
                   CodMotvExc = '0131'
            WHERE  IdePol = nIdePolRen;
            UPDATE CERT_RAMO
            SET    StsCertRamo= 'ANU',
                   FecExc     = dFecIniVigRen,
                   CodMotvExc = '0131'
            WHERE  IdePol = nIdePolRen;
         END IF;

         RETURN (nIdePolRen);
      ELSE
         BEGIN
            SELECT CodProd, NumPol
            INTO   cCodProd, nNumPol
            FROM   POLIZA
            WHERE  IdePol = nIdePol;
         END;
         RAISE_APPLICATION_ERROR (-20052,
                                  PR.mensaje ('ABD',
                                              20052,
                                              'La p�liza ' || cCodProd || '-' || nNumPol || ' es una cotizaci�n y no puede ser renovada',
                                              NULL,
                                              NULL));
      END IF;
      --Erick Zoque 20/05/2013 E_CON_20120821_1_7
     PR_FECHAS_EQUIVALENTES.CEVENTO := NULL;
   --<F Cotizacion>
   END RENOVAR;
   --///
   -- RENOVAR_CERTIFICADO
   PROCEDURE RENOVAR_CERTIFICADO (
      nIdePol        POLIZA.IdePol%TYPE,
      nIdePolRen     POLIZA.IdePol%TYPE,
      cCodPlan       CERT_RAMO.CodPLAN%TYPE,
      cRevPlan       CERT_RAMO.REVPLAN%TYPE,
      dFecIniRen     DATE) IS
      --
      CURSOR CERTIFICADOS_C IS
         SELECT NumCert
         FROM   CERTIFICADO
         WHERE  IdePol = nIdePol
         AND    StsCert = 'ACT';
      --
      cTipoId       VARCHAR2 (3);
      nNumId        NUMBER (15);
      cDvId         VARCHAR2 (2);
      cCodEps       VARCHAR2 (6);
      cRenovar      VARCHAR2 (1);
      cCodCli       VARCHAR2 (20);
      nContCert     NUMBER (5)    := 0;
      nContSinEPS   NUMBER (5)    := 0;
      --
      rControlPoliza      CONTROL_POLIZA%ROWTYPE; --<<BBVA 31/03/2017 #21556 4sight-EFBC Variable record para recuperar el estado actual de CONTROL_POLIZA
      nNumCotiFlex        POLIZA.Numcotiflex%TYPE; --<<BBVA 04/04/2017 #21556 4sight-EFBC Variable record para recuperar el NumCotiFlex de POLIZA
   --
   --<< OJRG>> 23/06/2017  Se crea variable para capturar resultado de PR.SALUD y eliminar multiples concurrencias a este procedimiento. cc 22292

      v_pr_salud VARCHAR2(10);

   BEGIN
      PR_OPERACION.INICIAR ('RENOV', 'POLIZA', TO_CHAR (nIdePol) || '-' || TO_CHAR (nIdePolRen));

   --<<OJRG> 23/06/2017 Inclusion de la nueva variable cc 22292.

      v_pr_salud := NVL (PR.SALUD (nIdePol), 'N');

      FOR CP IN CERTIFICADOS_C LOOP
         nContCert  := nContCert + 1;
         --
         -- Validaci�n de Asegurado inscrito en EPS.
         -- JVS.
         cRenovar   := 'S';
         --
         IF v_pr_salud = 'S' THEN
            BEGIN
               SELECT CodCli
               INTO   cCodCli
               FROM   ASEGURADO
               WHERE  IdePol = nIdePol
               AND    NumCert = CP.NumCert
               AND    NVL (IndAsegTitular, 'N') = 'S';
            EXCEPTION
               WHEN NO_DATA_FOUND THEN
                  cCodCli  := NULL;
               WHEN TOO_MANY_ROWS THEN
                  RAISE_APPLICATION_ERROR (-20100, 'Asegurado titular duplicado para el certificado: ' || TO_CHAR (CP.NumCert) || '. ' || SQLERRM);
               WHEN OTHERS THEN
                  RAISE_APPLICATION_ERROR (-20100, 'Error leyendo la tabla ASEGURADO. ' || SQLERRM);
            END;
            --
            IF cCodCli IS NOT NULL THEN
               BEGIN
                  SELECT TipoId, NumId, DvId
                  INTO   cTipoId, nNumId, cDvId
                  FROM   CLIENTE
                  WHERE  CodCli = cCodCli;
               EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                     RAISE_APPLICATION_ERROR (-20100, 'Cliente Inexistente' || SQLERRM);
                  WHEN TOO_MANY_ROWS THEN
                     RAISE_APPLICATION_ERROR (-20100, 'Cliente Duplicado' || SQLERRM);
                  WHEN OTHERS THEN
                     RAISE_APPLICATION_ERROR (-20100, 'Error leyendo la tabla CLIENTE. ' || SQLERRM);
               END;
               --
               IF     cTipoId IS NOT NULL
                  AND nNumId IS NOT NULL
                  AND cDvId IS NOT NULL THEN
                  cCodEps  := PR_EPS_TERCERO.VALIDA_EPS_TERCERO (cTipoId, nNumId, cDvId, dFecIniRen);
                  IF cCodEps IS NULL THEN
                     IF PR_POLIZA.cTipoSusc = 'C' THEN
                        nContSinEPS  := nContsinEPS + 1;
                        cRenovar     := 'N';
                     ELSIF PR_POLIZA.cTipoSusc = 'I' THEN
                        RAISE_APPLICATION_ERROR (-20100,
                                                 'Asegurado Titular del certificado ' || TO_CHAR (CP.NumCert) || ' no inscrito o no vigente en EPS '
                                                 || SQLERRM);
                     END IF;
                  END IF;
               END IF;
            END IF;
         END IF;
         --
         IF cRenovar = 'S' THEN
            PR_CERTIFICADO.RENOVAR (nIdePol, CP.NumCert, nIdePolRen, cCodPlan, cRevPlan);
         END IF;
      END LOOP;
--      IF NVL (PR.SALUD (nIdePol), 'N') = S THEN

      IF v_pr_salud = 'S' THEN  --<<OJRG>> 23/06/2016, Se utiliza nuevamente la variable que reemplaza a PR.SALUD. cc 22292
         IF     nContCert = nContSinEPS
            AND (    nContCert != 0
                 AND nContSinEPS != 0) THEN
            RAISE_APPLICATION_ERROR (-20100, PR.MENSAJE ('AIF', 22073, NULL, NULL, NULL));
         END IF;
      END IF;
      --<<BBVA 31/03/2017 #21556 4Sight-EFBC Se actualiza el Indrecauonline a N en la renovacion
      --para habilitar la poliza para el envio a cobro
      BEGIN
        SELECT CP.*
          INTO rControlPoliza
          FROM CONTROL_POLIZA CP, POLIZA P
         WHERE P.Idepol = nIdePol
           AND CP.Idecotiza = P.NumCotiFlex;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          rControlPoliza.Idecotiza := NULL;
        WHEN OTHERS THEN
          rControlPoliza.Idecotiza := NULL;
      END;
      IF rControlPoliza.Idecotiza IS NOT NULL THEN
        rControlPoliza.Indrecauonline := 'N';
        ACTUALIZA_CONTROL_POLIZA(rControlPoliza);
      END IF;
      -->>BBVA 31/03/2017 #21556 4Sight-EFBC
      --PR_OPERACION.TERMINAR;
   END RENOVAR_CERTIFICADO;
   --////
   -- AnulAR

FUNCTION ANULAR
  ( nIdePol          NUMBER,
    nNumCertAnul     NUMBER,
    cTipoMov         VARCHAR2,
    nPorcCortoPlazo  NUMBER,
    cAnulCert        VARCHAR2
  )
RETURN NUMBER
IS
  nNumOper  PLS_INTEGER;
BEGIN
  PR_POLIZA.CARGAR (nIdePol);
  IF PR_POLIZA.cIndTipoPro = '8' THEN --Realiza el cruce de ACtas
    nNumOper := PR_CANCEL_SUPLE.ANULA_POL
      (nIdePol,nNumCertAnul,cTipoMov,nPorcCortoPlazo,cAnulCert);
      
    --<<COTCSA-MX# 23453--
    /*  
    --BBVA Consis Redmine 13581 24/07/2015 L.A
    BEGIN
     PR_POLIZA.VALIDA_CANCELA_SALDO (nIdePol);
    END;
    --BBVA Consis Redmine 13581 24/07/2015 L.A
    */
    -->>COTCSA-MX# 23453--
    
  ELSE --NO Realiza el cruce de ACtas
    nNumOper := ANULAR_SIS
      (nIdePol,nNumCertAnul,cTipoMov,nPorcCortoPlazo,cAnulCert);

   --<<BBVA 29/09/2016 #20756 Consis EDMV Se crea un nuevo procedimiento, para cumplir con el cruce de poliza AcselWeb con prorrata 5
   IF PR_POLIZA.cIndTipoPro = '5' AND PR_POLIZA.cNumCotiFlex IS NOT NULL THEN
      PR_POLIZA.CRUCE_ACTAS_WEB(nIdePol);
   END IF;

  END IF;
  RETURN nNumOper;
END ANULAR;

   FUNCTION ANULAR_SIS (nIdePol      NUMBER,
                    nNumCertAnul     NUMBER,
                    cTipoMov         VARCHAR2,
                    nPorcCortoPlazo  NUMBER,
                    cAnulCert        VARCHAR2) RETURN NUMBER IS
      -- --
      nIdeRec                NUMBER;
      dFecIniVig            DATE;
      dFecFinVig            DATE;
      dFecAnulPol           DATE;
      dFecLiq               DATE;
      dFecMov               DATE;--                               := SYSDATE;
      nPrimaCortoPlazo      NUMBER (14, 2)                     := 0;
      nCant_Resp_Pago       NUMBER (14);
      nExiste               NUMBER (10);
      nExisteOblig          NUMBER (10);
      nPrimaFactorDev       NUMBER (14, 2);
      nPrimaAnual           NUMBER (14, 2);
      nDiasAno              NUMBER (4);
      nNumCert              NUMBER (10);
      nDiasDevengados       NUMBER (3);
      nPrimaAdevolver       NUMBER (14, 2)                     := 0;
      nPrimaFactorDevCert   NUMBER (14, 2);
      nPrimaAnualCert       NUMBER (14, 2);
      nPrimaFactMoneda      NUMBER (14, 2);
      nPrimaDevolverCert    NUMBER (14, 2);
      nMtoOblig             NUMBER (14, 2);
      nNumOperAnu           NUMBER (14);
      nMtoGiroLocal         NUMBER (14, 2);
      nNumOper              NUMBER (14);
      nMtoCob               NUMBER (14, 2)                     := 0;
      nMtoAct               NUMBER (14, 2)                     := 0;
      nMtoCobCert           NUMBER (14, 2);
      nMtoActCert           NUMBER (14, 2);
      nNumFinanc            NUMBER (14);
      nIdeComp              NUMBER (14)                        := NULL;
      nMtoActCertTot        NUMBER (14, 2)                     := 0;
      nPrimaSiniestro       NUMBER (14, 2)                     := 0;
      nNumENDoso            NUMBER (10);
      cCodCliRp             VARCHAR2 (20);
      cStsRec               VARCHAR2 (3);
      cCodOfiemi            VARCHAR2 (6);
      cTipoAnul             POLIZA.TipoAnul%TYPE;
      cIndTipoPro           VARCHAR2 (1);
      cCodInter             VARCHAR2 (6);
      cCodCobrador          VARCHAR2 (6);
      cTipoCobrador         VARCHAR2 (3);
      cCodProd              VARCHAR2 (4);
      cCodCia               VARCHAR2 (2);
      cIndRepPago           VARCHAR2 (1);
      cIndCobro             VARCHAR2 (1);
      cCodFormaPago         VARCHAR2 (1);
      nDiasDevolver         NUMBER                             := 0;
      nDiasDevolverSin      NUMBER                             := 0;
      nMTOOPER              NUMBER                             := 0;
      nMTOOPERANU           NUMBER                             := 0;
      cIndDevPrima          VARCHAR2 (1);
      cIndDevConSin         VARCHAR2 (1);
      cCodCobert            VARCHAR2 (4);
      nIdeCobert            NUMBER (14);
      nMONTO_CANC           NUMBER (14, 2)                     := 0;
      nMONTO_CP             NUMBER (14, 2)                     := 0;
      cCodFormCan           POLIZA.CodFormCan%TYPE;
      nPrimaDevolverCP      NUMBER (14, 2);
      nNumAcrePro           ACREENCIA.NumAcre%TYPE;
      dFecVencAcre          DATE;
      dFecFinVigPro         DATE;
      nCancelar             NUMBER (14, 2)                     := 0;
      nDevolver             NUMBER (14, 2)                     := 0;
      nTotRecaDctoRamo      NUMBER (14, 2)                     := 0;
      nIdeMovPrima          MOV_PRIMA.IdeMovPrima%TYPE;
      nMtoPrimaBruta        NUMBER (14, 2)                     := 0;
      nNumCertCanc          NUMBER (10);
      cCodRamoCert          VARCHAR2 (4);
      dFecVctMin            GIROS_FINANCIAMIENTO.FecVct%TYPE;
      --
      nMonto_Canc_Cert      NUMBER (14, 2);
      nMonto_Canc_CertN     NUMBER (14, 2);
      nMonto_Oblig_Cert     NUMBER (14, 2)                     := 0;
      --
      nMonto_Canc_Ramo      NUMBER (14, 2);
      nMonto_Oblig_Ramo     NUMBER (14, 2);
      --
      nCantCertRamo         NUMBER                             := 0;
      nNumCertRamo          NUMBER                             := 0;
      nTotCert              NUMBER (14, 2);
      nPrimaProDev          NUMBER (14, 2)                     := 0;
      cTipoSusc             POLIZA.TipoSusc%TYPE;
      nExiste_Oblig         NUMBER (1);
      cStsOperFacult        OPER_FACULT.StsOperFacult%TYPE;
      cIndAnular            VARCHAR2 (1);
--      nIndPolVida           NUMBER (1);  EC - 23/01/2006
      nSumaCedida           NUMBER (14, 2);
      nMontoPrestamo        NUMBER (14, 2);
      nIdeAseg              ASEGURADO.IdeAseg%TYPE;
      cGenRetiro            VARCHAR2 (1);
      nDIVIDE               NUMBER                             := 0;
      nPORCTOTAL            NUMBER                             := 0;
      nAJUSTECANC           NUMBER                             := 0;
      nMontoTotal           NUMBER                             := 0;
      nPorcCorr             NUMBER                             := 0;
      cCodMotvAnul          POLIZA.CodMotvAnul%TYPE;
      dFecIniRec            DATE;
      nRespPag              NUMBER(5);  -- EC - 23/01/2006
      nSumaAsegAnt          NUMBER;
      nDias                 NUMBER;
      cCodcliPol            varchar (120);
      cCodcliCert           varchar (120);
      cCantGiro             PLAN_FINANCIAMIENTO.NroGiros%TYPE;
      cTipoAnulProd         VARCHAR2(1);
      nDiasRecibo           FRECUENCIA.DiasRecibo%TYPE;
      cIndAnulComer         VARCHAR2(1):='N';
      nPrima_Minima         NUMBER (14, 2);
      nExisteOp             NUMBER               := 0;
      nCantMeses            frecuencia.cantmeses%TYPE;
      -- --
      nProGiroPendCanc   NUMBER(22,8);
      nProPrimaDeven     NUMBER(22,8);
      nProPrimaDevol     NUMBER(22,8);
      nNumMeses          NUMBER;
      nPrimaAnualDev     OPER_POL.MTOPRIMAANUALDEV%TYPE := 0;
      cCodplanFrac       VARCHAR2(10);
      cModplanfrac       VARCHAR2(10);
      cTipoPago          VARCHAR2(10);
      nCantPago          INTEGER;
      cCodInterC         INTERMEDIARIO.CodInter%TYPE:= NULL;
  --<< BBVA - 1.57.1.15 EFBC RM21875 - Definicion de variables
    nMtoDetAcre      DET_ACRE.MTODETACRELOCAL%TYPE := 0;
    dFecMAxVigCob    FACTURA.FECVENCFACT%TYPE;
    nTotalFacs       NUMBER(10):= 0;
    dFecVenFacReal   DATE;
    nTotalDiasCob    NUMBER(10):= 0; -- BBVA - EFBC RM21875
    nFrecuenciaGir   NUMBER := 0; -- BBVA - EFBC RM21875
    nDiasDev1        NUMBER(10):=0; -- BBVA - EFBC RM21875
    nNumDiaDevol     INTEGER; -- BBVA - EFBC RM21875
    nNumGiro     INTEGER; -- BBVA - EFBC 1.82 RM21875
  -->> BBVA - 1.57.1.15 EFBC RM21875
    nFlag            NUMBER(1) := 0; -->> BBVA - 1.0.1.1 EFBC RM22491
    cValidaFecAnul   VARCHAR2(1); --BBVA-JJBD 11/03/2018
    nMtoOperDEV      OPER_POL.MtoOper%TYPE; --COTECSA-MX #23453--
    cINDPRORROGA     VARCHAR2(1) := 'N';--COTECSA-FM *23718* 
    cCodAcepRiesgoBase DIST_COA.CODACEPRIESGO%TYPE; --JR 22484
    --
    cIndAnulaIniVig  VARCHAR2(1); --COTECSA #23911--
    
      CURSOR VERIFICA_DIST_FACULT IS
         SELECT DM.IdeRea, DM.NumMod, D.NumCert, D.CodRamoCert, DM.SumaDistRea
         FROM   DIST_REA_MOD DM, DIST_REA D
         WHERE  D.IdePol     = nIdePol
         AND    DM.IdeRea    = D.IdeRea
         AND    D.IdeCttoRea = 99999999999999
         AND    DM.NumMod    = (SELECT MAX(DM.NumMod)
                                FROM   DIST_REA_MOD DM, DIST_REA D
                                WHERE  D.IdePol     = nIdePol
                                AND    DM.IdeRea    = D.IdeRea
                                AND    D.IdeCttoRea = 99999999999999);
      -- --
      CURSOR DIST_COA_Q IS
         SELECT CodAcepRiesgo
         FROM   DIST_COA
         WHERE  IdePol = nIdePol
         AND    StsCoa IN ('ACT', 'MOD')
         AND    CodAcepRiesgo <> cCodAcepRiesgoBase; --JR 22484
      -- --
      CURSOR OBLIGACION_DISMIN_Q IS
         SELECT DISTINCT O.NumOblig
         FROM   OPER_POL OP, OBLIGACION O
         WHERE  OP.IdePol = nIdePol
         AND    OP.TipoOp IN ('MOD')
         AND    OP.MtoOper < 0
         AND    O.NumOper = OP.NumOper
         AND    O.NumOblig IN (SELECT NumOblig
                               FROM   DET_OBLIG DO
                               WHERE  DO.NumOblig = O.NumOblig
                               AND    DO.CodCptoEgre = 'DISMIN')
         AND    O.StsOblig = 'ACT'
         AND    (   OP.NumCert = nNumCertAnul
                 OR nNumCertAnul IS NULL);
      -- --
      CURSOR OBLIGACION_DISMIN_ANU IS
         SELECT DISTINCT O.NumOblig
         FROM   OPER_POL OP, OBLIGACION O
         WHERE  OP.IdePol = nIdePol
         AND    OP.TipoOp IN ('ANU')
         AND    OP.MtoOper < 0
         AND    O.NumOper = OP.NumOper
         AND    O.NumOblig IN (SELECT NumOblig
                               FROM   DET_OBLIG DO
                               WHERE  DO.NumOblig = O.NumOblig
                               AND    DO.CodCptoEgre = 'DISMIN')
         AND    O.StsOblig = 'ACT'
         AND    (   OP.NumCert = nNumCertAnul
                 OR nNumCertAnul IS NULL);
      -- --
      CURSOR OBLIG_DISMIN_FACT_Q IS
         SELECT DISTINCT F.IdeFact
         FROM   OPER_POL    OP,
                OBLIGACION  O,
                FACTURA     F
         WHERE  OP.IdePol  = nIdePol
         AND    OP.TipoOp  = 'MOD'
         AND    OP.MtoOper < 0
         AND    O.NumOper  = OP.NumOper
         AND    O.NumOblig IN (SELECT NumOblig
                               FROM   DET_OBLIG DO
                               WHERE  DO.NumOblig    = O.NumOblig
                               AND    DO.CodCptoEgre = 'DISMIN')
         AND   (OP.NumCert = nNumCertAnul OR nNumCertAnul IS NULL)
         AND    F.NumOper  = OP.NumOper
         AND    F.StsFact NOT IN ('ANU', 'COB');
      -- --
      CURSOR OPER_POL_Q IS
         SELECT O.NumOper, O.NumCert
         FROM   OPER_POL O
         WHERE  O.IdePol = nIdePol
         AND    O.TipoOp IN ('EMI', 'REM', 'MOD', 'REH', 'COM')   --GM agregado , 'COM'
         AND    O.IndAnul = 'N'
         AND    (   O.NumCert = nNumCertAnul
                 OR nNumCertAnul IS NULL)
         AND    (O.IdePol, O.NumCert, O.NumOPer) IN (
                   SELECT C.IdePol, C.NumCert, MAX (C.NumOper)
                   FROM   COND_FINANCIAMIENTO C, OPER_POL OP2
                   WHERE  C.IdePol = nIdePol
                   AND    OP2.IdePol = C.IdePol
                   AND    OP2.NumCert = C.NumCert
                   AND    OP2.NumOPER = C.NumOPER
                   AND    OP2.IndAnul = 'N'
                   AND    (   C.NumCert = nNumCertAnul
                           OR nNumCertAnul IS NULL)
                   AND    C.StsFin IN ('FRA', 'COB', 'ACT')
                   GROUP BY C.IdePol, C.NumCert)
         AND    O.NumCert NOT IN (SELECT C.NumCert
                                  FROM   CERTIFICADO C
                                  WHERE  C.IdePol = nIdePol
                                  AND    C.StsCert = 'EXC')
         GROUP BY O.NumOper, O.NumCert
         ORDER BY O.NumOper, O.NumCert;
      -- --
      CURSOR COBS_CANCELAR IS
         SELECT MC.IdePol, mc.NumCert, rbo.Codramocert
         FROM   mod_cobert MC, COBERT_CERT CC, RECIBO RBO
         WHERE  CC.IdePol = nIdePol
         AND    CC.NumCert = nNumCertCanc
         AND    CC.IdePol = MC.IdePol
         AND    CC.NumCert = MC.NumCert
         AND    CC.CodRAMOCERT = MC.CodRAMOCERT
         AND    CC.IDECOBERT = MC.IDECOBERT
         --<<BBVA Consis EFVC 11/06/2014 - #2409 Atencion revocacion desde incio vigencia
         AND    CC.STSCOBERT in ('ACT','EXC','DEC')
         AND    MC.stsmodcobert IN ('ACT','LIQ','ANU','REV') -->> BBVA - 1.0.1.1 EFBC RM22491
         --AND    TRUNC (MC.Fecfinvalid) >= dFecAnulPol
         --AND    TRUNC (MC.FecInivalid) <= dFecAnulPol
         -->>BBVA Consis EFVC 11/06/2014 -
         AND    RBO.IdeMovPrima = MC.IdeMovPrima
         AND    RBO.IdePol = MC.IdePol
         AND    RBO.NumCert = MC.NumCert
         AND    RBO.CodRamoCErt = MC.CodRamoCert
         AND    RBO.IdeMovPrima IN (SELECT IdeMovPrima
                                    FROM   MOV_PRIMA
                                    WHERE  IdeMovPrima = rbo.IdeMovPrima
                                    AND    StsMovPrima <> 'ANU')
         GROUP BY MC.IdePol, mc.NumCert, rbo.Codramocert
         UNION
         SELECT MC.IdePol, mc.NumCert, rbo.Codramocert
         FROM   mod_cobert MC, COBERT_BIEN CC, RECIBO RBO, BIEN_CERT BC
         WHERE  BC.IdePol = nIdePol
         AND    BC.NumCert = nNumCertCanc
         AND    BC.IdePol = MC.IdePol
         AND    BC.NumCert = MC.NumCert
         AND    BC.CodRAMOCERT = MC.CodRAMOCERT
         AND    BC.IDEBIEN = CC.IDEBIEN
         AND    CC.IDECOBERT = MC.IDECOBERT
         --<<BBVA Consis EFVC 11/06/2014 - #2409 Atencion revocacion desde incio vigencia
         AND    CC.STSCOBERT in ('ACT','EXC','DEC')
         AND    MC.stsmodcobert IN ('ACT','LIQ','ANU','REV') -->> BBVA - 1.0.1.1 EFBC RM22491
         --AND    TRUNC (MC.Fecfinvalid) >= dFecAnulPol
         --AND    TRUNC (MC.FecInivalid) <= dFecAnulPol
         -->>BBVA Consis EFVC 11/06/2014 -
         AND    RBO.IdeMovPrima = MC.IdeMovPrima
         AND    RBO.IdePol = MC.IdePol
         AND    RBO.NumCert = MC.NumCert
         AND    RBO.CodRamoCErt = MC.CodRamoCert
         AND    RBO.IdeMovPrima IN (SELECT IdeMovPrima
                                    FROM   MOV_PRIMA
                                    WHERE  IdeMovPrima = rbo.IdeMovPrima
                                    AND    StsMovPrima <> 'ANU')
         GROUP BY MC.IdePol, mc.NumCert, rbo.Codramocert
         UNION
         SELECT MC.IdePol, mc.NumCert, rbo.Codramocert
         FROM   mod_cobert MC, COBERT_ASEG CC, RECIBO RBO, ASEGURADO BC
         WHERE  BC.IdePol = nIdePol
         AND    BC.NumCert = nNumCertCanc
         AND    BC.IdePol = MC.IdePol
         AND    BC.NumCert = MC.NumCert
         AND    BC.CodRAMOCERT = MC.CodRAMOCERT
         AND    BC.IdeAseg = CC.IdeAseg
         AND    CC.IDECOBERT = MC.IDECOBERT
         --<<BBVA Consis EFVC 11/06/2014 - #2409 Atencion revocacion desde incio vigencia
         AND    CC.STSCOBERT in ('ACT','EXC','DEC')
         AND    MC.stsmodcobert IN ('ACT','LIQ','ANU','REV') -->> BBVA - 1.0.1.1 EFBC RM22491
         --AND    TRUNC (MC.Fecfinvalid) >= dFecAnulPol
         --AND    TRUNC (MC.FecInivalid) <= dFecAnulPol
         -->>BBVA Consis EFVC 11/06/2014 -
         AND    RBO.IdeMovPrima = MC.IdeMovPrima
         AND    RBO.IdePol = MC.IdePol
         AND    RBO.NumCert = MC.NumCert
         AND    RBO.CodRamoCErt = MC.CodRamoCert
         AND    RBO.IdeMovPrima IN (SELECT IdeMovPrima
                                    FROM   MOV_PRIMA
                                    WHERE  IdeMovPrima = rbo.IdeMovPrima
                                    AND    StsMovPrima <> 'ANU')
         GROUP BY MC.IdePol, mc.NumCert, rbo.Codramocert
         ORDER BY 2, 3;
      -- --
      CURSOR CERTIFICADO_Q IS
         SELECT NumCert, FecExc
         FROM   CERTIFICADO
         WHERE  IdePol = nIdePol
         AND    (   NumCert = nNumCertAnul
                 OR nNumCertAnul IS NULL)
         AND    StsCert NOT IN ('ANU', 'EXC', 'CAD');
      -- --
      CURSOR RECIBOS_ANULAR IS
         SELECT DISTINCT RBO.IdeRec, RBO.StsRec, CF.NumFinanc, CF.StsFin
         FROM   RECIBO              RBO,
                COND_FINANCIAMIENTO CF
         WHERE   RBO.IdePol  = nIdePol
         AND    (RBO.NumCert = nNumCertAnul OR nNumCertAnul IS NULL)
         AND     RBO.IdePol  = CF.IdePol
         AND     RBO.NumCert = CF.NumCert
         AND     RBO.NumOper = CF.NumOper
         AND     RBO.IdeMovPrima IN (SELECT MP.IdeMovPrima
                                     FROM   MOV_PRIMA MP
                                     WHERE  MP.IdeMovPrima = RBO.IdeMovPrima
                                     AND    MP.StsMovPrima IN ('ACT', 'COB'));
      -- --
      CURSOR AnulAR_RECIBOS_CESION IS
         SELECT DISTINCT RBO.IdeRec, RBO.StsRec
         FROM   RECIBO RBO
         WHERE  RBO.IdePol = nIdePol
         AND    (   RBO.NumCert = nNumCertAnul
                 OR nNumCertAnul IS NULL)
         AND    RBO.IdeMovPrima IN (SELECT IdeMovPrima
                                    FROM   MOV_PRIMA
                                    WHERE  IdeMovPrima = rbo.IdeMovPrima
                                    AND    StsMovPrima IN ('ACT', 'COB'));
      -- --
      CURSOR C_RESP_PAGO_Q IS
         SELECT RP.IdePol, RP.NumCert, RP.CodCli, PorcPago, CodPlanFracc, NumModPlanFracc, RP.Direc, RP.CodPais, RP.CodEstado, RP.CodCiudad,
                RP.CodMunicipio, RP.Telex, RP.Fax, RP.Zip, RP.Telef1, RP.Telef2, RP.Telef3, RP.CodCobrador, RP.CodViaCobro
         FROM   RESP_PAGO RP, CERTIFICADO C
         WHERE  C.StsCert NOT IN ('EXC', 'ANU', 'CAD')
         AND    C.NumCert = RP.NumCert
         AND    C.IdePol = RP.IdePol
         AND    (RP.NumCert = nNumCertCanc -- EC - 23/01/2006
         OR      nNumCertCanc IS NULL)
         AND    RP.IdePol = nIdePol;
      -- --
      CURSOR MOD_COBERT_SIN_Q IS
         SELECT SUM (PrimaFactMoneda) PrimaFactMoneda, MC.FecIniValid, MC.FecFinValid, MC.OrigModCobert, MC.IdeCobert, MC.CodRamoCert, NumOper,
                MC.NumCert
         FROM   MOD_COBERT MC, RECIBO RBO
         WHERE  MC.IdePol = nIdePol
         AND    MC.NumCert = nNumCert
         AND    MC.CodRAMOCERT = cCodRAMOCERT
         AND    MC.IdeMovPrima = RBO.IdeMovPrima
         AND    MC.IdePol = RBO.IdePol
         AND    MC.NumCert = RBO.NumCert
         AND    MC.CodRAMOCERT = RBO.CodRAMOCERT
         AND    Idecobert IN (SELECT C.Idecobert
                              FROM   COB_RES_GEN C, SINIESTRO S
                              WHERE  S.IdePol = nIdePol
                              AND    S.NumCert = nNumCert
                              AND    S.StsSin  NOT IN ('ANU','DEC') --<<COTECSA-MX #22049>>--
                              AND    S.IdeSin = C.IdeSin
                              AND    C.IdeCobert = MC.IdeCobert)
         GROUP BY MC.FecIniValid, MC.FecFinValid, MC.OrigModCobert, MC.IdeCobert, MC.CodRamoCert, NumOper, MC.NumCert
         UNION
         SELECT SUM (PrimaFactMoneda) PrimaFactMoneda, MC.FecIniValid, MC.FecFinValid, MC.OrigModCobert, MC.IdeCobert, MC.CodRamoCert, NumOper,
                MC.NumCert
         FROM   MOD_COBERT MC, RECIBO RBO
         WHERE  MC.IdePol = nIdePol
         AND    MC.NumCert = nNumCert
         AND    MC.CodRAMOCERT = cCodRAMOCERT
         AND    MC.IdeMovPrima = RBO.IdeMovPrima
         AND    MC.IdePol = RBO.IdePol
         AND    MC.NumCert = RBO.NumCert
         AND    MC.CodRAMOCERT = RBO.CodRAMOCERT
         AND    Idecobert IN (SELECT C.Idecobert
                              FROM   COB_RES_BIEN C, SINIESTRO S
                              WHERE  S.IdePol = nIdePol
                              AND    S.NumCert = nNumCert
                              AND    S.StsSin  NOT IN ('ANU','DEC') --<<COTECSA-MX #22049>>--
                              AND    S.IdeSin = C.IdeSin
                              AND    C.IdeCobert = MC.IdeCobert)
         GROUP BY MC.FecIniValid, MC.FecFinValid, MC.OrigModCobert, MC.IdeCobert, MC.CodRamoCert, NumOper, MC.NumCert
         UNION
         SELECT SUM (PrimaFactMoneda) PrimaFactMoneda, MC.FecIniValid, MC.FecFinValid, MC.OrigModCobert, MC.IdeCobert, MC.CodRamoCert, NumOper,
                MC.NumCert
         FROM   MOD_COBERT MC, RECIBO RBO
         WHERE  MC.IdePol      = nIdePol
         AND    MC.NumCert     = nNumCert
         AND    MC.CodRAMOCERT = cCodRAMOCERT
         AND    MC.IdeMovPrima = RBO.IdeMovPrima
         AND    MC.IdePol      = RBO.IdePol
         AND    MC.NumCert     = RBO.NumCert
         AND    MC.CodRAMOCERT = RBO.CodRAMOCERT
         AND    Idecobert IN (SELECT C.Idecobert
                              FROM   COB_RES_ASEG C, SINIESTRO S
                              WHERE  S.IdePol = nIdePol
                              AND    S.NumCert = nNumCert
                              AND    S.StsSin  NOT IN ('ANU','DEC') --<<COTECSA-MX #22049>>--
                              AND    S.IdeSin = C.IdeSin
                              AND    C.IdeCobert = MC.IdeCobert)
         GROUP BY MC.FecIniValid, MC.FecFinValid, MC.OrigModCobert, MC.IdeCobert, MC.CodRamoCert, NumOper, MC.NumCert;
      -- --
      CURSOR RESP_PAGO_MOV_Q IS
         SELECT RP.CodCli, RP.CodViaCobro
         FROM   RESP_PAGO_MOV RP, CLIENTE C
         WHERE  RP.CodCli = C.CodCli
         AND    NumOper = nNumOper
         AND    NumCert = nNumCertAnul;
      -- --
      CURSOR RECIBO_PAGAR_SIN_C IS
         SELECT IdeMovPrima, IdeRec
         FROM   RECIBO
         WHERE  NumOper = nNumOper
         AND    NVL (IndTipoRec, 'T') = 'D';
      -- --
      CURSOR OPER_X_OPER_Q IS
         SELECT C.NumOper
         FROM   COND_FINANCIAMIENTO C
         WHERE  C.IdePol   = nIdePol
         AND    (C.NumCert = nNumCertAnul OR nNumCertAnul IS NULL)
         AND    C.StsFin IN ('FRA', 'COB')
         AND    C.NumCert NOT IN (SELECT NumCert
                                  FROM   CERTIFICADO
                                  WHERE  IdePol = nIdePol
                                  AND    StsCert = 'EXC')
         AND    C.NumOper > (SELECT NVL (MAX (O.NumOper), 0)
                             FROM   OPER_POL O
                             WHERE  O.IdePol = nIdePol
                             AND    O.NumCert = C.NumCert
                             AND    O.TipoOp = 'ANU')
         GROUP BY NumOper
         UNION
         SELECT O.NumOper
         FROM   OPER_POL OP, OBLIGACION O
         WHERE  OP.NumOper = O.NumOper
         AND    IdePol     = nIdePol
         AND    TipoOp IN ('MOD')
         AND    MtoOper < 0
         AND    StsOblig = 'ACT'
         AND    (OP.NumCert = nNumCertAnul OR nNumCertAnul IS NULL);
      -- --
      CURSOR RESEG IS
         SELECT IdeRec, CodRamoCert
         FROM   RECIBO
         WHERE  NumOper = nNumOper;
      -- --
      CURSOR GEN_FRACC_AnulA IS
         SELECT DISTINCT NumCert
         FROM   OPER_POL
         WHERE  NumOper = nNumOper;
      -- --
      CURSOR MOV_Anul IS
         SELECT IdeMovPrima, MtoMoneda
         FROM   RECIBO
         WHERE  NumOper = PR_OPERACION.NumOper;
      -- --
      CURSOR FINANCIAMIENTOS IS
         SELECT *
         FROM   FINANCIAMIENTO_MODIFICADO
         WHERE  USUARIO = SUBSTR (USER, 1, 20)
         AND    IdePol  = nIdePol;
      -- --
   BEGIN
   -- --
   PR_VINISUSC.CARGAR;
   PR_POLIZA.CARGAR (nIdePol);
   PR_FECHAS_EQUIVALENTES.CEVENTO := '001';
   nSumaAsegAnt       := SUMA_ASEGURADA_POL(nIdePol);
   cIndAnular         := 'S';
   PR_POLIZA.cOperRev := 'ANU';
   -- --
   FOR VDF IN VERIFICA_DIST_FACULT LOOP
       BEGIN
         SELECT SUM (SumaCedida) SumaCedida
         INTO   nSumaCedida
         FROM   DIST_FACULT
         WHERE  IdeRea         = VDF.IdeRea
         AND    NumMod         = VDF.NumMod
         AND    NumCert        = VDF.NumCert
         AND    CodRamoCert    = VDF.CodRamoCert
         AND    StsDistFacult <> 'EXC';
       EXCEPTION
         WHEN NO_DATA_FOUND THEN
              nSumaCedida := 0;
         WHEN OTHERS THEN
              RAISE_APPLICATION_ERROR (-20288, PR.MENSAJE ('ABD', 20288, ' DIST_FACULT ' || SQLERRM, NULL, NULL));
       END;
       -- --
       IF NVL (nSumaCedida, 0) <> VDF.SumaDistRea  AND (NVL(nSumaCedida, 0)- VDF.SumaDistRea ) > 1  THEN
          RAISE_APPLICATION_ERROR (-20220, PR.MENSAJE ('AIF', 0021006, NULL, NULL, NULL));
       END IF;
   END LOOP;
   --//--
   BEGIN
     SELECT FecAnul, NVL (TipoAnul,'X')
     INTO   dFecAnulPol, cTipoAnul
     FROM   POLIZA
     WHERE  IdePol = nIdePol;
   EXCEPTION
     WHEN NO_DATA_FOUND THEN
        RAISE_APPLICATION_ERROR (-20205, PR.MENSAJE ('ABD', 20205, ' nIdePol ' || nIdePol, 'POLIZA', NULL));
     WHEN TOO_MANY_ROWS THEN
        RAISE_APPLICATION_ERROR (-20341, PR.MENSAJE ('ABD', 20341, ' nIdePol ' || nIdePol, 'POLIZA', NULL));
     WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR (-20288, PR.MENSAJE ('ABD', 20288, ' POLIZA ' || SQLERRM, NULL, NULL));
   END;
   -- No se puede cancelar poliza con fecha anul. anterior a la fecha de ocurrencia del siniestro
   IF dFecAnulPol < PR_POLIZA.FechaUltSiniestro (nIdePol, NULL) THEN
      RAISE_APPLICATION_ERROR (-20100,'La fecha de anulaci�n de la poliza (' || dFecAnulPol || '), es menor que la fecha de ocurrencia de siniestro ('|| PR_POLIZA.FechaUltSiniestro (nIdePol, NULL) || ').');
   END IF;
   -- No se puede cancelar poliza con fecha anul. anterior a la ultima modificaci�n o endoso
   IF dFecAnulPol < PR_POLIZA.FechaUltModifPoliza (nIdePol, NULL) AND cTipoAnul NOT IN ('M', 'P') THEN
      RAISE_APPLICATION_ERROR (-20100,'La fecha de anulaci�n de la poliza (' || dFecAnulPol || '), es menor que la fecha de �ltima modificaci�n ('|| PR_POLIZA.FechaUltModifPoliza (nIdePol, NULL) || ').');
   END IF;
   -- No se puede cancelar poliza con siniestros pendientes
   IF NVL (PR_POLIZA.NumSiniestrosPend (nIdePol, NULL), 0) > 0 THEN
      BEGIN PR_OPER_USUARIO.AUTORIZAR ('076', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
      EXCEPTION WHEN OTHERS THEN
         RAISE_APPLICATION_ERROR (-20100, 'La poliza tiene siniestros pendientes, no se puede anular.');
      END;
   END IF;
   -- Fin de validacion
   --<<BBVA-JJBD 11/03/2018 Se agrega validacion de fecha de anulacion Vs vigencia de la poliza.
   cValidaFecAnul := PR.BUSCA_PARAMETRO('362');
   IF NVL(cValidaFecAnul,'S') = 'S' THEN
     IF dFecAnulPol < PR_POLIZA.dFecIniVig THEN
       RAISE_APPLICATION_ERROR (-20100,'La fecha de anulaci�n de la p�liza (' || dFecAnulPol || '), es menor que la fecha de inicio de vigencia ('|| PR_POLIZA.dFecIniVig || ').');
     END IF;
     --
     IF dFecAnulPol > PR_POLIZA.dFecFinVig THEN
       RAISE_APPLICATION_ERROR (-20100,'La fecha de anulaci�n de la p�liza (' || dFecAnulPol || '), es mayor que la fecha fin de vigencia ('|| PR_POLIZA.dFecFinVig || ').');
     END IF;
   END IF;
   -->>BBVA-JJBD 11/03/2018
   --//--
   --<<COTECSA #23911--
   BEGIN
       SELECT   NVL(PR.IndAnulaIniVig,'N')
       INTo     cIndAnulaIniVig
       FROM     PRODUCTO PR
       WHERE    PR.CodProd  = PR_POLIZA.cCodProd;
   EXCEPTION
        WHEN OTHERS THEN
            cIndAnulaIniVig := 'N';
   END;

   IF cIndAnulaIniVig = 'S' THEN
     IF dFecAnulPol != PR_POLIZA.dFecIniVig THEN
       RAISE_APPLICATION_ERROR (-20100,'Este Producto esta Configurado solo para Anularse a Inicio de Vigencia.');
     END IF;
   END IF;
   -->>COTECSA #23911--
      
   IF cIndAnular = 'S' THEN
      PR_OPERACION.INICIAR ('ANULA', 'POLIZA', TO_CHAR (nIdePol));
      PR_B$_POLIZA.PUSH (nIdePol, 'ANULA', 'PRC');   -- Guardar Reflejo de La Poliza
      BEGIN
        SELECT FecIniVig, FecFinVig, CodOfiemi, NVL (TipoAnul, 'X'), FecAnul,
               IndTipoPro, CodFormPago, CodProd, cTipoSusc, NVL (CodFormCan, 'N'), CodMotvAnul
        INTO   dFecIniVig, dFecFinVig, cCodOfiemi, cTipoAnul, dFecAnulPol,
               cIndTipoPro, cCodFormaPago, cCodProd, cTipoSusc, cCodFormCan, cCodMotvAnul
        FROM   POLIZA
        WHERE  IdePol     = nIdePol
        AND    FecIniVig  IS NOT NULL
        AND    ((TipoVig  = 'D' AND FecFinVig IS NULL) OR ( FecFinVig IS NOT NULL)) -- Defecto 8326 Debe permitir Anular Polizas con Vigencia Abierta
        AND    CodOfiemi  IS NOT NULL
        AND    IndTipoPro IS NOT NULL;
      EXCEPTION
       WHEN NO_DATA_FOUND THEN
           RAISE_APPLICATION_ERROR (-20205, PR.MENSAJE ('ABD', 20205, ' nIdePol ' || nIdePol, 'POLIZA', NULL));
       WHEN TOO_MANY_ROWS THEN
           RAISE_APPLICATION_ERROR (-20341, PR.MENSAJE ('ABD', 20341, ' nIdePol ' || nIdePol, 'POLIZA', NULL));
       WHEN OTHERS THEN
           RAISE_APPLICATION_ERROR (-20288, PR.MENSAJE ('ABD', 20288, ' POLIZA ' || SQLERRM, NULL, NULL));
      END;
      -- --
      IF cTipoAnul = 'N' THEN
         BEGIN UPDATE POLIZA SET TipoAnul = 'M' WHERE  IdePol = nIdePol;
         EXCEPTION
            WHEN OTHERS THEN
               RAISE_APPLICATION_ERROR (-20288,PR.MENSAJE ('ABD', 20288,' UPDATE - POLIZA nIdePol ' || nIdePol || ' ' || SQLERRM,NULL, NULL));
         END;
         cTipoAnul  := 'M';
      END IF;
      -- --
      IF nNumCertAnul IS NULL THEN
        IF  cTipoAnul IS NULL OR dFecAnulPol IS NULL THEN
            RAISE_APPLICATION_ERROR (-20100, 'Para Anular la Poliza  se requieren datos de Fecha y Tipo de Anulacion');
        END IF;
      END IF;
      -- --
      BEGIN
        SELECT I.CodInter, NVL (CodCobrador, '000001')
        INTO   cCodInter, cCodCobrador
        FROM   PART_INTER_POL PI, INTERMEDIARIO I
        WHERE  PI.CodInter = I.CodInter
        AND    IdePol      = nIdePol
        AND    PI.IndLider = 'S';
      EXCEPTION
       WHEN NO_DATA_FOUND THEN
         RAISE_APPLICATION_ERROR (-20205, PR.MENSAJE ('ABD', 20205, ' nIdePol ' || nIdePol, 'PART_INTER_POL', NULL));
       WHEN TOO_MANY_ROWS THEN
         RAISE_APPLICATION_ERROR (-20341, PR.MENSAJE ('ABD', 20341, ' nIdePol ' || nIdePol, 'PART_INTER_POL', NULL));
       WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR (-20288, PR.MENSAJE ('ABD', 20288, ' PART_INTER_POL ' || SQLERRM, NULL, NULL));
      END;
      -- --
      IF cCodCobrador = cCodInter THEN
         cTipoCobrador  := 'INT';
      ELSIF cCodCobrador = '000001' THEN
         cTipoCobrador  := 'DIR';
      ELSE
         cTipoCobrador  := 'COB';
      END IF;
      -- --
      IF nNumCertAnul IS NOT NULL THEN
         BEGIN
           SELECT FecExc, FecIng, FecFin, tipoanul --BBVA Consis 27/08/2013 Se consulta el Tipo Anulacion del certificado
           INTO   dFecAnulPol, dFecIniVig, dFecFinVig, cTipoAnul
           FROM   CERTIFICADO
           WHERE  IdePol  = nIdePol
           AND    NumCert = nNumCertAnul;
         EXCEPTION
           WHEN NO_DATA_FOUND THEN
             RAISE_APPLICATION_ERROR (-20205, PR.MENSAJE ('ABD', 20205, ' nIdePol ' || nIdePol, 'CERTIFICADO', NULL));
           WHEN TOO_MANY_ROWS THEN
             RAISE_APPLICATION_ERROR (-20341, PR.MENSAJE ('ABD', 20341, ' nIdePol ' || nIdePol, 'CERTIFICADO', NULL));
           WHEN OTHERS THEN
             RAISE_APPLICATION_ERROR (-20288, PR.MENSAJE ('ABD', 20288, ' CERTIFICADO ' || SQLERRM, NULL, NULL));
         END;
         -- --
         IF dFecAnulPol IS NULL THEN
            RAISE_APPLICATION_ERROR (-20100, 'Debe Incluir la Fecha de Anulacion al CertIFicado');
         END IF;
      ELSE
         IF cTipoAnul = 'M' THEN
            BEGIN
             SELECT 1
             INTO   nExiste
             FROM   GIROS_FINANCIAMIENTO G,
                    COND_FINANCIAMIENTO  C
             WHERE  G.StsGiro = 'COB'
             AND    G.NumFinanc = C.NumFinanc
             AND    C.IdePol = nIdePol
             AND    ROWNUM < 2;
            EXCEPTION
               WHEN NO_DATA_FOUND THEN nExiste  := 0;
               WHEN TOO_MANY_ROWS THEN nExiste  := 1;
               WHEN OTHERS THEN
                    RAISE_APPLICATION_ERROR (-20288, PR.MENSAJE ('ABD', 20288, ' GIROS_FINANCIAMIENTO ' || SQLERRM, NULL, NULL));
            END;
            -- --
            BEGIN
              SELECT MIN (G.FecVct)
              INTO   dFecVctMin
              FROM   GIROS_FINANCIAMIENTO G, COND_FINANCIAMIENTO C
              WHERE  G.StsGiro IN ('ACT', 'REF')
              AND    G.NumFinanc = C.NumFinanc
              AND    C.IdePol    = nIdePol;
            EXCEPTION
               WHEN NO_DATA_FOUND OR TOO_MANY_ROWS THEN  dFecVctMin := NULL;
               WHEN OTHERS THEN
                    RAISE_APPLICATION_ERROR (-20288, PR.MENSAJE ('ABD', 20288, ' GIROS_FINANCIAMIENTO ' || SQLERRM, NULL, NULL));
            END;
            IF TRUNC (dFecAnulPol) < TRUNC (dFecVctMin) THEN
               RAISE_APPLICATION_ERROR (-20100,'La Fecha de Anulacion es menor a la Fecha de Vencimiento del primer '
                                            || 'giro pendiente de la Poliza. El Tipo de Anulacion debe ser a Prorrata.');
            ELSIF TRUNC (dFecAnulPol) > TRUNC (dFecVctMin) THEN
                   RAISE_APPLICATION_ERROR (-20100, 'La Fecha de Anul. es mayor a la Fecha de Vencimiento del 1RO giro PEND.');
            END IF;
          END IF;
      END IF;
      -- --
      cCodOfiemi  := PR_VINISUSC.OFICINA_EMISORA;
      cCodCia     := PR_VINISUSC.CodIGO_CIA;
      -- --
      IF PR.SALUD (nIdePol) = 'S' THEN
         PR_SALUD.ANULAR_POL_SALUD (nIdePol, nNumCertAnul, dFecAnulPol, cCodMotvAnul);
      END IF;
      -- --
      IF cTipoMov = 'D' THEN
         nFlag := 0; -->> BBVA - 1.0.1.1 EFBC RM22491
         FOR C IN OPER_X_OPER_Q LOOP
            nFlag := 1; -->> BBVA - 1.0.1.1 EFBC RM22491
            BEGIN UPDATE OPER_POL SET NumOperAnu = PR_OPERACION.nNumOper
            WHERE  NumOper = C.NumOper;
            EXCEPTION
               WHEN OTHERS THEN
                 RAISE_APPLICATION_ERROR (-20288,PR.MENSAJE ('ABD', 20288,' UPDATE - OPER_POL NumOperAnu ' || PR_OPERACION.nNumOper || ' ' || SQLERRM,NULL, NULL));
            END;
         END LOOP;
         --<< BBVA - 1.0.1.1 EFBC RM22491 Se actualiza el NumOperAnu para operaciones REH de monto 0
         IF nFlag = 0 THEN
            BEGIN  -- EFBC
              UPDATE OPER_POL
                 SET NumOperAnu = PR_OPERACION.nNumOper
               WHERE NumOper =
                     (SELECT MAX(NumOper)
                        FROM OPER_POL
                       WHERE IdePol = nIdePol
                         AND NumCert = NVL(nNumCertAnul, NumCert)
                         AND NumOper < PR_OPERACION.nNumOper)
                         AND NumOperAnu IS NULL;
            EXCEPTION
               WHEN OTHERS THEN
                 RAISE_APPLICATION_ERROR (-20288,PR.MENSAJE ('ABD', 20288,' UPDATE - OPER_POL NumOperAnu ' || PR_OPERACION.nNumOper || ' ' || SQLERRM,NULL, NULL));
            END;
         END IF;
         -->> BBVA - 1.0.1.1 EFBC RM22491
      END IF;
      -- --
      nNumENDoso  := PR_POLIZA.NumERO_ENDOSO (cCodProd, cCodOfiemi, 'CANC');
      FOR D IN OPER_POL_Q LOOP
          cIndCobro        := 'N';
          nNumOper         := D.NumOPer;
          nNumCertCanc     := D.NumCert;
          cIndRepPago      := 'N';
          IF nNumCertAnul IS NOT NULL THEN
             FOR RP IN RESP_PAGO_MOV_Q LOOP
                  cCodCliRp  := RP.CodCli;
                  BEGIN
                     SELECT NVL (COUNT (*), 0)
                     INTO   nCant_Resp_Pago
                     FROM   RESP_PAGO_MOV RP, OPER_POL OP
                     WHERE  RP.NumOper  = OP.NumOper
                     AND    (RP.NumCert = nNumCertAnul OR nNumCertAnul IS NULL)
                     AND    RP.NumCert  = OP.NumCert
                     AND    OP.MtoOper  > 0
                     AND    RP.NumOper  = D.NumOper
                     AND    CodCli      = cCodCliRp;
                  EXCEPTION
                    WHEN NO_DATA_FOUND THEN nCant_Resp_Pago := 0;
                    WHEN OTHERS THEN
                         RAISE_APPLICATION_ERROR (-20288, PR.MENSAJE ('ABD', 20288, ' RESP_PAGO_MOV ' || SQLERRM, NULL, NULL));
                  END;
                  IF nCant_Resp_Pago > 1 THEN
                     IF   cAnulCert = 'N' THEN
                          RAISE_APPLICATION_ERROR (-20100, 'No se puede Anular el CertIFicado debera Excluirlo.');
                     ELSE cIndRepPago  := 'S';
                     END IF;
                  END IF;
             END LOOP;
          END IF;
          -- --
          IF cTipoAnul = 'M' THEN
             -- VerIFica que no existan oblig. pEND. del fraccionamiento anterior
             BEGIN
               SELECT 1
               INTO   nExiste_Oblig
               FROM   OBLIGACION O, OPER_POL OP
               WHERE  NOT EXISTS (SELECT 1
                                  FROM   DET_OBLIG_RAMO DE
                                  WHERE  DE.Numoblig = O.NumOblig)
               AND    O.StsOblig = 'ACT'
               AND    O.NumOper  = OP.NumOper
               AND    OP.IdePol  = nIdePol
               AND    ROWNUM < 2;
             EXCEPTION
               WHEN NO_DATA_FOUND THEN
                    nExiste_Oblig  := 0;
                    BEGIN
                      SELECT 1
                      INTO   nExiste_Oblig
                      FROM   OBLIGACION O
                      WHERE  NOT EXISTS (SELECT 1
                                         FROM   DET_OBLIG_RAMO DE
                                         WHERE  DE.NumOblig = O.NumOblig)
                      AND    O.StsOblig = 'ACT'
                      AND    O.NumOblig IN (SELECT DISTINCT (R.NumOBLIG)
                                            FROM   REL_OBLACR_FRAC R, ACREENCIA A
                                            WHERE  R.IdePol = nIdePol
                                            AND    R.NumCert = D.NumCert
                                            AND    R.NumAcre = A.NumAcre
                                            AND    A.StsAcre = 'ACT');
                   EXCEPTION
                     WHEN NO_DATA_FOUND THEN nExiste_Oblig  := 0;
                     WHEN TOO_MANY_ROWS THEN nExiste_Oblig  := 1;
                     WHEN OTHERS THEN
                          RAISE_APPLICATION_ERROR (-20288, PR.MENSAJE ('ABD', 20288, ' OBLIGACION ' || SQLERRM, NULL, NULL));
                   END;
             END;
             -- --
             IF nExiste_Oblig = 1 THEN
                RAISE_APPLICATION_ERROR (-20100,'Esta poliza no puede Anularse por monto PENDiente TIENE obligaciones '|| 'generadas bajo el esquema de fraccionamiento anterior. ');
             END IF;
             -- --
             -- Determina el neto pENDiente del certIFicado
             BEGIN
               SELECT NVL (SUM (E.MtoDetGiroMoneda), 0)
               INTO   nMonto_Canc_Cert
               FROM   DET_GIRO_FIN E, GIROS_FINANCIAMIENTO G, COND_FINANCIAMIENTO C
               WHERE  E.CodGrupoAcre = 'PRIMAS'
               AND    E.NumGiro      = G.NumGiro
               AND    E.NumFinanc    = G.NumFinanc
               AND    G.StsGiro      IN ('ACT', 'REF')
               AND    G.NumFinanc    = C.NumFinanc
               AND    C.NumCert      = D.NumCert
               AND    C.IdePol       = nIdePol;
             EXCEPTION
               WHEN NO_DATA_FOUND THEN nMonto_Canc_Cert := 0;
               WHEN OTHERS THEN
                   RAISE_APPLICATION_ERROR (-20288, PR.MENSAJE ('ABD', 20288, ' DET_GIRO_FIN ' || SQLERRM, NULL, NULL));
             END;
             BEGIN
               SELECT NVL (SUM (E.MtoDetGiroMoneda), 0)
               INTO   nMontoTotal
               FROM   DET_GIRO_FIN E, GIROS_FINANCIAMIENTO G, COND_FINANCIAMIENTO C
               WHERE  E.CodGrupoAcre = 'PRIMAS'
               AND    E.NumGiro      = G.NumGiro
               AND    E.NumFinanc    = G.NumFinanc
               AND    G.StsGiro      IN ('ACT', 'REF')
               AND    G.NumFinanc    = C.NumFinanc
               AND    C.IdePol       = nIdePol;
             EXCEPTION
               WHEN NO_DATA_FOUND THEN  nMontoTotal := 0;
               WHEN OTHERS THEN
                   RAISE_APPLICATION_ERROR (-20288, PR.MENSAJE ('ABD', 20288, ' DET_GIRO_FIN ' || SQLERRM, NULL, NULL));
             END;
             IF (nMontoTotal = 0) THEN
                RAISE_APPLICATION_ERROR(-20100,'Esta poliza NO puede Anularse por Monto Pendiente, NO TIENE giros pendientes por cobrar. ');
             END IF;
             nPORCCORR          := nMonto_Canc_Cert / nMontoTotal;
             najustecanc        := 0;
             BEGIN
               SELECT NVL (M.MONTO, 0) * nPorcCorr, 1
               INTO   nMonto_Canc_Cert, najustecanc
               FROM   MONTO_CANCELAR_POLIZA M
               WHERE  M.IdePol = nIdePol;
             EXCEPTION
                WHEN OTHERS THEN
                   Nporccorr  := 0;
             END;
             BEGIN
              SELECT NVL (SUM (DE.MtoDetObligRamo), 0)
              INTO   nMonto_Oblig_Cert
              FROM   DET_OBLIG_RAMO DE, OBLIGACION O
              WHERE  DE.NumOblig   = O.NumOblig
              AND    DE.CodClaEgre = 'DEVOLU'
              AND    O.StsOblig    = 'ACT'
              AND    O.NumOblig IN (SELECT DISTINCT (R.NumOBLIG)
                                    FROM   REL_OBLACR_FRAC R, ACREENCIA A
                                    WHERE  R.IdePol = nIdePol
                                    AND    R.NumCert = D.NumCert
                                    AND    R.NumAcre = A.NumAcre
                                    AND    A.StsAcre = 'ACT');
             EXCEPTION
               WHEN NO_DATA_FOUND THEN
                    nMonto_Oblig_Cert := 0;
               WHEN OTHERS THEN
                   RAISE_APPLICATION_ERROR (-20288, PR.MENSAJE ('ABD', 20288, ' DET_OBLIG_RAMO ' || SQLERRM, NULL, NULL));
             END;
             -- Al pENDiente se le restan las obligaciones ACT
             nMonto_Canc_CertN  := nMonto_Canc_Cert - nMonto_Oblig_Cert;
             -- Si es un colectivo Anulara c/certIFicado de acuerdo a su giro pEND. mas antiguo
             IF cTipoSusc = 'C' THEN
                BEGIN
                  SELECT MIN (TRUNC (G.FecVct))
                  INTO   dFecVctMin
                  FROM   GIROS_FINANCIAMIENTO G, COND_FINANCIAMIENTO C
                  WHERE  G.StsGiro IN ('ACT', 'REF')
                  AND    G.NumFinanc = C.NumFinanc
                  AND    C.NumCert = D.NumCert
                  AND    C.IdePol = nIdePol;
                EXCEPTION
                   WHEN NO_DATA_FOUND THEN dFecVctMin := NULL;
                   WHEN OTHERS THEN
                      RAISE_APPLICATION_ERROR (-20288, PR.MENSAJE ('ABD', 20288, ' GIROS_FINANCIAMIENTO ' || SQLERRM, NULL, NULL));
                END;
                IF  dFecVctMin <> dFecAnulPol AND dFecVctMin IS NOT NULL THEN
                     dFecAnulPol  := dFecVctMin;
                END IF;
                -- --
                UPDATE CERTIFICADO SET FecExc = dFecAnulPol
                WHERE  IdePol = nIdePol AND NumCert = D.NumCert;
             END IF;
             -- Determina cuantos ramos del certIFicado seran procesados en la Anulacion
             IF cTipoMov = 'D' THEN
                 nCantCertRamo  := 0;
                 nNumCertRamo   := 0;
                 FOR I IN COBS_CANCELAR LOOP
                    nCantCertRamo  := nCantCertRamo + 1;
                 END LOOP;
             END IF;
          END IF;
          nPrimaSiniestro  := 0;
          -- Cursor de CertIFicados-Ramos involucrados en la Anulacion
          FOR X IN COBS_CANCELAR LOOP
               nMonto_Canc         := 0;
               nTotRecaDctoRamo    := 0;
               nPrimaProDev        := 0;
               BEGIN   -- Mto.Oblig por CertIFicado-Ramo
                  SELECT NVL (SUM (D.MtoDetObligRamo), 0)
                  INTO   nMonto_Oblig_Ramo
                  FROM   DET_OBLIG_RAMO D, OBLIGACION O
                  WHERE  D.NumOblig   = O.NumOblig
                  AND    D.CodClaEgre = 'DEVOLU'
                  AND    D.CodRamo    = X.CodRamoCert
                  AND    O.StsOblig   = 'ACT'
                  AND    O.NumOblig IN (SELECT DISTINCT (R.NumOBLIG)
                                        FROM   REL_OBLACR_FRAC R, ACREENCIA A
                                        WHERE  R.IdePol = X.IdePol
                                        AND    R.NumCert = X.NumCert
                                        AND    R.NumAcre = A.NumAcre
                                        AND    A.StsAcre = 'ACT');
               EXCEPTION
                  WHEN NO_DATA_FOUND THEN nMonto_Oblig_Ramo := 0;
                  WHEN OTHERS THEN
                     RAISE_APPLICATION_ERROR (-20288, PR.MENSAJE ('ABD', 20288, ' DET_OBLIG_RAMO ' || SQLERRM, NULL, NULL));
               END;
               -- --
               IF cTipoAnul = 'M' THEN
                  BEGIN
                     SELECT (SUM (R.PorcRec) / COUNT (DISTINCT G.NumGIRO)) / COUNT (DISTINCT c.NumFinanc)
                     INTO   nPORCTOTAL
                     FROM   DET_GIRO_FIN D, GIROS_FINANCIAMIENTO G, RECIBO RE, REC_FINANCIAMIENTO R, COND_FINANCIAMIENTO C
                     WHERE  D.CodGrupoAcre = 'PRIMAS'
                     AND    D.NumGiro      = G.NumGiro
                     AND    D.NumFinanc    = G.NumFinanc
                     AND    G.StsGiro IN ('ACT', 'REF')
                     AND    G.NumFinanc    = C.NumFinanc
                     AND    RE.IdeRec      = R.IdeRec
                     AND    R.NumFinanc    = C.NumFinanc
                     AND    C.NumCert      = X.NumCert
                     AND    C.IdePol       = X.IdePol;
                  EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                       RAISE_APPLICATION_ERROR (-20205, PR.MENSAJE ('ABD', 20205, ' nIdePol ' || X.IdePol, 'DET_GIRO_FIN', NULL));
                    WHEN OTHERS THEN
                       RAISE_APPLICATION_ERROR (-20288, PR.MENSAJE ('ABD', 20288, ' DET_GIRO_FIN ' || SQLERRM, NULL, NULL));
                  END;
                  IF nPORCTOTAL > 1.5 THEN
                     nDIVIDE  := 100;
                  ELSE
                     nDIVIDE  := 1;
                  END IF;
                  BEGIN
                     SELECT NVL (SUM (D.MtoDetGiroMoneda * (R.PorcRec / nDIVIDE)), 0)
                     INTO   nMonto_Canc_Ramo
                     FROM   DET_GIRO_FIN D, GIROS_FINANCIAMIENTO G, RECIBO RE, REC_FINANCIAMIENTO R, COND_FINANCIAMIENTO C
                     WHERE  D.CodGrupoAcre = 'PRIMAS'
                     AND    D.NumGiro      = G.NumGiro
                     AND    D.NumFinanc    = G.NumFinanc
                     AND    G.StsGiro IN ('ACT', 'REF')
                     AND    G.NumFinanc    = C.NumFinanc
                     AND    RE.CodRamoCert = X.CodRamoCert
                     AND    RE.IdeRec      = R.IdeRec
                     AND    R.NumFinanc    = C.NumFinanc
                     AND    C.NumCert      = X.NumCert
                     AND    C.IdePol       = X.IdePol;
                  EXCEPTION
                     WHEN NO_DATA_FOUND THEN  nMonto_Canc_Ramo := 0;
                     WHEN OTHERS THEN
                        RAISE_APPLICATION_ERROR (-20288, PR.MENSAJE ('ABD', 20288, ' DET_GIRO_FIN ' || SQLERRM, NULL, NULL));
                  END;
                  BEGIN
                     SELECT NVL ((M.MONTO * (R.PorcRec / nDIVIDE)), 0) * nPorcCorr
                     INTO   nMonto_Canc_Ramo
                     FROM   MONTO_CANCELAR_POLIZA M, RECIBO RE, REC_FINANCIAMIENTO R, COND_FINANCIAMIENTO C
                     WHERE  M.IdePol       = C.IdePol
                     AND    RE.CodRamoCert = X.CodRamoCert
                     AND    RE.IdeRec      = R.IdeRec
                     AND    R.NumFinanc    = C.NumFinanc
                     AND    RE.IdePol      = C.IdePol
                     AND    RE.NumOPER     = C.NumOPER
                     AND    RE.STSREC      = 'ACT'
                     AND    C.IdePol       = X.IdePol;
                  EXCEPTION
                     WHEN OTHERS THEN
                        nMonto_Canc_Ramo := 0;
                  END;
                  -- Neto pENDiente del Cert-Ramo
                  nMonto_Canc  := nMonto_Canc_Ramo - nMonto_Oblig_Ramo;
                  nMonto_Cp    := nMonto_Canc;
               END IF;
               --<<COTECSA 04/08/2018 FM *23718* SE CREA ESTE SELECT PARA CONDICIONAR SI LA DEVOLUCI�N ES UNA PRORROGA Y ES TIPO PRORRATA ES 2
               BEGIN
                  SELECT 'S'
                  INTO   cINDPRORROGA
                  FROM   POLIZA_PRORROGA
                  WHERE  IDEPOL = X.IdePol;
               EXCEPTION WHEN NO_DATA_FOUND THEN
                  cINDPRORROGA := 'N';
                         WHEN TOO_MANY_ROWS THEN
                  cINDPRORROGA := 'S';
               END;
               -->>COTECSA 04/08/2018 FM *23718* SE CREA ESTE SELECT PARA CONDICIONAR SI LA DEVOLUCI�N ES UNA PRORROGA Y ES TIPO PRORRATA ES 2
               -- Cursor con el ultimo mod_cobert por Cert-Ramo,
               -- donde se calcula la prima no devengada a prorrata
               --DV,FV,EZ CALCULO NUEVO PARA ANULACIONES A INICIO DE VIGENCIA LO CALCULABA MAL
               --CON EL METODO ANTERIOR
               IF dFecAnulPol = dFecIniVig THEN
                  FOR Y IN ( SELECT SUM(MC.PrimaFactMoneda) Monto_Canc,SUM(MC.PrimaFactMoneda) nMtoPrimaBruta,SUM(MC.PrimaFactMoneda) Monto_Cp
                             FROM   MOD_COBERT MC
                             WHERE  MC.IDEPOL = X.IdePol
                             AND    MC.NUMCERT = X.NumCert
                             AND    MC.CODRAMOCERT = X.CodRamoCert
                             AND    MC.StsModCobert IN ('ACT', 'LIQ', 'REV', 'ANU')) LOOP -->> BBVA - 1.0.1.1 EFBC RM22491

                       IF cTipoAnul <> 'M' THEN
                         --<<CONSIS - EFVC  REDMINE 22583- 21858. Redondeo Moneda Extranjera
                        IF PR_POLIZA.CCODMONEDA <> PR_VINISUSC.CCODMONEDA THEN
                           nMonto_Canc  := NVL (nMonto_Canc, 0) + NVL(Y.Monto_Canc,0);
                           nMonto_Cp    := NVL (nMonto_Cp, 0) + NVL(Y.Monto_Cp,0);
                        ELSE
                           nMonto_Canc  := NVL (nMonto_Canc, 0) + NVL (ROUND(Y.Monto_Canc,0), 0);
                           nMonto_Cp    := NVL (nMonto_Cp, 0) + NVL (ROUND(Y.Monto_Cp,0), 0);
                        END IF;
                      END IF;
                     --<<CONSIS - EFVC  REDMINE 22583- 21858. Redondeo Moneda Extranjera
                       IF PR_POLIZA.CCODMONEDA <> PR_VINISUSC.CCODMONEDA THEN
                           nTotRecaDctoRamo  := nTotRecaDctoRamo + (NVL(Y.MONTO_CANC,0) - Y.nMtoPrimaBruta);
                           nPrimaProDev      := nPrimaProDev + NVL (Y.Monto_Canc,0);
                       ELSE
                           nTotRecaDctoRamo  := nTotRecaDctoRamo + (NVL (ROUND(Y.MONTO_CANC,0), 0) - Y.nMtoPrimaBruta);
                           nPrimaProDev      := nPrimaProDev + NVL (ROUND(Y.Monto_Canc,0), 0);
                       END IF;
                     END LOOP;
                     --frontweb
                     nPrimaAnualDev := nPrimaProDev;
               --COTECSA *23718* FM 04/08/2018 SE COMENTA ESTA LINEA PARA COLOCAR UNA CONDICION ADICIONAL ELSIF PR_POLIZA.CINDTIPOPRO = '2' AND NVL(cINDPRORROGA,'N') = 'N' THEN --<<14/12/2017 TECNOCOM RM-22964
               ELSIF PR_POLIZA.CINDTIPOPRO = '2' AND NVL(cINDPRORROGA,'N') = 'N' THEN
                  --
                  FOR Z IN (SELECT SUM(MC.PrimaFactMoneda) Monto_Canc,SUM(MC.PrimaFactMoneda) nMtoPrimaBruta,SUM(MC.PrimaFactMoneda) Monto_Cp
                            FROM   MOD_COBERT MC
                            WHERE  MC.IDEPOL       = X.IdePol
                            AND    MC.NUMCERT      = X.NumCert
                            AND    MC.CODRAMOCERT  = X.CodRamoCert
                            AND    MC.StsModCobert IN ('ACT', 'LIQ', 'REV', 'ANU')) LOOP
                      --
                      IF cTipoAnul <> 'M' THEN
                         --<<CONSIS - EFVC  REDMINE 22583- 21858. Redondeo Moneda Extranjera
                         IF PR_POLIZA.CCODMONEDA <> PR_VINISUSC.CCODMONEDA THEN
                            nMonto_Canc  := NVL (nMonto_Canc, 0) + NVL (Z.Monto_Canc*(dFecFinVig-dFecAnulPol)/(dFecFinVig-dFecIniVig), 0);
                            nMonto_Cp    := NVL (nMonto_Cp, 0)   + NVL (Z.Monto_Cp*(dFecFinVig-dFecAnulPol)/(dFecFinVig-dFecIniVig), 0);
                         ELSE
                            nMonto_Canc  := NVL (nMonto_Canc, 0) + NVL (ROUND(Z.Monto_Canc*(dFecFinVig-dFecAnulPol)/(dFecFinVig-dFecIniVig),0), 0);
                            nMonto_Cp    := NVL (nMonto_Cp, 0)   + NVL (ROUND(Z.Monto_Cp*(dFecFinVig-dFecAnulPol)/(dFecFinVig-dFecIniVig),0), 0);
                         END IF;
                         --
                      END IF;
                      --<<CONSIS - EFVC  REDMINE 22583- 21858. Redondeo Moneda Extranjera
                      IF PR_POLIZA.CCODMONEDA <> PR_VINISUSC.CCODMONEDA THEN
                         nTotRecaDctoRamo  := nTotRecaDctoRamo + (NVL (Z.MONTO_CANC, 0) - Z.nMtoPrimaBruta);
                         nPrimaProDev      := nPrimaProDev + NVL (Z.Monto_Canc*(dFecFinVig-dFecAnulPol)/(dFecFinVig-dFecIniVig), 0);
                      ELSE
                         nTotRecaDctoRamo  := nTotRecaDctoRamo + (NVL (ROUND(Z.MONTO_CANC,0), 0) - Z.nMtoPrimaBruta);
                         nPrimaProDev      := nPrimaProDev + NVL (ROUND(Z.Monto_Canc*(dFecFinVig-dFecAnulPol)/(dFecFinVig-dFecIniVig),0), 0);
                      END IF;
                      --
                  END LOOP;  -->>14/12/2017 TECNOCOM RM-22964
                  --
               --<<COTECSA *23718* FM 04/08/2018 SE CREA ESTE PROCESO PARA REALIZAR LAS DEVOLUCIONES DE PRORROGA DE PRORRATA TIPO 2 CORRECTAMENTE
               ELSIF PR_POLIZA.CINDTIPOPRO = '2' AND NVL(cINDPRORROGA,'N') = 'S' THEN
                  --
                  FOR Z IN (SELECT MC.PrimaFactMoneda Monto_Canc,MC.PrimaFactMoneda nMtoPrimaBruta,MC.PrimaFactMoneda Monto_Cp
                                  ,MC.FECINIVALID,MC.FECFINVALID,MC.CODCOBERTMVTO
                            FROM   MOD_COBERT MC
                            WHERE  MC.IDEPOL       = X.IdePol
                            AND    MC.NUMCERT      = X.NumCert
                            AND    MC.CODRAMOCERT  = X.CodRamoCert
                            AND    MC.StsModCobert IN ('ACT', 'LIQ', 'REV', 'ANU')
                            ORDER BY MC.CODCOBERTMVTO) LOOP
                      -->>24317 toma fechas incorrectas cuando el MC lleva horas
                      dFecIniVig := TRUNC(Z.FECINIVALID);
                      dFecFinVig := TRUNC(Z.FECFINVALID);
                      -->>24317
                      IF cTipoAnul <> 'M' THEN
                         --
                         IF PR_POLIZA.CCODMONEDA <> PR_VINISUSC.CCODMONEDA THEN
                            IF dFecAnulPol < dFecFinVig THEN
                               nMonto_Canc  := NVL (nMonto_Canc, 0) + NVL (Z.Monto_Canc*(Z.FECFINVALID - dFecAnulPol)/(Z.FECFINVALID - Z.FECINIVALID), 0);
                               nMonto_Cp    := NVL (nMonto_Cp, 0)   + NVL (Z.Monto_Cp  *(Z.FECFINVALID - dFecAnulPol)/(Z.FECFINVALID - Z.FECINIVALID), 0);
                            END IF;   
                         ELSE 
                            IF dFecAnulPol < dFecFinVig THEN 
                               nMonto_Canc  := NVL (nMonto_Canc, 0) + NVL (ROUND(Z.Monto_Canc*(Z.FECFINVALID - dFecAnulPol)/(Z.FECFINVALID - Z.FECINIVALID),0), 0);
                               nMonto_Cp    := NVL (nMonto_Cp, 0)   + NVL (ROUND(Z.Monto_Cp  *(Z.FECFINVALID - dFecAnulPol)/(Z.FECFINVALID - Z.FECINIVALID),0), 0);
                            END IF;   
                         END IF;
                         --
                      END IF;
                      --
                      IF PR_POLIZA.CCODMONEDA <> PR_VINISUSC.CCODMONEDA THEN
                         IF dFecAnulPol < dFecFinVig THEN
                            nTotRecaDctoRamo  := nTotRecaDctoRamo + (NVL (Z.MONTO_CANC, 0) - Z.nMtoPrimaBruta);
                            nPrimaProDev      := nPrimaProDev + NVL (Z.Monto_Canc*(Z.FECFINVALID - dFecAnulPol)/(Z.FECFINVALID - Z.FECINIVALID), 0);
                         END IF;   
                      ELSE
                         IF dFecAnulPol < dFecFinVig THEN
                            nTotRecaDctoRamo  := nTotRecaDctoRamo + (NVL (ROUND(Z.MONTO_CANC,0), 0) - Z.nMtoPrimaBruta);
                            nPrimaProDev      := nPrimaProDev + NVL (ROUND(Z.Monto_Canc*(Z.FECFINVALID - dFecAnulPol)/(Z.FECFINVALID - Z.FECINIVALID),0), 0);
                         END IF;   
                      END IF;
                      --
                  END LOOP;
                  --
               -->>COTECSA *23718* FM 04/08/2018 SE CREA ESTE PROCESO PARA REALIZAR LAS DEVOLUCIONES DE PRORROGA DE PRORRATA TIPO 2 CORRECTAMENTE
               ELSE
                  BEGIN
                    SELECT pf.NroGiros, pr.TipoAnul
                    INTO   cCantGiro, cTipoAnulProd
                    FROM   PLAN_FINANCIAMIENTO pf, PRODUCTO pr, POLIZA po
                    WHERE  pr.CodProd = po.CodProd
                    AND    po.CodPlanFrac = pf.CodPlan
                    AND    po.ModPlanFrac = pf.ModPlan
                    AND    po.IdePol      = nIdePol;
                  EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                      cCantGiro     := 1;
                      cTipoAnulProd := 'P';
                  END;
                  --Anulaci�n con D�as Comerciales. DV. 02/07/2014
                  IF cCantGiro > 1 AND cTipoAnulProd = 'C'  THEN
                    BEGIN
                      SELECT f.DiasRecibo, F.CantMeses
                      INTO   nDiasRecibo, nCantMeses
                      FROM   FRECUENCIA f
                      WHERE  f.CantRecibos = cCantGiro
                      AND    RowNum = 1;
                    EXCEPTION
                      WHEN NO_DATA_FOUND THEN
                         RAISE_APPLICATION_ERROR (-20100,'Anulaci�n Comercial - No Est� Configurado Factor para Cantidad de Recibos: '||cCantGiro);
                    END;
                    cIndAnulComer := 'S';
                  ELSE
                    cIndAnulComer := 'N';
                  END IF;
                  BEGIN
                    SELECT SUM(MC.PriMinAjuste)
                    INTO   nPrima_Minima
                    FROM   MOD_COBERT MC
                    WHERE  MC.IDEPOL = X.IdePol
                    AND    MC.NUMCERT = X.NumCert
                    AND    MC.CODRAMOCERT = X.CodRamoCert
                    AND    MC.StsModCobert IN ('ACT', 'LIQ', 'REV', 'ANU'); -->> BBVA - 1.0.1.1 EFBC RM22491
                  END;
                  IF NVL(nPrima_Minima,0) > 0 THEN
                      cCodFormCan:='S';
                  END IF;

                 BEGIN
                   SELECT RES.CodPlanFracc, RES.NumModPlanFracc
                     INTO cCodplanFrac, cModplanfrac
                     FROM RESP_PAGO RES
                    WHERE RES.IdePol = X.IdePol
                      AND RES.NumCert = X.NumCert;
                 END;

                  BEGIN
                    SELECT L.CODLVAL, P.Nrogiros
                      INTO cTipoPago, nCantPago
                      FROM PLAN_FINANCIAMIENTO P, LVAL L
                     WHERE L.TIPOLVAL= 'CANTREC'
                       AND P.NROGIROS= L.DESCRIP
                       AND P.CODPLAN = cCodplanFrac
                       AND P.MODPLAN = cModplanfrac
                       AND L.CODLVAL IN ('M','S','A','T','X');
                  END;


                  PR_MOD_COBERT.PRORRATA_FRONTWEB(pFecIniPol       => dFecIniVig,
                                                  pFecFinPol       => dFecFinVig,
                                                  pFecCorte        => dFecAnulPol,
                                                  pCodFormPago     => cTipoPago,
                                                  nNumMeses        => nNumMeses,
                                                  nProGiroPendCanc => nProGiroPendCanc,
                                                  nProPrimaDeven   => nProPrimaDeven,
                                                  nProPrimaDevol   => nProPrimaDevol);
            --<< BBVA - 1.57.1.15 - EFBC RM21875 - Se agrega calculo de nProPrimaDevol para Prorrata 8
            --<< BBVA - 1.0.1.1 EFBC RM22491 - Se comenta este c�digo dado que se incluy� este c�lculo en PR_MOD_COBERT.PRORRATA_FRONTWEB
            -->> BBVA - 1.57.1.15 - EFBC RM21875
                  nPrimaAnualDev := 0;
                  FOR Y IN (SELECT NVL (ROUND (((DECODE (NVL (cCodFormCan, 'N'), 'N', MC.PrimaMoneda, MC.PrimaFactMoneda)
                                                 / DECODE (NVL (cCodFormCan, 'N'),
                                                          -- 'N', PR_MOD_COBERT.NumDIAS (TRUNC (MC.FecIniValid), TRUNC (MC.FecFinValid)),
                                                          'N',DECODE(POL.IndTipoPro,'7',TRUNC (MC.FecFinValid) - TRUNC (MC.FecIniValid), PR_MOD_COBERT.NumDIAS (TRUNC (MC.FecIniValid), TRUNC (MC.FecFinValid))),
                                                           TRUNC (MC.FecFinValid) - TRUNC (MC.FecIniValid)))
                                                * DECODE(cIndAnulComer,'S',nDiasRecibo*(MONTHS_BETWEEN(TRUNC (MC.FecFinValid),TRUNC (dFecAnulPol) + DECODE (POL.IndTipoPro, '1', 1, 0))/nCantMeses),
                                                  (TRUNC (MC.FecFinValid) - TRUNC (dFecAnulPol) + DECODE (POL.IndTipoPro, '1', 1, 0)))
                                               ),
                                               2)
                                        * DECODE (SIGN (NVL (MP.MtoRecaDcto, 0)),
                                                  0, 1,
                                                  -1, (1 - (ABS (MP.MtoRecaDcto) / (ABS ((MP.MtoRecaDcto * -1) + MP.MtoMoneda)))),
                                                  1, (1 + (ABS (MP.MtoRecaDcto) / (ABS ((MP.MtoRecaDcto * -1) + MP.MtoMoneda))))),
                                        0) Monto_Canc,
                                   NVL (ROUND (((DECODE (NVL (cCodFormCan, 'N'), 'N', MC.PrimaMoneda, MC.PrimaFactMoneda)
                                                 / DECODE (NVL (cCodFormCan, 'N'),
                                                           'N', PR_MOD_COBERT.NumDIAS (TRUNC (MC.FecIniValid), TRUNC (MC.FecFinValid)),
                                                           TRUNC (MC.FecFinValid) - TRUNC (MC.FecIniValid)))
                                                * (TRUNC (MC.FecFinValid) - TRUNC (dFecAnulPol) + DECODE (POL.IndTipoPro, '1', 1, 0))),
                                               2),
                                        0) nMtoPrimaBruta,
                                   NVL (ROUND (((MC.PrimaMoneda --<<BBVA Consis 19/07/2013 Se deja la variable de la prima moneda, para que tome el caluclo correcto en la devocuion del calculo a Corto Plazo, al igual que tome la fecha en que se esta haciendo la Anulacion PrimaFactMoneda
                                                 / (DECODE ((TRUNC (CR.FecFinValid) - TRUNC (CR.FecIniValid)),0,1,(TRUNC (CR.FecFinValid) - TRUNC (CR.FecIniValid)))
                                                 + DECODE (POL.IndTipoPro, '1', 1, 0)))
                                                * (TRUNC (MC.FecFinValid) - TRUNC (dFecAnulPol) + DECODE (POL.IndTipoPro, '1', 1, 0))),
                                               2)
                                        * DECODE (SIGN (NVL (MP.MtoRecaDcto, 0)),
                                                  0, 1,
                                                  -1, (1 - (ABS (MP.MtoRecaDcto) / (ABS ((MP.MtoRecaDcto * -1) + MP.MtoMoneda)))),
                                                  1, (1 + (ABS (MP.MtoRecaDcto) / (ABS ((MP.MtoRecaDcto * -1) + MP.MtoMoneda))))),
                                        0) Monto_Cp,
                                   MC.IDECOBERT,
                                   MC.PRIMAMONEDA,
                                   MC.PRIMA
                            FROM   POLIZA POL, MOD_COBERT MC, COBERT_CERT CC, CERT_RAMO CR, MOV_PRIMA MP
                            WHERE  POL.IdePol     = X.IdePol
                            AND    CR.IdePol      = X.IdePol
                            AND    CR.CodRamoCert = X.CodRamoCert
                            AND    CR.NumCert     = X.NumCert
                            AND    CC.IdePol      = CR.IdePol
                            AND    CC.CodRamoCert = CR.CodRamoCert
                            AND    CC.NumCert     = CR.NumCert
                            AND    CC.IdePol      = MC.IdePol
                            AND    CC.NumCert     = MC.NumCert
                            AND    CC.CodRamoCert = MC.CodRamoCert
                            AND    CC.IdeCobert   = MC.IdeCobert
                            AND    CC.StsCobert IN ('ACT', 'LIQ', 'REV', 'ANU')   --GM AGREGADO IN ('ACT','LIQ') -->> BBVA - 1.0.1.1 EFBC RM22491
                            AND    MC.NumMod IN (
                                      SELECT MAX (NumMod)
                                      FROM   MOD_COBERT M, RECIBO R, OPER_POL O
                                      WHERE  M.IdeCobert          = MC.IdeCobert
                                      AND    NVL (M.PrimaFactMoneda, 0) <> 0
                                      AND    M.StsModCobert IN ('ACT', 'LIQ', 'REV', 'ANU')   --GM AGREGADO IN ('ACT','LIQ') -->> BBVA - 1.0.1.1 EFBC RM22491
                                      AND    M.IdeMovPrima        = R.IdeMovPrima
                                      AND    O.NumOper            = R.NumOper
                                      AND    O.NumCert            = R.NumCert
                                      AND    NVL (O.IndAnul, 'N') = 'N'
                                      AND    ((POL.CodFormPago    = 'M' AND TRUNC (MC.FecIniValid) >= dFecAnulPol) OR
                                            (POL.CodFormPago <> 'M'))
                                      ) --dv. 12/08/2013
                            AND    MP.IdeMovPrima = MC.IdeMovPrima
                              AND    ((POL.CodFormPago = 'M' AND TRUNC (MC.FecIniValid) >= dFecAnulPol) OR
                                  (POL.CodFormPago <> 'M'))
                            UNION
                            SELECT NVL (ROUND (((DECODE (NVL (cCodFormCan, 'N'), 'N', MC.PrimaMoneda, MC.PrimaFactMoneda)
                                                 / DECODE (NVL (cCodFormCan, 'N'),
                                                          -- 'N', PR_MOD_COBERT.NumDIAS (TRUNC (MC.FecIniValid), TRUNC (MC.FecFinValid)),
                                                          'N',DECODE(POL.IndTipoPro,'7',TRUNC (MC.FecFinValid) - TRUNC (MC.FecIniValid), PR_MOD_COBERT.NumDIAS (TRUNC (MC.FecIniValid), TRUNC (MC.FecFinValid))),
                                                           TRUNC (MC.FecFinValid) - TRUNC (MC.FecIniValid)))
                                                * DECODE(cIndAnulComer,'S',nDiasRecibo*(MONTHS_BETWEEN(TRUNC (MC.FecFinValid),TRUNC (dFecAnulPol) + DECODE (POL.IndTipoPro, '1', 1, 0))/nCantMeses),
                                                  (TRUNC (MC.FecFinValid) - TRUNC (dFecAnulPol) + DECODE (POL.IndTipoPro, '1', 1, 0)))
                                               ),
                                               2)
                                        * DECODE (SIGN (NVL (MP.MtoRecaDcto, 0)),
                                                  0, 1,
                                                  -1, (1 - (ABS (MP.MtoRecaDcto) / (ABS ((MP.MtoRecaDcto * -1) + MP.MtoMoneda)))),
                                                  1, (1 + (ABS (MP.MtoRecaDcto) / (ABS ((MP.MtoRecaDcto * -1) + MP.MtoMoneda))))),
                                        0) Monto_Canc,
                                   NVL (ROUND (((DECODE (NVL (cCodFormCan, 'N'), 'N', MC.PrimaMoneda, MC.PrimaFactMoneda)
                                                 / DECODE (NVL (cCodFormCan, 'N'),
                                                           'N', PR_MOD_COBERT.NumDIAS (TRUNC (MC.FecIniValid), TRUNC (MC.FecFinValid)),
                                                           TRUNC (MC.FecFinValid) - TRUNC (MC.FecIniValid)))
                                                * (TRUNC (MC.FecFinValid) - TRUNC (dFecAnulPol) + DECODE (POL.IndTipoPro, '1', 1, 0))),
                                               2),
                                        0) nMtoPrimaBruta,
                                   NVL (ROUND (((MC.PrimaFactMoneda
                                                 / (DECODE ((TRUNC (CR.FecFinValid) - TRUNC (CR.FecIniValid)),0,1,(TRUNC (CR.FecFinValid) - TRUNC (CR.FecIniValid)))
                                                 + DECODE (POL.IndTipoPro, '1', 1, 0)))
                                                * (TRUNC (MC.FecFinValid) - TRUNC (MC.FecIniValid) + DECODE (POL.IndTipoPro, '1', 1, 0))),
                                               2)
                                        * DECODE (SIGN (NVL (MP.MtoRecaDcto, 0)),
                                                  0, 1,
                                                  -1, (1 - (ABS (MP.MtoRecaDcto) / (ABS ((MP.MtoRecaDcto * -1) + MP.MtoMoneda)))),
                                                  1, (1 + (ABS (MP.MtoRecaDcto) / (ABS ((MP.MtoRecaDcto * -1) + MP.MtoMoneda))))),
                                        0) Monto_Cp,
                                   MC.IDECOBERT,
                                   MC.PRIMAMONEDA,
                                   MC.PRIMA
                            FROM   POLIZA POL, MOD_COBERT MC, COBERT_BIEN CC, CERT_RAMO CR, BIEN_CERT BC, MOV_PRIMA MP
                            WHERE  POL.IdePol     = X.IdePol
                            AND    CR.IdePol      = X.IdePol
                            AND    CR.CodRamoCert = X.CodRamoCert
                            AND    CR.NumCert     = X.NumCert
                            AND    BC.IdePol      = CR.IdePol
                            AND    BC.CodRamoCert = CR.CodRamoCert
                            AND    BC.NumCert     = CR.NumCert
                            AND    CC.IdeBien     = BC.IdeBien
                            AND    CC.IdeCobert   = MC.IdeCobert
                            AND    CC.StsCobert IN ('ACT', 'LIQ', 'REV', 'ANU')   --GM AGREGADO IN ('ACT','LIQ') -->> BBVA - 1.0.1.1 EFBC RM22491
                            AND    MC.NumMod IN (
                                      SELECT MAX (NumMod)
                                      FROM   MOD_COBERT M, RECIBO R, OPER_POL O
                                      WHERE  M.IdeCobert          = MC.IdeCobert
                                      AND    NVL (M.PrimaFactMoneda, 0) <> 0
                                      AND    M.StsModCobert IN ('ACT', 'LIQ', 'REV', 'ANU')   --GM AGREGADO IN ('ACT','LIQ') -->> BBVA - 1.0.1.1 EFBC RM22491
                                      AND    M.IdeMovPrima        = R.IdeMovPrima
                                      AND    O.NumOper            = R.NumOper
                                      AND    O.NumCert            = R.NumCert
                                      AND    NVL (O.IndAnul, 'N') = 'N'
                                      AND    ((POL.CodFormPago = 'M' AND TRUNC (MC.FecIniValid) >= dFecAnulPol) OR
                                            (POL.CodFormPago <> 'M'))
                                      ) --dv. 12/08/2013
                            AND    MP.IdeMovPrima = MC.IdeMovPrima
                              AND    ((POL.CodFormPago = 'M' AND TRUNC (MC.FecIniValid) >= dFecAnulPol) OR
                                  (POL.CodFormPago <> 'M'))
                            UNION
                            SELECT NVL (ROUND (((DECODE (NVL (cCodFormCan, 'N'), 'N', MC.PrimaMoneda, MC.PrimaFactMoneda)
                                                 / DECODE (NVL (cCodFormCan, 'N'),
                                                          -- 'N', DECODE(POL.IndTipoPro,'5',360,PR_MOD_COBERT.NumDIAS (TRUNC (MC.FecIniValid), TRUNC (MC.FecFinValid))),
                                                          'N', DECODE(POL.IndTipoPro,'5',360,'7',TRUNC (MC.FecFinValid) - TRUNC (MC.FecIniValid), DECODE(POL.IndTipoPro,'5',360,PR_MOD_COBERT.NumDIAS (TRUNC (MC.FecIniValid), TRUNC (MC.FecFinValid)))),
                                                           TRUNC (MC.FecFinValid) - TRUNC (MC.FecIniValid)))
                                                * DECODE(cIndAnulComer,'S',nDiasRecibo*(MONTHS_BETWEEN(TRUNC (MC.FecFinValid),TRUNC (dFecAnulPol) + DECODE (POL.IndTipoPro, '1', 1, 0))/nCantMeses),
                                                   DECODE(POL.IndTipoPro,'5',ABS(ROUND(MONTHS_BETWEEN(TRUNC (MC.FecFinValid),TRUNC (dFecAnulPol))))*30,
                                                  (TRUNC (MC.FecFinValid) - TRUNC (dFecAnulPol) + DECODE (POL.IndTipoPro, '1', 1, 0))))
                                                ),
                                               2)
                                        * DECODE (SIGN (NVL (MP.MtoRecaDcto, 0)),
                                                  0, 1,
                                                  -1, (1 - (ABS (MP.MtoRecaDcto) / (ABS ((MP.MtoRecaDcto * -1) + MP.MtoMoneda)))),
                                                  1, (1 + (ABS (MP.MtoRecaDcto) / (ABS ((MP.MtoRecaDcto * -1) + MP.MtoMoneda))))),
                                        0) Monto_Canc,
                                   NVL (ROUND (((DECODE (NVL (cCodFormCan, 'N'), 'N', MC.PrimaMoneda, MC.PrimaFactMoneda)
                                                 / DECODE (NVL (cCodFormCan, 'N'),
                                                           'N', DECODE(POL.IndTipoPro,'5',360,PR_MOD_COBERT.NumDIAS (TRUNC (MC.FecIniValid), TRUNC (MC.FecFinValid))),
                                                           TRUNC (MC.FecFinValid) - TRUNC (MC.FecIniValid)))
                                                * DECODE(POL.IndTipoPro,'5',ABS(ROUND(MONTHS_BETWEEN(TRUNC (MC.FecFinValid),TRUNC (dFecAnulPol))))*30,
                                                  (TRUNC (MC.FecFinValid) - TRUNC (dFecAnulPol) + DECODE (POL.IndTipoPro, '1', 1, 0)))
                                                ),
                                               2),
                                        0) nMtoPrimaBruta,
                                   NVL (ROUND (((MC.PrimaFactMoneda
                                                 / (DECODE ((TRUNC (CR.FecFinValid) - TRUNC (CR.FecIniValid)),0,1,(TRUNC (CR.FecFinValid) - TRUNC (CR.FecIniValid)))
                                                 + DECODE (POL.IndTipoPro, '1', 1, 0)))
                                                * (TRUNC (MC.FecFinValid) - TRUNC (MC.FecIniValid) + DECODE (POL.IndTipoPro, '1', 1, 0))),
                                               2)
                                        * DECODE (SIGN (NVL (MP.MtoRecaDcto, 0)),
                                                  0, 1,
                                                  -1, (1 - (ABS (MP.MtoRecaDcto) / (ABS ((MP.MtoRecaDcto * -1) + MP.MtoMoneda)))),
                                                  1, (1 + (ABS (MP.MtoRecaDcto) / (ABS ((MP.MtoRecaDcto * -1) + MP.MtoMoneda))))),
                                        0) Monto_Cp,
                                   MC.IDECOBERT,
                                   MC.PRIMAMONEDA,
                                   MC.PRIMA
                            FROM   POLIZA POL, MOD_COBERT MC, COBERT_ASEG CC, CERT_RAMO CR, ASEGURADO ASE, MOV_PRIMA MP
                            WHERE  POL.IdePol = X.IdePol
                            AND    CR.IdePol = X.IdePol
                            AND    CR.CodRamoCert = X.CodRamoCert
                            AND    CR.NumCert = X.NumCert
                            AND    ASE.IdePol = CR.IdePol
                            AND    ASE.CodRamoCert = CR.CodRamoCert
                            AND    ASE.NumCert = CR.NumCert
                            AND    CC.IdeAseg = ASE.IdeAseg
                            AND    CC.IdeCobert = MC.IdeCobert
                            AND    CC.StsCobert IN ('ACT', 'LIQ', 'REV', 'ANU')   --GM AGREGADO IN ('ACT','LIQ') -->> BBVA - 1.0.1.1 EFBC RM22491
                            AND    MC.NumMod IN (
                                      SELECT MAX (NumMod)
                                      FROM   MOD_COBERT M, RECIBO R, OPER_POL O
                                      WHERE  M.IdeCobert          = MC.IdeCobert
                                      AND    NVL (M.PrimaFactMoneda, 0) <> 0
                                      AND    M.StsModCobert IN ('ACT', 'LIQ', 'REV', 'ANU')   --GM AGREGADO IN ('ACT','LIQ') -->> BBVA - 1.0.1.1 EFBC RM22491
                                      AND    M.IdeMovPrima        = R.IdeMovPrima
                                      AND    O.NumOper            = R.NumOper
                                      AND    O.NumCert            = R.NumCert
                                      AND    NVL (O.IndAnul, 'N') = 'N'
                                      AND    ((POL.CodFormPago    = 'M' AND TRUNC (MC.FecIniValid) >= dFecAnulPol) OR
                                             (POL.CodFormPago <> 'M'))  )
                                      AND    MP.IdeMovPrima = MC.IdeMovPrima
                                      AND    ((POL.CodFormPago = 'M' AND TRUNC (MC.FecIniValid) >= dFecAnulPol) OR
                                  (POL.CodFormPago <> 'M'))
                            ORDER BY Monto_Cp ASC) LOOP
               --   IF PR_POLIZA.CINDTIPOPRO <> '8' THEN
                  IF PR_POLIZA.CINDTIPOPRO NOT IN ('2','8') THEN --14/12/2017 TECNOCOM RM-22964
                     IF cTipoAnul <> 'M' THEN
                     --<<CONSIS - EFVC  REDMINE 22583- 21858. Redondeo Moneda Extranjera
                       IF PR_POLIZA.CCODMONEDA <> PR_VINISUSC.CCODMONEDA THEN
                         nMonto_Canc  := NVL (nMonto_Canc, 0) + NVL (Y.Monto_Canc, 0);
                         nMonto_Cp    := NVL (nMonto_Cp, 0)   + NVL (Y.Monto_Cp, 0);
                       ELSE
                         nMonto_Canc  := NVL (nMonto_Canc, 0) + NVL (ROUND(Y.Monto_Canc,0), 0);
                         nMonto_Cp    := NVL (nMonto_Cp, 0)   + NVL (ROUND(Y.Monto_Cp,0), 0);
                       END IF;
                     END IF;
                     --<<CONSIS - EFVC  REDMINE 22583- 21858. Redondeo Moneda Extranjera
                      IF PR_POLIZA.CCODMONEDA <> PR_VINISUSC.CCODMONEDA THEN
                         nTotRecaDctoRamo  := nTotRecaDctoRamo + (NVL (Y.MONTO_CANC, 0) - Y.nMtoPrimaBruta);
                         nPrimaProDev      := nPrimaProDev + NVL (Y.Monto_Canc, 0);
                      ELSE
                         nTotRecaDctoRamo  := nTotRecaDctoRamo + (NVL (ROUND(Y.MONTO_CANC,0), 0) - Y.nMtoPrimaBruta);
                         nPrimaProDev      := nPrimaProDev + NVL (ROUND(Y.Monto_Canc,0), 0);
                     END IF;
                  ELSE
                     --n_PrimaFact := (nProGiroPendCanc * ) + (nProPrimaDevol * n_Prima);

                     IF cTipoAnul <> 'M' THEN
                     --<<CONSIS - EFVC  REDMINE 22583- 21858. Redondeo Moneda Extranjera
                        IF PR_POLIZA.CCODMONEDA <> PR_VINISUSC.CCODMONEDA THEN
                           nMonto_Canc  := NVL (nMonto_Canc, 0) + NVL (nProGiroPendCanc*Y.PrimaMoneda, 0)+ NVL (nProPrimaDevol*Y.PrimaMoneda, 0);
                           --nMonto_Cp    := NVL (nMonto_Cp, 0) + NVL (ROUND(Y.Monto_Cp,0), 0);
                        ELSE
                           nMonto_Canc  := NVL (nMonto_Canc, 0) + NVL (ROUND(nProGiroPendCanc*Y.PrimaMoneda,0), 0)+ NVL (ROUND(nProPrimaDevol*Y.PrimaMoneda,0), 0);
                        END IF;
                     END IF;
                     --nTotRecaDctoRamo  := nTotRecaDctoRamo + (NVL (ROUND(Y.MONTO_CANC,0), 0) - Y.nMtoPrimaBruta);
                     --<<CONSIS - EFVC  REDMINE 22583- 21858. Redondeo Moneda Extranjera
                     IF PR_POLIZA.CCODMONEDA <> PR_VINISUSC.CCODMONEDA THEN
                        nPrimaProDev      := nPrimaProDev + NVL (nProGiroPendCanc*Y.PrimaMoneda, 0)+ NVL (nProPrimaDevol*Y.PrimaMoneda, 0);
                        nPrimaAnualDev    := nPrimaAnualDev + Y.PrimaMoneda;
                     ELSE
                        nPrimaProDev      := nPrimaProDev + NVL (ROUND(nProGiroPendCanc*Y.PrimaMoneda,0), 0)+ NVL (ROUND(nProPrimaDevol*Y.PrimaMoneda,0), 0);
                        nPrimaAnualDev    := nPrimaAnualDev + Y.PrimaMoneda;
                     END IF;
                  END IF;
                 END LOOP;
               END IF;
               -- --
               --<<CONSIS - EFVC  REDMINE 22583- 21858. Redondeo Moneda Extranjera
               IF PR_POLIZA.CCODMONEDA <> PR_VINISUSC.CCODMONEDA THEN
                  nPrimaDevolverCert  := nMonto_Canc;
                  nPrimaDevolverCP    := nMonto_Cp;
               ELSE
                  nPrimaDevolverCert  := ROUND(nMonto_Canc,0);
                  nPrimaDevolverCP    := ROUND(nMonto_Cp,0);
               END IF;

                nNumCert            := X.NumCert;

               BEGIN
                  SELECT MAX (R.IDEREC)
                  INTO   nIdeRec
                  FROM   RECIBO R
                  WHERE  R.IdePol = X.IdePol
                  AND    R.NumCert = X.NumCert
                  AND    R.CodRAMOCERT = X.CodRAMOCERT
                  AND    NOT EXISTS (SELECT 1
                                     FROM   OPER_POL O
                                     WHERE  O.NumOper = R.NumOper
                                     AND    O.TipoOp = 'ESM'
                                     AND    O.NumCert = R.NumCert);
               EXCEPTION
                 WHEN NO_DATA_FOUND THEN  nIdeRec := NULL;
                 WHEN OTHERS THEN
                      RAISE_APPLICATION_ERROR (-20288, PR.MENSAJE ('ABD', 20288, ' RECIBO ' || SQLERRM, NULL, NULL));
               END;
               IF cAnulCert = 'S' THEN
                  nPrimaDevolverCP    := 0;
                  nPrimaDevolverCert  := 0;
               END IF;
               cCodRamoCert        := X.CodRamoCert;
               nPrimaSiniestro     := 0;
               -- Si la cancelacion es por monto pENDiente no debe eFectuarse el calculo de reclamos
               IF cTipoAnul <> 'M' THEN
                  FOR v IN MOD_COBERT_SIN_Q LOOP
                     BEGIN
                        SELECT NVL (SUM (NVL (LEAST (TRUNC (ADD_MONTHS (g.Fecvct, p.frecuencia)), dFecFinVig) - TRUNC (GREATEST (g.Fecvct, dFecAnulPol)),0)),0)
                        INTO   nDiasDevolverSin
                        FROM   GIROS_FINANCIAMIENTO G, PLAN_FINANCIAMIENTO P, COND_FINANCIAMIENTO C
                        WHERE  (   dFecAnulPol BETWEEN g.Fecvct AND ADD_MONTHS (Fecvct, p.frecuencia)
                                OR g.FecVct > dFecAnulPol)
                        AND    g.StsGiro = 'COB'
                        AND    g.NumFinanc = c.NumFinanc
                        AND    p.modplan = c.modplan
                        AND    p.Codplan = C.Codplan
                        AND    c.IdePol = nIdePol
                        AND    c.NumOper = v.NumOper
                        AND    c.NumCert = v.NumCert;
                     EXCEPTION
                       WHEN NO_DATA_FOUND THEN  nDiasDevolverSin := NULL;
                     END;
                     IF v.OrigModCobert = 'C' THEN   -- Busca en Coberturas del CertIFicado
                        BEGIN
                           SELECT MAX (CP.IndDevPrima), MAX (P.IndDevConSin), MAX (C.CodCobert)
                           INTO   cIndDevPrima, cIndDevConSin, cCodCobert
                           FROM   POLIZA P, COBERT_PLAN_PROD CP, COBERT_CERT C
                           WHERE  CP.CodProd = P.Codprod
                           AND    P.IdePol = C.IdePol
                           AND    CP.CodRamoPlan = C.CodRamoCert
                           AND    CP.CodPlan = C.CodPlan
                           AND    CP.RevPlan = C.RevPlan
                           AND    CP.CodCobert = C.CodCobert
                           AND    C.IdeCobert = v.IdeCobert;
                        EXCEPTION
                          WHEN NO_DATA_FOUND THEN
                               cIndDevPrima := NULL;
                               cIndDevConSin:= NULL;
                               cCodCobert   := NULL;
                          WHEN OTHERS THEN
                               RAISE_APPLICATION_ERROR (-20288, PR.MENSAJE ('ABD', 20288, ' COBERT_PLAN_PROD ' || SQLERRM, NULL, NULL));
                        END;
                     ELSIF v.OrigModCobert = 'B' THEN   -- Busca en Coberturas del Bien
                        BEGIN
                           SELECT MAX (CP.IndDevPrima), MAX (P.IndDevConSin), MAX (C.CodCobert)
                           INTO   cIndDevPrima, cIndDevConSin, cCodCobert
                           FROM   POLIZA P, COBERT_PLAN_PROD CP, BIEN_CERT B, COBERT_BIEN C
                           WHERE  CP.CodProd = P.Codprod
                           AND    P.IdePol = B.IdePol
                           AND    CP.CodRamoPlan = B.CodRamoCert
                           AND    CP.CodPlan = B.CodPlan
                           AND    CP.RevPlan = B.RevPlan
                           AND    B.IdeBien = C.IdeBien
                           AND    CP.CodCobert = C.CodCobert
                           AND    C.IdeCobert = v.IdeCobert;
                        EXCEPTION
                          WHEN NO_DATA_FOUND THEN
                               cIndDevPrima  := NULL;
                               cIndDevConSin := NULL;
                               cCodCobert    := NULL;
                        END;
                     ELSIF v.OrigModCobert = 'A' THEN   -- Busca en Coberturas del Asegurado
                        BEGIN
                           SELECT MAX (CP.IndDevPrima), MAX (P.IndDevConSin), MAX (C.CodCobert)
                           INTO   cIndDevPrima, cIndDevConSin, cCodCobert
                           FROM   POLIZA P, COBERT_PLAN_PROD CP, ASEGURADO A, COBERT_ASEG C
                           WHERE  CP.CodProd = P.Codprod
                           AND    P.IdePol = A.IdePol
                           AND    CP.CodRamoPlan = A.CodRamoCert
                           AND    CP.CodPlan = A.CodPlan
                           AND    CP.RevPlan = A.RevPlan
                           AND    A.IdeAseg = C.IdeAseg
                           AND    CP.CodCobert = C.CodCobert
                           AND    C.IdeCobert = v.IdeCobert;
                        EXCEPTION
                          WHEN NO_DATA_FOUND THEN
                               cIndDevPrima := NULL;
                               cIndDevConSin:= NULL;
                               cCodCobert   := NULL;
                          WHEN OTHERS THEN
                               RAISE_APPLICATION_ERROR (-20288, PR.MENSAJE ('ABD', 20288, ' POLIZA ' || SQLERRM, NULL, NULL));
                        END;
                     END IF;
                     -- VerIFica si en configuracion la cobertura devuelve prima
                     IF cIndDevPrima = 'S' THEN
                        -- VerIFica si se quiere devolver coberturas aFectadas en STROS para la poliza
                        IF cIndDevConSin = 'S' THEN
                           --  VerIFica si el usuario tiene autorizacion para devolver
                           --  coberturas con siniestros.
                           PR_OPER_USUARIO.AUTORIZAR ('080', v.CodRamoCert, cCodCobert, NULL, NULL, NULL, 0, NULL);
                           nPrimaFactMoneda  := 0;   -- Se deja en cero para que
                                                     -- no se reste del monto a devolver
                        ELSE   -- Se suma para que se reste del monto a devolver
                           nPrimaFactMoneda  := v.PrimaFactMoneda;
                        END IF;
                     ELSE   -- Se suma para que se reste del monto a devolver
                        nPrimaFactMoneda  := v.PrimaFactMoneda;
                     END IF;
                     IF nPrimaFactMoneda <> 0 THEN
                        nPrimaSiniestro  := nPrimaSiniestro + ((nPrimaFactMoneda * nDiasDevolverSin) / TO_NUMBER (v.FecFinValid - v.FecIniValid)) * 1;
                     END IF;
                  END LOOP;

                     --<<CONSIS - EFVC  REDMINE 22583- 21858. Redondeo Moneda Extranjera
                  IF PR_POLIZA.CCODMONEDA <> PR_VINISUSC.CCODMONEDA THEN
                     nPrimadevolverCert  := (nPrimadevolverCert - nPrimaSiniestro);
                     nPrimaProDev        := nPrimaProDev - nPrimaSiniestro;
                     nPrimadevolverCP    := (nPrimadevolverCP - nPrimaSiniestro);
                  ELSE
                     nPrimadevolverCert  := (nPrimadevolverCert - ROUND(nPrimaSiniestro,0));
                     nPrimaProDev        := nPrimaProDev - ROUND(nPrimaSiniestro,0);
                     nPrimadevolverCP    := (nPrimadevolverCP - ROUND(nPrimaSiniestro,0));
                  END IF;


               END IF;
               IF NVL (nPorcCortoPlazo, 0) <> 0 THEN
                      --<<CONSIS - EFVC  REDMINE 22583- 21858. Redondeo Moneda Extranjera
                  IF PR_POLIZA.CCODMONEDA <> PR_VINISUSC.CCODMONEDA THEN
                     --<<BBVA Consis 19/07/2013 - Se Actualiza el valor de la variable para que tenga en cuenta la devolucion del caluclo del corto plazo
                     nPrimaDevolverCert  := ROUND(nPrimadevolverCert - (nPrimaDevolverCP * nPorcCortoPlazo) / 100,0);
                     --<<BBVA Consis 19/07/2013
                  ELSE
                     nPrimaDevolverCert  := nPrimadevolverCert - (nPrimaDevolverCP * nPorcCortoPlazo) / 100;
                  END IF;

               END IF;
               nNumOperAnu         := PR_OPERACION.nNumOper;
               IF cTipoMov = 'D' THEN
                  nNumCertRamo  := nNumCertRamo + 1;   -- Contador de Ramos del certIFicado
                  BEGIN
                     SELECT 1
                     INTO   nExiste
                     FROM   OPER_POL
                     WHERE  NumOper = PR_OPERACION.nNumOper
                     AND    NumCert = X.NumCert;
                  EXCEPTION
                     WHEN NO_DATA_FOUND THEN nExiste  := 0;
                     WHEN TOO_MANY_ROWS THEN nExiste  := 1;
                  END;
                  IF cAnulCert = 'S' THEN
                     nPrimaDevolverCert  := 0;
                     nMtoActCert         := 0;
                  END IF;
                  -- Si la cancelacion es por monto PENDiente se verIFican
                  -- las dIFerencias de centavos
                  IF cTipoAnul = 'M' THEN
                     nMonto_Canc_CertN  := nMonto_Canc_CertN - nPrimaDevolverCert;
                     -- Si es el ultimo ramo del certIFicado verIFicara las dIFerencias de centavos
                     IF     nCantCertRamo = nNumCertRamo AND nMonto_Canc_CertN <> 0 THEN
                        IF ABS (nMonto_Canc_CertN) < .05 THEN
                           nPrimaDevolverCert  := nPrimaDevolverCert + nMonto_Canc_CertN;
                        ELSE
                           RAISE_APPLICATION_ERROR (-20100,' NO Existe la prima pENDiente.IdePol y CertIFicado ' || x.IdePol || ' y ' || x.NumCert || ' '
                                                        || ' DIVIDE ' || nDIVIDE || ' (Cancelacion por Monto PENDiente).' || nMonto_Canc_CertN
                                                        || ' CERT DEV ' || nPrimaDevolverCert || ' PMA SIN ' || nPRIMASINIESTRO || ' AnulCERT '
                                                        || cAnulCERT || ' 1: ' || nMonto_Canc_Cert || ' 2: ' || nMonto_Canc || ' RAMO ' || X.CodRAMOCERT);
                        END IF;
                     END IF;
                  END IF;
                  --
                  IF nExiste = 0 THEN
                     --nNumENDoso  := PR_POLIZA.NumERO_ENDOSO (cCodProd, cCodOfiemi, 'CANC');
                     BEGIN
                        SELECT MIN (FecIniVig)
                        INTO   dFecIniRec
                        FROM   RECIBO
                        WHERE  NumOper = PR_OPERACION.nNumOper;
                     EXCEPTION
                        WHEN NO_DATA_FOUND THEN
                           dFecIniRec  := SYSDATE;
                     END;
                     --Erick Zoque 20/05/2013 E_CON_20120821_1_7
                     IF dFecIniRec < PR_FECHAS_EQUIVALENTES.FECHA_EMISION (dFecAnulPol, PR_POLIZA.cCodCia,'001') THEN
                        PR_OPER_USUARIO.AUTORIZAR ('127', NULL, NULL, NULL, NULL, NULL, 0, NULL, cCodProd, 'ANU', dFecIniRec);
                     END IF;
                     dFecMov := PR_FECHAS_EQUIVALENTES.FECHA_EMISION (SYSDATE, PR_POLIZA.cCodcia,'001');
                     --<< COTECSA-MX 05/03/2018 *22067* MERGE 1.66 jarz 12/08/2016. Deje el valor del llamado
                     --Merge de la Version v 1.0.1.3.1.2 con la Version v 1.0.1.4.
                     IF cCodProd = 'VIPU' AND  PR_POLIZA.pMontoDevVipu > 0 THEN
                        nPrimaDevolverCert := PR_POLIZA.pMontoDevVipu;
                        nPrimaProDev       := PR_POLIZA.pMontoDevVipu;
                        nPrimaAnualDev     := 0;
                        nMtoActCert        := 0;
                     END IF;
                     -->> COTECSA-MX 05/03/2018 *22067* MERGE 1.66 jarz 12/08/2016. Deje el valor del llamado
                      --<<CONSIS - EFVC  REDMINE 22583- 21858. Redondeo Moneda Extranjera
                     IF PR_POLIZA.CCODMONEDA <> PR_VINISUSC.CCODMONEDA THEN
                          INSERT INTO OPER_POL
                                                   (IdePol, NumCert, NumOper, FecMov, MtoOper, TipoOp, MtoOperAnu,
                                                    NumEndoso, IndAnul, MtoOperAnuPro,MtoPrimaAnualDev)
                                            VALUES (nIdePol, X.NumCert, PR_OPERACION.nNumOper, dFecMov, NVL (nPrimaDevolverCert, 0) * -1, 'ANU', NVL (nMtoActCert, 0) * -1,
                                                    nNumENDoso, 'N', NVL (nPrimaProDev, 0) * -1,NVL(nPrimaAnualDev,0));
                     ELSE
                        INSERT INTO OPER_POL
                                    (IdePol, NumCert, NumOper, FecMov, MtoOper, TipoOp, MtoOperAnu,
                                     NumEndoso, IndAnul, MtoOperAnuPro,MtoPrimaAnualDev)
                             VALUES (nIdePol, X.NumCert, PR_OPERACION.nNumOper, dFecMov, NVL (ROUND(nPrimaDevolverCert,0), 0) * -1, 'ANU', NVL (ROUND(nMtoActCert,0), 0) * -1,
                                     nNumENDoso, 'N', NVL (ROUND(nPrimaProDev,0), 0) * -1,NVL(nPrimaAnualDev,0));
                     END IF;

                     UPDATE RECIBO SET TIPOOPE = 'ANU'
                     WHERE  IdePol = nIdePol
                     AND    NumCert = X.NumCert
                     AND    NumOper = PR_OPERACION.nNumOper;

                     PR_POLIZA.ORDEN_COA (nIdePol, PR_OPERACION.nNumOper, 'CANC', 'S');
                     PR_POLIZA.ACTUALIZA_ENDOSO (cCodProd, cCodOfiemi, 'CANC');
                  ELSE
                      --<<CONSIS - EFVC  REDMINE 22583- 21858. Redondeo Moneda Extranjera
                     IF PR_POLIZA.CCODMONEDA <> PR_VINISUSC.CCODMONEDA THEN
                        UPDATE OPER_POL
                        SET MtoOper = (MtoOper + NVL (nPrimaDevolverCert, 0) * -1),
                            MtoOperAnu = (MtoOperAnu + nMtoActCert * -1),
                            MtoOperAnuPro = (MtoOperAnuPro + NVL (nPrimaProDev, 0) * -1),
                            MtoPrimaAnualDev = MtoPrimaAnualDev+NVL(nPrimaAnualDev,0)
                        WHERE  NumOper = PR_OPERACION.nNumOper
                        AND    NumCert = X.NumCert;
                     ELSE
                        UPDATE OPER_POL
                        SET MtoOper = ROUND((MtoOper + NVL (nPrimaDevolverCert, 0) * -1),0),
                            MtoOperAnu = ROUND((MtoOperAnu + nMtoActCert * -1),0),
                            MtoOperAnuPro = ROUND((MtoOperAnuPro + NVL (nPrimaProDev, 0) * -1),0),
                            MtoPrimaAnualDev = ROUND(MtoPrimaAnualDev+NVL(nPrimaAnualDev,0),0)
                        WHERE  NumOper = PR_OPERACION.nNumOper
                        AND    NumCert = X.NumCert;
                     END IF;
                  END IF;
                  FOR RP IN C_RESP_PAGO_Q LOOP
                     BEGIN
                        SELECT DISTINCT (1)
                        INTO   nExiste
                        FROM   RESP_PAGO_MOV
                        WHERE  IdePol = RP.IdePol
                        AND    NumCert = X.NumCert
                        AND    NumOPER = PR_OPERACION.nNumOper
                        AND    CodCli = RP.CodCli;
                     EXCEPTION
                        WHEN NO_DATA_FOUND THEN
                           nExiste  := 0;
                     END;
                     IF NVL (nExiste, 0) = 0 THEN
                        INSERT INTO RESP_PAGO_MOV
                                    (IdePol, NumCert, CodCli, PorcPago, CodPlanFracc, NumModPlanFracc, NumOper,
                                     Direc, CodPais, CodEstado, CodCiudad, CodMunicipio, Telex, Fax, Zip, Telef1,
                                     Telef2, Telef3, CodCobrador, CodViaCobro)
                             VALUES (RP.IdePol, X.NumCert, RP.CodCli, RP.PorcPago, RP.CodPlanFracc, RP.NumModPlanFracc, PR_OPERACION.nNumOper,
                                     RP.Direc, RP.CodPais, RP.CodEstado, RP.CodCiudad, RP.CodMunicipio, RP.Telex, RP.Fax, RP.Zip, RP.Telef1,
                                     RP.Telef2, RP.Telef3, RP.CodCobrador, RP.CodViaCobro);
                     END IF;
                  END LOOP;
--                Se eval�a si realmente se generaron Responsables de Pago para la Operaci�n
--                Inicio EC - 23/01/2006
                  BEGIN
                     SELECT NVL(COUNT(*),0)
                       INTO nRespPag
                       FROM RESP_PAGO_MOV
                      WHERE IdePol  = X.IdePol
                        AND NumCert = X.NumCert
                        AND NumOper = PR_OPERACION.nNumOper;
                  EXCEPTION
                     WHEN NO_DATA_FOUND THEN
                          nRespPag := 0;
                  END;
                  IF NVL(nRespPag,0) = 0 THEN
                     RAISE_APPLICATION_ERROR (-20052, PR.MENSAJE ('ABD', 20052, 'No se Generaron Responsables de Pago para la Operaci�n  ' || PR_OPERACION.nNumOper, NULL, NULL));
                  END IF;
--                Fin EC - 23/01/2006
                  IF     cIndCobro = 'S'
                     AND NVL (nPrimaDevolverCert, 0) <> 0 THEN
                     PR_COND_FINANCIAMIENTO.GENERAR (nIdePol, X.NumCert, PR_OPERACION.nNumOper);
                  END IF;
               ELSE
                  IF NVL (cTipoAnul, ' ') = 'M' THEN
                  --<<CONSIS - EFVC  REDMINE 22583- 21858. Redondeo Moneda Extranjera
                     IF PR_POLIZA.CCODMONEDA <> PR_VINISUSC.CCODMONEDA THEN
                        nMtoActCert         := nMonto_Canc_Ramo;
                        nPrimaDevolverCert  := nMonto_Oblig_Ramo;
                     ELSE
                        nMtoActCert         := ROUND(nMonto_Canc_Ramo,0);
                        nPrimaDevolverCert  := ROUND(nMonto_Oblig_Ramo,0);
                     END IF;
                  END IF;
                  BEGIN
                     SELECT 1
                     INTO   nExiste
                     FROM   T$_OPER_POL
                     WHERE  NumOper = PR_OPERACION.nNumOper
                     AND    NumCert = X.NumCert;
                  EXCEPTION
                     WHEN NO_DATA_FOUND THEN nExiste  := 0;
                     WHEN TOO_MANY_ROWS THEN nExiste  := 1;
                  END;
                  IF cAnulCert = 'S' THEN
                     nPrimaDevolverCert  := 0;
                     nMtoActCert         := 0;
                  END IF;
                  IF nExiste = 0 THEN
                     INSERT INTO T$_OPER_POL
                                 (IdePol, NumCert, NumOper, FecMov, MtoOper, TipoOp, MtoOperAnu)
                          VALUES (nIdePol, X.NumCert, PR_OPERACION.nNumOper, dFecMov, NVL (ROUND(nPrimaDevolverCert,0), 0), 'ANU', NVL (ROUND(nMtoActCert,0), 0));
                  ELSE
                     UPDATE T$_OPER_POL
                     SET MtoOper    = ROUND(MtoOper    + NVL (nPrimaDevolverCert, 0),0),
                         MtoOperAnu = ROUND(MtoOperAnu + NVL (nMtoActCert, 0),0)
                     WHERE  NumOper = PR_OPERACION.nNumOper
                     AND    NumCert = X.NumCert;
                  END IF;
               END IF;
               BEGIN
                  SELECT StsRec
                  INTO   cStsRec
                  FROM   RECIBO
                  WHERE  IdeRec = nIdeRec;
               EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    RAISE_APPLICATION_ERROR (-20205, PR.MENSAJE ('ABD', 20205, ' nIdeRec ' || nIdeRec, 'RECIBO', NULL));
                WHEN TOO_MANY_ROWS THEN
                    RAISE_APPLICATION_ERROR (-20341, PR.MENSAJE ('ABD', 20341, ' nIdeRec ' || nIdeRec, 'RECIBO', NULL));
                WHEN OTHERS THEN
                    RAISE_APPLICATION_ERROR (-20288, PR.MENSAJE ('ABD', 20288, ' RECIBO ' || SQLERRM, NULL, NULL));
               END;
               -- --
               IF nPrimaDevolverCert <> 0 AND cTipoMov = 'D' THEN
                  PR_MOV_PRIMA.GENERAR_DEVOLUCION (nIdeRec, nPrimaDevolverCert, dFecAnulPol, NULL);
                  nMtoActCertTot  := nMtoActCertTot + nMtoActCert;
                  IF nTotRecaDctoRamo <> 0 AND cTipoAnul <> 'M' THEN
                     BEGIN
                        SELECT MAX (IdeMovPrima)
                        INTO   nIdeMovPrima
                        FROM   RECIBO
                        WHERE  IdePol = nIdePol
                        AND    NumCert = X.NumCert
                        AND    CodRamoCert = X.CodRamoCert
                        AND    NumOper = PR_OPERACION.nNumOper;
                     EXCEPTION
                        WHEN NO_DATA_FOUND THEN nIdeMovPrima := NULL;
                     END;
                     IF nIdeMovPrima IS NOT NULL THEN
                        UPDATE MOV_PRIMA
                        SET MtoRecaDcto = nTotRecaDctoRamo * -1
                        WHERE  IdeMovPrima = nIdeMovPrima;
                     END IF;
                  END IF;
               END IF;
            END LOOP;
            ---- VerIFica que no queden dIFerencias en centavos
            IF     cTipoMov <> 'D'
               AND cTipoAnul = 'M' THEN
               BEGIN
                  SELECT MtoOper, MtoOperAnu
                  INTO   nMtoOper, nMtoOperAnu
                  FROM   T$_OPER_POL
                  WHERE  NumOper = PR_OPERACION.nNumOper
                  AND    NumCert = D.NumCert;
               EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                     nMtoOper    := NULL;
                     nMtoOperAnu := NULL;
                  WHEN TOO_MANY_ROWS THEN
                     RAISE_APPLICATION_ERROR (-20341, PR.MENSAJE ('ABD', 20341, ' PR_OPERACION.nNumOper ' || PR_OPERACION.nNumOper, 'T$_OPER_POL', NULL));
                  WHEN OTHERS THEN
                     RAISE_APPLICATION_ERROR (-20288, PR.MENSAJE ('ABD', 20288, ' T$_OPER_POL ' || SQLERRM, NULL, NULL));
               END;
                --<<CONSIS - EFVC  REDMINE 22583- 21858. Redondeo Moneda Extranjera
                IF nMonto_Canc_Cert <> ABS (nMtoOperAnu) THEN
                  IF PR_POLIZA.CCODMONEDA <> PR_VINISUSC.CCODMONEDA THEN
                     UPDATE T$_OPER_POL
                     SET MtoOperAnu = nMonto_Canc_Cert
                     WHERE  NumOper = PR_OPERACION.nNumOper
                     AND    NumCert = D.NumCert;
                  ELSE
                     UPDATE T$_OPER_POL
                     SET MtoOperAnu = ROUND(nMonto_Canc_Cert,0)
                     WHERE  NumOper = PR_OPERACION.nNumOper
                     AND    NumCert = D.NumCert;
                  END IF;
               END IF;
               IF nMonto_Oblig_Cert <> ABS (nMtoOper) THEN
               --<<CONSIS - EFVC  REDMINE 22583- 21858. Redondeo Moneda Extranjera
                  IF PR_POLIZA.CCODMONEDA <> PR_VINISUSC.CCODMONEDA THEN
                     UPDATE T$_OPER_POL
                     SET MtoOper = nMonto_Oblig_Cert
                     WHERE  NumOper = PR_OPERACION.nNumOper
                     AND    NumCert = D.NumCert;
                  ELSE
                     UPDATE T$_OPER_POL
                     SET MtoOper = ROUND(nMonto_Oblig_Cert,0)
                     WHERE  NumOper = PR_OPERACION.nNumOper
                     AND    NumCert = D.NumCert;
                  END IF;
               END IF;
               
             END IF;
            -- --
            nMtoCob          := 0;
            nMtoAct          := 0;
         END LOOP;
         -- Anulacion Definitiva
         IF cTipoMov = 'D' THEN
            FOR X IN FINANCIAMIENTOS LOOP
               UPDATE GIROS_FINANCIAMIENTO
               SET StsGiro = X.StsFin
               WHERE  NumFinanc = X.NumFinanc
               AND    StsGiro = DECODE (X.StsFin, 'ANU', 'ACT', 'ANU');
            END LOOP;
            BEGIN DELETE FINANCIAMIENTO_MODIFICADO c WHERE  IdePol = nIdePol AND    USUARIO = SUBSTR (USER, 1, 20); END;
         END IF;
         -- --
         nNumOper    := PR_OPERACION.nNumOper;
         -- --
         --ez 03/04/2014 no se anula ningun tipo de factura para que se cruce con las obligaciones dismin
         /*FOR Y IN RECIBOS_AnulAR LOOP
            IF cTipoMov = 'D' THEN
               IF   Y.NumFinanc IS NOT NULL AND Y.StsFin <> 'COB' THEN
                  IF  cIndRepPago = 'N' THEN
                      PR_COND_FINANCIAMIENTO.ANULAR (Y.NumFinanc, nIdeComp, 'N');
                  END IF;
               END IF;
            END IF;
         END LOOP;*/
         -- --
         FOR X IN ANULAR_RECIBOS_CESION LOOP
            IF cAnulCert = 'N' THEN
               BEGIN
                  SELECT StsRec
                  INTO   cStsRec
                  FROM   RECIBO
                  WHERE  IdeRec = X.IdeRec;
               EXCEPTION
                  WHEN NO_DATA_FOUND THEN cStsRec := NULL;
               END;
               IF cStsRec = 'ACT' AND cTipoMov = 'D' THEN
                  PR_RECIBO.ANULAR (X.IdeRec, dFecAnulPol);
               END IF;
            END IF;
         END LOOP;
         ------
         IF cTipoMov = 'D' THEN
            IF cAnulCert = 'S' THEN
               PR_FACTURA.GENERAR (nNumOper);
               --  Genera Obligacion por Retribucion Administrativa
               PR_FRACCIONAMIENTO.OBLG_RETADM (nIdePol, nNumOper);
            END IF;
          /*  -- AnulA DISTRIBUCION DE COASEGURO (DIST_COA) JR 06092018 SE CAMBIA DE LUGAR PARA LEER LA OBLIGACION GENERADA
            FOR CP IN DIST_COA_Q LOOP
               PR_DIST_COA.AnulAR (nIdePol, CP.CodAcepRiesgo);
            END LOOP;*/
            --<<COTECSA-FM 28/08/2017 *22327* SE COMENTA ESTE IF DEBIDO A QUE NO SE DEBE ANULAR FACTURA
            -- AnulA OBLIGACIONES VIGENTES
            /*IF cAnulCert = 'N' THEN
                --EZoque 11/04/2014
               /*FOR O IN OBLIGACION_DISMIN_Q LOOP
                  PR_OBLIGACION.AnulAR_SIS ('PRC', O.NumOblig);
               END LOOP;*/
               -- Anula Facturas por Retribucion Administrativa
               /*FOR F IN OBLIG_DISMIN_FACT_Q LOOP
                  PR_FACTURA.ANULAR (F.IdeFact);
               END LOOP;
            END IF;*/
            -->>COTECSA-FM 28/08/2017 *22327* SE COMENTA ESTE IF DEBIDO A QUE NO SE DEBE ANULAR FACTURA
            PR_MOV_PRIMA.cCodInterLider  := cCodInter;
            -- AnulA LA POLIZA Y LOS CERTIFICADOS
            IF nNumCertAnul IS NULL THEN
               PR_POLIZA.GENERAR_TRANSACCION (nIdePol, 'ANULA');
            END IF;

            BEGIN
               SELECT 1
               INTO   nExisteOp
               FROM   OPER_POL OP
               WHERE  OP.IDEPOL  = nIdePol
               AND    OP.NumOper = nNumOper;
            EXCEPTION
                 WHEN NO_DATA_FOUND THEN
                            nExisteOp:=0;
                 WHEN TOO_MANY_ROWS THEN
                            nExisteOp:=1;
            END;

            IF nExisteOp = 1 THEN
           -- Evento 'ANU' - 'EMI'
                 PR_COMISIONES_ESTIMA.CREAR_OPER_AUTO_EXTORNO_ESQ (nIdepol,'EMI',nNumOper,NULL,NULL,NULL,NULL,NULL);
                 PR_COMISIONES_CANALES.CREAR_OPER_AUTO_EXTORNO_ESQ (nIdepol,'EMI',nNumOper,NULL,NULL,NULL,NULL,NULL);

         BEGIN
           SELECT DISTINCT CodInter
           INTO   cCodInterC
           FROM   CANAL_POLIZA
           WHERE  IdePol = nIdePol
           AND    tipointer = 'U';
           EXCEPTION WHEN NO_DATA_FOUND THEN
                       cCodInterC  := NULL;
               WHEN TOO_MANY_ROWS THEN
                    RAISE_APPLICATION_ERROR (-20341,'Se encontraron multiples actores tipo (U) Comercializadora');
         END;

         IF cCodInterC IS NOT NULL THEN
               PR_COMISIONES_ESTIMA.CREAR_OPER_AUTO_EXTORNO_ESQ (nIdepol,'REC',nNumOper,NULL,NULL,NULL,NULL,NULL,NULL,cCodInterC);
             END IF;

       END IF;
            -- --
            FOR AP IN CERTIFICADO_Q LOOP
               -- Si es por monto se Anula con la Fec. de su primer giro pENDiente
               IF     cTipoAnul = 'M' AND cTipoSusc = 'C' THEN
                  PR_CERTIFICADO.ANULAR (nIdePol, AP.NumCert, AP.FecExc);
               ELSE
                  PR_CERTIFICADO.ANULAR (nIdePol, AP.NumCert, dFecAnulPol);
               END IF;
               -- A traves del MtoRecaDcto de MOV_PRIMA se ajusta la dIFerencia entre el
               -- mod_cobert y el oper_pol cuando se Anula por Monto PENDiente
               IF cTipoAnul = 'M' THEN
                  FOR I IN MOV_Anul LOOP
                     BEGIN
                       SELECT NVL (SUM (NVL (PrimaFactMoneda, 0)), 0)
                       INTO   nTotCert
                       FROM   MOD_COBERT
                       WHERE  IdeMovPrima = I.IdeMovPrima;
                     EXCEPTION
                        WHEN NO_DATA_FOUND THEN nTotCert := 0;
                     END;
                     UPDATE MOV_PRIMA
                     SET MtoRecaDcto = (ABS (I.MtoMoneda) - ABS (nTotCert)) * -1
                     WHERE  IdeMovPrima = I.IdeMovPrima;
                  END LOOP;
               END IF;
               -- --
            END LOOP;
         END IF;
         IF nPrimaAdevolver = 0 THEN
            FOR D IN RECIBO_PAGAR_SIN_C LOOP
                PR_RECIBO.PAGAR (D.IdeRec);
            END LOOP;
         END IF;
         IF cTipoMov = 'D' THEN
            --<I Defect 3208> Javier Asmat/06.07.2005
            --Condicionamos que solo genere cobro si hay devoluci�n
            --<<COTECSA-MX #23453--
            --IF nPrimaDevolverCert <> 0 OR nPrimadevolverCP <> 0 THEN
               FOR GEN IN GEN_FRACC_ANULA LOOP

                   BEGIN
                      SELECT NVL(SUM(MtoOper),0)
                      INTO   nMtoOperDEV
                      FROM   OPER_POL OP
                      WHERE  OP.NumOper = nNumOper
                      AND    OP.NumCert = GEN.NumCert;
                   EXCEPTION
                      WHEN OTHERS THEN
                          nMtoOperDEV := 0;
                   END;

                   IF nMtoOperDEV <> 0 THEN

                       BEGIN
                          SELECT CODCLI
                          INTO  cCodCliPol
                          FROM   POLIZA
                          WHERE  IDEPOL = nIdePol;
                       EXCEPTION
                          WHEN OTHERS THEN
                              cCodCliPol := NULL;
                       END;
                       BEGIN
                          SELECT CODCLI
                          INTO  cCodCliCert
                          FROM   certificado
                          WHERE  IDEPOL   = nIdePol
                          and     numcert = GEN.NumCert;
                       EXCEPTION
                         WHEN OTHERS THEN
                             cCodCliCert := NULL;
                       END;
                       PR.CREAR_DATOS_TERCERO(cCodCliPol);
                       PR.CREAR_DATOS_TERCERO(cCodcliCert);
                       PR_FRACCIONAMIENTO.EMITIR_ALTAS (nIdePol, GEN.NumCert, nNumOper, 'N');

                   END IF; --nMtoOperDEV

               END LOOP;
            --END IF;
            --COTECSA-MX #23453--
            -- REASEGURO
            FOR R IN RESEG LOOP
               PR_GEN_REA.SUMA_RAMO_REA (R.IdeRec);
            END LOOP;
            PR_GEN_REA.GENERAR_CTA_TEC (nNumOper);
            PR_DIST_REA.COPIA_FACULT (nIdePol, nIdePol, 'D');
            --
            -- JR 06092018 22484 AnulA DISTRIBUCION DE COASEGURO (DIST_COA)
            cCodAcepRiesgoBase  := PR_ACEP_RIESGO.ACEP_RIESGO_BASE (cCodcia); 
            FOR CP IN DIST_COA_Q LOOP
              PR_DIST_COA.AnulAR (nIdePol, CP.CodAcepRiesgo);
            END LOOP;
            --
         END IF;
         PR_OPERACION.TERMINAR;
      END IF;
      --Erick Zoque 04/09/2013 E_CON_20120815_1_1
      IF cTipoMov = 'D' THEN
        COMP_SUMAASEG(nIdePol,nNumOper,'ANU',nSumaAsegAnt);
      END IF;
      --Erick Zoque 20/05/2013 E_CON_20120821_1_7
      PR_FECHAS_EQUIVALENTES.CEVENTO := NULL;
      PR_POLIZA.cOperRev := NULL;
      /*IF cTipoMov = 'D' THEN
         nDias := PR_POLIZA.DFECFINVIG - TRUNC(SYSDATE);
         IF nDias > 365 THEN
            PR_ACREENCIA.VALIDA_DIFERIDA(nNumOper,'ANU');
         END IF;
      END IF;*/

      RETURN (nNumOper);
   END ANULAR_SIS;
   --//
   --TRASPASO
   PROCEDURE TRASPASO (nIdePol NUMBER, cCodInter VARCHAR2, cIndLider VARCHAR2, nPorcPart NUMBER, dFecTrasp DATE, cCodInterOld VARCHAR2) IS
      nIdeFact    NUMBER (14);
      nNumOper    NUMBER (14);
      nNumOperD   NUMBER (14);
      CURSOR FRACC_ACRE IS
         SELECT NumAcre, CF.NumOper
         FROM   COND_FINANCIAMIENTO CF, GIROS_FINANCIAMIENTO GF
         WHERE  CF.NumFinanc = GF.NumFinanc
         AND    CF.IdePol = nIdePol
         AND    GF.StsGiro = 'ACT'
         AND    NumAcre IS NOT NULL;
      CURSOR REC_MOV IS
         SELECT IdeMovPrima
         FROM   RECIBO
         WHERE  NumOper = nNumOperD
         AND    StsRec = 'ACT'
         AND    FecEmi > dFecTrasp;
   BEGIN
      PR_OPERACION.INICIAR ('TRASP', 'POLIZA', TO_CHAR (nIdePol) || '-' || cCodInter);
      nNumOper  := PR_OPERACION.nNumOper;
      FOR X IN FRACC_ACRE LOOP
         nNumOperD  := X.NumOper;
         BEGIN
            SELECT IdeFact
            INTO   nIdeFact
            FROM   ACREENCIA
            WHERE  NumAcre = X.NumAcre;
         END;
         BEGIN
            UPDATE FACTURA
            SET CODINTER = cCodInter
            WHERE  IdeFact = nIdeFact
            AND    StsFact = 'ACT';
         END;
         FOR RD IN REC_MOV LOOP
            UPDATE PART_INTER_MOV
            SET CodInter = cCodInter,
                PorcPart = nPorcPart
            WHERE  CodInter = cCodInterOld
            AND    IdeMovPrima = RD.IdeMovPrima;
         END LOOP;
      END LOOP;
      UPDATE PART_INTER_POL
      SET CodInter = cCodInter,
          PorcPart = nPorcPart
      WHERE  CodInter = cCodInterOld
      AND    IdePol = nIdePol;
      UPDATE ARTICULO_41
      SET CodInter = cCodInter
      WHERE  CodInter = cCodInterOld
      AND    IdePol = nIdePol;
      PR_OPERACION.TERMINAR;
   END;
      --//
   --
   FUNCTION NUMERO_ENDOSO (cCodProd VARCHAR2, cCodOfiemi VARCHAR2, cTipoEndoso VARCHAR2)
      RETURN NUMBER IS
      cTipoNum           VARCHAR2 (10);
      nUltimo_Numero     NUMBER (10);
      nRango_Final       NUMBER (10);
      cIndEndosoConsec   VARCHAR2 (1);
      nExiste            NUMBER := 0;
   BEGIN
      BEGIN
         SELECT Num_Apolice, NVL (IndEndosoConsec, 'N')
         INTO   cTipoNum, cIndEndosoConsec
         FROM   PRODUCTO
         WHERE  CodProd = cCodprod;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            RAISE_APPLICATION_ERROR (-20100, 'No Existe el producto = ' || cCodprod);
      END;
      --
      IF cTipoNum = 'RANGO' THEN
         IF     cIndEndosoConsec = 'S'
            AND BUSCA_LVAL ('TPENDOSO', cTipoEndoso) = 'S' THEN
            --Se Modifico para que el consecutivo de endoso comience en cero.  10/07/2015 Tecnocom
            BEGIN
              SELECT NVL(COUNT(*),0)
                  into nExiste
              FROM   OPER_POL
              WHERE  IdePol = nIdePol;
              IF nExiste = 0 THEN
                 nUltimo_Numero := 0;
              ELSE
                 SELECT MAX(NumEndoso) + 1
                  INTO   nUltimo_Numero
                  FROM   OPER_POL
                  WHERE  IdePol =    nIdePol;
              END IF;
            END;
         ELSE
            BEGIN
               SELECT Ultimo_Numero, Rango_final
               INTO   nUltimo_Numero, nRango_Final
               FROM   NumERACION_POLIZA
               WHERE  CodOfi = cCodOfiemi
               AND    Codprod = cCodprod
               AND    Documento = cTipoENDoso
               FOR UPDATE OF Ultimo_Numero;
            EXCEPTION
               WHEN NO_DATA_FOUND THEN
                  RAISE_APPLICATION_ERROR (-20270,
                                           PR.MENSAJE ('ABD', 20270, NULL, NULL, NULL) || '-' || cCodOfiemi || '-' || cCodprod || '-' || cTipoEndoso);
            END;
            --
            IF nRango_Final < nUltimo_Numero THEN
               RAISE_APPLICATION_ERROR (-20271, PR.MENSAJE ('ABD', 20271, NULL, NULL, NULL));
            END IF;
         END IF;
      --<I Javier Asmat/25.06.2004/Capitalizaci�n>
      --Se agrega condici�n para los Titulos de Capitalizaci�n
      ELSIF cTipoNum = 'TITULO' THEN
         IF cStsPol = 'INC' THEN
            --En este caso actualizaremos el NumPol al ACTIVAR con el numero de tarjeta
            BEGIN
               SELECT NumDoc
               INTO   nUltimo_Numero
               FROM   DOC_POLIZA
               WHERE  IdePol = nIdePol;
            EXCEPTION
               WHEN NO_DATA_FOUND THEN
                  RAISE_APPLICATION_ERROR (-20100, PR.MENSAJE ('AIF', 22047, NULL, NULL, NULL));
            END;
         ELSE
            --En este caso se obtiene un numero cualquiera, no es el definitivo, por lo mismo se usa una secuencia
            BEGIN
               SELECT SQ_NUMTITULO.NEXTVAL
               INTO   nUltimo_Numero
               FROM   DUAL;
            END;
         END IF;
      --<F Capitalizaci�n>
      ELSE
         IF     cIndEndosoConsec = 'S'
            AND BUSCA_LVAL ('TPENDOSO', cTipoEndoso) = 'S' THEN
            --Se Modifico para que el consecutivo de endoso comience en cero.  10/07/2015 Tecnocom
            BEGIN
              SELECT NVL(COUNT(*),0)
                  into nExiste
              FROM   OPER_POL
              WHERE  IdePol = nIdePol;
              IF nExiste = 0 THEN
                 nUltimo_Numero := 0;
              ELSE
                 SELECT MAX(NumEndoso) + 1
                  INTO   nUltimo_Numero
                  FROM   OPER_POL
                  WHERE  IdePol =    nIdePol;
              END IF;
            END;
         ELSE
            BEGIN
               SELECT Ultimo_Numero
               INTO   nUltimo_Numero
               FROM   CORRELATIVO_PRODUCTO
               WHERE  CodProd = cCodprod
               AND    Documento = cTipoEndoso
               FOR UPDATE OF Ultimo_Numero;
            EXCEPTION
               WHEN NO_DATA_FOUND THEN
                  RAISE_APPLICATION_ERROR (-20270,
                                           PR.MENSAJE ('ABD', 20270, NULL, NULL, NULL) || '-' || cCodOfiemi || '-' || cCodprod || '-' || cTipoEndoso);
            END;
         END IF;
      END IF;
      --
      RETURN (nUltimo_Numero);
   END;
   --
   PROCEDURE ACTUALIZA_ENDOSO (cCodProd VARCHAR2, cCodOfiemi VARCHAR2, cTipoENDoso VARCHAR2) IS
      nNum_Apolice       VARCHAR2 (10);
      cCodRamo           VARCHAR2 (4);
      nUltimo_Numero     NUMBER (10);
      cIndEndosoConsec   VARCHAR2 (1);
   BEGIN
      BEGIN
         SELECT Num_Apolice, NVL (IndEndosoConsec, 'N')
         INTO   nNum_Apolice, cIndEndosoConsec
         FROM   PRODUCTO
         WHERE  CodProd = cCodprod;
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, PR.MENSAJE ('ABD', 22052, 'Error en Busqueda Producto ' || cCodprod, NULL, NULL));
      END;
      --
      IF nNum_Apolice = 'RANGO' THEN
         IF cIndEndosoConsec = 'N' THEN
            BEGIN
               UPDATE NUMERACION_POLIZA
               SET Ultimo_Numero = Ultimo_Numero + 1
               WHERE  CodOfi = cCodOfiemi
               AND    Codprod = cCodprod
               AND    Documento = cTipoEndoso;
            EXCEPTION
               WHEN OTHERS THEN
                  RAISE_APPLICATION_ERROR (-20205, PR.MENSAJE ('AIF', 20290, 'NUMERACION_POLIZA', NULL, NULL));
            END;
         ELSE
            IF BUSCA_LVAL ('TPENDOSO', cTipoEndoso) = 'N' THEN
               BEGIN
                  UPDATE NUMERACION_POLIZA
                  SET Ultimo_Numero = Ultimo_Numero + 1
                  WHERE  CodOfi = cCodOfiemi
                  AND    Codprod = cCodprod
                  AND    Documento = cTipoEndoso;
               EXCEPTION
                  WHEN OTHERS THEN
                     RAISE_APPLICATION_ERROR (-20205, PR.MENSAJE ('AIF', 20290, 'NUMERACION_POLIZA', NULL, NULL));
               END;
            END IF;
         END IF;
      ELSE
         IF cIndEndosoConsec = 'N' THEN
            BEGIN
               UPDATE CORRELATIVO_PRODUCTO
               SET Ultimo_Numero = Ultimo_Numero + 1
               WHERE  CodProd = cCodProd
               AND    Documento = cTipoENDoso;
            EXCEPTION
               WHEN OTHERS THEN
                  RAISE_APPLICATION_ERROR (-20205, PR.MENSAJE ('AIF', 20290, 'CORRELATIVO_PRODUCTO', NULL, NULL));
            END;
         ELSE
            IF BUSCA_LVAL ('TPENDOSO', cTipoEndoso) = 'N' THEN
               BEGIN
                  UPDATE CORRELATIVO_PRODUCTO
                  SET Ultimo_Numero = Ultimo_Numero + 1
                  WHERE  CodProd = cCodProd
                  AND    Documento = cTipoENDoso;
               EXCEPTION
                  WHEN OTHERS THEN
                     RAISE_APPLICATION_ERROR (-20205, PR.MENSAJE ('AIF', 20290, 'CORRELATIVO_PRODUCTO', NULL, NULL));
               END;
            END IF;
         END IF;
      END IF;
   END;
   --
   --
   PROCEDURE FACT_COMPLEMENTO (nIdePol NUMBER, dFecOperMov DATE, nNumOperRef NUMBER, nValor NUMBER, cTipoOp VARCHAR2) IS
      --  cTipoOp IdentIFica el Tipo de operacion a realizar
      -- 'COM'= Complemento, 'DRS'= DIFerencia en reservas --
      nNumOperNew            NUMBER (14);
      nNumCert               NUMBER (10);
      nMtoOperTot            NUMBER (14, 2);
      dFecOper               DATE;
      nPorcOperPol           NUMBER;
      nMtoOperNew            NUMBER (14, 2);
      nMtoRecTot             NUMBER (14, 2);
      nPorcRecibo            NUMBER;
      nMtoReciboNew          NUMBER (14, 2);
      nMtoReciboNewLoc       NUMBER (14, 2);
      nMtoComReciboNew       NUMBER (14, 2);
      nMtoComReciboNewLoc    NUMBER (14, 2);
      nMtoComOrigNewLocal    NUMBER (14, 2);
      nMtoComOrigNewMoneda   NUMBER (14, 2);
      nIdeMovPrimaNew        NUMBER (14);
      nPorcPartCoa           NUMBER (8, 4);
      cCodMoneda             VARCHAR2 (3);
      nIdeMovPrima           NUMBER (14);
      nPorcAgen              NUMBER (8, 4);
      nIdeRec                NUMBER (14);
      cTipoPdcion            VARCHAR2 (1);
      cCodOfiemi             VARCHAR2 (6);
      nMtoPagoCoase          NUMBER (14, 2);
      nNumOperAcepRiesgo     NUMBER (14);
      nMtoOper               NUMBER (14, 2);
      nMtoCom                NUMBER (14, 2);
      nNumModMax             NUMBER (3);
      nTasa                  NUMBER (8, 4);
      nPrimaMoneda           NUMBER (14, 2);
      nPrima                 NUMBER (14, 2);
      nSumaAseg              NUMBER (14, 2);
      nMtoPrima              NUMBER (14, 2);
      cCodProd               VARCHAR2 (4);
      nNumENDoso             NUMBER (10);
      cCodPlan               VARCHAR2 (3);
      cRevPlan               VARCHAR2 (3);
      dFecIniRec             DATE;
      cFlgObl                VARCHAR2 (1)                   := 'N';
      --
      cIndAjusteBlanket      POLIZA.IndAjusteBlanket%TYPE;
      cSigno                 VARCHAR2 (1)                   := 'P';
      -- Defecto Nro. 3423 Realizado por Eduardo Ocando 27/08/2005.
      nExiste                NUMBER                         := 0;
      nMesesFactor           NUMBER (3);
      --
      CURSOR OPER_POL_Q IS
         SELECT IdePol, NumCert, DECODE (MtoOper, 0, 1, MtoOper) MtoOper, NumOper NumOperOld
         FROM   OPER_POL
         WHERE  NumOper = nNumOperRef;
      CURSOR RECIBO_Q IS
         SELECT R.CodRamoCert, R.IdeRec, R.IdeMovPrima, R.CodRec, R.NumRec, R.StsRec, R.FecSts, R.CodOfiemi, R.TasaCambio, R.MtoLocal, R.MtoMoneda,
                R.FecEmi, R.FecIniVig, R.FecFinVig, R.PorcCom, R.MtoComLocal, R.MtoComMoneda, MP.CodPlan, MP.RevPlan, R.MtoComOrigLocal,
                R.MtoComOrigMoneda, ROUND ((R.MtoComOrigMoneda * 100) / DECODE (R.MtoMoneda, 0, 1), 4) PorcComOrig
         FROM   RECIBO R, MOV_PRIMA MP
         WHERE  R.IdeMovPrima = MP.IdeMovPrima
         AND    R.NumOper = nNumOperRef
         AND    R.NumCert = nNumCert;
      CURSOR PART_INTER_MOV_Q IS
         SELECT CodInter, IndLider, PorcNetoCom, PorcPart, PorComCedido
         FROM   PART_INTER_MOV
         WHERE  IdeMovPrima = nIdeMovPrima;
      CURSOR RESP_PAGO_Q IS
         SELECT CodCli, PorcPago, CodPlanFracc, NumModPlanFracc, Direc, CodPais, CodEstado, CodCiudad, CodMunicipio, Telex, Fax, Zip, Telef1, Telef2,
                Telef3, CodCobrador, CodViaCobro
         FROM   RESP_PAGO_MOV
         WHERE  IdePol = nIdePol
         AND    NumCert = nNumCert
         AND    NumOper = nNumOPerRef;
      CURSOR DIST_COA_MOV_Q IS
         SELECT CodAcepRiesgo, PorcPart, PorcGasto
         FROM   DIST_COA_MOV
         WHERE  NumOper = nNumOPerRef;
      CURSOR MOD_COBERT_Q IS
         SELECT IdeCobert, NumMod, PrimaMoneda, NumCert, CodRamoCert
         FROM   MOD_COBERT
         WHERE  IdeMovPrima = nIdeMovPrima;
      CURSOR RECIBO_OPER_Q IS
         SELECT NumCert, CodRamoCert
         FROM   RECIBO
         WHERE  NumOper = nNumOperNew;
   BEGIN
      IF nValor < 0 THEN
         cFlgObl  := 'S';
         cSigno   := 'N';
      END IF;
      PR_OPERACION.INICIAR ('COMP', 'POLIZA', TO_CHAR (nIdePol));
      nNumOperNew   := PR_OPERACION.nNumOper;
      PR_POLIZA.CARGAR (nIdePol);
      -- Anexado por Eduardo Ocando 25/08/2005.
      -- Defecto 3423 error en las faturas complementarias y fechas_liq
      nMesesFactor  := PR.MESES_FORMA_PAGO (PR_POLIZA.cCodFormPago);
      BEGIN
         BEGIN
            SELECT 1
            INTO   nExiste
            FROM   FECHAS_LIQ
            WHERE  IdePol = nIdePol
            AND    NumOper = nNumOperRef;
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               nExiste  := 0;
         END;
         --
         IF PR.BUSCA_LVAL ('CANTREC', PR_POLIZA.cCodFormPago) IN ('INVALIDO', '1') THEN   -- Forma De Pago Anual O Especial --
            IF nExiste = 0 THEN
               INSERT INTO FECHAS_LIQ
                           (IdePol, NumOper, FecLiq, IndOk)
                    VALUES (nIdePol, nNumOperRef, PR_POLIZA.dFecUltFact, 'N');
            END IF;
         ELSE   -- Si la Forma de Pago es Diferente a anual y si no existe un n�mero de
                -- Operaci�n para esa fecha.
            IF nExiste = 0 THEN
               INSERT INTO FecHAS_LIQ
                           (IdePol, NumOper, FecLiq, IndOk)
                    VALUES (nIdePol, nNumOperRef, ADD_MONTHS (PR_POLIZA.dFecUltFact, nMesesFactor * -1), 'N');
            END IF;
         END IF;
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100,
                                     PR.MENSAJE ('ABD',
                                                 22052,
                                                 'FECHAS_LIQ ' || 'nIdePol ' || nIdePol || ' nNumOperNew ' || nNumOperNew || SQLERRM,
                                                 NULL,
                                                 NULL));
      END;
      -- Fin de la modificaci�n 25/08/2005 Defecto Nro. 3423
      --
      BEGIN
         INSERT INTO COMPLEMENTO_LIQ
                     (IdePol, NumOper, NumOperComp)
              VALUES (nIdePol, nNumOperRef, nNumOperNew);
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, PR.MENSAJE ('ABD', 20289, 'COMPLEMENTO_LIQ' || SQLERRM, NULL, NULL));
      END;
      BEGIN
         SELECT CodMoneda, TipoPdcion, CodOfiSusc, CodProd, NVL (IndAjusteBlanket, 'N')
         INTO   cCodMoneda, cTipoPdcion, cCodOfiemi, cCodProd, cIndAjusteBlanket
         FROM   POLIZA
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            RAISE_APPLICATION_ERROR (-20100, PR.MENSAJE ('ABD', 20205, 'datos generales de poliza' || SQLERRM, 'POLIZA', NULL));
      END;
      BEGIN
         SELECT SUM (MtoOper)
         INTO   nMtoOperTot
         FROM   OPER_POL
         WHERE  NumOper = nNumOperRef;
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, PR.MENSAJE ('ABD', 20205, 'datos Operacion' || SQLERRM, 'OPER_POL', NULL));
      END;
      IF nMtoOperTot = 0 THEN
         nMtoOperTot  := 1;
      END IF;
      --Erick Zoque 20/05/2013 E_CON_20120821_1_7
      dFecOper      := PR_FECHAS_EQUIVALENTES.FECHA_EMISION (dFecOperMov, PR_POLIZA.cCodCia,'001');
      FOR Y IN OPER_POL_Q LOOP
         nNumCert      := Y.NumCert;
         nPorcOperPol  := Y.MtoOper * 100 / nMtoOperTot;
         nMtoOperNew   := nValor * nPorcOperPol / 100;
         nNumEndoso    := PR_POLIZA.NUMERO_ENDOSO (PR_POLIZA.cCodProd, PR_POLIZA.cCodOfiemi, 'EC');
         --*
         BEGIN
            INSERT INTO OPER_POL
                        (IdePol, NumCert, NumOper, FecMov, MtoOper, TipoOp, NumEndoso, IndAnul)
                 VALUES (Y.IdePol, Y.NumCert, nNumOperNew, dFecOper, nMtoOperNew, cTipoOp, nNumENDoso, 'N');
         EXCEPTION
            WHEN OTHERS THEN
               RAISE_APPLICATION_ERROR (-20100, PR.MENSAJE ('ABD', 20289, 'OPER_POL' || SQLERRM, NULL, NULL));
         END;
         UPDATE RECIBO SET TipoOpe = cTipoOp
         WHERE  IdePol = Y.IdePol
         AND    NumCert = Y.NumCert
         AND    NumOper = nNumOperNew;
         PR_POLIZA.ORDEN_COA (Y.IdePol, nNumOperNew, 'EC', 'S');
         PR_POLIZA.ACTUALIZA_ENDOSO (PR_POLIZA.cCodProd, PR_POLIZA.cCodOfiemi, 'EC');
         FOR T IN RESP_PAGO_Q LOOP
            BEGIN
               INSERT INTO RESP_PAGO_MOV
                           (IdePol, NumCert, NumOper, CodCli, PorcPago, CodPlanFracc, NumModPlanFracc, Direc, CodPais,
                            CodEstado, CodCiudad, CodMunicipio, Telex, Fax, Zip, Telef1, Telef2, Telef3, CodCobrador,
                            CodViaCobro)
                    VALUES (nIdePol, nNumCert, nNumOperNew, T.CodCli, T.PorcPago, T.CodPlanFracc, T.NumModPlanFracc, T.Direc, T.CodPais,
                            T.CodEstado, T.CodCiudad, T.CodMunicipio, T.Telex, T.Fax, T.Zip, T.Telef1, T.Telef2, T.Telef3, T.CodCobrador,
                            T.CodViaCobro);
            EXCEPTION
               WHEN OTHERS THEN
                  RAISE_APPLICATION_ERROR (-20100, PR.MENSAJE ('ABD', 20289, 'RESP_PAGO_MOV' || SQLERRM, NULL, NULL));
            END;
         END LOOP;
         BEGIN
            SELECT SUM (MtoMoneda)
            INTO   nMtoRecTot
            FROM   RECIBO
            WHERE  NumOper = nNumOperRef
            AND    NumCert = Y.NumCert;
         EXCEPTION
            WHEN OTHERS THEN
               RAISE_APPLICATION_ERROR (-20100, PR.MENSAJE ('ABD', 20205, 'datos recibo' || SQLERRM, 'RECIBO', NULL));
         END;
         FOR D IN RECIBO_Q LOOP
            nIdeMovPrima          := D.IdeMovPrima;
            IF     D.MtoMoneda = 0
               AND nMtoRecTot = 0 THEN
               nPorcRecibo  := 100;
            ELSE
               nPorcRecibo  := D.MtoMoneda * 100 / nMtoRecTot;
            END IF;
            -- << TD5994. Anusky Gonzalez. 25/01/2006 >>
            --nMtoReciboNewLoc      := (nMtoOperNew * PR.TASA_CAMBIO_ACTUAL (cCodMoneda, SYSDATE)) * nPorcRecibo / 100;
            nMtoReciboNewLoc      := (nMtoOperNew * PR_TASA_CAMBIO.BUSCA_TASA (cCodMoneda, SYSDATE, nIdePol)) * nPorcRecibo / 100;
            --
            nMtoReciboNew         := nMtoOperNew * nPorcRecibo / 100;
            nMtoComReciboNew      := nMtoReciboNew * D.PorcCom / 100;
            nMtoComReciboNewLoc   := nMtoReciboNewLoc * D.PorcCom / 100;
            nMtoComOrigNewLocal   := nMtoReciboNew * D.PorcComOrig / 100;
            nMtoComOrigNewMoneda  := nMtoReciboNew * D.PorcComOrig / 100;
            nIdeMovPrimaNew       := PR_MOV_PRIMA.Num_TEMP;
            nPorcPartCoa          := PR_DIST_COA.PART_LIDER (Y.IdePol);
            cCodPlan              := D.CodPlan;
            cRevPlan              := D.RevPlan;
            BEGIN
               INSERT INTO MOV_PRIMA
                           (IdeMovPrima, NumMovPrima, StsMovPrima, FecSts, IdePol, NumCert, CodRamoCert, CodMoneda, FecIniValid,
                            FecFinValid, CodOfiemi, TasaCambio, FecEmi, PorcPart, MtoLocal, MtoMoneda, MtoComLocal,
                            MtoComMoneda, PorcCom, PorcBonIF, MtoComRetLocal, MtoComRetMoneda, MtoPrimaRetLocal, MtoPrimaRetMoneda, CodPlan,
                            RevPlan, Tipo, MtoRecaDcto, MtoComOrigLocal, MtoComOrigMoneda)
                    VALUES (nIdeMovPrimaNew, nIdeMovPrimaNew, 'GEN', dFecOper, Y.IdePol, Y.NumCert, D.CodRamoCert, cCodMoneda, dFecOper,
                            D.FecFinVig, D.CodOfiemi, D.TasaCambio, dFecOper, nPorcPartCoa, nMtoReciboNewLoc, nMtoReciboNew, nMtoComReciboNewLoc,
                            nMtoComReciboNew, D.PorcCom, 0, 0, 0, 0, 0, cCodPlan,
                            cRevPlan, 'P', 0, nMtoComOrigNewLocal, nMtoComOrigNewMoneda);
            END;
            FOR F IN PART_INTER_MOV_Q LOOP
               BEGIN
                  INSERT INTO PART_INTER_MOV
                              (IdeMovPrima, CodInter, IndLider, PorcNetoCom, PorcPart, PorComCedido)
                       VALUES (nIdeMovPrimaNew, F.CodInter, F.IndLider, F.PorcNetoCom, F.PorcPart, F.PorComCedido);
               EXCEPTION
                  WHEN OTHERS THEN
                     RAISE_APPLICATION_ERROR (-20100, PR.MENSAJE ('ABD', 20289, 'PART_INTER_MOV' || SQLERRM, NULL, NULL));
               END;
            END LOOP;
            PR_MOV_PRIMA.ACTIVAR (nIdeMovPrimaNew);
            BEGIN
               SELECT SQ_RECIBO.NEXTVAL
               INTO   nIdeRec
               FROM   SYS.DUAL;
            END;
            BEGIN
               INSERT INTO RECIBO
                           (IdePol, NumCert, CodRamoCert, IdeMovPrima, IdeRec, CodRec, NumRec, StsRec, FecSts, CodOfiemi,
                            TasaCambio, MtoLocal, MtoMoneda, FecEmi, FecIniVig, FecFinVig, PorcCom, MtoComLocal,
                            MtoComMoneda, NumOper, IndTrasCart, MtoComOrigLocal, MtoComOrigMoneda)
                    VALUES (Y.IdePol, Y.NumCert, D.CodRamoCert, nIdeMovPrimaNew, nIdeRec, D.CodRamoCert, nIdeRec, 'INC', dFecOper, D.CodOfiemi,
                            D.TasaCambio, nMtoReciboNewLoc, nMtoReciboNew, dFecOper, dFecOper, D.FecFinVig, D.PorcCom, NVL (nMtoComReciboNewLoc, 0),
                            NVL (nMtoComReciboNew, 0), nNumOperNew, 'N', nMtoComOrigNewLocal, nMtoComOrigNewMoneda);
            EXCEPTION
               WHEN OTHERS THEN
                  RAISE_APPLICATION_ERROR (-20100, PR.MENSAJE ('ABD', 20289, 'RECIBO' || SQLERRM, NULL, NULL));
            END;
            PR_RECIBO.GENERAR_TRANSACCION (nIdeRec, 'ACTIV');
            BEGIN
               SELECT SUM (PrimaMoneda)
               INTO   nPrimaMoneda
               FROM   MOD_COBERT
               WHERE  IdeMovPrima = nIdeMovPrima;
            EXCEPTION
               WHEN OTHERS THEN
                  RAISE_APPLICATION_ERROR (-20100, PR.MENSAJE ('ABD', 20205, 'datos modificacion' || SQLERRM, 'MOD_COBERT', NULL));
            END;
            FOR T IN MOD_COBERT_Q LOOP
               BEGIN
                  SELECT Tasa, SumaAsegMoneda
                  INTO   nTasa, nSumaAseg
                  FROM   MOD_COBERT
                  WHERE  IdeCobert = T.IdeCobert
                  AND    NumMod = (SELECT MAX (M.NumMod)
                                   FROM   MOD_COBERT M
                                   WHERE  M.IdeCobert = T.IdeCobert);
               EXCEPTION
                  WHEN OTHERS THEN
                     RAISE_APPLICATION_ERROR (-20100, PR.MENSAJE ('ABD', 20205, 'datos modificacion' || SQLERRM, 'MOD_COBERT', NULL));
               END;
               BEGIN
                  SELECT MAX (NumMod) + 1
                  INTO   nNumModMax
                  FROM   MOD_COBERT
                  WHERE  IdeCobert = T.IdeCobert;
               EXCEPTION
                  WHEN OTHERS THEN
                     RAISE_APPLICATION_ERROR (-20100, PR.MENSAJE ('ABD', 20205, 'datos modificacion' || SQLERRM, 'MOD_COBERT', NULL));
               END;
               IF     T.PrimaMoneda = 0
                  AND nPrimaMoneda = 0 THEN
                  nPrima  := nMtoReciboNew;
               ELSE
                  nPrima  := nMtoReciboNew * (T.PrimaMoneda * 100 / nPrimaMoneda) / 100;
               END IF;
               PR_MOD_COBERT.GENERAR_LIQ_COMPLEMENTO (T.IdeCobert, T.NumMod, nNumModMax, nIdeMovPrimaNew, nSumaAseg, nPrima, dFecOper);
               -- Modificado   Eduardo Ocando 28/06/2005. N�mero de Operaci�n se anexa al movimiento del Ajuste Blanket
               IF cIndAjusteBlanket = 'S' THEN
                  BEGIN
                     UPDATE COBERT_AJUSTE_BLANKET CAJ
                     SET NumOper = PR_OPERACION.numoper
                     WHERE  CAJ.IdePolRen = nIdePol
                     AND    CAJ.CodRamo = T.CodRamoCert
                     AND    CAJ.StsAjuBlan IN ('VAL')
                     AND    CAJ.IdeAjuBlan = (SELECT MAX (IdeAjuBlan)
                                              FROM   COBERT_AJUSTE_BLANKET COAJ
                                              WHERE  COAJ.IdePolRen = CAJ.IdePolRen);
                  END;
               END IF;
            -- Fin del Modificaci�n Nro de Defecto 2570 Ajuste Blanket
            END LOOP;
            nMtoPrima             := PR_MOV_PRIMA.GENERAR_RENGLONES (nIdeMovPrimaNew);
            IF PR.BUSCA_PARAMETRO ('034') = 'NO' THEN
               PR_GEN_REA.SUMA_RAMO_REA (nIdeRec);
            END IF;
         END LOOP;
         BEGIN
            SELECT MIN (FecIniVig)
            INTO   dFecIniRec
            FROM   RECIBO
            WHERE  NumOper = nNumOperNew;
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               dFecIniRec  := SYSDATE;
         END;
         --Erick Zoque 20/05/2013 E_CON_20120821_1_7
         IF dFecIniRec < PR_FECHAS_EQUIVALENTES.FECHA_EMISION (SYSDATE, PR_POLIZA.cCodcia,'001') THEN
            PR_OPER_USUARIO.AUTORIZAR ('127', NULL, NULL, NULL, NULL, NULL, 0, NULL, cCodProd, cTipoOp, dFecIniRec);
         END IF;
         PR_FRACCIONAMIENTO.EMITIR_ALTAS (nIdePol, nNumCert, nNumOperNew, cSigno);
      --- Eliminada llamada a PR_POLIZA.FACT_COMPLEMENTO_OBL porque ya la rutina anterior realiza todos los
      --- Movimientos necesarios
      --END IF;
      END LOOP;
      FOR D IN DIST_COA_MOV_Q LOOP
         BEGIN
            SELECT NVL (MAX (NumOperAcepRiesgo), 0) + 1
            INTO   nNumOperAcepRiesgo
            FROM   DIST_COA_MOV
            WHERE  CodAcepRiesgo = D.CodAcepRiesgo
            AND    CodOfiemi = cCodOfiemi;
         EXCEPTION
            WHEN OTHERS THEN
               RAISE_APPLICATION_ERROR (-20100, PR.MENSAJE ('ABD', 20205, 'datos Cosaseguro' || SQLERRM, 'DIST_COA_MOV', NULL));
         END;
         BEGIN
            SELECT NVL (SUM (MtoOper), 0) * D.PorcPart / 100
            INTO   nMtoOper
            FROM   OPER_POL
            WHERE  NumOper = nNumOperNew;
         EXCEPTION
            WHEN OTHERS THEN
               RAISE_APPLICATION_ERROR (-20100, PR.MENSAJE ('ABD', 20205, 'datos operacion' || SQLERRM, 'OPER_POL', NULL));
         END;
         nMtoCom        :=
                   (PR.COMISION (nNumOperNew, 'COM') + PR.COMISION (nNumOperNew, 'AGE') + PR.COMISION (nNumOperNew, 'PRO')) * NVL (D.PorcPart, 100)
                   / 100;
         nMtoPagoCoase  := (nMtoOper - nMtoCom) - (nMtoOper * D.PorcGasto / 100);
         BEGIN
            INSERT INTO DIST_COA_MOV
                        (Tipoper, NumOper, NumSec, CodAcepRiesgo, CodOfiemi, NumOperAcepRiesgo, PorcPart, StsCoa, PorcGasto, MtoCoase)
                 VALUES ('001', nNumOperNew, 0, D.CodAcepRiesgo, cCodOfiemi, nNumOperAcepRiesgo, D.PorcPart, 'ACT', D.PorcGasto, nMtoPagoCoase);
         EXCEPTION
            WHEN OTHERS THEN
               RAISE_APPLICATION_ERROR (-20100, PR.MENSAJE ('ABD', 20289, 'DIST_COA_MOV' || SQLERRM, NULL, NULL));
         END;
      END LOOP;
      IF cFlgObl = 'N' THEN
         PR_ACREENCIA.GENERAR (nNumOperNew);
      END IF;
      IF PR.BUSCA_PARAMETRO ('055') = 'SI' THEN
         IF cFlgObl = 'N' THEN
            --<I Defect 4110> Javier Asmat/09.08.2005
            --Se hace llamado a esta rutina que genera las facturas y la contabilidad de las mismas
            PR_FRACCIONAMIENTO.GENERAR (nNumOperNew);
         --PR_FACTURA.GENERAR (nNumOperNew);
         --<F Defect 4110>
         END IF;
      END IF;
      PR_GEN_REA.GENERAR_REDISTRIBUIR (nNumOperNew);
      PR_OPERACION.TERMINAR;
   END;
   --////
   --
   PROCEDURE ACT_OPER_INICIAL (nIdePol NUMBER, nNumOper NUMBER) IS
      nNumCert       NUMBER (10);
      cCodRamoCert   VARCHAR2 (4);
      CURSOR CERTIFICADO_INC IS
         SELECT NumCert
         FROM   CERTIFICADO
         WHERE  IdePol = nIdePol
         AND    StsCert = 'INC';
      CURSOR CERT_RAMO_INC IS
         SELECT CodRamoCert, CodPlan, RevPlan
         FROM   CERT_RAMO
         WHERE  IdePol = nIdePol
         AND    NumCert = nNumCert
         AND    StsCertRamo = 'INC';
   BEGIN
      FOR C IN CERTIFICADO_INC LOOP
         nNumCert  := C.NumCert;
         FOR CR IN CERT_RAMO_INC LOOP
            cCodRamoCert  := CR.CodRamoCert;
            UPDATE CLAU_CERT
            SET NumOper = nNumOper
            WHERE  IdePol = nIdePol
            AND    NumCert = nNumCert
            AND    CodPlan = CR.CodPlan
            AND    RevPlan = CR.RevPlan
            AND    CodRamoCert = cCodRamoCert
            AND    StsClau = 'INC';
            UPDATE ANEXO_CERT
            SET NumOper = nNumOper
            WHERE  IdePol = nIdePol
            AND    NumCert = nNumCert
            AND    CodPlan = CR.CodPlan
            AND    RevPlan = CR.RevPlan
            AND    CodRamoCert = cCodRamoCert
            AND    StsAnexo = 'INC';
         END LOOP;
      END LOOP;
   END;
   ---///
   PROCEDURE GENERA_ENDOSO (
      cCodProd          VARCHAR2,
      cCodOfiemi        VARCHAR2,
      nIdePol           NUMBER,
      nNumCert          NUMBER,
      nNumOper          NUMBER,
      dFecOper          DATE,
      cTexto_ENDoso     VARCHAR2,
      nNumENDoso        NUMBER) IS
      dFecIniRec   DATE;
   BEGIN
      PR_POLIZA.CARGAR (nIdePol);
      BEGIN
         SELECT MIN (FecIniVig)
         INTO   dFecIniRec
         FROM   RECIBO
         WHERE  NumOper = nNumOper;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            dFecIniRec  := SYSDATE;
      END;
      --Erick Zoque 20/05/2013 E_CON_20120821_1_7
      IF dFecIniRec < PR_FECHAS_EQUIVALENTES.FECHA_EMISION (SYSDATE, PR_POLIZA.cCodcia,'001') THEN
         PR_OPER_USUARIO.AUTORIZAR ('127', NULL, NULL, NULL, NULL, NULL, 0, NULL, cCodProd, 'EMI', dFecIniRec);
      END IF;
      INSERT INTO OPER_POL
                  (IdePol, NumCert, NumOper, FecMov, MtoOper, TipoOp, NumEndoso, IndAnul)
           VALUES (nIdePol, nNumCert, nNumOper, dFecOper, 0, 'EMI', nNumENDoso, 'N');
         UPDATE RECIBO SET TipoOpe = 'EMI'
         WHERE  IdePol = nIdePol
         AND    NumCert = nNumCert
         AND    NumOper = nNumOper;
      PR_POLIZA.ORDEN_COA (nIdePol, nNumOper, 'EMI', 'S');
      INSERT INTO TEXTO_ENDOSOS
                  (IdePol, NumCert, NumOper, Tipo_ENDoso, Fec_Endoso, Desc_Cambio)
           VALUES (nIdePol, nNumCert, nNumOper, 'EMI', dFecOper, cTexto_ENDoso);
   END GENERA_ENDOSO;
   ---////
   -- AnulA_ENDOSO_INICIAL
   PROCEDURE AnulA_ENDOSO_INICIAL (
      nIdePol           NUMBER,
      nNumCert          NUMBER,
      nNumOper          NUMBER,
      cTexto_ENDoso     VARCHAR2,
      nNumENDoso        NUMBER,
      dFecMov           DATE,
      nNumOperAnu       NUMBER,
      nProtocolo        NUMBER,
      dDatProt          DATE) IS
      cCodProd     POLIZA.CodProd%TYPE;
      nNumPol      NUMBER (10);
      dFecIniRec   DATE;
   BEGIN
      PR_POLIZA.CARGAR (nIdePol);
      SELECT CodProd, NumPol
      INTO   cCodProd, nNumPol
      FROM   POLIZA
      WHERE  IdePol = nIdePol;
      BEGIN
         SELECT MIN (FecIniVig)
         INTO   dFecIniRec
         FROM   RECIBO
         WHERE  NumOper = nNumOper;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            dFecIniRec  := SYSDATE;
      END;
      --Erick Zoque 20/05/2013 E_CON_20120821_1_7
      IF dFecIniRec < PR_FECHAS_EQUIVALENTES.FECHA_EMISION (SYSDATE, PR_POLIZA.cCodCia,'001') THEN
         PR_OPER_USUARIO.AUTORIZAR ('127', NULL, NULL, NULL, NULL, NULL, 0, NULL, cCodProd, 'ANU', dFecIniRec);
      END IF;
      INSERT INTO OPER_POL
                  (IdePol, NumCert, NumOper, FecMov, MtoOper, TipoOp, NumEndoso, IndAnul)
           VALUES (nIdePol, nNumCert, nNumOper, dFecMov, 0, 'ANU', nNumENDoso, 'N');
         UPDATE RECIBO SET TipoOpe = 'ANU'
         WHERE  IdePol = nIdePol
         AND    NumCert = nNumCert
         AND    NumOper = nNumOper;
      PR_POLIZA.ORDEN_COA (nIdePol, nNumOper, 'CANC', 'S');
      -- aqui debe insertar protocolo
      IF nProtocolo IS NOT NULL THEN
         PR.PROTOCOLO (nProtocolo, dDatProt, nNumPol, NULL, nNumENDoso, dFecMov, nNumOper, nNumCert);
      END IF;
      INSERT INTO TEXTO_ENDOSOS
                  (IdePol, NumCert, NumOper, Tipo_Endoso, Fec_Endoso, Desc_Cambio)
           VALUES (nIdePol, nNumCert, nNumOper, 'ANU', dFecMov, cTexto_ENDoso);
      UPDATE OPER_POL
      SET NumOperAnu = nNumOper,
          FecAnul = dFecMov,
          IndAnul = 'S'
      WHERE  NumOper = nNumOperAnu
      AND    NumCert = nNumCert
      AND    NumOperAnu IS NULL;
   END;
   --///
   -- ORDEN_COA
   PROCEDURE ORDEN_COA (nIdePol NUMBER, nNumOper NUMBER, TipoENDoso VARCHAR2, TipoPol VARCHAR2) IS
      nNumOrden     NUMBER (14);
      cTipoPdcion   VARCHAR2 (1);
      nExiste       NUMBER (1);
      cCodProd      VARCHAR (4);
      cCodOfiemi    VARCHAR2 (6);
      CURSOR DIST_COA IS
         SELECT CodAcepRiesgo
         FROM   DIST_COA
         WHERE  IdePol = nIdePol
         AND    IndLider <> 'S';
   BEGIN
      SELECT TipoPdcion, CodProd, CodOfiemi
      INTO   cTipoPdcion, cCodProd, cCodOfiemi
      FROM   POLIZA
      WHERE  IdePol = nIdePol;
      nExiste  := PR.EXISTE_ORDEN_COA (nIdePol, nNumOper, 'SG');
      IF     cTipoPdcion = 'C'
         AND nExiste = 0 THEN
         FOR DC IN DIST_COA LOOP
            nNumOrden  := PR_POLIZA.NumERO_ENDOSO (cCodProd, cCodOfiemi, 'ORCO');
            INSERT INTO ORDEN_COA
                        (IdePol, NumOper, CodAcepRiesgo, NumOrden)
                 VALUES (nIdePol, nNumOper, DC.CodAcepRiesgo, nNumOrden);
            PR_POLIZA.ACTUALIZA_ENDOSO (cCodProd, cCodOfiemi, 'ORCO');
         END LOOP;
      END IF;
   END ORDEN_COA;
   --///
   --CAMBIAR_VIGENCIA
   PROCEDURE CAMBIAR_VIGENCIA (nIdePol POLIZA.IdePol%TYPE, dFecIniVig POLIZA.FecINIVIG%TYPE, dFecFinVig POLIZA.FecFINVIG%TYPE) IS
   BEGIN
     -- CANAL POLIZA --
      BEGIN
         UPDATE CANAL_POLIZA
         SET FecIniVig  = dFecIniVig,
             FecFinVig  = dFecFinVig
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, 'Error actualizando CERT_RAMO: ' || SQLERRM);
      END;

      -- CERTIFICADOS --
      BEGIN
         UPDATE CERTIFICADO C
         --- Si no existia antes, tomara la nueva fecha de inicio
         SET C.FecIng =
                (SELECT NVL (MAX (CE.FecIng), dFecIniVig)
                 FROM   CERTIFICADO CE, POLIZA P2, POLIZA P1
                 WHERE  P2.CodProd = P1.CodProd
                 AND    P2.NumPol = P1.NumPol
                 AND    NVL (P2.NumRen, 0) < NVL (P1.NumRen, 0)
                 AND    P1.IdePol = C.IdePol
                 AND    CE.IdePol = P2.IdePol
                 AND    CE.NumCert = C.NumCert),
             C.FecFin = dFecFinVig
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, 'Error actualizando CERTIFICADO: ' || SQLERRM);
      END;
      -- CERTIFICADO DEL RAMO --
      BEGIN
         UPDATE CERT_RAMO
         SET FecIniValid = dFecIniVig,
             FecFinValid = dFecFinVig
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, 'Error actualizando CERT_RAMO: ' || SQLERRM);
      END;
      -- COBERTURAS DEL CERTIFICADO --
      BEGIN
         UPDATE COBERT_CERT
         SET FecIniValid = dFecIniVig,
             FecFinValid = dFecFinVig
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, 'Error actualizando COBERT_CERT: ' || SQLERRM);
      END;
      -- COBERTURAS DEL ASEGURADO --
      BEGIN
         UPDATE COBERT_ASEG
         SET FecIniValid = dFecIniVig,
             FecFinValid = dFecFinVig
         WHERE  IdeAseg IN (SELECT IdeAseg
                            FROM   ASEGURADO
                            WHERE  IdePol = nIdePol);
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, 'Error actualizando COBERT_ASEG: ' || SQLERRM);
      END;
      -- BIENES DEL CERTIFICADO --
      BEGIN
         UPDATE BIEN_CERT
         SET FecIniValid = dFecIniVig,
             FecFinValid = dFecFinVig
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, 'Error actualizando BIEN_CERT: ' || SQLERRM);
      END;
      -- COBERTURAS DEL BIEN  --
      BEGIN
         UPDATE COBERT_BIEN
         SET FecIniValid = dFecIniVig,
             FecFinValid = dFecFinVig
         WHERE  IdeBien IN (SELECT IdeBien
                            FROM   BIEN_CERT
                            WHERE  IdePol = nIdePol);
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, 'Error actualizando COBERT_BIEN: ' || SQLERRM);
      END;
      -- MOVIMIENTOS DE LAS COBERTURAS --
      BEGIN
         UPDATE MOD_COBERT
         SET FecIniValid = dFecIniVig,
             FecFinValid = dFecFinVig
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, 'Error actualizando MOD_COBERT: ' || SQLERRM);
      END;
      -- ESTADISTICAS DEL CERTIDFICADO RAMO --
      BEGIN
         UPDATE EST_CERT
         SET FecIniValid = dFecIniVig,
             FecFinVAlid = dFecFinVig
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, 'Error actualizando EST_CERT: ' || SQLERRM);
      END;
      -- ANEXO DEL CERTIDFICADO RAMO --
      BEGIN
         UPDATE ANEXO_CERT
         SET FecIniValid = dFecIniVig,
             FecFinValid = dFecFinVig
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, 'Error actualizando ANEXO_CERT: ' || SQLERRM);
      END;
      -- ANEXO POLIZA --
      BEGIN
         UPDATE ANEXO_POL
         SET FecIniValid = dFecIniVig,
             FecFinValid = dFecFinVig
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, 'Error actualizando ANEXO_POL: ' || SQLERRM);
      END;
      --CERTIFICADOS DEL VEHICULO --
      BEGIN
         UPDATE CERT_VEH
         SET FecIniVig = dFecIniVig,
             FecFinVig = dFecFinVig
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, 'Error actualizando CERT_VEH: ' || SQLERRM);
      END;
      --CLASULAS DEL CERTIFICADO --
      BEGIN
         UPDATE CLAU_CERT
         SET FecIniValid = dFecIniVig,
             FecFinValid = dFecFinVig
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, 'Error actualizando CLAU_CERT: ' || SQLERRM);
      END;
      --
      BEGIN
         UPDATE MOD_RECA_DCTO_CERTIF
         SET FecIniValid = dFecIniVig,
             FecFinValid = dFecFinVig
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, 'Error actualizando MOD_RECA_DCTO_CERTIF: ' || SQLERRM);
      END;
      -- COASEGURO --
      BEGIN
         UPDATE DIST_COA
         SET FecIniValid = dFecIniVig,
             FecFinValid = dFecFinVig
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, 'Error actualizando DIST_COA: ' || SQLERRM);
      END;
      -- MOVIMIENTO DEL ASEGURADO  --
      BEGIN
         UPDATE MOD_ASEG
         SET FecIniValid = dFecIniVig,
             FecFinValid = dFecFinVig
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, 'Error actualizando MOD_ASEG: ' || SQLERRM);
      END;
      -- RECAR Y DESC DE LA POLIZA --
      BEGIN
         UPDATE RECA_DCTO_POL
         SET FecIniValid = dFecIniVig,
             FecFinValid = dFecFinVig
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, 'Error actualizando RECA_DCTO_POL: ' || SQLERRM);
      END;
      -- RECA_DCTO_CERTIF --
      BEGIN
         UPDATE RECA_DCTO_CERTIF
         SET FecIniValid = dFecIniVig,
             FecFinValid = dFecFinVig
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, 'Error actualizando RECA_DCTO_CERTIF: ' || SQLERRM);
      END;
      -- RECA_DCTO_CERTIF_ASEG --
      BEGIN
         UPDATE RECA_DCTO_CERTIF_ASEG
         SET FecIniValid = dFecIniVig,
             FecFinValid = dFecFinVig
         WHERE  IdeAseg IN (SELECT IdeAseg
                            FROM   ASEGURADO
                            WHERE  IdePol = nIdePol
                            AND    TRUNC (FecING) < TRUNC (dFecIniVig));
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, 'Error actualizando RECA_DCTO_CERTIF_ASEG: ' || SQLERRM);
      END;
      -- RECA_DCTO_CERTIF_BIEN --
      BEGIN
         UPDATE RECA_DCTO_CERTIF_BIEN
         SET FecIniValid = dFecIniVig,
             FecFinValid = dFecFinVig
         WHERE  IdeBien IN (SELECT IdeBien
                            FROM   BIEN_CERT
                            WHERE  IdePol = nIdePol
                            AND    TRUNC (FecINIVALID) < TRUNC (dFecIniVig));
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, 'Error actualizando RECA_DCTO_CERTIF_BIEN: ' || SQLERRM);
      END;
      BEGIN
         UPDATE DEC_TRANSPORTE
         SET FecIniValid = dFecIniVig,
             FecFinValid = dFecFinVig
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, 'Error actualizando DEC_TRANSPORTE: ' || SQLERRM);
      END;
      --
      BEGIN
         UPDATE DEC_GEN
         SET FecIniValid = dFecIniVig,
             FecFinValid = dFecFinVig
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, 'Error actualizando DEC_GEN: ' || SQLERRM);
      END;
      -- ACTUALIZANDO FecHA DE INGRESO --
      BEGIN
         UPDATE ASEGURADO ASE
         SET FecIng =
                (SELECT NVL (MAX (AS1.FecIng), dFecIniVig)
                 FROM   ASEGURADO AS1, CERTIFICADO CE, POLIZA P2, POLIZA P1
                 WHERE  P2.CodProd = P1.CodProd
                 AND    P2.NumPol = P1.NumPol
                 AND    NVL (P2.NumRen, 0) < NVL (P1.NumRen, 0)
                 AND    P1.IdePol = CE.IdePol
                 AND    CE.IdePol = nIdePol
                 AND    CE.NumCert = ASE.NumCert
                 AND    AS1.IdePol = P2.IdePol
                 AND    AS1.NumCert = ASE.NumCert
                 AND    AS1.CodCli = ASE.CodCli
                 AND    AS1.CodRamoCert = ASE.CodRamoCert)
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, 'Error actualizando ASEGURADO: ' || SQLERRM);
      END;
      --
      BEGIN
         UPDATE BENEFICIARIO BENE
         SET FecING =
                (SELECT NVL (MAX (BE1.FecIng), dFecIniVig)
                 FROM   BENEFICIARIO BE1, CERTIFICADO CE, POLIZA P2, POLIZA P1
                 WHERE  P2.CodProd = P1.CodProd
                 AND    P2.NumPol = P1.NumPol
                 AND    NVL (P2.NumRen, 0) < NVL (P1.NumRen, 0)
                 AND    P1.IdePol = CE.IdePol
                 AND    CE.IdePol = nIdePol
                 AND    CE.NumCert = BENE.NumCert
                 AND    BE1.IdePol = P2.IdePol
                 AND    BE1.NumCert = BENE.NumCert
                 AND    BE1.TipoId = BENE.TipoId
                 AND    BE1.NumId = BENE.NumId
                 AND    BE1.DvId = BENE.DvId
                 AND    BE1.CodRamoCert = BENE.CodRamoCert)
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, 'Error actualizando BENEFICIARIO: ' || SQLERRM);
      END;
      --
      BEGIN
         UPDATE BENEF_ASEG BEA
         SET FecIng =
                (SELECT NVL (MAX (AS1.FecIng), dFecIniVig)
                 FROM   POLIZA P2, POLIZA P1, CERTIFICADO CE, ASEGURADO AS2, ASEGURADO AS1, BENEF_ASEG BE1
                 WHERE  P2.CodProd = P1.CodProd
                 AND    P2.NumPol = P1.NumPol
                 AND    NVL (P2.NumRen, 0) < NVL (P1.NumRen, 0)
                 AND    P1.IdePol = CE.IdePol
                 AND    CE.IdePol = nIdePol
                 AND    CE.NumCert = AS1.NumCert
                 AND    AS2.IdePol = P2.IdePol
                 AND    AS2.NumCert = AS1.NumCert
                 AND    AS2.CodCli = AS1.CodCli
                 AND    AS1.IdeAseg = BEA.IdeAseg
                 AND    BE1.IdeAseg = AS2.IdeAseg
                 AND    BE1.NomBen = BEA.NomBen
                 AND    BE1.NumBen = BEA.NumBen)
         WHERE  IdeAseg IN (SELECT IdeAseg
                            FROM   ASEGURADO
                            WHERE  IdePol = nIdePol);
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, 'Error actualizando BENEF_ASEG: ' || SQLERRM);
      END;
      --
      BEGIN
         UPDATE COND_VEH COV
         SET FecIng =
                (SELECT NVL (MAX (CV1.FecIng), dFecIniVig)
                 FROM   COND_VEH CV1, CERTIFICADO CE, POLIZA P2, POLIZA P1
                 WHERE  P2.CodProd = P1.CodProd
                 AND    P2.NumPol = P1.NumPol
                 AND    NVL (P2.NumRen, 0) < NVL (P1.NumRen, 0)
                 AND    P1.IdePol = CE.IdePol
                 AND    CE.IdePol = nIdePol
                 AND    CE.NumCert = COV.NumCert
                 AND    CV1.NomCondVeh = COV.NomCondVeh
                 AND    CV1.NumIdCond = COV.NumIdCond
                 AND    CV1.Idepol = P2.IdePol
                 AND    CV1.NumCert = COV.NumCert)
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, 'Error actualizando COND_VEH: ' || SQLERRM);
      END;
   --
   END CAMBIAR_VIGENCIA;
   --///
   --CARCULO_TASA_PROMEDIO
   FUNCTION CARCULO_TASA_PROMEDIO (
      nIdePol          POLIZA.IdePol%TYPE,
      cCodRamocert     CERT_RAMO.CodRamocert%TYPE,
      cCodCobert       COBERT_PLAN_PROD.CodCobert%TYPE,
      cTipoSecion      VARCHAR2)
      RETURN NUMBER IS
      nCantAsegurados   NUMBER (30);
      nTasaPromedio     NUMBER (8, 6);
      nTasa             NUMBER (8, 6);
      nSumAsegurada     COBERT_ASEG.SumaAsegMoneda%TYPE;
      nPrimaMoneda      COBERT_ASEG.PRIMAMONEDA%TYPE;
   BEGIN
      -- SE LOS ASEGURADOS DE LA POLIZA --
      nCantAsegurados  :=
                    (PR_ASEGURADO.CANT_DE_ASEG (nIdePol, cCodRamocert, cCodCobert) + PR_CERTIFICADO.CANT_DE_CERT (nIdePol, cCodRamocert, cCodCobert));
      nTasa            := (PR_COBERT_CERT.TASA_ASEG_CERT (nIdePol, cCodCobert) + PR_COBERT_ASEG.TASA_ASEG (nIdePol, cCodCobert));
      nSumAsegurada    := (PR_COBERT_CERT.SUMA_ASEG_CERT (nIdePol, cCodCobert) + PR_COBERT_ASEG.SUMA_ASEG (nIdePol, cCodCobert));
      nPrimaMoneda     := (PR_COBERT_CERT.PRIMA_ASEG_CERT (nIdePol, cCodCobert) + PR_COBERT_ASEG.PRIMA_ASEG (nIdePol, cCodCobert));
      --
      IF cTipoSecion = '001' THEN
         IF NVL (nCantAsegurados, 0) <> 0 THEN
            nTasaPromedio  := (nTasa / nCantAsegurados);
         END IF;
      ELSIF cTipoSecion = '002' THEN
         IF NVL (nCantAsegurados, 0) <> 0 THEN
            nTasaPromedio  := (nPrimaMoneda / nSumAsegurada);
         END IF;
      ELSE
         RAISE_APPLICATION_ERROR (-20100, ' No existe un carculo para la Tasa Promedio en la Poliza.');
      END IF;
      --
      IF NVL (nCantAsegurados, 0) = 0 THEN
         RETURN (0);
      ELSE
         RETURN (nTasaPromedio);
      END IF;
   END CARCULO_TASA_PROMEDIO;
   --//
   --REHABILITAR
   PROCEDURE REHABILITAR (nIdePol NUMBER, nTotal NUMBER, cCodPlan VARCHAR2, cModPlan VARCHAR2, dFecRehab DATE, nMtoTotGiro NUMBER, nMtoComCalc NUMBER) IS
      nNumCert        NUMBER (10);
      nCantGiros      NUMBER (10)                   := 0;
      nNumFinancOld   NUMBER (14);
      cTipoId         TERCERO.TipoId%TYPE;
      nNumId          TERCERO.NumId%TYPE;
      cDvId           TERCERO.Dvid%TYPE;
      cCodInter       INTERMEDIARIO.CodInter%TYPE;
      cCodCobrador    VARCHAR2 (6);
      nNumFinanc      NUMBER (14);
      nNumOper        NUMBER (14);
      nMtoTotGiroC    NUMBER (14, 2);
      nNumOperOld     NUMBER (14);
      nPorcPago       NUMBER;
      nNumENDoso      NUMBER (10);
      cCodOfiemi      VARCHAR2 (6);
      cCodProd        VARCHAR2 (4);
      nIdeRecAnu      NUMBER (14);
      nNumOperAct     NUMBER (14);
      nIdeFact        FACTURA.IdeFact%TYPE;
      nIdeComp        COMPROBANTE.IdeComp%TYPE;
      cCodOper        COMPROBANTE.TipComp%TYPE      := '214';
      nNumOblig       OBLIGACION.NumOblig%TYPE;
      nNumAcre        ACREENCIA.NumAcre%TYPE;
      nNumAcreAnu     ACREENCIA.NumAcre%TYPE;
      nSumaAsegAnt    NUMBER;
      nFlag           NUMBER := 0;
      cIndAbono       VARCHAR2(1);

      CURSOR REG_CERTIFICADO_ANU IS
         SELECT NumCert
         FROM   CERTIFICADO
         WHERE  IdePol = nIdePol
         AND    StsCert IN ('ANU', 'SAL', 'PRO', 'CAD')
       -- <I TD 3815 >- Gina Camacho -15/11/2005 -No se deben rehabilitar los certificados
       --afectados por siniestros
       AND    Numcert NOT IN  (SELECT NumCert
                               FROM SINIESTRO S
                               WHERE IDEPOL    = nIdePol
                               AND S.StsSin    <> 'ANU');
      -- <F TD 3815>   ;
      CURSOR Q_FINANC_C IS
         SELECT CF.NumFinanc, CF.NumCert, SUM (MtoComGiro) MtoComGiro
         FROM   COND_FINANCIAMIENTO CF, GIROS_FINANCIAMIENTO GF
         WHERE  GF.NumFinanc = CF.NumFinanc
         AND    GF.StsGiro = 'ANU'
         AND    CF.IdePol = nIdePol
         AND    NumRefracc IS NULL
         AND    NVL (IndRehab, 0) <> 1
         AND    cf.NumFinanc IN (SELECT MAX (NumFinanc)
                                 FROM   COND_FINANCIAMIENTO C
                                 WHERE  C.IdePol = nIdePol
                                 AND    C.NumRefracc IS NULL
                                 AND    NVL (C.IndRehab, 0) <> 1
                                 AND    C.NumCert = CF.NumCert)
         GROUP BY CF.NumFinanc, CF.NumCert;
      CURSOR REC_CUR IS
         SELECT DISTINCT (O.NumOper) nNumOper
         FROM   OPER_POL O, RECIBO R
         WHERE  O.IdePol = nIdePol
         AND    O.IdePol = R.IdePol
         AND    O.TipoOp = 'ANU'
         AND    O.NumOper = R.NumOper
         AND    R.StsRec = 'ACT';
      CURSOR C_RECIBOS IS
         SELECT DISTINCT A.NumAcre, REC.CodRamoCert, REC.Iderec
         FROM   ACREENCIA A, GIROS_FINANCIAMIENTO GF, REC_FINANCIAMIENTO RF, RECIBO REC
         WHERE  A.Numacre = nNumacreAnu
         AND    GF.Numacre = A.Numacre
         AND    RF.numfinanc = GF.Numfinanc
         AND    REC.iderec = RF.Iderec;
   BEGIN
   --Erick Zoque 20/05/2013 E_CON_20120821_1_7
   PR_FECHAS_EQUIVALENTES.CEVENTO := '001';
      nSumaAsegAnt := SUMA_ASEGURADA_POL(nIdePol);
      PR_OPERACION.INICIAR ('REHAB', 'POLIZA', TO_CHAR (nIdePol));
      nNumOper    := PR_OPERACION.nNumOper;
      PR_POLIZA.CARGAR (nIdePol);
      cCodProd    := PR_POLIZA.cCodProd;
      cCodOfiemi  := PR_POLIZA.cCodOfiemi;
      --<<BBVA EFVC Consis 29/10/2015 - Se cambia temporalmente indicado para que permita hacer la rehabilitacion de polizas
      BEGIN
         SELECT NVL(IndAbono,'N') INTO cIndAbono
         FROM  PRODUCTO
         WHERE  CodProd = cCodProd;
      END;
      IF nFlag = 0 AND cIndAbono = 'N' THEN
         nFlag := 1;
         UPDATE PRODUCTO
         SET IndAbono='S'
         WHERE CodProd = cCodProd;
      END IF;
      -->>BBVA EFVC Consis 29/10/2015
      IF nTotal = 0 THEN
         FOR R_CERT IN REG_CERTIFICADO_ANU LOOP
            nNumCert  := R_CERT.NumCert;
            PR_CERTIFICADO.REHABILITAR (nIdePol, nNumCert);
            BEGIN
               SELECT MAX (NumFinanc)
               INTO   nNumFinanc
               FROM   COND_FINANCIAMIENTO
               WHERE  IdePol = nIdePol
               AND    NumCert = nNumCert
               AND    StsFin = 'ANU';
            EXCEPTION
               WHEN NO_DATA_FOUND THEN
                  nNumFinanc  := NULL;
            END;
            IF nNumFinanc IS NOT NULL THEN
               UPDATE COND_FINANCIAMIENTO
               SET IndRehab = 1
               WHERE  NumFinanc = nNumFinanc;
            END IF;
         END LOOP;
         IF PR_POLIZA.cStsPol IN ('SAL', 'PRO') THEN
            UPDATE POLIZA
            SET FecFinVig = PR_MOD_COBERT.dFecFinVig,
                FecRen = PR_MOD_COBERT.dFecFinVig
            WHERE  IdePol = nIdePol;
         END IF;
         PR_POLIZA.GENERAR_TRANSACCION (nIdePol, 'REHAB');
         -- <<I. TD  7174. Anusky Gonzalez. 09/02/2006 >>
         FOR C_COA IN (SELECT CodAcepRiesgo
                       FROM DIST_COA
                       WHERE IdePol = nIdePol
                       AND   StsCoa = 'ANU'
                       AND   FecExc IS NULL) LOOP
            PR_DIST_COA.GENERAR_TRANSACCION (nIdePol,C_COA.CodAcepRiesgo, 'REHAB');
         END LOOP;
         -- <<F. TD  7174. Anusky Gonzalez. 09/02/2006 >>
         --DV. Se quita activaci�n desde rehabilitaci�n para permitir evaluar niveles de autoridad
         PR_POLIZA.cOperRev := 'REH';
         nNumOperAct  := PR_POLIZA.ACTIVAR (nIdePol, 'D');
         PR_ACREENCIA.GENERAR (nNumOperAct);
         PR_FRACCIONAMIENTO.GENERAR (nNumOperAct);
         PR_FRACCIONAMIENTO.FACTUR_REHAB (nNumOperAct);
         PR_POLIZA.COMPENSAR_REH(nIdePol);

         --<< BBVA - 1.0.1.1 EFBC RM22491 - Se cambia el estado de la operaci�n a REH. Se cambia el monto 0 a 1 para que procese reaseguro de esta operacion
         UPDATE OPER_POL
            SET Tipoop = PR_POLIZA.cOperRev, MtoOper = DECODE(MtoOper, 0 , 1, MtoOper)
          WHERE Numoper = nNumOperAct;
         -->> BBVA - 1.0.1.1 EFBC RM22491
      ELSE
         nNumENDoso  := PR_POLIZA.NumERO_ENDOSO (cCodProd, cCodOfiemi, 'EREA');
         FOR QF IN Q_FINANC_C LOOP
            BEGIN
               SELECT CodInter, A.CodCobrador
               INTO   cCodInter, cCodCobrador
               FROM   FACTURA F, ACREENCIA A
               WHERE  F.IdeFact = A.IdeFact
               AND    A.NumAcre IN (SELECT MAX (NumAcre)
                                    FROM   GIROS_FINANCIAMIENTO
                                    WHERE  NumFinanc = QF.NumFinanc);
            EXCEPTION
               WHEN NO_DATA_FOUND THEN
                  SELECT CodInterLider, CodCobrador
                  INTO   cCodInter, cCodCobrador
                  FROM   ACREENCIA A
                  WHERE  A.NumAcre IN (SELECT MAX (NumAcre)
                                       FROM   GIROS_FINANCIAMIENTO
                                       WHERE  NumFinanc = QF.NumFinanc);
            END;
            BEGIN
               SELECT NumFinanc, TipoId, NumId, DvId, NumOper, PorcPago, NumCert
               INTO   nNumFinancOld, cTipoId, nNumId, cDvId, nNumOperOld, nPorcPago, nNumCert
               FROM   COND_FINANCIAMIENTO
               WHERE  NumFinanc = QF.NumFinanc;
            END;
            --Erick Zoque 20/05/2013 E_CON_20120821_1_7
            IF dFecRehab < PR_FECHAS_EQUIVALENTES.FECHA_EMISION (SYSDATE, PR_POLIZA.cCodCia,'001') THEN
               PR_OPER_USUARIO.AUTORIZAR ('127', NULL, NULL, NULL, NULL, NULL, 0, NULL, cCodProd, 'REH', dFecRehab);
            END IF;
            INSERT INTO OPER_POL
                        (IDEPOL, NUMCERT, NUMOPER, FECMOV, MTOOPER, TIPOOP, MTOOPERANU, NUMENDOSO, INDANUL)
                 VALUES (nIdePol, nNumCert, PR_OPERACION.nNumOper, dFecRehab, 0, 'REH', 0, nNumENDoso, 'N');
            UPDATE RECIBO SET TipoOpe = 'REH'
            WHERE  IdePol = nIdePol
            AND    NumCert = nNumCert
            AND    NumOper = PR_OPERACION.nNumOper;

            BEGIN
               SELECT SUM (MtoDetGiroLocal)
               INTO   nMtoTotGiroC
               FROM   DET_GIRO_FIN
               WHERE  NumDetGiro = 1
               AND    NumFinanc = QF.NumFinanc
               AND    NumGiro IN (SELECT NumGiro
                                  FROM   GIROS_FINANCIAMIENTO
                                  WHERE  StsGiro = 'ANU'
                                  AND    NumFinanc = QF.NumFinanc);
            END;
            PR_COND_FINANCIAMIENTO.REFRACCIONAR_RESP (nIdePol,
                                                      nNumCert,
                                                      nPorcPago,
                                                      cTipoId,
                                                      nNumId,
                                                      cDvId,
                                                      nMtoTotGiroC,
                                                      QF.MtoComGiro,
                                                      nMtoTotGiroC,
                                                      dFecRehab,
                                                      cCodPlan,
                                                      cModPlan,
                                                      cCodInter,
                                                      cCodCobrador,
                                                      dFecRehab,
                                                      NULL,
                                                      nNumOper,
                                                      nNumOperOld,
                                                      'N',
                                                      'N');
            UPDATE COND_FINANCIAMIENTO
            SET IndRehab = 1
            WHERE  NumFinanc = QF.NumFinanc;
         END LOOP;
         PR_POLIZA.ACTUALIZA_ENDOSO (cCodProd, cCodOfiemi, 'EREA');
         FOR R_CERT IN REG_CERTIFICADO_ANU LOOP
            nNumCert  := R_CERT.NumCert;
            PR_CERTIFICADO.REHABILITAR (nIdePol, nNumCert);
         END LOOP;
         PR_POLIZA.GENERAR_TRANSACCION (nIdePol, 'REHAB');
         -- <<I. TD  7174. Anusky Gonzalez. 09/02/2006 >>
         FOR C_COA IN (SELECT CodAcepRiesgo
                       FROM DIST_COA
                       WHERE IdePol = nIdePol
                       AND   StsCoa = 'ANU'
                       AND   FecExc IS NULL) LOOP
            PR_DIST_COA.GENERAR_TRANSACCION (nIdePol,C_COA.CodAcepRiesgo, 'REHAB');
         END LOOP;
         -- <<F. TD  7174. Anusky Gonzalez. 09/02/2006 >>
         PR_ACREENCIA.GENERAR (nNumOper);
         IF PR.BUSCA_PARAMETRO ('055') = 'SI' THEN
            PR_FACTURA.GENERAR (nNumOper);
         END IF;
         COMP_SUMAASEG(nIdePol,nNumOper,'ACT',nSumaAsegAnt);
      END IF;
      --<<BBVA EFVC Consis 29/10/2015 Si se cambai indiciador se deja a como estaba inicialmente.
      IF nFlag = 1 THEN
         UPDATE PRODUCTO
         SET IndAbono='N'
         WHERE CodProd = cCodProd;
      END IF;
      -->>BBVA EFVC Consis 29/10/2015
      FOR I IN REC_CUR LOOP
         UPDATE RECIBO
         SET StsRec = 'ANU'
         WHERE  NumOper = nNumOper
         AND    StsRec <> 'ANU';
      END LOOP;
      --<< BBVA - 1.0.1.1 EFBC RM22491 - Se actualiza el NUMREVER en la modificaci�n reversada. Se actualiza el estado a REV en la nueva modificaci�n.
      UPDATE MOD_COBERT
         SET NUMREVER = nNumOper
       WHERE IDEPOL = nIdePol
         AND NUMREVER IS NULL
         AND IDEMOVPRIMA IN
             (SELECT IDEMOVPRIMA FROM RECIBO WHERE IDEPOL = nIdePol)
         AND NUMMOD IN
             (SELECT MAX(NUMMOD) - 1 FROM MOD_COBERT WHERE IDEPOL = nIdePol);

      UPDATE MOD_COBERT
         SET STSMODCOBERT = 'REV'
       WHERE IDEPOL = nIdePol
         AND IDEMOVPRIMA IN
             (SELECT IDEMOVPRIMA FROM RECIBO WHERE IDEPOL = nIdePol)
         AND NUMMOD IN
             (SELECT MAX(NUMMOD) FROM MOD_COBERT WHERE IDEPOL = nIdePol);
      -->> BBVA - 1.0.1.1 EFBC RM22491
      /* ESVI032 -- Javier Asmat -- 01.03.2005
      Codigo innecesario para la rehabilitaci�n, se debe buscar la acreencia generada durante la misma
      -- Anular Factura TCR
      BEGIN
         SELECT MAX (NumOperAnu)
         INTO   nNumOperOld
         FROM   OPER_POL
         WHERE  IdePol = nIdePol;
      END;
      BEGIN
         SELECT F.IdeFact, A.NumAcre
         INTO   nIdeFact, nNumAcre
         FROM   FACTURA F, ACREENCIA A
         WHERE  F.NumOper = nNumOperOld
         AND    A.IdeFact = F.IdeFact;
      END;
      BEGIN
         SELECT MAX (NumOblig)
         INTO   nNumOblig
         FROM   OBLIGACION
         WHERE  NumOper = nNumOperOld;
      END;
      -- Buscar Acreenciada por Anulacion
      BEGIN
         SELECT MAX (NumAcre)
         INTO   nNumAcreAnu
         FROM   REL_OBLACR_FRAC
         WHERE  NumOblig = nNumOblig;
      END;
      */
      --Buscamos la acreencia a ser contabilizada
      BEGIN
         SELECT A.NumAcre
         INTO   nNumAcre
         FROM   ACREENCIA A, FACTURA F
         WHERE  F.NumOper = nNumOperAct
         AND    A.IdeFact = F.IdeFact;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            nNumAcre := 0; -->> BBVA - 1.0.1.1 EFBC RM22491 - REH CUANDO LA POLIZA FUE ANULADA A FIN DE VIGENCIA
            --RAISE_APPLICATION_ERROR (-20052, PR.MENSAJE ('ABD', 20052, 'No existe acreencias para la operaci�n ' || nNumOperAct, NULL, NULL));
         WHEN TOO_MANY_ROWS THEN
            BEGIN
               SELECT MAX (A.NumAcre)
               INTO   nNumAcre
               FROM   ACREENCIA A, FACTURA F
               WHERE  F.NumOper = nNumOperAct
               AND    A.IdeFact = F.IdeFact
               AND    F.StsFact = 'INC'
               AND    A.StsAcre = 'ACT';
            END;
      END;
      BEGIN
         SELECT NVL (MAX (IdeComp), 0)
         INTO   nIdeComp
         FROM   COMPROBANTE
         WHERE  NumOper = NVL (nNumOperAct, nNumOper);
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            nIdeComp  := 0;
      END;
      IF NVL (nIdeComp, 0) <> 0 AND NVL(nNumAcre, 0) <> 0 THEN -->> BBVA - 1.0.1.1 EFBC RM22491
         FOR Y IN C_RECIBOS LOOP
            PR_MOV_DEFINITIVO.GEN_MOV (nNumAcre, cCodOper, nIdeComp, Y.CodRamoCert, Y.Iderec);
         END LOOP;
         PR_COMPROBANTE.CERRAR (nIdeComp, 'PRC');
         PR_COMPROBANTE.REGISTRA_ERROR (nIdeComp, NULL, NULL, nNumAcre, NULL, SYSDATE, NULL, NULL, 'PR_POLIZA.REHABILITAR', cCodOper);
      END IF;
      /* Codigo innecesario
      IF nIdeFact IS NOT NULL THEN
         PR_FACTURA.ANULAR (nIdeFact);
      END IF;
      */
      --
      PR_OPERACION.TERMINAR;
      --Erick Zoque 20/05/2013 E_CON_20120821_1_7
      PR_FECHAS_EQUIVALENTES.CEVENTO := NULL;
   END REHABILITAR;
      --///
   -- RENOV_ACTIVA
   -- La variable cIndMovPolRen, ser� utilizada para indicarle al proceso, que no se trata
   -- de un reverso normal de renovaci�n, sino, que el usuario, ya teniendo la p�liza
   -- renovada, decide a trav�s del BOTON que est� en MANTPOL, llevar la p�liza de estatus
   -- REN a estatus ACT para hacerle modificaciones, entonces, es cuando se encender�
   -- el indicador IndMovPolRen en la tabla POLIZA...
   -- Pedro Ferreira, FEBRERO/2005
   PROCEDURE RENOV_ACTIVA (nIdePol NUMBER, cIndmovPolRen VARCHAR2) IS
      CURSOR C_CERTIFICADO IS
         SELECT NumCert
         FROM   CERTIFICADO
         WHERE  IdePol = nIdePol
         AND    StsCert = 'REN';
   BEGIN
   --Erick Zoque 20/05/2013 E_CON_20120821_1_7
   PR_FECHAS_EQUIVALENTES.CEVENTO := '001';
      FOR X IN C_CERTIFICADO LOOP
         PR_CERTIFICADO.RENOV_ACTIVA (nIdePol, X.NumCert);
      END LOOP;
      --
      IF NVL (cIndMovPolRen, 'N') = 'S' THEN
         BEGIN
            UPDATE POLIZA
            SET IndMovPolRen = 'S'
            WHERE  IdePol = nIdePol;
         EXCEPTION
            WHEN OTHERS THEN
               RAISE_APPLICATION_ERROR (-20100, PR.MENSAJE ('AIF', 22052, SQLERRM, NULL, NULL));
         END;
      END IF;
      PR_POLIZA.GENERAR_TRANSACCION (nIdePol, 'RNVAC');
      PR_DIST_COA.renov_activa (nidepol);
      --Erick Zoque 20/05/2013 E_CON_20120821_1_7
      PR_FECHAS_EQUIVALENTES.CEVENTO := NULL;
   END;
   --
   -- ACTIVA_RENOV
   PROCEDURE ACTIVA_RENOV (nIdePol NUMBER) IS
      CURSOR C_CERTIFICADO IS
         SELECT NumCert
         FROM   CERTIFICADO
         WHERE  IdePol = nIdePol
         AND    StsCert = 'ACT';
   BEGIN
   --Erick Zoque 20/05/2013 E_CON_20120821_1_7
   PR_FECHAS_EQUIVALENTES.CEVENTO := '001';
      FOR X IN C_CERTIFICADO LOOP
         PR_CERTIFICADO.ACTIVA_RENOV (nIdePol, X.NumCert);
      END LOOP;
      --
      BEGIN
         UPDATE POLIZA
         SET IndMovPolRen = 'N'
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, PR.MENSAJE ('AIF', 22052, SQLERRM, NULL, NULL));
      END;
      PR_POLIZA.GENERAR_TRANSACCION (nIdePol, 'ACRNV');
      --<I Defect 3407> Javier Asmat/22.07.2005
      --Se cambia estado de DIST_COA de ACT a REN
      PR_DIST_COA.ACTIV_RENOV (nIdePol);
      --Erick Zoque 20/05/2013 E_CON_20120821_1_7
      PR_FECHAS_EQUIVALENTES.CEVENTO := NULL;
   --<F Defect 3407>
   END;
   --
      -- Saldar o prorrogar la poliza
   PROCEDURE SALDAR_PRORROGAR (nIdePol NUMBER, nNumCert NUMBER, cCodPlan VARCHAR2, cRevPlan VARCHAR2, cCodRamoCert VARCHAR2, cIndProSal VARCHAR2) IS
      CSTSPOL   POLIZA.STSPOL%TYPE;
   BEGIN
      IF cIndProSal = 'SALDA' THEN
         PR_OPER_USUARIO.AUTORIZAR ('068', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
         PR_OPERACION.INICIAR ('SALDA', 'POLIZA', nIdePol);
      ELSIF cIndProSal = 'PRORR' THEN
         PR_OPER_USUARIO.AUTORIZAR ('069', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
         PR_OPERACION.INICIAR ('PRORR', 'POLIZA', nIdePol);
      ELSIF cIndProSal = 'INVAL' THEN
         PR_OPER_USUARIO.AUTORIZAR ('068', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
         PR_OPERACION.INICIAR ('INVAL', 'POLIZA', nIdePol);
      END IF;
      PR_B$_POLIZA.PUSH (nIdePol, cIndProSal, 'PRC');
      PR_CERTIFICADO.SALDAR_PRORROGAR (nIdePol, nNumCert, cCodPlan, cRevPlan, cCodRamoCert, cIndProSal);
      BEGIN
         SELECT StsPol
         INTO   cStsPol
         FROM   POLIZA
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            RAISE_APPLICATION_ERROR (-20100, 'No Existe EL idepol = ' || nIdePol);
      END;
      IF cStsPol NOT IN ('PRO', 'SAL') THEN
         PR_POLIZA.GENERAR_TRANSACCION (nIdePol, cIndProSal);
      END IF;
      PR_OPERACION.TERMINAR;
   END SALDAR_PRORROGAR;
   --//
   --
   PROCEDURE ANULAR_SIN_CALC (nIdePol NUMBER, dFecAnul DATE, cCodMotvAnul VARCHAR2, cTextMotvAnul VARCHAR2) IS
      nNumCert         NUMBER (10);
      nNumOper         NUMBER (14);
      nTotAnuPol       NUMBER (14, 2);
      nTotAnuCert      NUMBER (14, 2);
      cCodOfiemi       VARCHAR2 (6);
      nPolIFactura     NUMBER (14);
      cIndAdELSusc     VARCHAR2 (1);
      nIndPolVida      NUMBER (1);
      nMontoPrestamo   NUMBER (14, 2);
      cCodProd         POLIZA.CodProd%TYPE;
      nIdeAseg         ASEGURADO.IdeAseg%TYPE;
      cGenRetiro       VARCHAR2 (1);
      dFecSts          DATE;
      CURSOR REG_CERTIFICADO_ANU IS
         SELECT NumCert
         FROM   CERTIFICADO
         WHERE  IdePol = nIdePol
         AND    StsCert NOT IN ('ANU', 'EXC', 'CAD');
      CURSOR FINAN_OPER IS
         SELECT C.NumOper
         FROM   COND_FINANCIAMIENTO C
         WHERE  C.IdePol = nIdePol
         AND    C.StsFin = 'FRA'
         AND    C.NumCert = nNumCert;
      CURSOR FINAN IS
         SELECT C.NumFinanc, SUM (C.MtoNetoFrac) MtoNetoFrac
         FROM   COND_FINANCIAMIENTO C
         WHERE  C.IdePol = nIdePol
         AND    C.StsFin = 'FRA'
         AND    C.NumCert = nNumCert
         GROUP BY C.NumFinanc;
   BEGIN
   --Erick Zoque 20/05/2013 E_CON_20120821_1_7
      UPDATE POLIZA
      SET    CLASEANUL = 'RVC'
      WHERE  IdePol  = nIdePol;
   PR_FECHAS_EQUIVALENTES.CEVENTO := '001';
      PR_OPERACION.INICIAR ('ANULA', 'POLIZA', TO_CHAR (nIdePol));
      nNumOper    := PR_OPERACION.nNumOper;
      BEGIN
         SELECT CodOfiemi, CodProd
         INTO   cCodOfiemi, cCodProd
         FROM   POLIZA
         WHERE  IdePol = nIdePol;
      END;
      IF cCodMotvAnul IS NOT NULL THEN
         BEGIN
            UPDATE POLIZA
            SET CodMotvAnul = cCodMotvAnul,
                TextMotvAnul = cTextMotvAnul,
                FecAnul = dFecAnul,
                FecOper = dFecAnul
            WHERE  IdePol = nIdePol;
         END;
      END IF;
      nTotAnuPol  := 0;
      PR_POLIZA.CARGAR (nIdePol);
      --Erick Zoque 20/05/2013 E_CON_20120821_1_7
      dFecSts     := PR_FECHAS_EQUIVALENTES.FECHA_EMISION (SYSDATE, PR_POLIZA.cCodCia,'001');
      FOR R_CERT IN REG_CERTIFICADO_ANU LOOP
         nTotAnuCert  := 0;
         nNumCert     := R_CERT.NumCert;
         PR_CERTIFICADO.AnulAR (nIdePol, nNumCert, dFecAnul);
         BEGIN
            --Erick Zoque 20/05/2013 E_CON_20120821_1_7
            IF dFecAnul < PR_FECHAS_EQUIVALENTES.FECHA_EMISION (SYSDATE, PR_POLIZA.cCodCia,'001') THEN
               PR_OPER_USUARIO.AUTORIZAR ('127', NULL, NULL, NULL, NULL, NULL, 0, NULL, cCodProd, 'ESM', dFecAnul);
            END IF;
            INSERT INTO OPER_POL
                        (TipoOp, IdePol, NumCert, NumOper, FecMov, MtoOper, IndAnul)
                 VALUES ('ESM', nIdePol, nNumCert, nNumOper, dFecSts, 0, 'N');

          --<<BBVA Consis 21/07/2015 - Se agrega registro en la tabla para guadar mvto.
            BEGIN
              INSERT INTO TEXTO_ENDOSOS(IdePol,NumCert,NumOper,Tipo_endoso,Fec_Endoso,Desc_Cambio)
              VALUES(nIdePol,nNumCert,nNumOper,'ESM',SYSDATE,'ANULADA POR SINIESTRO');
            END;
            -->>BBVA Consis 21/07/2015.

            UPDATE RECIBO SET TipoOpe = 'ESM'
            WHERE  IdePol = nIdePol
            AND    NumCert = nNumCert
            AND    NumOper = nNumOper;
         EXCEPTION
            WHEN OTHERS THEN
               -- RR 23/01/2006
               RAISE_APPLICATION_ERROR(-20100,PR.MENSAJE('ABD', 20289, 'OPER_POL. '||SQLERRM, NULL, NULL));
         END;
         FOR A IN FINAN_OPER LOOP
            UPDATE OPER_POL
            SET NumOperAnu = nNumOper
            WHERE  NumOper = A.NumOper
            AND    NumCert = nNumCert;
         END LOOP;
         FOR A IN FINAN LOOP
            --EZ 30/05/2014 incidencia de revocaciones 1682
            --PR_COND_FINANCIAMIENTO.AnulAR (A.NumFinanc, NULL, 'N');
            nTotAnuCert  := nTotAnuCert + A.MtoNetoFrac;
         END LOOP;
         nTotAnuPol   := nTotAnuPol + nTotAnuCert;
         IF nTotAnuCert > 0 THEN
            UPDATE OPER_POL
            SET MtoOperAnu = -nTotAnuCert
            WHERE  NumOper = nNumOper
            AND    NumCert = nNumCert;
         END IF;
      END LOOP;
      PR_POLIZA.GENERAR_TRANSACCION (nIdePol, 'ANULA');
      BEGIN
         SELECT 1, P.CodProd, P.CodOfiemi, A.IdeAseg
         INTO   nIndPolVida, cCodProd, cCodOfiemi, nIdeAseg
         FROM   POLIZA P, CERT_RAMO CR, ASEGURADO A, COBERT_ASEG CA, VAL_GAR_PLAN_VIDA V
         WHERE  P.IdePol = nIdePol
         AND    CR.IdePol = P.IdePol
         AND    A.IdePol = P.IdePol
         AND    A.NumCert = CR.NumCert
         AND    A.CodRamoCert = CR.CodRamoCert
         AND    A.CodPlan = CR.CodPlan
         AND    A.RevPlan = CR.RevPlan
         AND    CA.IdeAseg = A.IdeAseg
         AND    V.CodProd = P.CodProd
         AND    V.CodPlan = A.CodPlan
         AND    V.RevPlan = A.RevPlan
         AND    V.CodRamo = A.CodRamoCert
         AND    V.CodCobert = CA.CodCobert;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            nIndPolVida  := 0;
         WHEN TOO_MANY_ROWS THEN
            nIndPolVida  := 1;
      END;
--       Se elimina este proceso porque NO procede... de acuerdo a Test Director No. 5510
--       Conversado con Clara Galindo el 24/11/2005  - Edwin Cu�llar
      /*IF nIndPolVida = 1 THEN
         --  Generar la Liquidacion del fondo asociado a la poliza.
         BEGIN
            SELECT NVL (SUM (ValorMov), 0)
            INTO   nMontoPrestamo
            FROM   FONDO F, FONDO_POL FP
            WHERE  FP.IdePol = nIdePol
            AND    F.IdeFondo = FP.IdeFondo;
         EXCEPTION
            WHEN OTHERS THEN
               nMontoPrestamo  := 0;
         END;
         IF nMontoPrestamo > 0 THEN
            cGenRetiro  := PR_PRESTAMO_VIDA.GENERAR_RETIRO_TOTAL (cCodProd, nIdePol, nIdeAseg, nMontoPrestamo, cCodOfiemi, dFecAnul);
         END IF;
      END IF;*/
      PR_OPERACION.TERMINAR;
      --Erick Zoque 20/05/2013 E_CON_20120821_1_7
      PR_FECHAS_EQUIVALENTES.CEVENTO := NULL;
   END;
   --///
   -- CAMBIO_MONEDA
   PROCEDURE CAMBIO_MONEDA (nIdePol POLIZA.IdePol%TYPE, cCodMoneda VINISUSC.CodMoneda%TYPE) IS
   BEGIN
      -- DATOS DEL CERTIFICADO --
      BEGIN
         UPDATE COBERT_CERT
         SET CodMoneda = cCodMoneda
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, 'Error actualizando COBERT_CERT: ' || SQLERRM);
      END;
      -- DATOS DEL ASEGURADO --
      BEGIN
         UPDATE ASEGURADO
         SET CodMoneda = cCodMoneda
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, 'Error actualizando ASEGURADO: ' || SQLERRM);
      END;
      -- COBERTURAS DEL ASEGURADO
      BEGIN
         UPDATE COBERT_ASEG
         SET CodMoneda = cCodMoneda
         WHERE  IdeAseg IN (SELECT IdeAseg
                            FROM   ASEGURADO
                            WHERE  IdePol = nIdePol);
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, 'Error actualizando COBERT_ASEG: ' || SQLERRM);
      END;
      --   DATOS DE BIEN --
      BEGIN
         UPDATE BIEN_CERT
         SET CodMoneda = cCodMoneda
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, 'Error actualizando BIEN_CERT: ' || SQLERRM);
      END;
      --  COBERTURAS DEL BIEN
      BEGIN
         UPDATE COBERT_BIEN
         SET CodMoneda = cCodMoneda
         WHERE  IdeBien IN (SELECT IdeBien
                            FROM   BIEN_CERT
                            WHERE  IdePol = nIdePol);
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, 'Error actualizando COBERT_BIEN: ' || SQLERRM);
      END;
      -- DATOS PARTICULARES --
      BEGIN
         UPDATE DEC_TRANSPORTE
         SET CodMoneda = cCodMoneda
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, 'Error actualizando DEC_TRANSPORTE: ' || SQLERRM);
      END;
   END CAMBIO_MONEDA;
   --//
   --
   PROCEDURE CAMBIO_PLAN_FRACC (nIdePol POLIZA.IdePol%TYPE, cCodPlan PLAN_FINANCIAMIENTO.CodPLAN%TYPE, cModPlan PLAN_FINANCIAMIENTO.MODPLAN%TYPE) IS
   --
   BEGIN
      BEGIN
         UPDATE RESP_PAGO
         SET CodPLANFRACC = cCodPlan,
             NumMODPLANFRACC = cModPlan
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, 'Error actualizando RESP_PAGO: ' || SQLERRM);
      END;
      BEGIN
         UPDATE RESP_PAGO_POL
         SET CodPLANFRACC = cCodPlan,
             NumMODPLANFRACC = cModPlan
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, 'Error actualizando RESP_PAGO_POL: ' || SQLERRM);
      END;
   END CAMBIO_PLAN_FRACC;
   --//
   PROCEDURE REFLEJAR_REVERSO (nIdePol POLIZA.IdePol%TYPE) IS
      --
      cStsPol       POLIZA.STSPOL%TYPE;
      cCodProceso   VARCHAR2 (2000);
      nNum          NUMBER;
      nExiste       NUMBER (1);
   --
   BEGIN
      BEGIN   -- REVERSO DE OPERACION --
         SELECT COUNT (*)
         INTO   nNum
         FROM   OPER_POL
         WHERE  IdePol = nIdePol
         AND    TipoOP = 'EMI'
         AND    IndAnul = 'S';
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            nNum  := 0;
      END;
      IF nNum = 1 THEN
         cCodProceso  := 'REVOP';
      ELSE
         cCodProceso  := 'REVER';
      END IF;
      --
      BEGIN
         SELECT 1
         INTO   nExiste
         FROM   CERTIFICADO
         WHERE  StsCert IN ('INC', 'MOD')
         AND    IdePol = nIdePol;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            nExiste  := 0;
         WHEN TOO_MANY_ROWS THEN
            nExiste  := 1;
      END;
      IF nExiste = 0 THEN
         BEGIN
            SELECT StsPol
            INTO   cStsPol
            FROM   POLIZA
            WHERE  IdePol = nIdePol;
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               RAISE_APPLICATION_ERROR (-20100, 'La poliza Indicada: ' || TO_CHAR (nIdePol) || ' no se encuentra registrada.');
         END;
         IF PR_POLIZA.VALIDAR_TRANSACCION (cCodProceso, cStsPol) THEN
            PR_POLIZA.GENERAR_TRANSACCION (nIdePol, cCodProceso);
         END IF;
      END IF;
   END REFLEJAR_REVERSO;
   --//
   -- ELIMINAR_POLIZA
   PROCEDURE ELIMINAR_POLIZA (nIdePol POLIZA.IdePol%TYPE) IS
   --
   BEGIN
      -- Eliminar los beneficiarios de los asegurados
      --
      DELETE BENEF_ASEG
      WHERE  IdeAseg IN (SELECT IdeAseg
                         FROM   ASEGURADO
                         WHERE  IdePol = nIdePol);
      --
      -- Eliminar los recargos y descuentos de loa asegurados.
      --
      DELETE RECA_DCTO_CERTIF_ASEG
      WHERE  IdeAseg IN (SELECT IdeAseg
                         FROM   ASEGURADO
                         WHERE  IdePol = nIdePol);
      --
      -- Eliminar los asegurados de un de un CertIFicado
      --
      DELETE ASEGURADO
      WHERE  IdePol = nIdePol;
      --
      -- Eliminar las coberturas de los asegurados.
      --
      DELETE COBERT_ASEG
      WHERE  IdeAseg IN (SELECT IdeAseg
                         FROM   ASEGURADO
                         WHERE  IdePol = nIdePol);
      --
      -- Eliminar las coberturas de los asegurados.
      --
      DELETE COBERT_ASEG
      WHERE  IdeAseg IN (SELECT IdeAseg
                         FROM   ASEGURADO
                         WHERE  IdePol = nIdePol);
      --
      -- Bienes del CertIFicado
      -- Eliminar Los Recargos/Descuentos de los bienes.
      --
      DELETE RECA_DCTO_CERTIF_BIEN
      WHERE  IdeBien IN (SELECT IdeBien
                         FROM   BIEN_CERT
                         WHERE  IdePol = nIdePol);
      --
      -- Eliminar las coberturas de los bienes
      --
      DELETE COBERT_BIEN
      WHERE  IdeBien IN (SELECT IdeBien
                         FROM   BIEN_CERT
                         WHERE  IdePol = nIdePol);
      --
      -- Eliminar los Bienes del CertIFicado.
      --
      DELETE BIEN_CERT
      WHERE  IdePol = nIdePol;
      --
      -- Eliminar datos particulares de Calderas.
      --
      DELETE DATOS_CALDERA
      WHERE  IdePol = nIdePol;
      --
      -- Eliminar datos particulares de Dinero.
      --
      DELETE DATOS_DINERO
      WHERE  IdePol = nIdePol;
      --
      -- Eliminar datos particulares de Fidelidad.
      --
      DELETE DATOS_FIDELIDAD
      WHERE  IdePol = nIdePol;
      --
      -- Eliminar datos particulares de Fianza.
      --
      DELETE DATOS_PARTICULARES_FIANZAS
      WHERE  IdePol = nIdePol;
      --
      -- Eliminar datos particulares de Vida.
      --
      DELETE DATOS_PARTICULARES_VIDA
      WHERE  IdePol = nIdePol;
      --
      -- Eliminar datos de Particulares de IncENDio
      --
      DELETE DATOS_PART_INCENDIO
      WHERE  IdePol = nIdePol;
      --
      -- Eliminar datos particulares de Robo.
      --
      DELETE DATOS_ROBO
      WHERE  IdePol = nIdePol;
      -- Eliminar datos particulares de Transporte.
      --
      DELETE TRANSPORTE
      WHERE  IdePol = nIdePol;
      --
      -- Eliminar datos particulares de Embarcacion.
      --
      DELETE DAT_PART_EMBARCACION
      WHERE  IdePol = nIdePol;
      --
      -- Eliminar datos particulares de Aviacion.
      --
      DELETE AVIACION
      WHERE  IdePol = nIdePol;
      --
      -- Eliminar Coberturas del certIFicado.
      --
      DELETE COBERT_CERT
      WHERE  IdePol = nIdePol;
      --
      -- Eliminar los recargos y descuentos del certIFicado de la poliza con Estatus MOD.
      --
      DELETE RECA_DCTO_CERTIF
      WHERE  IdePol = nIdePol;
      --
      -- Eliminar los responsables de pagos de la poliza con Estatus MOD.
      --
      DELETE RESP_PAGO
      WHERE  IdePol = nIdePol;
      --
      -- Eliminar los datos particulares de vehiculo.
      --
      DELETE CERT_VEH
      WHERE  IdePol = nIdePol;
      --
      -- CertIFicado - Ramos
      -- Eliminar las estadisticas del certIFicado.
      --
      DELETE EST_CERT
      WHERE  IdePol = nIdePol;
      --
      -- Eliminar las clausulas del certIFicado.
      --
      DELETE CLAU_CERT
      WHERE  IdePol = nIdePol;
      --
      -- Eliminar los anexos del certIFicado.
      --
      DELETE ANEXO_CERT
      WHERE  IdePol = nIdePol;
      --
      -- Eliminar las modIFicaciones de las coberturas.
      --
      DELETE MOD_COBERT
      WHERE  IdePol = nIdePol;
      --
      -- Eliminar los ramos del certIFicado.
      --
      DELETE CERT_RAMO
      WHERE  IdePol = nIdePol;
      --
      -- Eliminar Direccion del Riesgo del CertIFicado.
      --
      DELETE DIREC_RIESGO_CERT
      WHERE  IdePol = nIdePol;
      --
      -- Eliminar CertIFicado de la Poliza.
      --
      DELETE CERTIFICADO
      WHERE  IdePol = nIdePol;
      --
      -- Eliminar los recargos y descuentos de la poliza.
      --
      DELETE RECA_DCTO_POL
      WHERE  IdePol = nIdePol;
      --
      -- Eliminar los clientes de la poliza.
      --
      DELETE POLIZA_CLIENTE
      WHERE  IdePol = nIdePol;
      --
      -- Eliminar los intermediarios de la poliza.
      --
      DELETE PART_INTER_POL
      WHERE  IdePol = nIdePol;
      --
      -- Eliminar la poliza.
      --
      DELETE POLIZA
      WHERE  IdePol = nIdePol;
      --
      -- Eliminar Copia de La Poliza.
      --
      DELETE B$_POLIZA
      WHERE  IdePol = nIdePol;
   --
   END ELIMINAR_POLIZA;
   --///
PROCEDURE REVERSAR_RENOVACION (nIdePol POLIZA.IdePol%TYPE) IS
  nNumPol        POLIZA.NumPol%TYPE;
  cCodPol        POLIZA.CodPOL%TYPE;
  nIdePolRen     POLIZA.IdePol%TYPE;
  nNumRen        POLIZA.NumREN%TYPE;
  cCodProd       POLIZA.CodPROD%TYPE;
  cStsPol        POLIZA.STSPOL%TYPE;
  nCantOper      NUMBER;
  cCodOfiSusc    POLIZA.CodOfiSusc%TYPE;
  --<I Defect 5609> O.Marin/11-01-2006/Reversi�n de renovaciones
  cIPlazoEdad    CARACTERISTICA_PLAN_VIDA.IPlazoEdad%TYPE;
  nAnosPlazoEdad CARACTERISTICA_PLAN_VIDA.AnosPlazoEdad%TYPE;
  --<F Defect 5609>
BEGIN
  --Erick Zoque 20/05/2013 E_CON_20120821_1_7
  PR_FECHAS_EQUIVALENTES.CEVENTO := '001';
  -- Datos de la Poliza --
  BEGIN
    SELECT NumPol, CodPol, CodProd, StsPol, CodOfiSusc, NumRen
    INTO   nNumPol, cCodPol, cCodProd, cStsPol, cCodOfiSusc, nNumRen
    FROM   POLIZA
    WHERE  IdePol = nIdePol;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RAISE_APPLICATION_ERROR (-20100, 'No se encontro datos de la poliza que se desea reversar su renovacion.');
    WHEN TOO_MANY_ROWS THEN
      RAISE_APPLICATION_ERROR (-20100, 'Existe mas de un POLIZA. ' || TO_CHAR (nIdePol));
    WHEN OTHERS THEN
      RAISE_APPLICATION_ERROR (-20100, 'Error al seleccionar desde tabla POLIZA ' || SQLERRM);
  END;
  -- Valida que la Poliza Seleccionada esta en Estatus MOD
  IF cStsPol != 'INC' THEN
    RAISE_APPLICATION_ERROR (-20100, 'No EXISTE la p�liza con STATUS INC, que fue creada en el proceso de la RENOV.');
  END IF;
  IF nNumRen = 0 THEN
    --<I Defect 5609> O.Marin/11-01-2006/Reversi�n de renovaciones
    BEGIN
      SELECT CV.IPlazoEdad, CV.AnosPlazoEdad
      INTO   cIPlazoEdad,nAnosPlazoEdad
      FROM   CERT_RAMO CR,
             RAMO_PLAN_PROD RP,
             CARACTERISTICA_PLAN_VIDA CV
      WHERE  CR.IdePol = nIdePol
        AND  CR.CodPlan = RP.CodPlan
        AND  CR.RevPlan = RP.RevPlan
        AND  CR.CodRamoCert = RP.CodRamoPlan
        AND  RP.CodProd = cCodProd
        AND  NVL(RP.IndRamoOblig, 'N') = 'S'
        AND  RP.CodProd = CV.CodProd
        AND  RP.CodPlan = CV.CodPlan
        AND  RP.RevPlan = CV.RevPlan
        AND  RP.CodRamoPlan = CV.CodRamo;
    EXCEPTION
      WHEN OTHERS THEN
        cIPlazoEdad := NULL;
        nAnosPlazoEdad := NULL;
    END;
    IF cIPlazoEdad = 'P' THEN
      nNumRen := nAnosPlazoEdad;
    ELSE
      RAISE_APPLICATION_ERROR (-20220, 'No existen renovaciones anteriores a la Poliza. ' || cCodPol || '-' || nNumPol);
    END IF;
    --<F Defect 5609>
  ELSE
    BEGIN
      SELECT MAX(IdePol)
      INTO   nIdePolRen
      FROM   POLIZA
      WHERE  NVL (NumRen, 0) = (nNumRen - 1)
        AND  NumPol = nNumPol
        AND  CodPol = cCodPol
        AND  CodProd = cCodProd
        AND  CodOfiSusc = cCodOfiSusc;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        RAISE_APPLICATION_ERROR (-20220, 'No existen poliza anterior para poder eFectuar la reversion');
      WHEN TOO_MANY_ROWS THEN
        RAISE_APPLICATION_ERROR (-20100, 'Existe mas de un POLIZA. ' || cCodPol || '-' || nNumPol);
      WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR (-20100, 'Error al seleccionar desde tabla POLIZA ' || SQLERRM);
    END;
  END IF;
  -- Borra La Poliza Si No Tiene Operaciones --
  BEGIN
    SELECT NVL (COUNT (*), 0)
    INTO   nCantOper
    FROM   OPER_POL
    WHERE  IdePol = nIdePol;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      nCantOper  := 0;
  END;
  IF nCantOper = 0 THEN
    PR_POLIZA.ELIMINAR_POLIZA (nIdePol);
  ELSE
    RAISE_APPLICATION_ERROR (-20220, 'No puede revertir renovacion si ya realizo operaciones en la poliza');
  END IF;
  --Erick Zoque 20/05/2013 E_CON_20120821_1_7
  PR_FECHAS_EQUIVALENTES.CEVENTO := NULL;
END REVERSAR_RENOVACION;
   --///
   --VALIDAR_TRANSACCION
   FUNCTION VALIDAR_TRANSACCION (cCodProceso VARCHAR2, cStsPol VARCHAR2)
      RETURN BOOLEAN IS
      nExiste   NUMBER (1);
   BEGIN
      BEGIN
         SELECT 1
         INTO   nExiste
         FROM   REGLAS_STS
         WHERE  NomTabla = 'POLIZA'
         AND    CodProceso = cCodProceso
         AND    StsInicial = cStsPol;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            RETURN (FALSE);
      END;
      RETURN (TRUE);
   END VALIDAR_TRANSACCION;
   --//
   --REVERTIR
   PROCEDURE REVERTIR (nIdePol POLIZA.IdePol%TYPE) IS
      nNumOper    B$_POLIZA.NumOPER%TYPE;
      cTipoProc   B$_POLIZA.TipoPROC%TYPE;
      CURSOR B$_POLIZA_C IS
         SELECT NumOper, TipoProc
         FROM   B$_POLIZA
         WHERE  IdePol = nIdePol
         ORDER BY NumOper DESC;
   BEGIN
   --Erick Zoque 20/05/2013 E_CON_20120821_1_7
   PR_FECHAS_EQUIVALENTES.CEVENTO := '001';
      PR_OPERACION.INICIAR ('REVER', 'POLIZA', TO_CHAR (nIdePol));
      FOR ST IN B$_POLIZA_C LOOP
         nNumOper   := ST.NumOper;
         cTipoProc  := ST.TipoProc;
         EXIT;
      END LOOP;
      --
      IF cTipoProc = 'USR' THEN
         PR_POLIZA.REVERTIR_SIS (nIdePol, nNumOper);
      ELSE
         RAISE_APPLICATION_ERROR (-20100, 'El Estado Actual No Se Puede Revertir.');
      END IF;
      PR_OPERACION.TERMINAR;
      --Erick Zoque 20/05/2013 E_CON_20120821_1_7
      PR_FECHAS_EQUIVALENTES.CEVENTO := NULL;
   END REVERTIR;
   --//
   -- REV_OP
   PROCEDURE REV_OP (nIdePol POLIZA.IdePol%TYPE, nNumOper B$_POLIZA.NumOPER%TYPE) IS
      n_Chk   NUMBER;
   BEGIN
      n_Chk  := PR_B$_POLIZA.CHK (nIdePol, nNumOPer);
      IF n_Chk = 1 THEN
         PR_B$_POLIZA.POP (nIdePol);
      ELSIF n_Chk = -1 THEN
         RAISE_APPLICATION_ERROR (-20100, 'El Estado Actual No Se Puede Revertir.');
      END IF;
   END REV_OP;
   --REVERTIR_SIS
   PROCEDURE REVERTIR_SIS (nIdePol POLIZA.IdePol%TYPE, nNumOper B$_POLIZA.NumOper%TYPE) IS
      -- CERTIFICADOS DE LA POLIZA --
      CURSOR CERTIFICADO_C IS
         SELECT NumCert, StsCert
         FROM   CERTIFICADO
         WHERE  IdePol = nIdePol;
      -- ANEXO DE LA POLIZA --
      CURSOR ANEXO_POL_C IS
         SELECT IdeAnexo, StsAnexo
         FROM   ANEXO_POL
         WHERE  IdePol = nIdePol;
      -- CLIENTE DE LA POLIZA --
      CURSOR POLIZA_CLIENTE_C IS
         SELECT StsPolCli, CodCli
         FROM   POLIZA_CLIENTE
         WHERE  IdePol = nIdePol;
      -- DISTRIBUCION DE COASEGURO DE LA POLIZA --
      CURSOR DIST_COA_C IS
         SELECT CodAcepRiesgo, StsCoa
         FROM   DIST_COA
         WHERE  IdePol = nIdePol;
      --BBVA EFVC 24/05/2013 Consis Se agrega Condicion para REVERTIR Descuento definidos a nivel de poliza.
      CURSOR RECA_DCTO_POL_C IS
         SELECT IdeRecaDcto,StsRecaDcto
         FROM   RECA_DCTO_POL
         WHERE  IdePol = nIdePol;
   --
   BEGIN
      PR_POLIZA.REV_OP (nIdePol, nNumOper);
      FOR X IN CERTIFICADO_C LOOP
         IF PR_CERTIFICADO.VALIDAR_TRANSACCION ('REVER', X.StsCert) THEN
            PR_CERTIFICADO.REVERTIR_SIS (nIdePol, X.NumCert, nNumOper);
         END IF;
      END LOOP;
      FOR X IN ANEXO_POL_C LOOP
         IF PR_ANEXO_POL.VALIDAR_TRANSACCION ('REVER', X.StsAnexo) THEN
            PR_ANEXO_POL.REVERTIR_SIS (nIdePol, X.IdeAnexo, nNumOper);
         END IF;
      END LOOP;
      --  --
      FOR X IN POLIZA_CLIENTE_C LOOP
         IF PR_POLIZA_CLIENTE.VALIDAR_TRANSACCION ('REVER', X.StsPolCli) THEN
            PR_POLIZA_CLIENTE.REVERTIR_SIS (nIdePol, X.CodCli);
         END IF;
      END LOOP;
      --  --
      FOR X IN DIST_COA_C LOOP
         IF PR_DIST_COA.VALIDAR_TRANSACCION ('REVER', X.StsCoa) THEN
            PR_DIST_COA.REVERTIR_SIS (nIdePol, X.CodAcepRiesgo, nNumOper);
         END IF;
      END LOOP;
      --<<BBVA EFVC 24/05/2013 Consis Se agrega Condicion para REVERTIR Descuento definidos a nivel de poliza.
      FOR X IN RECA_DCTO_POL_C LOOP
         IF PR_RECA_DCTO_POL.VALIDAR_TRANSACCION ('REVER', X.StsRecaDcto) THEN
            PR_RECA_DCTO_POL.REVERTIR_SIS (nIdePol, X.IdeRecaDcto);
         END IF;
      END LOOP;
      -->>BBVA EFVC 24/05/2013
      -- --
      DELETE FecHAS_LIQ
      WHERE  IdePol = nIdePol
      AND    NumOper = nNumOper;
      --<<BBVA Consis 21/10/2013 --Si se revierte la poliza que revierta los niveles generados y esten en estado INC
      DELETE VIOLA_AUTORIZA_NIVEL VAN
      WHERE  VAN.STSVIOLA in ('INC','MOD')
      AND    VAN.IDEPOL    = nIdePol;
      -->>BBVA Consis 21/10/2013

   --
   END REVERTIR_SIS;
   --//
   -- REVERTIR_OPERACION
   PROCEDURE REVERTIR_OPERACION_OLD (nIdePol POLIZA.IdePol%TYPE, nNumOper OPER_POL.NumOPER%TYPE) IS
      --
      nExiste             NUMBER;
      nNumENDoso          NUMBER;
      nNumCert            CERTIFICADO.NumCert%TYPE;
      nIdeComp            NUMBER (14);
      nNumMod             NUMBER (3);
      nNumModRev          NUMBER (3);
      cCodRamoCert        CERT_RAMO.CodRamoCert%TYPE;
      cCodOfiemi          POLIZA.CodOfiemi%TYPE;
      cOrigModCobert      MOD_COBERT.OrigModCobert%TYPE;
      cTipoOp             VARCHAR2 (3);
      nIdeMovPrima        MOV_PRIMA.IdeMovPrima%TYPE;
      dFecAnul            DATE;
      nMtoOper            OPER_POL.MtoOper%TYPE;
      nMtoOperAnu         OPER_POL.MtoOperAnu%TYPE;
      nMtoDevolMovPrima   MOV_PRIMA.MtoComMoneda%TYPE;
      --
      -- REACRGO Y DESCUENTO --
      CURSOR Q_DET_OPER_POL IS
         SELECT IdePol, NumCert, Numoper, Numdetoper, Descdet, Mtodetoper, Codrecadcto, Tiporecadcto, Iderecadcto, Origmodrecadcto
         FROM   DET_OPER_POL
         WHERE  NumOper = nNumOper;
      -- COBERTURAS DE LA OPERACION --
      CURSOR Q_RECIBO_MOV_PRIMA IS
         SELECT IdeMovPrima, R.IdePol, R.NumCert, R.CodRamoCert, CR.CodPlan, CR.RevPlan, CR.StsCertRamo, R.IdeRec, R.FecIniVig, R.FecFinVig
         FROM   RECIBO R, CERT_RAMO CR
         WHERE  R.IdePol = CR.IdePol
         AND    R.NumCert = CR.NumCert
         AND    R.CodRamoCert = CR.CodRamoCert
         AND    NumOper = nNumOper;
      -- MOVIMIENTO DE COBERTURAS (SEGUN SU IdeMovPrima) -- (1)
      CURSOR Q_MOD_COBERT IS
         SELECT IdeCobert
         FROM   MOD_COBERT
         WHERE  IdeMovPrima = nIdeMovPrima
         GROUP BY IdeCobert;
      --MOVIMIENTO DE COBERTURAS (SEGUN EL RAMO) -- (2)
      CURSOR C_MOD_COBERT IS
         SELECT IdeCobert
         FROM   MOD_COBERT
         WHERE  IdePol = nIdePol
         AND    NumCert = nNumCert
         AND    CodRamoCert = cCodRamoCert
         AND    IdeMovPrima = nIdeMovPrima
         GROUP BY IdeCobert;
      -- OBLIGACIONES (DE LA OPERACION) --
      CURSOR OBLIGACION_Q IS
         SELECT NumOblig
         FROM   OBLIGACION
         WHERE  NumOper = nNumOper;
      -- RECIBOS (DE LA OPERACION ANTERIOR) --
      CURSOR C_RECIBO_Q IS
         SELECT IdeRec
         FROM   RECIBO
         WHERE  NumOper = nNumOper
         ORDER BY IdeRec;
      ---- FRACCIONAMIENTO --
      CURSOR FRACC_X_CERTIFICADO_Q IS
         SELECT NumFinanc, NumCert, NumOper, TipoId, NumId, DvId, CodMoneda, SUM (MtoNetoFrac) MtoNetoFrac
         FROM   COND_FINANCIAMIENTO
         WHERE  IdePol = nIdePol
         AND    NumOper = nNumOPer
         AND    StsFin = 'FRA'
         GROUP BY NumFinanc, NumCert, NumOper, TipoId, NumId, DvId, CodMoneda;
      -- OPERACION --
      CURSOR OPER_POL_Q IS
         SELECT NumCert, MtoOper, MtoOperAnu
         FROM   OPER_POL
         WHERE  NumOper = nNumOper;
      -- OBLIGACIONES (DE LA OPERACION) --
      CURSOR FACTURA IS
         SELECT IdeFact
         FROM   FACTURA
         WHERE  NumOper = nNumOper;
   --
   BEGIN
      PR_OPER_USUARIO.AUTORIZAR ('035', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
      PR_OPERACION.INICIAR ('REVER', 'OPER_POL', TO_CHAR (nIdePol));
      PR_POLIZA.CARGAR (nIdePol);   -- DATOS DE LA POLIZA --
      --Erick Zoque 20/05/2013 E_CON_20120821_1_7
      dFecAnul    := PR_FecHAS_EQUIVALENTES.FecHA_EMISION (SYSDATE, PR_POLIZA.cCodCia,'001');   -- FecHA DE AnulACION --
      PR_VINISUSC.CARGAR;
      cCodOfiemi  := PR_VINISUSC.OFICINA_EMISORA;   -- VARIABLES GENERALES --
      FOR Y IN Q_RECIBO_MOV_PRIMA LOOP   -- MOVIMIENTO DE PRIMA -- RECIBO --
         nNumCert      := Y.NumCert;
         cCodRamoCert  := Y.CodRamoCert;
         nIdeMovPrima  := Y.IdeMovPrima;
         FOR Z IN Q_MOD_COBERT LOOP
            BEGIN
               SELECT NVL (MAX (NumMod), 0)
               INTO   nNumModRev
               FROM   MOD_COBERT
               WHERE  IdeMovPrima = Y.IdeMovPrima;
            EXCEPTION
               WHEN NO_DATA_FOUND THEN
                  RAISE_APPLICATION_ERROR (-20100, 'No se puede revertir la operacion (ER.3)!');
            END;
            UPDATE DEC_TRANSPORTE
            SET StsDec = 'REV'
            WHERE  IdeDec IN (SELECT IdeDec
                              FROM   DEC_TRANSPORTE_COBERT
                              WHERE  IdeCobert = Z.IdeCobert
                              AND    NumMod = nNumModRev);
            UPDATE DEC_GEN
            SET StsDec = 'REV'
            WHERE  IdeDec IN (SELECT IdeDec
                              FROM   DEC_GEN_COBERT
                              WHERE  IdeCobert = Z.IdeCobert
                              AND    NumMod = nNumModRev);
         END LOOP;
         BEGIN
            UPDATE MOD_COBERT
            SET StsModCobert = 'REV'
            WHERE  IdeMovPrima = Y.IdeMovPrima;
         END;
         FOR Z IN C_MOD_COBERT LOOP   -- Dejar Registro de lo Sucedido  --
            BEGIN
               SELECT NumMod, OrigModCobert
               INTO   nNumMod, cOrigModCobert
               FROM   MOD_COBERT
               WHERE  NumMod = (SELECT MAX (NumMod)
                                FROM   MOD_COBERT
                                WHERE  IdeCobert = Z.IdeCobert)
               AND    IdeCobert = Z.IdeCobert;
            EXCEPTION
               WHEN NO_DATA_FOUND THEN
                  RAISE_APPLICATION_ERROR (-20100, 'No se puede revertir la operacion (ER.3)!');
            END;
            IF cOrigModCobert = 'C' THEN   -- COBERTURAS DEL CERTIFICADO --
               PR_COBERT_CERT.GENERAR_MODIFICACION ('REV', Z.IdeCobert, 'C');
               BEGIN
                  UPDATE COBERT_CERT COC
                  SET COC.IndExcluir = 'N'
                  WHERE  COC.IdeCobert = Z.IdeCobert;
               END;
            ELSIF cOrigModCobert = 'A' THEN   -- COBERTURAS DE ASEGURADO --
               PR_COBERT_ASEG.GENERAR_MODIFICACION ('REV', Z.IdeCobert, 'A');
            ELSIF cOrigModCobert = 'B' THEN   -- COBERTURAS DE BIENES  --
               PR_COBERT_BIEN.GENERAR_MODIFICACION ('REV', Z.IdeCobert, 'B');
            END IF;
         END LOOP;
      END LOOP;
      --
      FOR X IN OPER_POL_Q LOOP
         nMtoOper     := X.MtoOper;
         nMtoOperAnu  := X.MtoOperAnu;
         BEGIN
            SELECT 1
            INTO   nExiste
            FROM   OPER_POL
            WHERE  NumOper = PR_OPERACION.nNumOper
            AND    NumCert = X.NumCert;
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               nExiste  := 0;
            WHEN TOO_MANY_ROWS THEN
               nExiste  := 1;
         END;
         IF nExiste = 0 THEN
            nNumENDoso  := PR_POLIZA.NumERO_ENDOSO (PR_POLIZA.cCodProd, cCodOfiemi, 'CANC');
            --Erick Zoque 20/05/2013 E_CON_20120821_1_7
            IF dFecAnul < PR_FECHAS_EQUIVALENTES.FECHA_EMISION (SYSDATE, PR_POLIZA.cCodCia,'001') THEN
               PR_OPER_USUARIO.AUTORIZAR ('127', NULL, NULL, NULL, NULL, NULL, 0, NULL, cCodProd, 'REV', dFecAnul);
            END IF;
            INSERT INTO OPER_POL
                        (IdePol, NumCert, NumOper, FecMov, MtoOper, TipoOp, MtoOperAnu,
                         NumEndoso, IndAnul)
                 VALUES (nIdePol, X.NumCert, PR_OPERACION.nNumOper, dFecAnul, (NVL (nMtoOperAnu, 0) * (-1)), 'REV', (NVL (nMtoOper, 0) * (-1)),
                         nNumENDoso, 'S');
            UPDATE RECIBO SET TipoOpe = 'REV'
            WHERE  IdePol = nIdePol
            AND    NumCert = X.NumCert
            AND    NumOper = PR_OPERACION.nNumOper;
            PR_POLIZA.ORDEN_COA (nIdePol, PR_OPERACION.nNumOper, 'CANC', 'S');
            PR_POLIZA.ACTUALIZA_ENDOSO (cCodProd, cCodOfiemi, 'CANC');
         ELSE
            UPDATE OPER_POL
            SET MtoOper = (MtoOper + (NVL (nMtoOper, 0) * (-1))),
                MtoOperAnu = (MtoOperAnu + (NVL (nMtoOperAnu, 0) * (-1)))
            WHERE  NumOper = PR_OPERACION.nNumOper
            AND    NumCert = X.NumCert;
         END IF;
      END LOOP;
      --
      FOR RF IN C_RECIBO_Q LOOP
         BEGIN
            SELECT MP.MtoMoneda
            INTO   nMtoDevolMovPrima
            FROM   MOV_PRIMA MP, RECIBO R
            WHERE  MP.IdeMovPrima = R.IdeMovPrima
            AND    IdeRec = RF.IdeRec;
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               RAISE_APPLICATION_ERROR (-20205, PR.MENSAJE ('ABD', 20205, TO_CHAR (RF.IdeRec), 'MOV_PRIMA', NULL));
            WHEN TOO_MANY_ROWS THEN
               RAISE_APPLICATION_ERROR (-20100, 'Existe mas de un MOV_PRIMA. ' || RF.IdeRec);
            WHEN OTHERS THEN
               RAISE_APPLICATION_ERROR (-20100, 'Error al seleccionar desde tabla MOV_PRIMA ' || SQLERRM);
         END;
         PR_MOV_PRIMA.GENERAR_DEVOLUCION (RF.IdeRec, nMtoDevolMovPrima, dFecAnul, 'N');   -- REASEGURO
      END LOOP;
      BEGIN
         UPDATE MOD_RECA_DCTO_CERTIF
         SET StsModRecaDcto = 'REV'
         WHERE  NumOper = nNumOper;
      END;
      BEGIN
         UPDATE MOD_RECA_DCTO_CERTIF_ASEG
         SET StsModRecaDcto = 'REV'
         WHERE  NumOper = nNumOper;
      END;
      BEGIN
         UPDATE MOD_RECA_DCTO_CERTIF_BIEN
         SET StsModRecaDcto = 'REV'
         WHERE  NumOper = nNumOper;
      END;
      FOR X IN Q_DET_OPER_POL LOOP   -- Dejar Registro de lo Sucedido  --
         IF X.OrigModRecaDcto = 'C' THEN
            PR_RECA_DCTO_CERTIF.GENERAR_MODIFICACION ('REV', X.IdeRecaDcto);
         ELSIF X.OrigModRecaDcto = 'A' THEN
            PR_RECA_DCTO_CERTIF_ASEG.GENERAR_MODIFICACION ('REV', X.IdeRecaDcto);
         ELSIF X.OrigModRecaDcto = 'B' THEN
            PR_RECA_DCTO_CERTIF_BIEN.GENERAR_MODIFICACION ('REV', X.IdeRecaDcto);
         END IF;
         INSERT INTO DET_OPER_POL
                     (IdePol, NumCert, Numoper, Numdetoper, Descdet, Mtodetoper, Codrecadcto,
                      Tiporecadcto, Iderecadcto, Origmodrecadcto)
              VALUES (X.IdePol, X.NumCert, PR_OPERACION.nNumOper, X.Numdetoper, X.Descdet, (NVL (X.Mtodetoper, 0) * (-1)), X.Codrecadcto,
                      X.TIPORECADCTO, X.IDERECADCTO, X.ORIGMODRECADCTO);
      END LOOP;
      FOR G IN C_RECIBO_Q LOOP   -- AnulAR RECIBO  --
         PR_RECIBO.AnulAR (G.IdeRec, dFecAnul);
      END LOOP;
      FOR X IN OBLIGACION_Q LOOP   -- AnulAR OBLIGACIONES  --
         PR_OBLIGACION.AnulAR_SIS ('PRC', X.NumOblig);
      END LOOP;
      FOR F IN FACTURA LOOP
         PR_FACTURA.AnulAR_SIS (F.IdeFAct);
      END LOOP;
      PR_GEN_REA.AnulA_CTA_TEC (nNumOper);   -- REVERTIR REASEGURADORES --
      BEGIN
         UPDATE OPER_POL
         SET IndAnul = 'S',
             FecAnul = dFecAnul,
             NumOperAnu = PR_OPERACION.nNumOper
         WHERE  NumOper = nNumOper;
      END;
      --
      BEGIN
         UPDATE RECIBO
         SET StsRec = 'REV'
         WHERE  NumOper = PR_OPERACION.nNumOper;
      END;
      --
      PR_POLIZA.REVERTIR_SIS (nIdePol, nNumOper);   -- REVIERTE LA POLIZA   --
      PR_OPERACION.TERMINAR;
   END REVERTIR_OPERACION_OLD;
   --///
   PROCEDURE GENERA_ARTICULO_41 (nIdePol POLIZA.IdePol%TYPE) IS
      cExiste   VARCHAR2 (1);
      CURSOR ART_41 IS
         --AJUSTE SETI
         SELECT /*+ index(C idx_03_CERT_RAMO)*/ distinct P.IdePol, C.CodRamoCert, P.CodInter, I.TipoInter
         FROM   INTERMEDIARIO I, PART_INTER_POL P, CERT_RAMO C
         WHERE  I.CodInter = P.CodInter
         AND    C.IdePol = P.IdePol
         AND    C.IdePol = nIdePol;
         --FIN AJUSTE SETI
   BEGIN
      FOR X IN ART_41 LOOP
         BEGIN
            SELECT DISTINCT ('S')
            INTO   cExiste
            FROM   ARTICULO_41
            WHERE  IdePol = X.IdePol
            AND    CodRAMO = X.CodRamoCert
            AND    CodINTER = X.CodInter;
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               INSERT INTO ARTICULO_41
                    VALUES (X.IdePol, X.CodRamoCert, X.CodInter, DECODE (X.TipoInter, 'E', 100, 0));
         END;
      END LOOP;
   END;
   PROCEDURE FACT_DIF_RSRVA (nIdePol NUMBER, nNumOperRef NUMBER) IS
      nNumOperNew          NUMBER (14);
      nNumCert             NUMBER (10);
      nMtoOperTot          NUMBER (14, 2);
      dFecOper             DATE;
      nPorcOperPol         NUMBER;
      nMtoOperNew          NUMBER (14, 2);
      nMtoRecTot           NUMBER (14, 2);
      nPorcRecibo          NUMBER;
      nMtoReciboNew        NUMBER (14, 2);
      nMtoComReciboNew     NUMBER (14, 2);
      nIdeMovPrimaNew      NUMBER (14);
      nPorcPartCoa         NUMBER (8, 4);
      cCodMoneda           VARCHAR2 (3);
      nIdeMovPrima         NUMBER (14);
      nPorcAgen            NUMBER (8, 4);
      nIdeRec              NUMBER (14);
      cTipoPdcion          VARCHAR2 (1);
      cCodOfiemi           VARCHAR2 (6);
      nMtoPagoCoase        NUMBER (14, 2);
      nNumOperAcepRiesgo   NUMBER (14);
      nMtoOper             NUMBER (14, 2);
      nMtoCom              NUMBER (14, 2);
      nNumModMax           NUMBER (3);
      nTasa                NUMBER (8, 4);
      nPrimaMoneda         NUMBER (14, 2);
      nPrima               NUMBER (14, 2);
      nSumaAseg            NUMBER (14, 2);
      nMtoPrima            NUMBER (14, 2);
      cCodProd             VARCHAR2 (4);
      nNumENDoso           NUMBER (10);
      cCodPlan             VARCHAR2 (3);
      cRevPlan             VARCHAR2 (3);
      nTotOper             NUMBER (14, 2);
      nTotOperCert         NUMBER (14, 2);
      cCodPlanFin          VARCHAR2 (6);
      cModPlan             VARCHAR2 (3);
      nMtoLocal            RECIBO.MtoLocal%TYPE;
      CURSOR OPER_POL_Q IS
         SELECT IdePol, NumCert, MtoOper
         FROM   OPER_POL
         WHERE  NumOper = nNumOperRef;
         TYPE TABLE_OPERPOL IS TABLE OF OPER_POL_Q%ROWTYPE INDEX BY PLS_INTEGER;
    OPP TABLE_OPERPOL;
      CURSOR RECIBO_Q IS
         SELECT R.CodRamoCert, R.IdeRec, R.IdeMovPrima, R.CodRec, R.NumRec, R.StsRec, R.FecSts, R.CodOfiemi, R.TasaCambio, R.MtoLocal, R.MtoMoneda,
                R.FecEmi, R.FecIniVig, R.FecFinVig, R.PorcCom, R.MtoComLocal, R.MtoComMoneda, MP.CodPlan, MP.RevPlan
         FROM   RECIBO R, MOV_PRIMA MP
         WHERE  R.IdeMovPrima = MP.IdeMovPrima
         AND    R.NumOper = nNumOperRef
         AND    R.NumCert = nNumCert;
      CURSOR PART_INTER_MOV_Q IS
         SELECT CodInter, IndLider, PorcNetoCom, PorcPart, PorComCedido
         FROM   PART_INTER_MOV
         WHERE  IdeMovPrima = nIdeMovPrima;
      CURSOR RESP_PAGO_Q IS
         SELECT CodCli, PorcPago, CodPlanFracc, NumModPlanFracc, Direc, CodPais, CodEstado, CodCiudad, CodMunicipio, Telex, Fax, Zip, Telef1, Telef2,
                Telef3, CodCobrador, CodViaCobro
         FROM   RESP_PAGO_MOV
         WHERE  IdePol = nIdePol
         AND    NumCert = nNumCert
         AND    NumOper = nNumOPerRef;
      CURSOR DIST_COA_MOV_Q IS
         SELECT CodAcepRiesgo, PorcPart, PorcGasto
         FROM   DIST_COA_MOV
         WHERE  NumOper = nNumOPerRef;
      CURSOR MOD_COBERT_Q IS
         SELECT IdeCobert, NumMod, PrimaMoneda
         FROM   MOD_COBERT
         WHERE  IdeMovPrima = nIdeMovPrima;
      CURSOR RECIBO_OPER_Q IS
         SELECT NumCert, CodRamoCert
         FROM   RECIBO
         WHERE  NumOper = nNumOperNew;
      CURSOR COB_DIF IS
         SELECT Z.IDECOBERT, Z.NumMOD
         FROM   MOD_COB_DIF_RSRVA Z, MOD_COBERT X
         WHERE  Z.IDECOBERT = X.IDECOBERT
         AND    Z.NumMOD = X.NumMOD
         AND    X.IdePol = nIdePol
         AND    X.NumCert = nNumCert;
   BEGIN
      BEGIN
         SELECT NVL (SUM (DR.PRIMAMONEDA), 0)
         INTO   nTotOper
         FROM   MOD_COB_DIF_RSRVA DR, MOD_COBERT MC, POLIZA P
         WHERE  P.IdePol = nIdePol
         AND    MC.IdePol = P.IdePol
         AND    DR.IdeCobert = MC.IdeCobert
         AND    DR.NumMod = MC.NumMod
         AND    DR.IdeMovPrima IS NULL;
      END;
      IF nTotOper != 0 THEN
         PR_OPERACION.INICIAR ('DRES', 'POLIZA', TO_CHAR (nIdePol));
         nNumOperNew  := PR_OPERACION.nNumOper;
         PR_POLIZA.CARGAR (nIdePol);
         --Erick Zoque 20/05/2013 E_CON_20120821_1_7
         dFecOper     := PR_FecHAS_EQUIVALENTES.FecHA_EMISION (SYSDATE, PR_POLIZA.cCodCia,'001');
         INSERT INTO FecHAS_LIQ
                     (IdePol, NumOper, FecLiq, IndOk)
              VALUES (nIdePol, nNumOperNew, dFecOper, 'N');
         INSERT INTO COMPLEMENTO_LIQ
                     (IdePol, NumOper, NumOperComp)
              VALUES (nIdePol, nNumOperRef, nNumOperNew);
         BEGIN
            SELECT CodMoneda, TipoPdcion, CodOfiemi, CodProd
            INTO   cCodMoneda, cTipoPdcion, cCodOfiemi, cCodProd
            FROM   POLIZA
            WHERE  IdePol = nIdePol;
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               RAISE_APPLICATION_ERROR (-20100, 'No Existe el Idepol  = ' || nIdePol);
         END;
         BEGIN
            SELECT SUM (MtoOper)
            INTO   nMtoOperTot
            FROM   OPER_POL
            WHERE  NumOper = nNumOperRef;
         END;
       ------  FOR Y IN OPER_POL_Q LOOP     consis 07/10/2014   pedro p
         OPEN OPER_POL_Q;
        LOOP
           FETCH OPER_POL_Q BULK COLLECT
           INTO OPP LIMIT 1000;
           EXIT WHEN OPP.COUNT = 0;
           FOR i IN 1 .. OPP.COUNT LOOP
            BEGIN
               SELECT NVL (SUM (DR.PRIMAMONEDA), 0)
               INTO   nTotOperCert
               FROM   MOD_COBERT MC, MOD_COB_DIF_RSRVA DR
               WHERE  MC.IdePol = nIdePol
               AND    MC.NumCert = OPP(i).NumCert
               AND    DR.IdeCobert = MC.IdeCobert
               AND    DR.NumMod = MC.NumMod
               AND    DR.IdeMovPrima IS NULL;
            END;
            IF nTotOperCert != 0 THEN
               nNumCert     := OPP(i).NumCert;
               nMtoOperNew  := nTotOperCert;
               nNumENDoso   := PR_POLIZA.NumERO_ENDOSO (PR_POLIZA.cCodProd, PR_POLIZA.cCodOfiemi, 'EC');
               INSERT INTO OPER_POL
                           (IdePol, NumCert, NumOper, FecMov, MtoOper, TipoOp, NumEndoso, IndAnul)
                    VALUES (OPP(i).IdePol, OPP(i).NumCert, nNumOperNew, dFecOper, nMtoOperNew, 'DRS', nNumEndoso, 'N');
               UPDATE RECIBO SET TIPOOPE = 'DRS'
               WHERE  IdePol = OPP(i).IdePol
               AND    NumCert = OPP(i).NumCert
               AND    NumOper = nNumOperNew;
               PR_POLIZA.ACTUALIZA_ENDOSO (PR_POLIZA.cCodProd, PR_POLIZA.cCodOfiemi, 'EC');
               BEGIN
                  SELECT CPV.CodPlanFin, CPV.ModPlan
                  INTO   cCodPlanFin, cModPlan
                  FROM   CARACTERISTICA_PLAN_VIDA CPV, CERT_RAMO CR
                  WHERE  CR.IdePol = OPP(i).IdePol
                  AND    CR.NumCert = OPP(i).NumCert
                  AND    CPV.CodPROD = PR_POLIZA.cCodProd
                  AND    CPV.CodPLAN = CR.CodPLAN
                  AND    CPV.REVPLAN = CR.REVPLAN
                  AND    CPV.CodRAMO = CR.CodRAMOCERT
                  AND    CR.StsCertRamo = 'ACT';
               END;
               FOR T IN RESP_PAGO_Q LOOP
                  INSERT INTO RESP_PAGO_MOV
                              (IdePol, NumCert, NumOper, CodCli, PorcPago, CodPlanFracc, NumModPlanFracc, Direc, CodPais, CodEstado,
                               CodCiudad, CodMunicipio, Telex, Fax, Zip, Telef1, Telef2, Telef3, CodCobrador, CodViaCobro)
                       VALUES (nIdePol, nNumCert, nNumOperNew, T.CodCli, T.PorcPago, cCodPlanFin, cModPlan, T.Direc, T.CodPais, T.CodEstado,
                               T.CodCiudad, T.CodMunicipio, T.Telex, T.Fax, T.Zip, T.Telef1, T.Telef2, T.Telef3, T.CodCobrador, T.CodViaCobro);
               END LOOP;
               BEGIN
                  SELECT SUM (mtoLocal)
                  INTO   nMtoRecTot
                  FROM   RECIBO
                  WHERE  NumOper = nNumOperRef
                  AND    NumCert = OPP(i).NumCert;
               END;
               FOR D IN RECIBO_Q LOOP
                  nIdeMovPrima      := D.IdeMovPrima;
                  nPorcRecibo       := 100;
                  nMtoReciboNew     := nMtoOperNew * nPorcRecibo / 100;
                  nMtoComReciboNew  := 0;
                  nIdeMovPrimaNew   := PR_MOV_PRIMA.Num_TEMP;
                  cCodPlan          := D.CodPlan;
                  cRevPlan          := D.RevPlan;
                  INSERT INTO MOV_PRIMA
                              (IdeMovPrima, NumMovPrima, StsMovPrima, FecSts, IdePol, NumCert, CodRamoCert, CodMoneda, FecIniValid,
                               FecFinValid, CodOfiemi, TasaCambio, FecEmi, PorcPart, MtoLocal, MtoMoneda, MtoComLocal, MtoComMoneda,
                               PorcCom, PorcBonIF, MtoComRetLocal, MtoComRetMoneda, MtoPrimaRetLocal, MtoPrimaRetMoneda, CodPlan, RevPlan, Tipo,
                               MtoRecaDcto)
                       VALUES (nIdeMovPrimaNew, nIdeMovPrimaNew, 'GEN', dFecOper, OPP(i).IdePol, OPP(i).NumCert, D.CodRamoCert, cCodMoneda, D.FecIniVig,
                               D.FecFinVig, D.CodOfiemi, D.TasaCambio, dFecOper, nPorcPartCoa, 0, nMtoReciboNew, nMtoComReciboNew, nMtoComReciboNew,
                               D.PorcCom, 0, 0, 0, 0, nMtoReciboNew, cCodPlan, cRevPlan, 'P',
                               0);
                  PR_MOV_PRIMA.ACTIVAR (nIdeMovPrimaNew);
                  FOR CD IN COB_DIF LOOP
                     UPDATE MOD_COB_DIF_RSRVA
                     SET IdeMovPrima = nIdeMovPrimaNew
                     WHERE  IdeCobert = CD.IdeCobert
                     AND    NumMod = CD.NumMod;
                  END LOOP;
                  BEGIN
                     SELECT SQ_RECIBO.NEXTVAL
                     INTO   nIdeRec
                     FROM   SYS.DUAL;
                  END;
                  --
                  INSERT INTO RECIBO
                              (IdePol, NumCert, CodRamoCert, IdeMovPrima, IdeRec, CodRec, NumRec, StsRec, FecSts, CodOfiemi,
                               TasaCambio, MtoLocal, MtoMoneda, FecEmi, FecIniVig, FecFinVig, PorcCom, MtoComLocal,
                               MtoComMoneda, NumOper, IndTrasCart)
                       VALUES (OPP(i).IdePol, OPP(i).NumCert, D.CodRamoCert, nIdeMovPrimaNew, nIdeRec, D.CodRamoCert, nIdeRec, 'INC', dFecOper, D.CodOfiemi,
                               D.TasaCambio, nMtoReciboNew, nMtoReciboNew, dFecOper, D.FecIniVig, D.FecFinVig, D.PorcCom, NVL (nMtoComReciboNew, 0),
                               NVL (nMtoComReciboNew, 0), nNumOperNew, 'N');
                  PR_RECIBO.GENERAR_TRANSACCION (nIdeRec, 'ACTIV');
                  nMtoPrima         := PR_MOV_PRIMA.GENERAR_RENGLONES (nIdeMovPrimaNew);
               END LOOP;
               IF nTotOperCert > 0 THEN
                  PR_FRACCIONAMIENTO.EMITIR (nIdePol, OPP(i).NumCert, nNumOperNew);
               ELSE
                  PR_PRE_OBLIG.GENERAR (nIdePol, nNumCert, nNumOperNew);
               END IF;
            END IF;
                 END LOOP;
                 ------------------ consis  07/10/2014
         --COMMIT;
         END LOOP;
         CLOSE OPER_POL_Q;
         ------------------ consis  07/10/2014
         PR_ACREENCIA.GENERAR (nNumOperNew);
         PR_FRACCIONAMIENTO.GENERAR (nNumOperNew);
         IF PR.BUSCA_PARAMETRO ('055') = 'SI' THEN
            PR_FACTURA.GENERAR (nNumOperNew);
         END IF;
         PR_OPERACION.TERMINAR;
      END IF;
   END;
   --////
   PROCEDURE GENERA_COMISION_POLIZA (nIdePol POLIZA.IdePol%TYPE) IS
      cExiste    VARCHAR2 (1);
      nPorcCom   POLIZA_FACTOR_G.PorcCom%TYPE;
      nPorcRet   POLIZA_FACTOR_G.PorcCom%TYPE;
      --
      cIndComi   POLIZA_FACTOR_G.IndComi%TYPE;   --> Modificado por EOZ, Indica Si desea Cobrar comisi�n en el Factor_G
      --
      CURSOR COM_POL IS
         SELECT IdePol, CodRamoCert
         FROM   CERT_RAMO C
         WHERE  IdePol = nIdePol
         GROUP BY IdePol, CodRamoCert;
   BEGIN
      nPorcCom  := 0;


     --<<BBVA Consis 27/02/2013 - Se hace calculo comison con % de factor-g de la poliza
     /*    BEGIN
            SELECT PorcCom, NVL (IndComi, 'S')
            INTO   nPorcCom, cIndComi
            FROM   POLIZA_FACTOR_G
            WHERE  IdePol = nIdePol;
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               nPorcCom  := 0;
         END;*/
     BEGIN
       SELECT Porcentaje,'S'
       INTO   nPorcCom , cIndComi
       FROM   POLIZA_FACTORG
       WHERE  IdePol = nIdePol
       AND Concepto='COM';
     EXCEPTION
       WHEN NO_DATA_FOUND THEN
         nPorcCom := 0;
     END;
     -->>BBVA Consis 27/02/2013 -
     BEGIN
       SELECT Porcentaje
       INTO   nPorcRet
       FROM   POLIZA_FACTORG
       WHERE  IdePol = nIdePol
       AND    Concepto='RET';
     EXCEPTION
       WHEN NO_DATA_FOUND THEN
         nPorcRet  := 0;
     END;
     -->>BBVA Consis 06/03/2013 -
      -- Modificado por EOZ  Fecha 25/04/2005.  Indicador si cobra comsi�n F_G Defecto Nro. 1838
      IF cIndComi = 'N' THEN
         nPorcCom  := 0;
      END IF;
      --
      FOR X IN COM_POL LOOP
         BEGIN
            SELECT DISTINCT ('S')
            INTO   cExiste
            FROM   COMISION_POLIZA
            WHERE  IdePol = X.IdePol
            AND    CodRAMO = X.CodRamoCert;
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               INSERT INTO COMISION_POLIZA
               (IdePol, CodRamo, PorcCom, PorcRet)
               VALUES
                  (X.IdePol, X.CodRamoCert, nPorcCom, nPorcRet);
         END;
      END LOOP;
   END;   -- GENERA_COMISION_POLIZA
   -- REVERTIR_OPERACION
   PROCEDURE REVERTIR_OPERACION (nIdePol POLIZA.IdePol%TYPE, nNumOper OPER_POL.NumOPER%TYPE) IS
      --
      nNumCert             CERTIFICADO.NumCert%TYPE;
      cCodRamoCert         CERT_RAMO.CodRamoCert%TYPE;
      cOrigModCobert       MOD_COBERT.OrigModCobert%TYPE;
      cTipoOp              VARCHAR2 (3);
      nNumOperMov          OPER_POL.NumOper%TYPE;
      cCodMoneda           VARCHAR2 (3);
      --<I Defect 2343> Javier Asmat/05.05.2005
      --Se cambia el tipo de dato para las variables de Suma, Tasa y Prima para evitar error de value error
      nSumaAsegMoneda      MOD_COBERT.SumaAsegMoneda%TYPE;   --NUMBER (14, 2);
      nPrimaMoneda         MOD_COBERT.PrimaMoneda%TYPE;   --NUMBER (14, 2);
      nTasa                MOD_COBERT.Tasa%TYPE;   --NUMBER;
      --<F Defect 2343>
      dFecIniValid         DATE;
      dFecFinValid         DATE;
      cIndIncRen           VARCHAR2 (1);
      cIndPlazoEspera      VARCHAR2 (1);
      dFecIniPlazoEspera   DATE;
      dFecFinPlazoEspera   DATE;
      nNumModNew           MOD_COBERT.NumMod%TYPE;
      nNumModRev           MOD_COBERT.NumMod%TYPE;
      nIdeMovPrima         MOV_PRIMA.IdeMovPrima%TYPE;
      cCodOfiemi           POLIZA.CodOfiemi%TYPE;
      cStsPol              POLIZA.StsPol%TYPE;
      cStsCobert           COBERT_CERT.StsCobert%TYPE;
      nIdeAseg             ASEGURADO.IdeAseg%TYPE;
      cStsAseg             ASEGURADO.StsAseg%TYPE;
      cStsBien             BIEN_CERT.StsBien%TYPE;
      nIdeBien             BIEN_CERT.IdeBien%TYPE;
      cStsCertRamo         CERT_RAMO.StsCertRamo%TYPE;
      cCodPlan             CERT_RAMO.CodPlan%TYPE;
      cRevPlan             CERT_RAMO.RevPlan%TYPE;
      cStsCert             CERTIFICADO.StsCert%TYPE;
      cSts                 CERTIFICADO.StsCert%TYPE;
      cClaseBien           MOD_BIEN_CERT.ClaseBien%TYPE;
      cCodBien             MOD_BIEN_CERT.CodBien%TYPE;
      cStsModBien          MOD_BIEN_CERT.StsModBien%TYPE;
      nMtoValBien          MOD_BIEN_CERT.MtoValBien%TYPE;
      nMtoValBienMoneda    MOD_BIEN_CERT.MtoValBienMoneda%TYPE;
      cTipPrimRiesgo       MOD_BIEN_CERT.TipPrimRiesgo%TYPE;
      nPorcPrimRiesgo      MOD_BIEN_CERT.PorcPrimRiesgo%TYPE;
      nMtoFactDctoMoneda   MOD_ASEG.MtoFactDctoMoneda%TYPE;
      nPrima               MOD_ASEG.Prima%TYPE;
      nMtoFactReca         MOD_ASEG.MtoFactReca%TYPE;
      nMtoFactDcto         MOD_ASEG.MtoFactDcto%TYPE;
      nMtoTotRecaMoneda    MOD_ASEG.MtoTotRecaMoneda%TYPE;
      nMtoTotDctoMoneda    MOD_ASEG.MtoTotDctoMoneda%TYPE;
      nMtoFactRecaMoneda   MOD_ASEG.MtoFactRecaMoneda%TYPE;
      nMtoSueldo           MOD_ASEG.MtoSueldo%TYPE;
      nSumaAseg            MOD_ASEG.SumaAseg%TYPE;
      nNroSalarios         MOD_ASEG.NroSalarios%TYPE;
      nNumOperInterOrig    OPER_INTER.NumOperInter%TYPE;
      nIdeRecCont          RECIBO.IdeRec%TYPE;
      cCodOper             OPERACION_CONTAB.CodOper%TYPE         := '027';
      nIdeComp             COMPROBANTE.IdeComp%TYPE;
      nNumFinanc           COND_FINANCIAMIENTO.numfinanc%TYPE;
      dFecMov              DATE;
      cStsModCobert        MOD_COBERT.StsModCobert%TYPE;   --<Defect 2964/Javier Asmat/22.06.2005> Variable para estado del MOD_COBERT
      nNumOperRevTemp      OPER_POL.NumOper%TYPE;   --<Defect 3389/Javier Asmat/13.09.2005> Variable para numero de operaci�n temporal
      --<I Defect  3516-3517> Javier Asmat/25.08.2005
      --Variables para atender los Defects indicados
      nNumModCob           MOD_COBERT.NumMod%TYPE;
      cIndPerCob           VARCHAR2 (2);
      dFecIniValidCob      DATE;
      dFecFinValidCob      DATE;
      nNumRev              NUMBER (5);
      --<F Defect 3516-3517>
      cIndFactComp         VARCHAR2 (1);   -- Anusky Gonzalez. 25/10/2005
      -- MOVIMIENTO DE IdeMovPrima
      CURSOR MOV_PRIMA_Q IS
         SELECT M.IdeMovPrima, M.IdeCobert, M.NumMod, M.FecIniValid, M.FecFinValid   --<Defect  3516-3517> Anusky Gonzalez - 19/09/2005
         FROM   RECIBO R, MOD_COBERT M
         WHERE  R.NumOper     = nNumOperMov
         AND    R.IdeMovPrima = M.IdeMovPrima
         AND    M.Idepol      = nIdepol;
      -- MOVIMIENTO DE IdeMovPrima BIENES
      CURSOR MOV_PRIMA_B IS
         SELECT M.IdeMovPrima, M.IdeBien, M.NumMod, M.FecIniValid, M.NumCert
         FROM   RECIBO R, MOD_BIEN_CERT M
         WHERE  R.NumOper = nNumOperMov
         AND    R.IdeMovPrima = M.IdeMovPrima;
      -- MOVIMIENTO DE IdeMovPrima ASEGURADO
      CURSOR MOV_PRIMA_A IS
         SELECT M.IdeMovPrima, M.IdeAseg, M.NumMod, M.FecIniValid, M.NumCert
         FROM   RECIBO R, MOD_ASEG M
         WHERE  R.NumOper = nNumOperMov
         AND    R.IdeMovPrima = M.IdeMovPrima;
      CURSOR Q_DET_OPER_POL IS
         SELECT IdePol, NumCert, Numoper, Numdetoper, Descdet, Mtodetoper, Codrecadcto, Tiporecadcto, Iderecadcto, Origmodrecadcto
         FROM   DET_OPER_POL
         WHERE  NumOper = nNumOper;
      CURSOR OBLIGACION_Q IS
         SELECT NumOblig
         FROM   OBLIGACION
         WHERE  NumOper = nNumOper
         AND    StsOblig = 'ACT';
      -- RECIBOS (DE LA OPERACION ANTERIOR) --
      CURSOR C_RECIBO_Q IS
         SELECT IdeRec
         FROM   RECIBO
         WHERE  NumOper = nNumOper
         AND    StsRec = 'ACT'
         ORDER BY IdeRec;
      ---- FRACCIONAMIENTO --
      CURSOR FRACC_X_CERTIFICADO_Q IS
         SELECT NumFinanc, NumCert, NumOper, TipoId, NumId, DvId, CodMoneda, SUM (MtoNetoFrac) MtoNetoFrac
         FROM   COND_FINANCIAMIENTO
         WHERE  IdePol = nIdePol
         AND    NumOper = nNumOPer
         GROUP BY NumFinanc, NumCert, NumOper, TipoId, NumId, DvId, CodMoneda;
      -- OBLIGACIONES (DE LA OPERACION) --
      CURSOR FACTURA IS
         SELECT IdeFact, StsFact   --<Defect 3333/Javier Asmat/14.07.2005> Se agrega el estado de la factura
         FROM   FACTURA
         WHERE  NumOper = nNumOper;
         --AND    StsFact = 'ACT';  <Defect 3333/Javier Asmat/14.07.2005> Se comenta el estado porque debe ingresar para generar la contabilidad de anulaci�n
      -- COMISIONES
      CURSOR OP_REV (nNumOblig NUMBER) IS
         SELECT DISTINCT OP.NumOperInter
         FROM   OPER_INTER OP, OBLIGACION OB
         WHERE  OB.NumOblig = nNumOblig
         AND    OP.NumObligDev = OB.NumOblig;
      -- Contabilidad
      CURSOR C_OBLIG_RAMO (nNumOblig NUMBER) IS
         SELECT DISTINCT (CodRamo) CodRamoObl
         FROM   DET_OBLIG_RAMO
         WHERE  NumOblig = nNumOblig;
   --
   BEGIN
   --Erick Zoque 20/05/2013 E_CON_20120821_1_7
   PR_FECHAS_EQUIVALENTES.CEVENTO := '001';
      PR_OPER_USUARIO.AUTORIZAR ('035', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
      nNumOperMov            := nNumOper;
      PR_OPERACION.INICIAR ('REVER', 'OPER_POL', TO_CHAR (nIdePol));
      PR_POLIZA.cOperRev     := 'REV';
      PR_POLIZA.CARGAR (nIdePol);
      dFecMov                := PR_FECHAS_EQUIVALENTES.FECHA_EMISION (SYSDATE, PR_POLIZA.cCodcia,'001');
      --Fase 1 Anulaciones y reversos --------------------------------------------------------
      BEGIN
         UPDATE MOD_RECA_DCTO_CERTIF
         SET StsModRecaDcto = 'REV'
         WHERE  NumOper = nNumOper;
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, 'ERROR actualizando MOD_RECA_DCTO_CERTIF: ' || SQLERRM);
      END;
      BEGIN
         UPDATE MOD_RECA_DCTO_CERTIF_ASEG
         SET StsModRecaDcto = 'REV'
         WHERE  NumOper = nNumOper;
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, 'ERROR actualizando MOD_RECA_DCTO_CERTIF_ASEG: ' || SQLERRM);
      END;
      BEGIN
         UPDATE MOD_RECA_DCTO_CERTIF_BIEN
         SET StsModRecaDcto = 'REV'
         WHERE  NumOper = nNumOper;
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, 'ERROR actualizando MOD_RECA_DCTO_CERTIF_BIEN: ' || SQLERRM);
      END;
      FOR X IN Q_DET_OPER_POL LOOP   -- Dejar Registro de lo Sucedido  --
         IF X.OrigModRecaDcto = 'C' THEN
            PR_RECA_DCTO_CERTIF.GENERAR_MODIFICACION ('REV', X.IdeRecaDcto);
         ELSIF X.OrigModRecaDcto = 'A' THEN
            PR_RECA_DCTO_CERTIF_ASEG.GENERAR_MODIFICACION ('REV', X.IdeRecaDcto);
         ELSIF X.OrigModRecaDcto = 'B' THEN
            PR_RECA_DCTO_CERTIF_BIEN.GENERAR_MODIFICACION ('REV', X.IdeRecaDcto);
         END IF;
      END LOOP;
      FOR X IN FRACC_X_CERTIFICADO_Q LOOP
         PR_COND_FINANCIAMIENTO.AnulAR (X.NumFinanc, nIdeComp, 'S');   -- FACCIONAMIENTO --
         nNumFinanc  := X.NumFinanc;
      END LOOP;
      FOR G IN C_RECIBO_Q LOOP   -- AnulAR RECIBO  --
         BEGIN
            SELECT IdeMovPrima
            INTO   nIdeMovPrima
            FROM   RECIBO
            WHERE  IdeRec = G.IdeRec;
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               RAISE_APPLICATION_ERROR (-20100, 'No existen datos en RECIBO para IDEREC: ' || G.IdeRec);
         END;
         PR_MOV_PRIMA.GENERAR_TRANSACCION (nIdeMovPrima, 'ANULA');
         BEGIN
            UPDATE RECIBO
            SET StsRec = 'REV',
                FecAnul = dFecMov
            WHERE  IdeRec = G.IdeRec;
         EXCEPTION
            WHEN OTHERS THEN
               RAISE_APPLICATION_ERROR (-20100, 'ERROR actualizando RECIBO: ' || SQLERRM);
         END;
      END LOOP;
      --
      FOR X IN OBLIGACION_Q LOOP   -- AnulAR OBLIGACIONES
         PR_OBLIGACION.ANULAR_SIS ('PRC', X.NumOblig);
         nNumOperInterOrig  := NULL;
         OPEN OP_REV (X.NumOblig);
         LOOP
            FETCH  OP_REV
            INTO   nNumOperInterOrig;
            EXIT WHEN OP_REV%NOTFOUND;
            PR_OPER_INTER.CREAR_OPER_AUTO_REVER (nNumOperInterOrig);
         END LOOP;
         CLOSE OP_REV;
      END LOOP;
      FOR F IN FACTURA LOOP   -- AnulAR FACTURAS
         --<I Defect 3333/Javier Asmat/14.07.2005>
         --Se pregunta por el estado de la factura, si esta es activa, se procede a anular la misma
         IF F.Stsfact = 'ACT' THEN
            PR_FACTURA.ANULAR_SIS (F.IdeFAct);
         END IF;
         -- Contabilizacion de anulacion
         nIdeComp  := PR_COMPROBANTE.ABRIR (cCodOper, PR_POLIZA.cCodOfiSusc, nNumOper, PR_POLIZA.cCodCia, PR_POLIZA.cTipoPdcion, PR_POLIZA.cCodMoneda);
         IF nIdeComp > 0 THEN
            FOR RC IN (SELECT R.CodRamoCert, MAX (RF.IdeRec) IdeRec
                       FROM   RECIBO R, REC_FINANCIAMIENTO RF
                       WHERE  R.IdeRec = RF.IdeRec
                       AND    RF.NumFinanc = nNumFinanc
                       GROUP BY R.CodRamoCert) LOOP
               FOR CA IN (SELECT NumAcre
                          FROM   GIROS_FINANCIAMIENTO
                          WHERE  NumFinanc = nNumFinanc
                      AND  NumAcre IN (SELECT NumAcre
                                      FROM   ACREENCIA A
                                  WHERE  IdeFact = F.IdeFact)) LOOP --<Defect 5398> O.Marin/30-11-2005/contabilizaci�n reversi�n
                  PR_MOV_DEFINITIVO.GEN_MOV (CA.NumAcre, cCodOper, nIdeComp, RC.CodRamoCert, RC.IdeRec);
               END LOOP;
            END LOOP;
            PR_COMPROBANTE.CERRAR (nIdeComp, 'PRC');
            PR_COMPROBANTE.REGISTRA_ERROR (nIdeComp, NULL, NULL, NULL, NULL, SYSDATE, NULL, NULL, 'PR_OPER_INTER.CREAR_OPER_AUTO_REVER', cCodOper);
            --<I Defect 3389> Javier Asmat/13.09.2005
            --Actualizamos los comprobantes con el numero de operaci�n temporal de reversi�n
            UPDATE COMPROBANTE
            SET NumOper = PR_OPERACION.NUMOPER
            WHERE  IdeComp = nIdeComp;
         --<F Defect 3389>
         END IF;
      --
      END LOOP;
      --
      PR_GEN_REA.ANULA_CTA_TEC (nNumOper);   -- REVERTIR REASEGURADORES
      BEGIN
         DELETE MOD_COBERT
         WHERE  IdePol = nIdePol
         AND    StsModCobert = 'VAL';   -- REVERSO
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, 'ERROR eliminando MOD_COBERT: ' || SQLERRM);
      END;
      FOR Z IN MOV_PRIMA_B LOOP
         nIdeMovPrima  := Z.IdeMovPrima;
         nIdeBien      := Z.IdeBien;
         nNumCert      := Z.NumCert;
         BEGIN
            SELECT idebien, Codramocert, Codplan, revplan, clasebien, Codbien, mtovalbien, mtovalbienmoneda, Fecinivalid, Fecfinvalid, tipprimriesgo,
                   porcprimriesgo
            INTO   Z.IdeBien, cCodRamoCert, cCodPlan, cRevPlan, cClaseBien, cCodbien, nMtoValBien, nMtoValBienMoneda, dFecIniValid, dFecFinValid,
                   cTipPrimRiesgo, nPorcPrimRiesgo
            FROM   MOD_BIEN_CERT
            WHERE  IdeBien = Z.IdeBien
            AND    NumMod = (Z.NumMod - 1);
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               SELECT idebien, Codramocert, Codplan, revplan, clasebien, Codbien, mtovalbien, mtovalbienmoneda, Fecinivalid, Fecfinvalid,
                      tipprimriesgo, porcprimriesgo
               INTO   Z.IdeBien, cCodRamoCert, cCodPlan, cRevPlan, cClaseBien, cCodbien, nMtoValBien, nMtoValBienMoneda, dFecIniValid, dFecFinValid,
                      cTipPrimRiesgo, nPorcPrimRiesgo
               FROM   MOD_BIEN_CERT
               WHERE  IdeBien = Z.IdeBien
               AND    NumMod = Z.NumMod;
         END;
         BEGIN
            SELECT MAX (NumMod) + 1
            INTO   nNumModNew
            FROM   MOD_BIEN_CERT
            WHERE  IdeBien = Z.IdeBien;
         END;
         INSERT INTO MOD_BIEN_CERT
                     (idebien, IdePol, NumCert, Codramocert, Codplan, revplan, Nummod, clasebien, Codbien, stsmodbien, mtovalbien,
                      mtovalbienmoneda, Fecinivalid, Fecfinvalid, tipprimriesgo, porcprimriesgo, IdeMovPrima, IdeMovPrimat)
              VALUES (Z.IdeBien, nIdePol, Z.NumCert, cCodRamoCert, cCodPlan, crevplan, nNumModNew, cClaseBien, cCodBien, 'VAL', nMtoValBien,
                      nMtoValBienMoneda, dFecIniValid, dFecfinvalid, cTipPrimRiesgo, nPorcPrimRiesgo, 0, 0);
         --
         PR_MOD_BIEN_CERT.INCLUIR_REV_OPE (nIdeBien, nNumModNew);
      END LOOP;
      FOR Z IN MOV_PRIMA_A LOOP
         nIdeMovPrima  := Z.IdeMovPrima;
         nIdeAseg      := Z.IdeAseg;
         nNumCert      := Z.NumCert;
         BEGIN
            SELECT IdeAseg, Codramocert, Codplan, revplan, primamoneda, mtototrecamoneda, mtototdctomoneda, mtofactrecamoneda, mtofactdctomoneda,
                   CodMoneda, prima, mtofactreca, mtofactdcto, Fecinivalid, Fecfinvalid, mtosueldo, sumaaseg, SumaAsegMoneda, nrosalarios
            INTO   Z.IdeAseg, cCodRamoCert, cCodPlan, cRevPlan, nPrimaMoneda, nMtoTotRecaMoneda, nMtoTotDctoMoneda, nMtoFactRecaMoneda,
                   nMtoFactDctoMoneda, cCodMoneda, nPrima, nMtoFactReca, nMtoFactDcto, dFecIniValid, dFecFinValid, nMtoSueldo, nSumaAseg,
                   nSumaAsegMoneda, nNroSalarios
            FROM   MOD_ASEG
            WHERE  IdeAseg = Z.IdeAseg
            AND    NumMod = (Z.NumMod - 1);
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               SELECT IdeAseg, Codramocert, Codplan, revplan, primamoneda, mtototrecamoneda, mtototdctomoneda, mtofactrecamoneda, mtofactdctomoneda,
                      CodMoneda, prima, mtofactreca, mtofactdcto, Fecinivalid, Fecfinvalid, mtosueldo, sumaaseg, SumaAsegMoneda, nrosalarios
               INTO   Z.IdeAseg, cCodRamoCert, cCodPlan, cRevPlan, nPrimaMoneda, nMtoTotRecaMoneda, nMtoTotDctoMoneda, nMtoFactRecaMoneda,
                      nMtoFactDctoMoneda, cCodMoneda, nPrima, nMtoFactReca, nMtoFactDcto, dFecIniValid, dFecFinValid, nMtoSueldo, nSumaAseg,
                      nSumaAsegMoneda, nNroSalarios
               FROM   MOD_ASEG
               WHERE  IdeAseg = Z.IdeAseg
               AND    NumMod = Z.NumMod;
         END;
         BEGIN
            SELECT MAX (NumMod) + 1
            INTO   nNumModNew
            FROM   MOD_ASEG
            WHERE  IdeAseg = Z.IdeAseg;
         END;
         INSERT INTO MOD_ASEG
                     (IdePol, NumCert, Codramocert, IdeAseg, Codplan, revplan, Nummod, stsmodaseg, primamoneda, mtototrecamoneda,
                      mtototdctomoneda, mtofactrecamoneda, mtofactdctomoneda, CodMoneda, prima, mtofactreca, mtofactdcto, Fecinivalid,
                      Fecfinvalid, IdeMovPrima, IdeMovPrimat, mtosueldo, sumaaseg, SumaAsegMoneda, nrosalarios)
              VALUES (nIdePol, Z.NumCert, cCodRamoCert, Z.IdeAseg, cCodPlan, crevplan, nNumModNew, 'VAL', nPrimaMoneda, nMtoTotRecaMoneda,
                      nMtoTotDctoMoneda, nMtoFactRecaMoneda, nMtoFactDctoMoneda, cCodMoneda, nPrima, nMtoFactReca, nMtoFactDcto, dFecIniValid,
                      dFecFinValid, 0, 0, nMtoSueldo, nSumaAseg, nSumaAsegMoneda, nNroSalarios);
         --
         PR_MOD_ASEG.INCLUIR_REV_OPE (nIdeAseg, nNumModNew);
      END LOOP;
      FOR Z IN MOV_PRIMA_Q LOOP
         nIdeMovPrima  := Z.IdeMovPrima;
         BEGIN
            --< Defect  3516-3517> Anusky Gonzalez - 19/09/2005 SELECT OrigModCobert, Tasa, CodMoneda, SumaAsegMoneda, PrimaMoneda, Z.FecIniValid, FecFinValid, IndIncRen, IndPlazoEspera,
            SELECT OrigModCobert, Tasa, CodMoneda, SumaAsegMoneda, PrimaMoneda, Z.FecIniValid, Z.FecFinValid, IndIncRen, IndPlazoEspera,
                   FecIniPlazoEspera, FecFinPlazoEspera, NumCert, CodRamoCert, IndFactComp   -- Anusky Gonzalez. 25/10/2005
            INTO   cOrigModCobert, nTasa, cCodMoneda, nSumaAsegMoneda, nPrimaMoneda, dFecIniValid, dFecFinValid, cIndIncRen, cIndPlazoEspera,
                   dFecIniPlazoEspera, dFecFinPlazoEspera, nNumCert, cCodRamoCert, cIndFactComp   -- Anusky Gonzalez. 25/10/2005
            FROM   MOD_COBERT
            WHERE  IdeCobert = Z.IdeCobert
            AND    NumMod = (Z.NumMod - 1);
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               BEGIN
                  --< Defect  3516-3517> Anusky Gonzalez SELECT OrigModCobert, 0, CodMoneda, 0, 0, Z.FecIniValid, FecFinValid, IndIncRen, IndPlazoEspera,
                  SELECT OrigModCobert, Tasa, CodMoneda, SumaAsegMoneda, 0, Z.FecIniValid, Z.FecFinValid, IndIncRen, IndPlazoEspera,
                         FecIniPlazoEspera, FecFinPlazoEspera, NumCert, CodRamoCert, IndFactComp   -- Anusky Gonzalez. 25/10/2005
                  INTO   cOrigModCobert, nTasa, cCodMoneda, nSumaAsegMoneda, nPrimaMoneda, dFecIniValid, dFecFinValid, cIndIncRen, cIndPlazoEspera,
                         dFecIniPlazoEspera, dFecFinPlazoEspera, nNumCert, cCodRamoCert, cIndFactComp   -- Anusky Gonzalez. 25/10/2005
                  FROM   MOD_COBERT
                  WHERE  IdeCobert = Z.IdeCobert
                  AND    NumMod = Z.NumMod;
               END;
         END;
         IF cOrigModCobert IN ('C', 'G') THEN
            BEGIN
               SELECT StsCobert
               INTO   cStsCobert
               FROM   COBERT_CERT
               WHERE  IdeCobert = Z.IdeCobert;
            EXCEPTION
               WHEN NO_DATA_FOUND THEN
                  RAISE_APPLICATION_ERROR (-20100, 'No Existe EL Z.IdeCobert = ' || Z.IdeCobert);
            END;
            IF cStsCobert = 'EXC' THEN
               PR_COBERT_CERT.GENERAR_TRANSACCION (Z.IdeCobert, 'REVER');
               BEGIN
                  UPDATE COBERT_CERT
                  SET IndExcluir = NULL,
                      FecExc = NULL,
                      CodMotvExc = NULL,
                      TextMotvExc = NULL
                  WHERE  IdeCobert = Z.IdeCobert;
               EXCEPTION
                  WHEN OTHERS THEN
                     RAISE_APPLICATION_ERROR (-20100, 'Error actualizando COBERT_CERT: ' || SQLERRM);
               END;
            END IF;
         ELSIF cOrigModCobert = 'A' THEN
            BEGIN
               SELECT StsCobert, IdeAseg
               INTO   cStsCobert, nIdeAseg
               FROM   COBERT_ASEG
               WHERE  IdeCobert = Z.IdeCobert;
            EXCEPTION
               WHEN NO_DATA_FOUND THEN
                  RAISE_APPLICATION_ERROR (-20100, 'No existen datos en COBERT_ASEG para IDECOBERT: ' || Z.IdeCobert);
            END;
            IF cStsCobert = 'EXC' THEN
               PR_COBERT_ASEG.GENERAR_TRANSACCION (Z.IdeCobert, 'REVER');
               BEGIN
                  UPDATE COBERT_ASEG
                  SET IndExcluir = NULL,
                      FecExc = NULL,
                      CodMotvExc = NULL,
                      TextMotvExc = NULL
                  WHERE  IdeCobert = Z.IdeCobert;
               EXCEPTION
                  WHEN OTHERS THEN
                     RAISE_APPLICATION_ERROR (-20100, 'Error actualizando COBERT_ASEG: ' || SQLERRM);
               END;
            END IF;
            BEGIN
               SELECT StsAseg
               INTO   cStsAseg
               FROM   ASEGURADO
               WHERE  IdeAseg = nIdeAseg;
            END;
            IF cStsAseg = 'EXC' THEN
               PR_ASEGURADO.GENERAR_TRANSACCION (nIdeAseg, 'REVER');
               UPDATE ASEGURADO
               SET IndExcluir = NULL,
                   FecExc = NULL,
                   CodMotvExc = NULL,
                   TextMotvExc = NULL
               WHERE  IdeAseg = nIdeAseg;
            END IF;
         ELSIF cOrigModCobert = 'B' THEN
            BEGIN
               SELECT StsCobert, IdeBien
               INTO   cStsCobert, nIdeBien
               FROM   COBERT_BIEN
               WHERE  IdeCobert = Z.IdeCobert;
            EXCEPTION
               WHEN NO_DATA_FOUND THEN
                  RAISE_APPLICATION_ERROR (-20100, 'No Existe el Z.IdeCobert 2 = ' || Z.IdeCobert);
            END;
            IF cStsCobert = 'EXC' THEN
               PR_COBERT_BIEN.GENERAR_TRANSACCION (Z.IdeCobert, 'REVER');
               UPDATE COBERT_BIEN
               SET IndExcluir = NULL,
                   FecExc = NULL,
                   CodMotvExc = NULL,
                   TextMotvExc = NULL
               WHERE  IdeCobert = Z.IdeCobert;
            END IF;
            BEGIN
               SELECT StsBien
               INTO   cStsBien
               FROM   BIEN_CERT
               WHERE  IdeBien = nIdeBien;
            EXCEPTION
               WHEN NO_DATA_FOUND THEN
                  RAISE_APPLICATION_ERROR (-20100, 'No Existe el Z.nIdeBien 2 = ' || nIdeBien);
            END;
            IF cStsBien = 'EXC' THEN
               PR_BIEN_CERT.GENERAR_TRANSACCION (nIdeBien, 'REVER');
               UPDATE BIEN_CERT
               SET IndExcluir = NULL,
                   FecExc = NULL,
                   CodMotvExc = NULL,
                   TextMotvExc = NULL
               WHERE  IdeBien = nIdeBien;
            END IF;
         END IF;
         BEGIN
            SELECT StsCertRamo, CodPlan, RevPlan
            INTO   cStsCertRamo, cCodPlan, cRevPlan
            FROM   CERT_RAMO
            WHERE  IdePol = nIdePol
            AND    NumCert = nNumCert
            AND    CodRamocert = cCodRamoCert;
         END;
         IF cStsCertRamo = 'EXC' THEN
            PR_CERT_RAMO.GENERAR_TRANSACCION (nIdePol, nNumCert, cCodPlan, cRevPlan, cCodRamoCert, 'REVER');
            UPDATE CERT_RAMO
            SET IndExcluir = NULL,
                FecExc = NULL,
                CodMotvExc = NULL,
                TextMotvExc = NULL
            WHERE  IdePol = nIdePol
            AND    NumCert = nNumCert
            AND    CodRamocert = cCodRamoCert;
         END IF;
         BEGIN
            SELECT StsCert
            INTO   cStsCert
            FROM   CERTIFICADO
            WHERE  IdePol = nIdePol
            AND    NumCert = nNumCert;
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               RAISE_APPLICATION_ERROR (-20100, 'No Existe el (nIdePol=nNumCert) = ' || nIdePol || ' = ' || nNumCert);
         END;
         IF cStsCert = 'EXC' THEN
            PR_CERTIFICADO.GENERAR_TRANSACCION (nIdePol, nNumCert, 'REVER');
            UPDATE CERTIFICADO
            SET IndExcluir = NULL,
                FecExc = NULL,
                CodMotvExc = NULL,
                TextMotvExc = NULL
            WHERE  IdePol = nIdePol
            AND    NumCert = nNumCert;
         END IF;
         BEGIN
            SELECT NVL (MAX (NumMod), 0)
            INTO   nNumModRev
            FROM   MOD_COBERT
            WHERE  IdeCobert = Z.IdeCobert;
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               RAISE_APPLICATION_ERROR (-20100, 'No se puede revertir la operacion (ER.3)!');
         END;
         UPDATE DEC_TRANSPORTE
         SET StsDec = 'INC'
         WHERE  IdeDec IN (SELECT IdeDec
                           FROM   DEC_TRANSPORTE_COBERT
                           WHERE  IdeCobert = Z.IdeCobert
                           AND    NumMod = nNumModRev);
         BEGIN
            SELECT MAX (NumMod) + 1
            INTO   nNumModNew
            FROM   MOD_COBERT
            WHERE  IdeCobert = Z.IdeCobert;
         END;
         INSERT INTO MOD_COBERT
                     (IdeCobert, NumMod, OrigModCobert, StsModCobert, Tasa, CodMoneda, SumaAsegMoneda, PrimaMoneda, FecIniValid, FecFinValid,
                      IndIncRen, IndPlazoEspera, FecIniPlazoEspera, FecFinPlazoEspera, IdePol, NumCert, CodRamoCert, IndSiniestro, IndCobEsp, IndMov,
                      IndFactComp)
              VALUES (Z.IdeCobert, nNumModNew, cOrigModCobert, 'VAL', nTasa, cCodMoneda, nSumaAsegMoneda, nPrimaMoneda, dFecIniValid, dFecFinValid,
                      cIndIncRen, cIndPlazoEspera, dFecIniPlazoEspera, dFecFinPlazoEspera, nIdePol, nNumCert, cCodRamoCert, 'N', 'N', 1,
                      cIndFactComp);
         PR_MOD_COBERT.INCLUIR (Z.IdeCobert, nNumModNew);
      END LOOP;
      UPDATE CERTIFICADO
      SET IndExcluir = NULL,
          TipoAnul = NULL
      WHERE  IdePol = nIdePol
      AND    IndExcluir = 'R';
      PR_POLIZA.nNumOperRev  := nNumOper;
      nNumOperRevTemp        := PR_OPERACION.NUMOPER;   --<Defect 3389/Javier Asmat/13.09.2005 Se guarda el numero de operaci�n temporal para luego ser actualizado
      nNumOperMov            := PR_POLIZA.ACTIVAR (nIdePol, 'D');
      PR_POLIZA.cOperRev     := 'EMI';
      --<I Defect 2964> Javier Asmat/22.06.2005
      --Si la forma de pago es distinta de Anual, y lo que se revirtio fue un movimiento de facturaci�n de periodo
      --entonces, debemos regresar la fecha de ultima facturaci�n al periodo anterior
      IF PR.BUSCA_LVAL ('CANTREC', PR_POLIZA.cCodFormPago) NOT IN ('INVALIDO', '1') THEN
         --Buscamos el estado del movimiento anterior al movimiento revertido
         --<I Defect 3516-3517> Javier Asmat/25.08.2005
         --Evaluamos mejor lo estados para saber si realmente debemos actualizar la fecha de ultima facturaci�n
         cIndPerCob  := 'NO';
         FOR Z IN MOV_PRIMA_Q LOOP
            nNumModCob  := Z.NumMod - 1;
            WHILE TRUE LOOP
               --<I Defect 3516-3517> Anusky Gonzalez/27.09.2005
               nNumRev     := 1;
               WHILE TRUE LOOP
                  BEGIN
                     SELECT StsModCobert
                     INTO   cStsModCobert
                     FROM   MOD_COBERT
                     WHERE  IdeCobert = Z.IdeCobert
                     AND    NumMod = nNumModCob;   --Se resta menos 2 pues la reversi�n no anula el movimiento reversado sino genera uno nuevo
                  EXCEPTION
                     WHEN NO_DATA_FOUND THEN
                        cStsModCobert  := NULL;
                  END;
                  IF cStsModCobert = 'REV' THEN
                     nNumModCob  := nNumModCob - 1;
                     nNumRev     := nNumRev + 1;
                  ELSE
                     EXIT;
                  END IF;
               END LOOP;
               nNumModCob  := nNumModCob - nNumRev;
               BEGIN
                  SELECT StsModCobert
                  INTO   cStsModCobert
                  FROM   MOD_COBERT
                  WHERE  IdeCobert = Z.IdeCobert
                  AND    NumMod = nNumModCob;
               EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                     cStsModCobert  := NULL;
               END;
               --<F Defect 3516-3517> Anusky Gonzalez/27.09.2005
               IF    cStsModCobert != 'REV'
                  OR cStsModCobert IS NULL THEN
                  IF cStsModCobert = 'LIQ' THEN
                     cIndPerCob  := 'SI';
                  END IF;
                  EXIT;
               END IF;
               IF cStsModCobert = 'REV' THEN
                  nNumModCob  := nNumModCob - 1;
               END IF;
            END LOOP;
         END LOOP;
         --Evaluamos los estados
         IF cIndPerCob = 'SI' THEN
            UPDATE POLIZA
            SET FecUltFact = ADD_MONTHS (FecUltFact, PR.MESES_FORMA_PAGO (PR_POLIZA.cCodFormPago) * -1)
            WHERE  IdePol = nIdePol;
         END IF;
      --<F Defect 3516-3517>
      END IF;
      --<F Defect 2964>
      --<I Defect 3389> Javier Asmat/13.09.2005
      --Se asocia la contabilidad de la operaci�n reversada con la operaci�n propia del reverso
      UPDATE COMPROBANTE
      SET NumOper = nNumOperMov
      WHERE  NumOper = nNumOperRevTemp;
      --<F Defect 3389>
      UPDATE OPER_POL
      SET IndAnul = 'S',
          FecAnul = dFecMov,
          NumOperAnu = nNumOperMov
      WHERE  NumOper = nNumOper;
      --
      UPDATE RECIBO
      SET StsRec = 'REV'
      WHERE  NumOper = nNumOperMov;
      --
      UPDATE OPER_POL
      SET TipoOp = 'REV'
      WHERE  NumOper = nNumOperMov;
      FOR Z IN MOV_PRIMA_A LOOP
         UPDATE MOD_ASEG
         SET StsModAseg = 'REV'
         WHERE  IdeAseg = Z.IdeAseg
         AND    NumMod = Z.NumMod;
      END LOOP;
      FOR Z IN MOV_PRIMA_B LOOP
         UPDATE MOD_BIEN_CERT
         SET StsModBien = 'REV'
         WHERE  IdeBien = Z.IdeBien
         AND    NumMod = Z.NumMod;
      END LOOP;
      FOR Z IN MOV_PRIMA_Q LOOP
         UPDATE MOD_COBERT
         SET StsModCobert = 'REV'
         WHERE  IdeCobert = Z.IdeCobert
         AND    NumMod = Z.NumMod;
      END LOOP;
      FOR Z IN MOV_PRIMA_B LOOP
         nIdeMovPrima  := Z.IdeMovPrima;
         nIdeBien      := Z.IdeBien;
         nNumCert      := Z.NumCert;
         BEGIN
            SELECT Codramocert, Codplan, revplan, clasebien, Codbien, mtovalbien, mtovalbienmoneda, Fecinivalid, Fecfinvalid, tipprimriesgo,
                   porcprimriesgo
            INTO   cCodRamoCert, cCodPlan, cRevPlan, cClaseBien, cCodbien, nMtoValBien, nMtoValBienMoneda, dFecIniValid, dFecFinValid,
                   cTipPrimRiesgo, nPorcPrimRiesgo
            FROM   MOD_BIEN_CERT
            WHERE  IdeBien = Z.IdeBien
            AND    NumMod = (Z.NumMod - 1);
         END;
         INSERT INTO MOD_BIEN_CERT
                     (idebien, IdePol, NumCert, Codramocert, Codplan, revplan, Nummod, clasebien, Codbien, stsmodbien, mtovalbien,
                      mtovalbienmoneda, Fecinivalid, Fecfinvalid, tipprimriesgo, porcprimriesgo, IdeMovPrima, IdeMovPrimat)
              VALUES (Z.IdeBien, nIdePol, Z.NumCert, cCodRamoCert, cCodPlan, crevplan, Z.NumMod + 1, cClaseBien, cCodBien, 'VAL', nMtoValBien,
                      nMtoValBienMoneda, dFecIniValid, dFecfinvalid, cTipPrimRiesgo, nPorcPrimRiesgo, 0, 0);
         PR_MOD_BIEN_CERT.INCLUIR_REV_OPE (nIdeBien, Z.NumMod + 1);
      END LOOP;
      FOR Z IN MOV_PRIMA_A LOOP
         nIdeMovPrima  := Z.IdeMovPrima;
         nIdeAseg      := Z.IdeAseg;
         nNumCert      := Z.NumCert;
         BEGIN
            SELECT IdeAseg, Codramocert, Codplan, revplan, primamoneda, mtototrecamoneda, mtototdctomoneda, mtofactrecamoneda, mtofactdctomoneda,
                   CodMoneda, prima, mtofactreca, mtofactdcto, Fecinivalid, Fecfinvalid, mtosueldo, sumaaseg, SumaAsegMoneda, nrosalarios
            INTO   Z.IdeAseg, cCodRamoCert, cCodPlan, cRevPlan, nPrimaMoneda, nMtoTotRecaMoneda, nMtoTotDctoMoneda, nMtoFactRecaMoneda,
                   nMtoFactDctoMoneda, cCodMoneda, nPrima, nMtoFactReca, nMtoFactDcto, dFecIniValid, dFecFinValid, nMtoSueldo, nSumaAseg,
                   nSumaAsegMoneda, nNroSalarios
            FROM   MOD_ASEG
            WHERE  IdeAseg = Z.IdeAseg
            AND    NumMod = (Z.NumMod - 1);
         END;
         INSERT INTO MOD_ASEG
                     (IdePol, NumCert, Codramocert, IdeAseg, Codplan, revplan, Nummod, stsmodaseg, primamoneda, mtototrecamoneda,
                      mtototdctomoneda, mtofactrecamoneda, mtofactdctomoneda, CodMoneda, prima, mtofactreca, mtofactdcto, Fecinivalid,
                      Fecfinvalid, IdeMovPrima, IdeMovPrimat, mtosueldo, sumaaseg, SumaAsegMoneda, nrosalarios)
              VALUES (nIdePol, Z.NumCert, cCodRamoCert, Z.IdeAseg, cCodPlan, crevplan, Z.NumMod + 1, 'VAL', nPrimaMoneda, nMtoTotRecaMoneda,
                      nMtoTotDctoMoneda, nMtoFactRecaMoneda, nMtoFactDctoMoneda, cCodMoneda, nPrima, nMtoFactReca, nMtoFactDcto, dFecIniValid,
                      dFecFinValid, 0, 0, nMtoSueldo, nSumaAseg, nSumaAsegMoneda, nNroSalarios);
         --
         PR_MOD_ASEG.INCLUIR_REV_OPE (nIdeAseg, Z.NumMod + 1);
      END LOOP;
      --<I Defect  3516-3517> Anusky Gonzalez - 19/09/2005
      UPDATE MOD_COBERT
      SET NumRever = nNumOper
      WHERE  (IdeCobert, NumMod) IN (SELECT M.IdeCobert, M.NumMod
                                     FROM   RECIBO R, MOD_COBERT M
                                     WHERE  R.NumOper = PR_POLIZA.nNumOperRev
                                     AND    R.IdeMovPrima = M.IdeMovPrima);
      --<F Defect  3516-3517> Anusky Gonzalez - 19/09/2005
      -- Determina si el proceso es invocado del PR_POLIZA.PROCESO_LOTE o cualquier otro
      -- Si es un proceso en lote no se debe crear el registro en MOD_COBERT
      IF NVL (PR_POLIZA.cIndProcesoLote, 'N') != 'S' THEN
         FOR Z IN MOV_PRIMA_Q LOOP
            BEGIN
               SELECT OrigModCobert, Tasa, CodMoneda, SumaAsegMoneda, PrimaMoneda, FecIniValid, FecFinValid, IndIncRen, IndPlazoEspera,
                      FecIniPlazoEspera, FecFinPlazoEspera, NumCert, CodRamoCert
               INTO   cOrigModCobert, nTasa, cCodMoneda, nSumaAsegMoneda, nPrimaMoneda, dFecIniValid, dFecFinValid, cIndIncRen, cIndPlazoEspera,
                      dFecIniPlazoEspera, dFecFinPlazoEspera, nNumCert, cCodRamoCert
               FROM   MOD_COBERT
               WHERE  IdeCobert = Z.IdeCobert
               AND    NumMod = (Z.NumMod - 1);
            END;
            INSERT INTO MOD_COBERT
                        (IdeCobert, NumMod, OrigModCobert, StsModCobert, Tasa, CodMoneda, SumaAsegMoneda, PrimaMoneda, FecIniValid,
                         FecFinValid, IndIncRen, IndPlazoEspera, FecIniPlazoEspera, FecFinPlazoEspera, IdePol, NumCert, CodRamoCert, IndSiniestro,
                         IndCobEsp, IndMov)
                 VALUES (Z.IdeCobert, Z.NumMod + 1, cOrigModCobert, 'VAL', nTasa, cCodMoneda, nSumaAsegMoneda, nPrimaMoneda, dFecIniValid,
                         dFecFinValid, cIndIncRen, cIndPlazoEspera, dFecIniPlazoEspera, dFecFinPlazoEspera, nIdePol, nNumCert, cCodRamoCert, 'N',
                         'N', 1);
            PR_MOD_COBERT.INCLUIR (Z.IdeCobert, Z.NumMod + 1);
         END LOOP;
         --<I Defect  3516-3517> Anusky Gonzalez - 19/09/2005
         FOR Z IN (SELECT M.IdeCobert, M.NumMod
                   FROM   RECIBO R, MOD_COBERT M
                   WHERE  R.NumOper = PR_POLIZA.nNumOperRev
                   AND    R.IdeMovPrima = M.IdeMovPrima) LOOP
            BEGIN
               SELECT FecIniValid, FecFinValid
               INTO   dFecIniValidCob, dFecFinValidCob
               FROM   MOD_COBERT
               WHERE  IdeCobert = Z.IdeCobert
               AND    NumMod = (Z.NumMod - 1);
            EXCEPTION
               WHEN NO_DATA_FOUND THEN
                  BEGIN
                     SELECT FecIniValid, FecFinValid
                     INTO   dFecIniValidCob, dFecFinValidCob
                     FROM   MOD_COBERT
                     WHERE  IdeCobert = Z.IdeCobert
                     AND    NumMod = Z.NumMod;
                  END;
            END;
            IF cOrigModCobert IN ('C', 'G') THEN
               BEGIN
                  UPDATE COBERT_CERT
                  SET FecIniValid = dFecIniValidCob,
                      FecFinValid = dFecFinValidCob
                  WHERE  IdeCobert = Z.IdeCobert;
               EXCEPTION
                  WHEN OTHERS THEN
                     RAISE_APPLICATION_ERROR (-20100, 'Error actualizando COBERT_CERT: ' || SQLERRM);
               END;
            ELSIF cOrigModCobert = 'A' THEN
               BEGIN
                  UPDATE COBERT_ASEG
                  SET FecIniValid = dFecIniValidCob,
                      FecFinValid = dFecFinValidCob
                  WHERE  IdeCobert = Z.IdeCobert;
               EXCEPTION
                  WHEN OTHERS THEN
                     RAISE_APPLICATION_ERROR (-20100, 'Error actualizando COBERT_ASEG: ' || SQLERRM);
               END;
            ELSIF cOrigModCobert = 'B' THEN
               UPDATE COBERT_BIEN
               SET FecIniValid = dFecIniValidCob,
                   FecFinValid = dFecFinValidCob
               WHERE  IdeCobert = Z.IdeCobert;
            END IF;
         END LOOP;
      END IF;
      --<I Defect  3516-3517> Anusky Gonzalez - 19/09/2005
      -- Reversi�n del Ajuste Blanket
      BEGIN
         UPDATE COBERT_AJUSTE_BLANKET
         SET StsAjuBlan = 'REV'
         WHERE  IdepolRen = nIdePol
         AND    Numoper = nNumOper;
      END;
      /*--Erick Zoque 04/09/2013 E_CON_20120815_1_1
      --REVERSION DE COMPROBANTE DE SUMAASEG
      COMP_SUMAASEG(nIdePol,nNumOperMov,'REV');*/
      --
      PR_OPERACION.TERMINAR;
      --Erick Zoque 20/05/2013 E_CON_20120821_1_7
      PR_FECHAS_EQUIVALENTES.CEVENTO := NULL;
   END REVERTIR_OPERACION;
   --COPIAR_POLIZA
   FUNCTION COPIAR_POLIZA (
      nIdePol          POLIZA.IdePol%TYPE,
      cCodPlan         CERT_RAMO.CodPLAN%TYPE,
      cRevPlan         CERT_RAMO.REVPLAN%TYPE,
      cTipoVig         POLIZA.TipoVig%TYPE,
      cCodFormPago     POLIZA.CodFormPago%TYPE,
      cCodPlanFrac     POLIZA.CodPlanFrac%TYPE,
      cModPlanFrac     POLIZA.ModPlanFrac%TYPE,
      dFecIniVig       DATE,
      dFecFinVig       DATE,
      nSumaAseg        NUMBER)
      RETURN NUMBER IS
      --
      dFecRen         POLIZA.FecRen%TYPE;
      nIndRenAuto     POLIZA.IndRenAuto%TYPE;
      nIdePolRen      POLIZA.IdePol%TYPE;
      dFecUltFact     POLIZA.FecUltFact%TYPE;
      cIndTipoPro     POLIZA.IndTipoPro%TYPE;
      nNumRen         POLIZA.NumRen%TYPE;
      cIndRenGar      POLIZA.IndRenGar%TYPE;
      cVigProxRen     POLIZA.VigProxRen%TYPE;
      --
      dFecFact        DATE;
      dFecIniVigRen   DATE;
      dFecFinVigRen   DATE;
      nMeses          NUMBER;
      nExiPol         NUMBER;
      cCodProd        POLIZA.CodProd%TYPE;
      cCodOfiSusc     POLIZA.CodOfiSusc%TYPE;
      cCodCia         POLIZA.CodCia%TYPE;
      dFecSts         DATE;
      --
      CURSOR CERTIFICADOS_C IS
         SELECT NumCert
         FROM   CERTIFICADO
         WHERE  IdePol = nIdePol
         AND    StsCert = 'ACT';
   BEGIN
      nSumaAsegCopia   := nSumaAseg;
      cCpyPlanFrac     := cCodPlanFrac;
      nCpyModPlanFrac  := cModPlanFrac;
      BEGIN
         SELECT 1, CodProd, CodOfiSusc, CodCia
         INTO   nExiPol, cCodProd, cCodOfiSusc, cCodCia
         FROM   POLIZA
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            RAISE_APPLICATION_ERROR (-20100, 'No se encontro datos de Poliza con identIFicador ' || nIdePol);
         WHEN TOO_MANY_ROWS THEN
            RAISE_APPLICATION_ERROR (-20100, 'Datos de poliza se encuentran duplicados para identIFicador ' || nIdePol);
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, 'Error al Selecionar datos de Poliza. ' || SQLERRM);
      END;
      BEGIN
         SELECT SQ_IdePol.NEXTVAL
         INTO   nIdePolRen
         FROM   DUAL;
      END;   -- NUEVO IdePol DE LA POLIZA --
      nNumPol          := PR_POLIZA.NumERO_ENDOSO (cCodProd, cCodOfiSusc, 'A');
      --Erick Zoque 20/05/2013 E_CON_20120821_1_7
      dFecSts          := PR_FECHAS_EQUIVALENTES.FECHA_EMISION (SYSDATE, cCodCia,'001');
      -- INSERTAR --
      INSERT INTO POLIZA
                  (IDEPOL, CODPOL, NUMPOL, CODCLI, TIPOCOTPOL, FECOPER, OBSERVA, NUMREN, INDNUMPOL, CODPROD, STSPOL, FECREN, FECINIVIG, FECFINVIG,
                   TIPOVIG, TIPOPDCION, TIPOFACT, INDCOA, CODFORMFCION, CODOFIEMI, CODOFISUSC, CODMONEDA, INDMULTIINTER, INDPOLADHESION, INDRENAUTO,
                   FECANUL, CODMOTVANUL, TEXTMOTVANUL, TIPOSUSC, CODFORMPAGO, TIPOANUL, FECULTFACT, CODPLANFRAC, MODPLANFRAC, INDCOBPROV, INDTIPOPRO,
                   CLASEPOL, PORCCTP, INDFACT, INDMOVPOLREN, INDRESPPAGO, FECINIPOL, IDECOT, INDRECUPREA, TIPOCOBRO, INDRENGAR, VIGPROXREN, NUMSOLIC,
                   INDCANCELA, INDIMPFACT, CODGRPPOL, EXPDIVS, INDDEVCONSIN, INDRENOVLOTE, NUMVECESIMP, CODFORMCAN, FECINIVIG1ERANO, CODCIA,
                   FECRESOLUCION, TIPTRM, FECTRMCONVENIDA)
         SELECT nIdePolRen, CodPol, nNumPol, CodCli, TipoCotPol, dFecSts, Observa, 0, IndNumPol, CodProd, 'VAL', dFecFinVig, dFecIniVig, dFecFinVig,
                cTipoVig, TipoPdcion, TipoFact, IndCoa, CodFormFcion, CodOfiemi, CodOfiSusc, CodMoneda, IndMultiInter, IndPolAdhesion, IndRenAuto,
                NULL, NULL, NULL, TipoSusc, cCodFormPago, TipoAnul, dFecIniVig, cCodPlanFrac, cModPlanFrac, IndCobProv, IndTipoPro, ClasePol, PorcCtp,
                IndFact, IndMovPolRen, IndRespPago, dFecIniVig, IdeCot, IndRecupRea, TipoCobro, IndRenGar, VigProxRen, NumSolic, IndCancela,
                IndImpFact, CodGrpPol, ExpDivs, IndDevConSin, IndRenovLote, NumVecesImp, CodFormCan, FecIniVig1erAno, CodCia, FecResolucion, TipTRM,
                FecTRMConvenida
         FROM   POLIZA
         WHERE  IdePol = nIdePol;
      --                    --
      FOR CP IN CERTIFICADOS_C LOOP
         PR_CERTIFICADO.COPIAR_CERTIFICADO (nIdePol, nIdePolRen, CP.NumCert, cCodPlan, cRevPlan, cCodPlanFrac, cModPlanFrac, dFecIniVig, dFecFinVig);
         --EZ SE AGREGA COPIA PARA DIRECCION DE RIESGO 25/07/2014
         PR_DIREC_RIESGO_CERT.COPIAR_DIREC_RIESGO(nIdePol, nIdePolRen, CP.NumCert);
      END LOOP;
      PR_PART_INTER_POL.RENOVAR (nIdePol, nIdePolRen);
      PR_POLIZA_CLIENTE.RENOVAR (nIdePol, nIdePolRen);
      PR_DIST_COA.RENOVAR (nIdePol, nIdePolRen, dFecIniVig, dFecFinVig);
      PR_RECA_DCTO_POL.RENOVAR (nIdePol, nIdePolRen, dFecIniVig, dFecFinVig);
      PR_ANEXO_POL.RENOVAR (nIdePol, nIdePolRen, dFecIniVig, dFecFinVig);
      PR_DEDUCIBLE_POLIZA.RENOVAR (nIdePol, nIdePolRen, dFecIniVigRen);
      PR_POLIZA.ACTUALIZA_ENDOSO (cCodProd, cCodOfiSusc, 'A');
      --
      RETURN (nIdePolRen);
   END COPIAR_POLIZA;
   --
   PROCEDURE REFACTURAR (nIdePol POLIZA.IdePol%TYPE, nNumOper OPER_POL.NumOPER%TYPE) IS
      nNumOperOLD   OPERACION.NumOPER%TYPE;
      CURSOR ACREENCIAS IS
         SELECT DISTINCT A.IDEFACT
         FROM   OPER_POL OP, ACREENCIA A, COND_FINANCIAMIENTO CF, GIROS_FINANCIAMIENTO GF, REL_OBLACR_FRAC ROF
         WHERE  OP.IdePol = nIdePol
         AND    OP.IdePol = CF.IdePol
         AND    OP.NumCert = CF.NumCert
         AND    OP.NumOPER = CF.NumOPER
         AND    CF.NumFinanc = GF.NumFinanc
         AND    GF.NumACRE = A.NumACRE
         AND    A.STSACRE = 'ACT'
         AND    ROF.NumACRE = A.NumACRE
         AND    A.IDEFACT IS NOT NULL;
   BEGIN
      FOR ACRE IN ACREENCIAS LOOP
         PR_FACTURA.AnulAR (ACRE.IDEFACT);
         SELECT NumOPER
         INTO   nNumOPEROLD
         FROM   FACTURA F
         WHERE  IDEFACT = ACRE.IDEFACT;
         PR_FACTURA.GENERAR_FACT_MOD (nIdePol, ACRE.IDEFACT, nNumOperOld);
      END LOOP;
   END;
   --
   PROCEDURE PROCESO_LOTE (
      nNumProceso     NUMBER,
      dFecBase        DATE,
      cCodProd        POLIZA.CodProd%TYPE,
      nNumPol         POLIZA.NumPol%TYPE,
      cOficEmi        POLIZA.CodOfiemi%TYPE,
      CCodCliente     POLIZA.CodCli%TYPE) IS
      nExisteSin           VARCHAR2 (1);
      nFactCob             VARCHAR2 (1);
      cError               VARCHAR2 (1)                                 := 'N';
      dFecProc             DATE;
      nDias                NUMBER;
      nVeces               NUMBER;
      cEjecutar            VARCHAR2 (1)                                 := 'N';
      cTipoPdcion          POLIZA.TipoPdcion%TYPE;
      ---PARA DIVIDENDOS
      nMes                 NUMBER;
      nTasaRENDRealTotal   NUMBER                                       := 1;
      nTasaRENDRealMens    NUMBER                                       := 1;
      nTasaRENDReal        NUMBER                                       := 1;
      nIdeRep              NUMBER                                       := 1;
      --- PARA PRESTAMOS
      cGenPrestamo         VARCHAR2 (1);
      nMontoPrestamo       NUMBER (14, 2);
      nIdeAseg             ASEGURADO.IdeAseg%TYPE;
      --- PARA PRORROGADO
      nFactor              NUMBER;
      dFecMaxFinVigCob     DATE;
      nStsPolPro           POLIZA.StsPol%TYPE;
      nIndPolVida          NUMBER (1);
      nAnoGenRescat        CARACTERISTICA_PLAN_VIDA.AnoGenRescat%TYPE;
      nNumRen              POLIZA.NumRen%TYPE;
      cTodoCob             VARCHAR2 (1);
      nNumero              NUMBER (14)                                  := 0;
      nMtoAcreMoneda       ACREENCIA.MtoAcreMoneda%TYPE;
      nNumOper             OPER_POL.NumOper%TYPE;
      cTipoId              TERCERO.TipoId%TYPE;
      nNumId               TERCERO.NumId%TYPE;
      cDvId                TERCERO.DvId%TYPE;
      nDiasTram            NUMBER (5);
      nAnulaPoliza         NUMBER (9)                                   := 0;
      cCodUsr              VARCHAR2 (15);
      cCodOper             VARCHAR2 (3)                                 := '213';
      CURSOR PROCESO IS
         SELECT CodDefPerfil, Descrip, Param, Periodicidad, Fecha
         FROM   DEF_PERFIL
         WHERE  CodPerfil = '0002'
         AND    CodDefPerfil = 2
         ORDER BY Orden;
      CURSOR POL_ANU IS
         SELECT IdePol, IndCan, FecAnul, CodProd, NumPol, CodOfiemi, FecIniVig, FecFinVig, CodProceso, NumRen
         FROM   T$_ANULA_POLIZA
         WHERE  NumProceso = nNumProceso
         AND    CodPol = NVL (cCodProd, CodPol)
         AND    NumPol = NVL (nNumPol, NumPol)
         AND    CodOfiSusc = NVL (cOficEmi, CodOfiSusc)
         AND    CodCli = NVL (cCodCliente, CodCli)
         AND    IndCan = 'S'
         ORDER BY CodProd, NumPol;
-----------------------------------------------------------------------      ---
      PROCEDURE GENERAR_ACREENCIA (cTipoId VARCHAR2, nNumId NUMBER, cDvId VARCHAR2, nNumOperAnu NUMBER, nMtoAcreMoneda NUMBER) IS
         nNumAcre       ACREENCIA.NumAcre%TYPE;
         dFecEmi        DATE;
         nIdeComp       NUMBER (14);
         cCodOper       VARCHAR2 (3)                   := '213';
         nMontoMin      ACREENCIA.MtoAcreMoneda%TYPE;
         nNumOblig      OBLIGACION.NumOblig%TYPE;
         nIdeRecOblig   RECIBO.IdeRec%TYPE;
         nNumAcreAnu    ACREENCIA.NumAcre%TYPE;
         nPorcIVA       NUMBER;
         nDiasRev       NUMBER (3);
         CURSOR C_Cpto IS
            SELECT CodGrupoAcre, CodCptoAcre, IndCptoImp
            FROM   CPTO_ACRE
            WHERE  CodGrupoAcre = 'REVOCA';
         CURSOR C_RECIBOS IS
            SELECT DISTINCT A.NumAcre, REC.CodRamoCert, REC.Iderec
            FROM   ACREENCIA A, GIROS_FINANCIAMIENTO GF, REC_FINANCIAMIENTO RF, RECIBO REC
            WHERE  A.Numacre = nNumacreAnu
            AND    gf.Numacre = A.Numacre
            AND    rf.numfinanc = gf.Numfinanc
            AND    Rec.iderec = rf.Iderec;
      BEGIN
         -- Buscar Obligacion generada por Anulacion
         BEGIN
            SELECT MAX (NumOblig)
            INTO   nNumOblig
            FROM   OBLIGACION
            WHERE  NumOper = nNumOperAnu;
         END;
         -- Buscar Acreenciada por Anulacion
         BEGIN
            SELECT MAX (NumAcre)
            INTO   nNumAcreAnu
            FROM   REL_OBLACR_FRAC
            WHERE  NumOblig = nNumOblig;
         END;
         BEGIN
            SELECT MAX (FecResolucion)
            INTO   dFecEmi
            FROM   POLIZA
            WHERE  IdePol = PR_POLIZA.nIdePol
            AND    FecResolucion IS NOT NULL;
         END;
         IF dFecEmi IS NULL THEN
            BEGIN
               SELECT MIN (FecMov)
               INTO   dFecEmi
               FROM   OPER_POL
               WHERE  IdePol = PR_POLIZA.nIdePol
               AND    NumOper = nNumOperAnu;
            END;
         END IF;
         BEGIN
            nMontoMin  := TO_NUMBER (PR.BUSCA_PARAMETRO ('079'));
         EXCEPTION
            WHEN OTHERS THEN
               RAISE_APPLICATION_ERROR (-20100, 'No se ha definido correctamente los Par�metros 079');
         END;
         IF nMontoMin < nMtoAcreMoneda THEN
            nNumAcre               := PR_ACREENCIA.CREAR (nMtoAcreMoneda, 'TCR', PR_POLIZA.cCodMoneda, cTipoId, nNumId, cDvId, PR_POLIZA.cCodCia);
            BEGIN
               nDiasRev  := TO_NUMBER (PR.BUSCA_PARAMETRO ('148'));
            EXCEPTION
               WHEN OTHERS THEN
                  RAISE_APPLICATION_ERROR (-20100, 'No se ha definido correctamente los Par�metros 148');
            END;
            BEGIN
               UPDATE ACREENCIA
               SET NumReferencia = nNumAcreAnu,
                   --Erick Zoque 20/05/2013 E_CON_20120821_1_7
                   FecVencAcre = PR_FECHAS_EQUIVALENTES.FECHA_EMISION (FecVencAcre, PR_POLIZA.cCodCia,'001') + nDiasRev
               WHERE  NumAcre = nNumAcre;
            EXCEPTION
               WHEN OTHERS THEN
                  RAISE_APPLICATION_ERROR (-20288, PR.MENSAJE ('ABD', 20288, 'ACREENCIA:' || SQLERRM, NULL, NULL));
            END;
            FOR C IN C_Cpto LOOP
               IF C.IndCptoImp = 'S' THEN
                  nPorcIVA  := NVL (PR_IMPUESTO.BUSCAR_IMPUESTO_CIA_CPTO (PR_POLIZA.cCodCia, C.CodGrupoAcre, C.CodCptoAcre, dFecEmi, 'I'), 0);
               ELSE
                  nPorcIVA  := 100;
               END IF;
               PR_DET_ACRE.DETALLE (nNumAcre, nMtoAcreMoneda * (nPorcIVA / 100), PR_POLIZA.cCodMoneda, C.CodGrupoAcre, C.CodCptoAcre);
            END LOOP;
            PR_DET_ACRE.ACT_MTO_ACRE (nNumAcre);
            PR_ACREENCIA.INCLUIR (nNumAcre);
            PR_ACREENCIA.ACTIVAR (nNumAcre);
            PR_OPERACION.nNumOper  := nNumOperAnu;
            PR_FACTURA.GENERA_FACT_OTROS (nNumAcre);
            -- Busca Comprobante de Anulaci�n
            BEGIN
               SELECT DISTINCT  CodOfi
               INTO   cCodOfiEmi
               FROM   COMPROBANTE
               WHERE  NumOper = nNumOperAnu;
            EXCEPTION
               WHEN NO_DATA_FOUND THEN
                  RAISE_APPLICATION_ERROR (-20205, PR.MENSAJE ('ABD', 20205, nNumOperAnu, 'COMPROBANTE', NULL));
            END;
            -- Busca tipo de Producci�n
            BEGIN
               SELECT TipoPdcion
               INTO   cTipoPdcion
               FROM   POLIZA
               WHERE  IdePol = PR_POLIZA.nIdePol;
            EXCEPTION
               WHEN NO_DATA_FOUND THEN
                  RAISE_APPLICATION_ERROR (-20205, PR.MENSAJE ('ABD', 20205, PR_POLIZA.nIdePol, 'POLIZA', NULL));
            END;
         --   nIdeComp               := PR_COMPROBANTE.ABRIR (cCodOper, cCodOfiEmi, nNumOperAnu, PR_POLIZA.cCodCia, cTipoPdcion, PR_POLIZA.cCodMoneda);
          /*  IF NVL (nIdeComp, 0) != 0 THEN
               FOR Y IN C_RECIBOS LOOP
                  PR_MOV_DEFINITIVO.GEN_MOV (nNumAcre, cCodOper, nIdeComp, Y.CodRamoCert, Y.Iderec);
               END LOOP;
               PR_COMPROBANTE.CERRAR (nIdeComp, 'PRC');
               PR_COMPROBANTE.REGISTRA_ERROR (nIdeComp, NULL, NULL, nNumAcre, NULL, SYSDATE, NULL, NULL, 'PR_ACREENCIA.GENERAR', cCodOper);
            END IF;*/
         END IF;
      END;
      FUNCTION OBTIENE_MONTO_ACREENCIA (nIdePol NUMBER)
         RETURN NUMBER IS
         nMtoMoneda   ACREENCIA.MtoAcreMoneda%TYPE;
         nDiasAcre    NUMBER (5);
      BEGIN
         BEGIN
            nDiasAcre  := TO_NUMBER (PR.BUSCA_PARAMETRO ('080'));
         EXCEPTION
            WHEN OTHERS THEN
               RAISE_APPLICATION_ERROR (-20100, 'No se ha definido correctamente los Par�metros 080');
         END;
         BEGIN
            SELECT SUM (ROUND (MtoMoneda / GREATEST ((FecFinValid - FecIniValid), 1), 2))
            INTO   nMtoMoneda
            FROM   MOV_PRIMA
            WHERE  IdePol = nIdePol;
            nMtoMoneda  := nMtoMoneda * nDiasAcre;
         END;
         RETURN (nMtoMoneda);
      END;
      --
      PROCEDURE REVERTIR_OPERACION (nIdePol POLIZA.IdePol%TYPE, nNumOper OPER_POL.NumOPER%TYPE) IS
         --
         nNumCert             CERTIFICADO.NumCert%TYPE;
         nIdeComp             NUMBER (14);
         cCodRamoCert         CERT_RAMO.CodRamoCert%TYPE;
         cOrigModCobert       MOD_COBERT.OrigModCobert%TYPE;
         cTipoOp              VARCHAR2 (3);
         nNumOperMov          OPER_POL.NumOper%TYPE;
         cCodMoneda           VARCHAR2 (3);
         --<I Defect 2343> Javier Asmat/05.05.2005
         --Se cambia el tipo de dato para las variables de Suma, Tasa y Prima para evitar error de value error
         nSumaAsegMoneda      MOD_COBERT.SumaAsegMoneda%TYPE;   --NUMBER (14, 2);
         nPrimaMoneda         MOD_COBERT.PrimaMoneda%TYPE;   --NUMBER (14, 2);
         nTasa                MOD_COBERT.Tasa%TYPE;   --NUMBER;
         --<F Defect 2343>
         dFecIniValid         DATE;
         dFecFinValid         DATE;
         cIndIncRen           VARCHAR2 (1);
         cIndPlazoEspera      VARCHAR2 (1);
         dFecIniPlazoEspera   DATE;
         dFecFinPlazoEspera   DATE;
         nNumModNew           MOD_COBERT.NumMod%TYPE;
         nNumModRev           MOD_COBERT.NumMod%TYPE;
         nIdeMovPrima         MOV_PRIMA.IdeMovPrima%TYPE;
         cCodOfiemi           POLIZA.CodOfiemi%TYPE;
         cStsPol              POLIZA.StsPol%TYPE;
         cStsCobert           COBERT_CERT.StsCobert%TYPE;
         nIdeAseg             ASEGURADO.IdeAseg%TYPE;
         cStsAseg             ASEGURADO.StsAseg%TYPE;
         cStsBien             BIEN_CERT.StsBien%TYPE;
         nIdeBien             BIEN_CERT.IdeBien%TYPE;
         cStsCertRamo         CERT_RAMO.StsCertRamo%TYPE;
         cCodPlan             CERT_RAMO.CodPlan%TYPE;
         cRevPlan             CERT_RAMO.RevPlan%TYPE;
         cStsCert             CERTIFICADO.StsCert%TYPE;
         cSts                 CERTIFICADO.StsCert%TYPE;
         cClaseBien           MOD_BIEN_CERT.ClaseBien%TYPE;
         cCodBien             MOD_BIEN_CERT.CodBien%TYPE;
         cStsModBien          MOD_BIEN_CERT.StsModBien%TYPE;
         nMtoValBien          MOD_BIEN_CERT.MtoValBien%TYPE;
         nMtoValBienMoneda    MOD_BIEN_CERT.MtoValBienMoneda%TYPE;
         cTipPrimRiesgo       MOD_BIEN_CERT.TipPrimRiesgo%TYPE;
         nPorcPrimRiesgo      MOD_BIEN_CERT.PorcPrimRiesgo%TYPE;
         nMtoFactDctoMoneda   MOD_ASEG.MtoFactDctoMoneda%TYPE;
         nPrima               MOD_ASEG.Prima%TYPE;
         nMtoFactReca         MOD_ASEG.MtoFactReca%TYPE;
         nMtoFactDcto         MOD_ASEG.MtoFactDcto%TYPE;
         nMtoTotRecaMoneda    MOD_ASEG.MtoTotRecaMoneda%TYPE;
         nMtoTotDctoMoneda    MOD_ASEG.MtoTotDctoMoneda%TYPE;
         nMtoFactRecaMoneda   MOD_ASEG.MtoFactRecaMoneda%TYPE;
         nMtoSueldo           MOD_ASEG.MtoSueldo%TYPE;
         nSumaAseg            MOD_ASEG.SumaAseg%TYPE;
         nNroSalarios         MOD_ASEG.NroSalarios%TYPE;
         dFecSts              DATE;
         -- MOVIMIENTO DE IdeMovPrima
         CURSOR MOV_PRIMA_Q IS
            SELECT M.IdeMovPrima, M.IdeCobert, M.NumMod, M.FecIniValid
            FROM   RECIBO R, MOD_COBERT M
            WHERE  R.NumOper = nNumOperMov
            AND    R.IdeMovPrima = M.IdeMovPrima;
         -- MOVIMIENTO DE IdeMovPrima BIENES
         CURSOR MOV_PRIMA_B IS
            SELECT M.IdeMovPrima, M.IdeBien, M.NumMod, M.FecIniValid, M.NumCert
            FROM   RECIBO R, MOD_BIEN_CERT M
            WHERE  R.NumOper = nNumOperMov
            AND    R.IdeMovPrima = M.IdeMovPrima;
         -- MOVIMIENTO DE IdeMovPrima ASEGURADO
         CURSOR MOV_PRIMA_A IS
            SELECT M.IdeMovPrima, M.IdeAseg, M.NumMod, M.FecIniValid, M.NumCert
            FROM   RECIBO R, MOD_ASEG M
            WHERE  R.NumOper = nNumOperMov
            AND    R.IdeMovPrima = M.IdeMovPrima;
         CURSOR Q_DET_OPER_POL IS
            SELECT IdePol, NumCert, Numoper, Numdetoper, Descdet, Mtodetoper, Codrecadcto, Tiporecadcto, Iderecadcto, Origmodrecadcto
            FROM   DET_OPER_POL
            WHERE  NumOper = nNumOper;
         CURSOR OBLIGACION_Q IS
            SELECT NumOblig
            FROM   OBLIGACION
            WHERE  NumOper = nNumOper
            AND    StsOblig = 'ACT';
         -- RECIBOS (DE LA OPERACION ANTERIOR) --
         CURSOR C_RECIBO_Q IS
            SELECT IdeRec
            FROM   RECIBO
            WHERE  NumOper = nNumOper
            AND    StsRec = 'ACT'
            ORDER BY IdeRec;
         ---- FRACCIONAMIENTO --
         CURSOR FRACC_X_CERTIFICADO_Q IS
            SELECT NumFinanc, NumCert, NumOper, TipoId, NumId, DvId, CodMoneda, SUM (MtoNetoFrac) MtoNetoFrac
            FROM   COND_FINANCIAMIENTO
            WHERE  IdePol = nIdePol
            AND    NumOper = nNumOPer
            GROUP BY NumFinanc, NumCert, NumOper, TipoId, NumId, DvId, CodMoneda;
         -- OBLIGACIONES (DE LA OPERACION) --
         CURSOR FACTURA IS
            SELECT IdeFact
            FROM   FACTURA
            WHERE  NumOper = nNumOper
            AND    StsFact = 'ACT';
      --
      BEGIN
         PR_OPER_USUARIO.AUTORIZAR ('035', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
         nNumOperMov            := nNumOper;
         PR_OPERACION.INICIAR ('REVER', 'OPER_POL', TO_CHAR (nIdePol));
         PR_POLIZA.cOperRev     := 'REV';
         PR_POLIZA.CARGAR (nIdePol);
         --Erick Zoque 20/05/2013 E_CON_20120821_1_7
         dFecSts                := PR_FECHAS_EQUIVALENTES.FECHA_EMISION (SYSDATE, PR_POLIZA.cCodCia,'01');
         --Fase 1 Anulaciones y reversos --------------------------------------------------------
         BEGIN
            UPDATE MOD_RECA_DCTO_CERTIF
            SET StsModRecaDcto = 'REV'
            WHERE  NumOper = nNumOper;
         EXCEPTION
            WHEN OTHERS THEN
               RAISE_APPLICATION_ERROR (-20100, 'ERROR actualizando MOD_RECA_DCTO_CERTIF: ' || SQLERRM);
         END;
         BEGIN
            UPDATE MOD_RECA_DCTO_CERTIF_ASEG
            SET StsModRecaDcto = 'REV'
            WHERE  NumOper = nNumOper;
         EXCEPTION
            WHEN OTHERS THEN
               RAISE_APPLICATION_ERROR (-20100, 'ERROR actualizando MOD_RECA_DCTO_CERTIF_ASEG: ' || SQLERRM);
         END;
         BEGIN
            UPDATE MOD_RECA_DCTO_CERTIF_BIEN
            SET StsModRecaDcto = 'REV'
            WHERE  NumOper = nNumOper;
         EXCEPTION
            WHEN OTHERS THEN
               RAISE_APPLICATION_ERROR (-20100, 'ERROR actualizando MOD_RECA_DCTO_CERTIF_BIEN: ' || SQLERRM);
         END;
         FOR X IN Q_DET_OPER_POL LOOP   -- Dejar Registro de lo Sucedido  --
            IF X.OrigModRecaDcto = 'C' THEN
               PR_RECA_DCTO_CERTIF.GENERAR_MODIFICACION ('REV', X.IdeRecaDcto);
            ELSIF X.OrigModRecaDcto = 'A' THEN
               PR_RECA_DCTO_CERTIF_ASEG.GENERAR_MODIFICACION ('REV', X.IdeRecaDcto);
            ELSIF X.OrigModRecaDcto = 'B' THEN
               PR_RECA_DCTO_CERTIF_BIEN.GENERAR_MODIFICACION ('REV', X.IdeRecaDcto);
            END IF;
         END LOOP;
         FOR X IN FRACC_X_CERTIFICADO_Q LOOP
            PR_COND_FINANCIAMIENTO.AnulAR (X.NumFinanc, nIdeComp, 'S');   -- FACCIONAMIENTO --
         END LOOP;
         FOR G IN C_RECIBO_Q LOOP   -- AnulAR RECIBO  --
            BEGIN
               SELECT IdeMovPrima
               INTO   nIdeMovPrima
               FROM   RECIBO
               WHERE  IdeRec = G.IdeRec;
            EXCEPTION
               WHEN NO_DATA_FOUND THEN
                  RAISE_APPLICATION_ERROR (-20100, 'No existen datos en RECIBO para IDEREC: ' || G.IdeRec);
            END;
            PR_MOV_PRIMA.GENERAR_TRANSACCION (nIdeMovPrima, 'ANULA');
            BEGIN
               UPDATE RECIBO
               SET StsRec = 'REV',
                   FecAnul = dFecSts
               WHERE  IdeRec = G.IdeRec;
            EXCEPTION
               WHEN OTHERS THEN
                  RAISE_APPLICATION_ERROR (-20100, 'ERROR actualizando RECIBO: ' || SQLERRM);
            END;
         END LOOP;
         --
         FOR X IN OBLIGACION_Q LOOP   -- AnulAR OBLIGACIONES
            PR_OBLIGACION.AnulAR_SIS ('PRC', X.NumOblig);
         END LOOP;
         FOR F IN FACTURA LOOP   -- AnulAR FACTURAS
            PR_FACTURA.AnulAR_SIS (F.IdeFAct);
         END LOOP;
         --
         PR_GEN_REA.AnulA_CTA_TEC (nNumOper);   -- REVERTIR REASEGURADORES
         BEGIN
            DELETE MOD_COBERT
            WHERE  IdePol = nIdePol
            AND    StsModCobert = 'VAL';   -- REVERSO
         EXCEPTION
            WHEN OTHERS THEN
               RAISE_APPLICATION_ERROR (-20100, 'ERROR eliminando MOD_COBERT: ' || SQLERRM);
         END;
         FOR Z IN MOV_PRIMA_B LOOP
            nIdeMovPrima  := Z.IdeMovPrima;
            nIdeBien      := Z.IdeBien;
            nNumCert      := Z.NumCert;
            BEGIN
               SELECT idebien, Codramocert, Codplan, revplan, clasebien, Codbien, mtovalbien, mtovalbienmoneda, Fecinivalid, Fecfinvalid,
                      tipprimriesgo, porcprimriesgo
               INTO   Z.IdeBien, cCodRamoCert, cCodPlan, cRevPlan, cClaseBien, cCodbien, nMtoValBien, nMtoValBienMoneda, dFecIniValid, dFecFinValid,
                      cTipPrimRiesgo, nPorcPrimRiesgo
               FROM   MOD_BIEN_CERT
               WHERE  IdeBien = Z.IdeBien
               AND    NumMod = (Z.NumMod - 1);
            EXCEPTION
               WHEN NO_DATA_FOUND THEN
                  SELECT idebien, Codramocert, Codplan, revplan, clasebien, Codbien, mtovalbien, mtovalbienmoneda, Fecinivalid, Fecfinvalid,
                         tipprimriesgo, porcprimriesgo
                  INTO   Z.IdeBien, cCodRamoCert, cCodPlan, cRevPlan, cClaseBien, cCodbien, nMtoValBien, nMtoValBienMoneda, dFecIniValid,
                         dFecFinValid, cTipPrimRiesgo, nPorcPrimRiesgo
                  FROM   MOD_BIEN_CERT
                  WHERE  IdeBien = Z.IdeBien
                  AND    NumMod = Z.NumMod;
            END;
            BEGIN
               SELECT MAX (NumMod) + 1
               INTO   nNumModNew
               FROM   MOD_BIEN_CERT
               WHERE  IdeBien = Z.IdeBien;
            END;
            INSERT INTO MOD_BIEN_CERT
                        (idebien, IdePol, NumCert, Codramocert, Codplan, revplan, Nummod, clasebien, Codbien, stsmodbien, mtovalbien,
                         mtovalbienmoneda, Fecinivalid, Fecfinvalid, tipprimriesgo, porcprimriesgo, IdeMovPrima, IdeMovPrimat)
                 VALUES (Z.IdeBien, nIdePol, Z.NumCert, cCodRamoCert, cCodPlan, crevplan, nNumModNew, cClaseBien, cCodBien, 'VAL', nMtoValBien,
                         nMtoValBienMoneda, dFecIniValid, dFecfinvalid, cTipPrimRiesgo, nPorcPrimRiesgo, 0, 0);
            --
            PR_MOD_BIEN_CERT.INCLUIR_REV_OPE (nIdeBien, nNumModNew);
         END LOOP;
         FOR Z IN MOV_PRIMA_A LOOP
            nIdeMovPrima  := Z.IdeMovPrima;
            nIdeAseg      := Z.IdeAseg;
            nNumCert      := Z.NumCert;
            BEGIN
               SELECT IdeAseg, Codramocert, Codplan, revplan, primamoneda, mtototrecamoneda, mtototdctomoneda, mtofactrecamoneda, mtofactdctomoneda,
                      CodMoneda, prima, mtofactreca, mtofactdcto, Fecinivalid, Fecfinvalid, mtosueldo, sumaaseg, SumaAsegMoneda, nrosalarios
               INTO   Z.IdeAseg, cCodRamoCert, cCodPlan, cRevPlan, nPrimaMoneda, nMtoTotRecaMoneda, nMtoTotDctoMoneda, nMtoFactRecaMoneda,
                      nMtoFactDctoMoneda, cCodMoneda, nPrima, nMtoFactReca, nMtoFactDcto, dFecIniValid, dFecFinValid, nMtoSueldo, nSumaAseg,
                      nSumaAsegMoneda, nNroSalarios
               FROM   MOD_ASEG
               WHERE  IdeAseg = Z.IdeAseg
               AND    NumMod = (Z.NumMod - 1);
            EXCEPTION
               WHEN NO_DATA_FOUND THEN
                  SELECT IdeAseg, Codramocert, Codplan, revplan, primamoneda, mtototrecamoneda, mtototdctomoneda, mtofactrecamoneda,
                         mtofactdctomoneda, CodMoneda, prima, mtofactreca, mtofactdcto, Fecinivalid, Fecfinvalid, mtosueldo, sumaaseg,
                         SumaAsegMoneda, nrosalarios
                  INTO   Z.IdeAseg, cCodRamoCert, cCodPlan, cRevPlan, nPrimaMoneda, nMtoTotRecaMoneda, nMtoTotDctoMoneda, nMtoFactRecaMoneda,
                         nMtoFactDctoMoneda, cCodMoneda, nPrima, nMtoFactReca, nMtoFactDcto, dFecIniValid, dFecFinValid, nMtoSueldo, nSumaAseg,
                         nSumaAsegMoneda, nNroSalarios
                  FROM   MOD_ASEG
                  WHERE  IdeAseg = Z.IdeAseg
                  AND    NumMod = Z.NumMod;
            END;
            BEGIN
               SELECT MAX (NumMod) + 1
               INTO   nNumModNew
               FROM   MOD_ASEG
               WHERE  IdeAseg = Z.IdeAseg;
            END;
            INSERT INTO MOD_ASEG
                        (IdePol, NumCert, Codramocert, IdeAseg, Codplan, revplan, Nummod, stsmodaseg, primamoneda, mtototrecamoneda,
                         mtototdctomoneda, mtofactrecamoneda, mtofactdctomoneda, CodMoneda, prima, mtofactreca, mtofactdcto, Fecinivalid,
                         Fecfinvalid, IdeMovPrima, IdeMovPrimat, mtosueldo, sumaaseg, SumaAsegMoneda, nrosalarios)
                 VALUES (nIdePol, Z.NumCert, cCodRamoCert, Z.IdeAseg, cCodPlan, crevplan, nNumModNew, 'VAL', nPrimaMoneda, nMtoTotRecaMoneda,
                         nMtoTotDctoMoneda, nMtoFactRecaMoneda, nMtoFactDctoMoneda, cCodMoneda, nPrima, nMtoFactReca, nMtoFactDcto, dFecIniValid,
                         dFecFinValid, 0, 0, nMtoSueldo, nSumaAseg, nSumaAsegMoneda, nNroSalarios);
            --
            PR_MOD_ASEG.INCLUIR_REV_OPE (nIdeAseg, nNumModNew);
         END LOOP;
         FOR Z IN MOV_PRIMA_Q LOOP
            nIdeMovPrima  := Z.IdeMovPrima;
            BEGIN
               SELECT OrigModCobert, Tasa, CodMoneda, SumaAsegMoneda, PrimaMoneda, Z.FecIniValid, FecFinValid, IndIncRen, IndPlazoEspera,
                      FecIniPlazoEspera, FecFinPlazoEspera, NumCert, CodRamoCert
               INTO   cOrigModCobert, nTasa, cCodMoneda, nSumaAsegMoneda, nPrimaMoneda, dFecIniValid, dFecFinValid, cIndIncRen, cIndPlazoEspera,
                      dFecIniPlazoEspera, dFecFinPlazoEspera, nNumCert, cCodRamoCert
               FROM   MOD_COBERT
               WHERE  IdeCobert = Z.IdeCobert
               AND    NumMod = (Z.NumMod - 1);
            EXCEPTION
               WHEN NO_DATA_FOUND THEN
                  BEGIN
                     SELECT OrigModCobert, 0, CodMoneda, 0, 0, Z.FecIniValid, FecFinValid, IndIncRen, IndPlazoEspera, FecIniPlazoEspera,
                            FecFinPlazoEspera, NumCert, CodRamoCert
                     INTO   cOrigModCobert, nTasa, cCodMoneda, nSumaAsegMoneda, nPrimaMoneda, dFecIniValid, dFecFinValid, cIndIncRen,
                            cIndPlazoEspera, dFecIniPlazoEspera, dFecFinPlazoEspera, nNumCert, cCodRamoCert
                     FROM   MOD_COBERT
                     WHERE  IdeCobert = Z.IdeCobert
                     AND    NumMod = Z.NumMod;
                  END;
            END;
            IF cOrigModCobert IN ('C', 'G') THEN
               BEGIN
                  SELECT StsCobert
                  INTO   cStsCobert
                  FROM   COBERT_CERT
                  WHERE  IdeCobert = Z.IdeCobert;
               EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                     RAISE_APPLICATION_ERROR (-20100, 'No Existe EL Z.IdeCobert = ' || Z.IdeCobert);
               END;
               IF cStsCobert = 'EXC' THEN
                  PR_COBERT_CERT.GENERAR_TRANSACCION (Z.IdeCobert, 'REVER');
                  BEGIN
                     UPDATE COBERT_CERT
                     SET IndExcluir = NULL,
                         FecExc = NULL,
                         CodMotvExc = NULL,
                         TextMotvExc = NULL
                     WHERE  IdeCobert = Z.IdeCobert;
                  EXCEPTION
                     WHEN OTHERS THEN
                        RAISE_APPLICATION_ERROR (-20100, 'Error actualizando COBERT_CERT: ' || SQLERRM);
                  END;
               END IF;
            ELSIF cOrigModCobert = 'A' THEN
               BEGIN
                  SELECT StsCobert, IdeAseg
                  INTO   cStsCobert, nIdeAseg
                  FROM   COBERT_ASEG
                  WHERE  IdeCobert = Z.IdeCobert;
               EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                     RAISE_APPLICATION_ERROR (-20100, 'No existen datos en COBERT_ASEG para IDECOBERT: ' || Z.IdeCobert);
               END;
               IF cStsCobert = 'EXC' THEN
                  PR_COBERT_ASEG.GENERAR_TRANSACCION (Z.IdeCobert, 'REVER');
                  BEGIN
                     UPDATE COBERT_ASEG
                     SET IndExcluir = NULL,
                         FecExc = NULL,
                         CodMotvExc = NULL,
                         TextMotvExc = NULL
                     WHERE  IdeCobert = Z.IdeCobert;
                  EXCEPTION
                     WHEN OTHERS THEN
                        RAISE_APPLICATION_ERROR (-20100, 'Error actualizando COBERT_ASEG: ' || SQLERRM);
                  END;
               END IF;
               BEGIN
                  SELECT StsAseg
                  INTO   cStsAseg
                  FROM   ASEGURADO
                  WHERE  IdeAseg = nIdeAseg;
               END;
               IF cStsAseg = 'EXC' THEN
                  PR_ASEGURADO.GENERAR_TRANSACCION (nIdeAseg, 'REVER');
                  UPDATE ASEGURADO
                  SET IndExcluir = NULL,
                      FecExc = NULL,
                      CodMotvExc = NULL,
                      TextMotvExc = NULL
                  WHERE  IdeAseg = nIdeAseg;
               END IF;
            ELSIF cOrigModCobert = 'B' THEN
               BEGIN
                  SELECT StsCobert, IdeBien
                  INTO   cStsCobert, nIdeBien
                  FROM   COBERT_BIEN
                  WHERE  IdeCobert = Z.IdeCobert;
               EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                     RAISE_APPLICATION_ERROR (-20100, 'No Existe el Z.IdeCobert 2 = ' || Z.IdeCobert);
               END;
               IF cStsCobert = 'EXC' THEN
                  PR_COBERT_BIEN.GENERAR_TRANSACCION (Z.IdeCobert, 'REVER');
                  UPDATE COBERT_BIEN
                  SET IndExcluir = NULL,
                      FecExc = NULL,
                      CodMotvExc = NULL,
                      TextMotvExc = NULL
                  WHERE  IdeCobert = Z.IdeCobert;
               END IF;
               BEGIN
                  SELECT StsBien
                  INTO   cStsBien
                  FROM   BIEN_CERT
                  WHERE  IdeBien = nIdeBien;
               EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                     RAISE_APPLICATION_ERROR (-20100, 'No Existe el Z.nIdeBien 2 = ' || nIdeBien);
               END;
               IF cStsBien = 'EXC' THEN
                  PR_BIEN_CERT.GENERAR_TRANSACCION (nIdeBien, 'REVER');
                  UPDATE BIEN_CERT
                  SET IndExcluir = NULL,
                      FecExc = NULL,
                      CodMotvExc = NULL,
                      TextMotvExc = NULL
                  WHERE  IdeBien = nIdeBien;
               END IF;
            END IF;
            BEGIN
               SELECT StsCertRamo, CodPlan, RevPlan
               INTO   cStsCertRamo, cCodPlan, cRevPlan
               FROM   CERT_RAMO
               WHERE  IdePol = nIdePol
               AND    NumCert = nNumCert
               AND    CodRamocert = cCodRamoCert;
            END;
            IF cStsCertRamo = 'EXC' THEN
               PR_CERT_RAMO.GENERAR_TRANSACCION (nIdePol, nNumCert, cCodPlan, cRevPlan, cCodRamoCert, 'REVER');
               UPDATE CERT_RAMO
               SET IndExcluir = NULL,
                   FecExc = NULL,
                   CodMotvExc = NULL,
                   TextMotvExc = NULL
               WHERE  IdePol = nIdePol
               AND    NumCert = nNumCert
               AND    CodRamocert = cCodRamoCert;
            END IF;
            BEGIN
               SELECT StsCert
               INTO   cStsCert
               FROM   CERTIFICADO
               WHERE  IdePol = nIdePol
               AND    NumCert = nNumCert;
            EXCEPTION
               WHEN NO_DATA_FOUND THEN
                  RAISE_APPLICATION_ERROR (-20100, 'No Existe el (nIdePol=nNumCert) = ' || nIdePol || ' = ' || nNumCert);
            END;
            IF cStsCert = 'EXC' THEN
               PR_CERTIFICADO.GENERAR_TRANSACCION (nIdePol, nNumCert, 'REVER');
               UPDATE CERTIFICADO
               SET IndExcluir = NULL,
                   FecExc = NULL,
                   CodMotvExc = NULL,
                   TextMotvExc = NULL
               WHERE  IdePol = nIdePol
               AND    NumCert = nNumCert;
            END IF;
            BEGIN
               SELECT NVL (MAX (NumMod), 0)
               INTO   nNumModRev
               FROM   MOD_COBERT
               WHERE  IdeCobert = Z.IdeCobert;
            EXCEPTION
               WHEN NO_DATA_FOUND THEN
                  RAISE_APPLICATION_ERROR (-20100, 'No se puede revertir la operacion (ER.3)!');
            END;
            UPDATE DEC_TRANSPORTE
            SET StsDec = 'INC'
            WHERE  IdeDec IN (SELECT IdeDec
                              FROM   DEC_TRANSPORTE_COBERT
                              WHERE  IdeCobert = Z.IdeCobert
                              AND    NumMod = nNumModRev);
            BEGIN
               SELECT MAX (NumMod) + 1
               INTO   nNumModNew
               FROM   MOD_COBERT
               WHERE  IdeCobert = Z.IdeCobert;
            END;
            INSERT INTO MOD_COBERT
                        (IdeCobert, NumMod, OrigModCobert, StsModCobert, Tasa, CodMoneda, SumaAsegMoneda, PrimaMoneda, FecIniValid,
                         FecFinValid, IndIncRen, IndPlazoEspera, FecIniPlazoEspera, FecFinPlazoEspera, IdePol, NumCert, CodRamoCert, IndSiniestro,
                         IndCobEsp, IndMov)
                 VALUES (Z.IdeCobert, nNumModNew, cOrigModCobert, 'VAL', nTasa, cCodMoneda, nSumaAsegMoneda, nPrimaMoneda, dFecIniValid,
                         dFecFinValid, cIndIncRen, cIndPlazoEspera, dFecIniPlazoEspera, dFecFinPlazoEspera, nIdePol, nNumCert, cCodRamoCert, 'N',
                         'N', 1);
            PR_MOD_COBERT.INCLUIR (Z.IdeCobert, nNumModNew);
         END LOOP;
         UPDATE CERTIFICADO
         SET IndExcluir = NULL,
             TipoAnul = NULL
         WHERE  IdePol = nIdePol
         AND    IndExcluir = 'R';
         PR_POLIZA.nNumOperRev  := nNumOper;
         nNumOperMov            := PR_POLIZA.ACTIVAR (nIdePol, 'D');
         PR_POLIZA.cOperRev     := 'EMI';
         UPDATE OPER_POL
         SET IndAnul = 'S',
             FecAnul = dFecSts,
             NumOperAnu = nNumOperMov
         WHERE  NumOper = nNumOper;
         --
         UPDATE RECIBO
         SET StsRec = 'REV'
         WHERE  NumOper = nNumOperMov;
         --
         UPDATE OPER_POL
         SET TipoOp = 'REV'
         WHERE  NumOper = nNumOperMov;
         UPDATE RECIBO SET TIPOOPE = 'REV'
         WHERE  NumOper = nNumOperMov;
         FOR Z IN MOV_PRIMA_A LOOP
            UPDATE MOD_ASEG
            SET StsModAseg = 'REV'
            WHERE  IdeAseg = Z.IdeAseg
            AND    NumMod = Z.NumMod;
         END LOOP;
         FOR Z IN MOV_PRIMA_B LOOP
            UPDATE MOD_BIEN_CERT
            SET StsModBien = 'REV'
            WHERE  IdeBien = Z.IdeBien
            AND    NumMod = Z.NumMod;
         END LOOP;
         FOR Z IN MOV_PRIMA_Q LOOP
            UPDATE MOD_COBERT
            SET StsModCobert = 'REV'
            WHERE  IdeCobert = Z.IdeCobert
            AND    NumMod = Z.NumMod;
         END LOOP;
         FOR Z IN MOV_PRIMA_B LOOP
            nIdeMovPrima  := Z.IdeMovPrima;
            nIdeBien      := Z.IdeBien;
            nNumCert      := Z.NumCert;
            BEGIN
               SELECT Codramocert, Codplan, revplan, clasebien, Codbien, mtovalbien, mtovalbienmoneda, Fecinivalid, Fecfinvalid, tipprimriesgo,
                      porcprimriesgo
               INTO   cCodRamoCert, cCodPlan, cRevPlan, cClaseBien, cCodbien, nMtoValBien, nMtoValBienMoneda, dFecIniValid, dFecFinValid,
                      cTipPrimRiesgo, nPorcPrimRiesgo
               FROM   MOD_BIEN_CERT
               WHERE  IdeBien = Z.IdeBien
               AND    NumMod = (Z.NumMod - 1);
            END;
            INSERT INTO MOD_BIEN_CERT
                        (idebien, IdePol, NumCert, Codramocert, Codplan, revplan, Nummod, clasebien, Codbien, stsmodbien, mtovalbien,
                         mtovalbienmoneda, Fecinivalid, Fecfinvalid, tipprimriesgo, porcprimriesgo, IdeMovPrima, IdeMovPrimat)
                 VALUES (Z.IdeBien, nIdePol, Z.NumCert, cCodRamoCert, cCodPlan, crevplan, Z.NumMod + 1, cClaseBien, cCodBien, 'VAL', nMtoValBien,
                         nMtoValBienMoneda, dFecIniValid, dFecfinvalid, cTipPrimRiesgo, nPorcPrimRiesgo, 0, 0);
            PR_MOD_BIEN_CERT.INCLUIR_REV_OPE (nIdeBien, Z.NumMod + 1);
         END LOOP;
         FOR Z IN MOV_PRIMA_A LOOP
            nIdeMovPrima  := Z.IdeMovPrima;
            nIdeAseg      := Z.IdeAseg;
            nNumCert      := Z.NumCert;
            BEGIN
               SELECT IdeAseg, Codramocert, Codplan, revplan, primamoneda, mtototrecamoneda, mtototdctomoneda, mtofactrecamoneda, mtofactdctomoneda,
                      CodMoneda, prima, mtofactreca, mtofactdcto, Fecinivalid, Fecfinvalid, mtosueldo, sumaaseg, SumaAsegMoneda, nrosalarios
               INTO   Z.IdeAseg, cCodRamoCert, cCodPlan, cRevPlan, nPrimaMoneda, nMtoTotRecaMoneda, nMtoTotDctoMoneda, nMtoFactRecaMoneda,
                      nMtoFactDctoMoneda, cCodMoneda, nPrima, nMtoFactReca, nMtoFactDcto, dFecIniValid, dFecFinValid, nMtoSueldo, nSumaAseg,
                      nSumaAsegMoneda, nNroSalarios
               FROM   MOD_ASEG
               WHERE  IdeAseg = Z.IdeAseg
               AND    NumMod = (Z.NumMod - 1);
            END;
            INSERT INTO MOD_ASEG
                        (IdePol, NumCert, Codramocert, IdeAseg, Codplan, revplan, Nummod, stsmodaseg, primamoneda, mtototrecamoneda,
                         mtototdctomoneda, mtofactrecamoneda, mtofactdctomoneda, CodMoneda, prima, mtofactreca, mtofactdcto, Fecinivalid,
                         Fecfinvalid, IdeMovPrima, IdeMovPrimat, mtosueldo, sumaaseg, SumaAsegMoneda, nrosalarios)
                 VALUES (nIdePol, Z.NumCert, cCodRamoCert, Z.IdeAseg, cCodPlan, crevplan, Z.NumMod + 1, 'VAL', nPrimaMoneda, nMtoTotRecaMoneda,
                         nMtoTotDctoMoneda, nMtoFactRecaMoneda, nMtoFactDctoMoneda, cCodMoneda, nPrima, nMtoFactReca, nMtoFactDcto, dFecIniValid,
                         dFecFinValid, 0, 0, nMtoSueldo, nSumaAseg, nSumaAsegMoneda, nNroSalarios);
            --
            PR_MOD_ASEG.INCLUIR_REV_OPE (nIdeAseg, Z.NumMod + 1);
         END LOOP;
         PR_OPERACION.TERMINAR;
      END REVERTIR_OPERACION;
   --
   BEGIN
      FOR X IN PROCESO LOOP
         nDias     := TRUNC (dFecBase) - TRUNC (X.Fecha);
         nVeces    := nDias;
         nDias     := nDias - 1;
         dFecProc  := TRUNC (dFecBase) - (nDias);
         --FOR Y IN 1 .. nVeces LOOP
         IF X.Periodicidad = 5 THEN   -- QUINCENAL
            IF TO_NUMBER (TO_CHAR (dFecProc, 'DD')) IN (15, TO_NUMBER (TO_CHAR (LAST_DAY (dFecBase), 'DD'))) THEN
               cEjecutar  := 'S';
            ELSE
               cEjecutar  := 'N';
            END IF;
         ELSIF X.Periodicidad = 4 THEN   -- MENSUAL
            IF TO_NUMBER (TO_CHAR (dFecProc, 'MM')) = TO_NUMBER (TO_CHAR (LAST_DAY (dFecBase), 'MM')) THEN
               cEjecutar  := 'S';
            ELSE
               cEjecutar  := 'N';
            END IF;
         ELSIF X.Periodicidad = 3 THEN   -- SEMANAL
            IF TO_CHAR (dFecProc, 'DAY') = 'VIERNES' THEN
               cEjecutar  := 'S';
            ELSE
               cEjecutar  := 'N';
            END IF;
         ELSIF X.Periodicidad = 2 THEN   -- DIARIA
            cEjecutar  := 'S';
         ELSIF X.Periodicidad = 1 THEN   -- SIEMPRE = DIARIA
            cEjecutar  := 'S';
         END IF;
         IF X.Param = 'PRORRAUT' THEN
            IF cEjecutar = 'S' THEN
               FOR P IN POL_ANU LOOP
                  IF P.IndCan = 'S' THEN
                     BEGIN
                        SELECT 1, AnoGenRescat
                        INTO   nIndPolVida, nAnoGenRescat
                        FROM   POLIZA POL, CERT_RAMO CR, ASEGURADO A, COBERT_ASEG CA, VAL_GAR_PLAN_VIDA V, CARACTERISTICA_PLAN_VIDA CPV
                        WHERE  POL.IdePol = P.IdePol
                        AND    CR.IdePol = POL.IdePol
                        AND    A.IdePol = POL.IdePol
                        AND    A.NumCert = CR.NumCert
                        AND    A.CodRamoCert = CR.CodRamoCert
                        AND    A.CodPlan = CR.CodPlan
                        AND    A.RevPlan = CR.RevPlan
                        AND    CA.IdeAseg = A.IdeAseg
                        AND    V.CodProd = POL.CodProd
                        AND    V.CodPlan = A.CodPlan
                        AND    V.RevPlan = A.RevPlan
                        AND    V.CodRamo = A.CodRamoCert
                        AND    V.CodCobert = CA.CodCobert
                        AND    CPV.CodProd = POL.CodProd
                        AND    CPV.CodPlan = A.CodPlan
                        AND    CPV.RevPlan = A.RevPlan
                        AND    CPV.CodRamo = A.CodRamoCert;
                     EXCEPTION
                        WHEN NO_DATA_FOUND THEN
                           nIndPolVida  := 0;
                        WHEN TOO_MANY_ROWS THEN
                           nIndPolVida  := 1;
                     END;
                     IF nIndPolVida = 1 THEN
                        cTodoCob  := 'S';
                        BEGIN
                           SELECT NumRen + 1
                           INTO   nNumRen
                           FROM   POLIZA
                           WHERE  IdePol = P.IdePol;
                        END;
                        IF nNumRen = nAnoGenRescat THEN
                           PR_SEGURO_SALDADO_PRORROGADO.FACTOR_FecHA (P.IdePol, P.FecIniVig, P.FecFinVig, nFactor, dFecMaxFinVigCob);
                           IF TRUNC (P.FecFinVig) != TRUNC (dFecMaxFinVigCob) THEN
                              cTodoCob  := 'N';
                           END IF;
                        END IF;
                        IF     nNumRen >= nAnoGenRescat
                           AND cTodoCob = 'S' THEN
                           PR_SEGURO_SALDADO_PRORROGADO.FACTOR_FecHA (P.IdePol, P.FecIniVig, P.FecFinVig, nFactor, dFecMaxFinVigCob);
                           PR_SEGURO_SALDADO_PRORROGADO.SALDAR_PRORROGAR (P.IdePol, dFecMaxFinVigCob, 'PRORROGAR', 'A', 0, NULL, NULL);
                           BEGIN
                              SELECT StsPol
                              INTO   nStsPolPro
                              FROM   POLIZA
                              WHERE  IdePol = P.IdePol;
                           EXCEPTION
                              WHEN NO_DATA_FOUND THEN
                                 RAISE_APPLICATION_ERROR (-20205, PR.MENSAJE ('ABD', 20205, TO_CHAR (P.IdePol), 'POLIZA', NULL));
                           END;
                           IF nStsPolPro = 'PRO' THEN
                              UPDATE T$_AnulA_POLIZA
                              SET IndCan = 'N',
                                  CodPROCESO = 'PRORROGADO'
                              WHERE  IdePol = P.IdePol;
                           END IF;
                        END IF;
                     END IF;
                  END IF;
               END LOOP;
            END IF;
         ELSIF X.Param = 'CANCAUT' THEN
            nAnulaPoliza  := 0;
            IF cEjecutar = 'S' THEN
               FOR POL IN POL_ANU LOOP
                  PR_POLIZA.CARGAR (POL.IdePol);
                  BEGIN
                     SELECT C.TipoId, C.NumId, C.DvId
                     INTO   cTipoId, nNumId, cDvId
                     FROM   POLIZA P, CLIENTE C
                     WHERE  P.IdePol = POL.IdePol
                     AND    C.CodCli = P.CodCli;
                  EXCEPTION
                     WHEN NO_DATA_FOUND THEN
                        RAISE_APPLICATION_ERROR (-20205, PR.MENSAJE ('ABD', 20205, cTipoId || '-' || nNumId || '-' || cDvId, 'CLIENTE', NULL));
                  END;
                  IF     POL.IndCan = 'S'
                     AND POL.CodProceso = 'SC' THEN
                     nMtoAcreMoneda  := OBTIENE_MONTO_ACREENCIA (POL.IdePol);
                     BEGIN
                        --EZ Se agrega al update claseanul para que anule la obligacion de la anulacion
                        UPDATE POLIZA
                        SET CodMotvAnul = '0001',
                            TextMotvAnul = 'Anulaci�n Masiva por Falta de Pago',
                            FecAnul = POL.FecAnul,
                            FecOper = POL.FecAnul,
                            TipoAnul = 'P',
                            ClaseAnul = 'TNP'
                        WHERE  IdePol = POL.IdePol;
                     END;
                     nNumOper        := PR_POLIZA.ANULAR (POL.IdePol, NULL, 'D', 0, 'N');
                     IF NVL (POL.NumRen, 0) = 0 THEN
                        nAnulaPoliza  := NVL (nAnulaPoliza, 0) + 1;
                        PR_CLIENTE.GENERAR_VETO (cTipoId, nNumId, cDvId, '27', 'Anulaci�n Masiva por Falta de Pago', PR_POLIZA.cCodOfiSusc);
                        --EZ Se comenta ya que en el proceso de anulacion no deberia generar
                        --facturas
                        --GENERAR_ACREENCIA (cTipoId, nNumId, cDvId, nNumOper, nMtoAcreMoneda);
                     END IF;
                  ELSIF     POL.IndCan = 'S'
                        AND POL.CodProceso = 'VC' THEN
                     BEGIN
                        --EZ Se agrega al update claseanul para que anule la obligacion de la anulacion
                        UPDATE POLIZA
                        SET CodMotvAnul = '0001',
                            TextMotvAnul = 'Anulaci�n Masiva por Falta de Pago',
                            FecAnul = POL.FecAnul,
                            FecOper = POL.FecAnul,
                            TipoAnul = 'P',
                            ClaseAnul = 'TNP'
                        WHERE  IdePol = POL.IdePol;
                     END;
                     nNumOper  := PR_POLIZA.ANULAR (POL.IdePol, NULL, 'D', 0, 'N');
                  ELSIF     POL.IndCan = 'S'
                        AND POL.CodProceso = 'EC' THEN
                     DECLARE
                        nNumOperPol   OPERACION.NumOper%TYPE;
                        nCobert       NUMBER (1)                 := 0;
                        nRamo         NUMBER (1)                 := 0;
                        nCert         NUMBER (1)                 := 0;
                        nIdeBien      BIEN_CERT.IdeBien%TYPE;
                        nIdeAseg      ASEGURADO.IdeAseg%TYPE;
                        cStsPol       POLIZA.StsPol%TYPE;
                        cStsOblig     OBLIGACION.StsOblig%TYPE;
                        CURSOR C_FACTVENC (nIdePol NUMBER) IS
                           SELECT DISTINCT O.NumOper
                           FROM   OPER_POL O, COND_FINANCIAMIENTO C, GIROS_FINANCIAMIENTO G, ACREENCIA A, FACTURA F
                           WHERE  O.IdePol = nIdePol
                           AND    O.TipoOp NOT IN ('ANU', 'REV')
                           AND    O.IndAnul = 'N'
                           AND    O.NumOper > (SELECT MIN (OP.NumOper)
                                               FROM   OPER_POL OP
                                               WHERE  OP.IdePol = O.IdePol
                                               AND    OP.TipoOp NOT IN ('ANU', 'REV'))
                           AND    C.NumOper = O.NumOper
                           AND    G.NumFinanc = C.NumFinanc
                           AND    A.NumAcre = G.NumAcre
                           AND    F.IdeFact = A.IdeFact
                           AND    F.StsFact = 'ACT'
                           AND    (F.FecVencFact + nDiasTram) < dFecBase;
                        CURSOR C_RECIBO (nNumOper NUMBER) IS
                           SELECT RE.IdePol, RE.NumCert, RE.CodRamoCert, MP.CodPlan, MP.RevPlan, MC.IdeCobert, MC.NumMod, MC.OrigModCobert
                           FROM   RECIBO RE, MOV_PRIMA MP, MOD_COBERT MC
                           WHERE  RE.NumOper = nNumOper
                           AND    MP.IdeMovPrima = RE.IdeMovPrima
                           AND    MC.IdeMovPrima = MP.IdeMovPrima;
                        CURSOR C_B$_COBERT (nNumOper NUMBER, nIdePol NUMBER, nNumCert NUMBER, cCodRamoCert VARCHAR2, nIdeCobert NUMBER, nNumMod NUMBER) IS
                           SELECT MAX (1)
                           FROM   B$_MOD_COBERT
                           WHERE  NumOper = nNumOper
                           AND    IdePol = nIdePol
                           AND    NumCert = nNumCert
                           AND    CodRamoCert = cCodRamoCert
                           AND    IdeCobert = nIdeCobert
                           AND    NumMod = nNumMod
                           AND    StsModcobert = 'INC';
                        CURSOR C_B$_BIEN (nIdeCobert NUMBER, nNumOper NUMBER) IS
                           SELECT MAX (1), MAX (CB.IdeBien)
                           FROM   B$_COBERT_BIEN CB, B$_BIEN_CERT BC
                           WHERE  CB.IdeCobert = nIdeCobert
                           AND    CB.NumOper = nNumOper
                           AND    CB.StsCobert = 'INC'
                           AND    BC.IdeBien = CB.IdeBien
                           AND    BC.NumOper = CB.NumOper
                           AND    BC.StsBien = 'INC';
                        CURSOR C_B$_ASEG (nIdeCobert NUMBER, nNumOper NUMBER) IS
                           SELECT MAX (1), MAX (CB.IdeAseg)
                           FROM   B$_COBERT_ASEG CB, B$_ASEGURADO BC
                           WHERE  CB.IdeCobert = nIdeCobert
                           AND    CB.NumOper = nNumOper
                           AND    CB.StsCobert = 'INC'
                           AND    BC.NumOper = CB.NumOper
                           AND    BC.IdeAseg = CB.IdeAseg
                           AND    BC.StsAseg = 'INC';
                        CURSOR C_B$_RAMO (nIdePol NUMBER, nNumCert NUMBER, cCodRamoCert VARCHAR2, nNumOper NUMBER) IS
                           SELECT MAX (1)
                           FROM   B$_CERT_RAMO
                           WHERE  IdePol = nIdePol
                           AND    NumCert = nNumCert
                           AND    CodRamoCert = cCodRamoCert
                           AND    NumOper = nNumOper
                           AND    StsCertRamo = 'INC';
                        CURSOR C_B$_CERT (nIdePol NUMBER, nNumCert NUMBER, nNumOper NUMBER) IS
                           SELECT MAX (1)
                           FROM   B$_CERTIFICADO
                           WHERE  IdePol = nIdePol
                           AND    NumCert = nNumCert
                           AND    NumOper = nNumOper
                           AND    StsCert = 'INC';
                        CURSOR C_OBLACR IS
                           SELECT R.NumOblig, R.NumAcre, F.IdeFact
                           FROM   REL_OBLACR_FRAC R, ACREENCIA A, FACTURA F
                           WHERE  R.NumOper = nNumOperPol
                           AND    A.NumAcre = R.NumAcre
                           AND    F.IdeFact = A.IdeFact
                           AND    EXISTS (SELECT 1
                                          FROM   OBLIGACION O
                                          WHERE  O.NumOblig = R.NumOblig
                                          AND    O.StsOblig = 'ACT')
                           AND    EXISTS (SELECT 1
                                          FROM   ACREENCIA A
                                          WHERE  A.NumAcre = R.NumAcre
                                          AND    A.StsAcre = 'ACT');
                     BEGIN
                        --EZ Se agrega al update claseanul para que anule la obligacion de la anulacion
                        UPDATE POLIZA
                        SET CodMotvAnul = '0001',
                            TextMotvAnul = 'Anulaci�n Masiva por Falta de Pago',
                            FecAnul = POL.FecAnul,
                            FecOper = POL.FecAnul,
                            TipoAnul = 'P',
                            ClaseAnul = 'TNP'
                        WHERE  IdePol = POL.IdePol;

                        BEGIN
                           nDiasTram  := TO_NUMBER (PR.BUSCA_PARAMETRO ('078'));
                        EXCEPTION
                           WHEN OTHERS THEN
                              RAISE_APPLICATION_ERROR (-20100, 'No se ha definido correctamente los Par�metros 079');
                        END;
                        FOR FV IN C_FACTVENC (POL.IdePol) LOOP
                           FOR RE IN C_RECIBO (FV.NumOper) LOOP
                              IF RE.NumMod > 1 THEN
                                 REVERTIR_OPERACION (POL.IdePol, FV.NumOper);
                              ELSE
                                 nCobert  := 0;
                                 nRamo    := 0;
                                 nCert    := 0;
                                 OPEN C_B$_COBERT (FV.NumOper, RE.IdePol, RE.NumCert, RE.CodRamoCert, RE.IdeCobert, RE.NumMod);
                                 FETCH  C_B$_COBERT
                                 INTO   nCobert;
                                 CLOSE C_B$_COBERT;
                                 IF NVL (nCobert, 0) = 1 THEN
                                    OPEN C_B$_RAMO (RE.IdePol, RE.NumCert, RE.CodRamoCert, FV.NumOper);
                                    FETCH  C_B$_RAMO
                                    INTO   nRamo;
                                    CLOSE C_B$_RAMO;
                                    IF NVL (nRamo, 0) = 1 THEN
                                       OPEN C_B$_CERT (RE.IdePol, RE.NumCert, FV.NumOper);
                                       FETCH  C_B$_CERT
                                       INTO   nCert;
                                       CLOSE C_B$_CERT;
                                    END IF;
                                    IF NVL (nCert, 0) = 1 THEN
                                       BEGIN
                                          UPDATE CERTIFICADO
                                          SET FecExc = POL.FecAnul,
                                              TipoAnul = 'P',
                                              CodMotvExc = '0004'
                                          WHERE  IdePol = RE.IdePol
                                          AND    NumCert = RE.NumCert;
                                       END;
                                       PR_CERTIFICADO.EXCLUIR (RE.IdePol, RE.NumCert);
                                    ELSIF NVL (nRamo, 0) = 1 THEN
                                       BEGIN
                                          UPDATE CERT_RAMO
                                          SET FecExc = POL.FecAnul,
                                              CodMotvExc = '0004'
                                          WHERE  IdePol = RE.IdePol
                                          AND    NumCert = RE.NumCert
                                          AND    CodPlan = RE.CodPlan
                                          AND    RevPlan = RE.RevPlan
                                          AND    CodRamoCert = RE.CodRamoCert;
                                       END;
                                       PR_CERT_RAMO.EXCLUIR (RE.IdePol, RE.NumCert, RE.CodPlan, RE.RevPlan, RE.CodRamoCert);
                                    ELSIF NVL (nCobert, 0) = 1 THEN
                                       IF RE.OrigModCobert = 'C' THEN
                                          BEGIN
                                             UPDATE COBERT_CERT
                                             SET FecExc = POL.FecAnul,
                                                 CodMotvExc = '0004'
                                             WHERE  IdeCobert = RE.IdeCobert;
                                          END;
                                          PR_COBERT_CERT.EXCLUIR (RE.IdeCobert);
                                       ELSIF RE.OrigModCobert = 'B' THEN
                                          OPEN C_B$_BIEN (RE.IdeCobert, FV.NumOper);
                                          FETCH  C_B$_BIEN
                                          INTO   nCobert, nIdeBien;
                                          CLOSE C_B$_BIEN;
                                          IF NVL (nCobert, 0) = 1 THEN
                                             BEGIN
                                                UPDATE BIEN_CERT
                                                SET FecExc = POL.FecAnul,
                                                    CodMotvExc = '0004'
                                                WHERE  IdeBien = nIdeBien;
                                             END;
                                             PR_BIEN_CERT.EXCLUIR (nIdeBien);
                                          ELSE
                                             BEGIN
                                                UPDATE COBERT_BIEN
                                                SET FecExc = POL.FecAnul,
                                                    CodMotvExc = '0004'
                                                WHERE  IdeCobert = RE.IdeCobert;
                                             END;
                                             PR_COBERT_BIEN.EXCLUIR (RE.IdeCobert);
                                          END IF;
                                       ELSIF RE.OrigModCobert = 'A' THEN
                                          OPEN C_B$_ASEG (RE.IdeCobert, FV.NumOper);
                                          FETCH  C_B$_ASEG
                                          INTO   nCobert, nIdeAseg;
                                          CLOSE C_B$_ASEG;
                                          IF NVL (nCobert, 0) = 1 THEN
                                             BEGIN
                                                UPDATE ASEGURADO
                                                SET FecExc = POL.FecAnul,
                                                    CodMotvExc = '0004'
                                                WHERE  IdeAseg = nIdeAseg;
                                             END;
                                             PR_ASEGURADO.EXCLUIR (nIdeAseg);
                                          ELSE
                                             BEGIN
                                                UPDATE COBERT_ASEG
                                                SET FecExc = POL.FecAnul,
                                                    CodMotvExc = '0004'
                                                WHERE  IdeCobert = RE.IdeCobert;
                                             END;
                                             PR_COBERT_ASEG.EXCLUIR (RE.IdeCobert);
                                          END IF;
                                       END IF;
                                    END IF;
                                 END IF;
                              END IF;
                              BEGIN
                                 SELECT StsPol
                                 INTO   cStsPol
                                 FROM   POLIZA
                                 WHERE  IdePol = POL.IdePol;
                              END;
                              IF cStsPol = 'MOD' THEN
                                 nNumOperPol  := PR_POLIZA.ACTIVAR (RE.IdePol, 'D');
                              END IF;
                              FOR O IN C_OBLACR LOOP
                                 BEGIN
                                    SELECT MAX (StsOblig)
                                    INTO   cStsOblig
                                    FROM   OBLIGACION
                                    WHERE  NumOblig = O.NumOblig;
                                 END;
                                 IF cStsOblig NOT IN ('ANU', 'VAL') THEN
                                    PR_OBLIGACION.ANULAR (O.NumOblig);
                                 END IF;
                                 BEGIN
                                    SELECT MAX (StsAcre)
                                    INTO   cStsOblig
                                    FROM   ACREENCIA
                                    WHERE  NumAcre = O.NumAcre;
                                 END;
                                 IF cStsOblig NOT IN ('ANU', 'VAL') THEN
                                    PR_ACREENCIA.ANULAR (O.NumAcre);
                                 END IF;
                                 BEGIN
                                    SELECT MAX (StsFact)
                                    INTO   cStsOblig
                                    FROM   FACTURA
                                    WHERE  IdeFact = O.IdeFact;
                                 END;
                                 IF cStsOblig NOT IN ('ANU', 'VAL') THEN
                                    PR_FACTURA.ANULAR (O.IdeFact);
                                 END IF;
                              END LOOP;
                           END LOOP;
                        END LOOP;
                     END;
                     nAnulaPoliza  := NVL (nAnulaPoliza, 0) + 1;
                     PR_CLIENTE.GENERAR_VETO (cTipoId, nNumId, cDvId, '27', 'Anulaci�n Masiva por Falta de Pago', PR_POLIZA.cCodOfiSusc);
                  END IF;
               END LOOP;
               -- Se eliminan los registros protegidos
               IF NVL (nAnulaPoliza, 0) > 0 THEN
                  BEGIN
                     SELECT RTRIM (USER, ' ')
                     INTO   cCodUsr
                     FROM   SYS.DUAL;
                  END;
                  /*                  BEGIN
                                       INSERT INTO HIST_PROT_POLIZA
                                          SELECT IdePol, IdeFact, cCodUsr, SYSDATE
                                          FROM   PROT_POLIZA;
                                    END; */
                  BEGIN
                     DELETE PROT_POLIZA
                     WHERE  (   NVL (INDANULTEM, 'N') = 'S'
                             OR NVL (IndAnulDef, 'N') = 'S')
                     AND    NVL (IndProv, 'N') = 'N';
                  END;
               END IF;
            END IF;
         END IF;
         nDias     := nDias - 1;
         dFecProc  := TRUNC (dFecBase) - (nDias);
         --END LOOP;
         UPDATE DEF_PERFIL
         SET Fecha = dFecProc
         WHERE  CodPerfil = '0002'
         AND    CodDefPerfil = X.CodDefPerfil;
      END LOOP;
   END;
   FUNCTION VALIDA_COBRO (nIdePol NUMBER, dFecAnul DATE)
      RETURN CHAR IS
      cFactCob        VARCHAR2 (1);
      dFecHastaGiro   DATE;
      nFrecuencia     NUMBER;
      CURSOR GIROS IS
         SELECT MAX (GF.NumFinanc) GNumFinanc, MAX (GF.NumGiro) GNumGiro, MAX (GF.FecVct) GFecVct, MAX (CF.CodPLAN) GCodPLAN,
                MAX (CF.MODPLAN) GMODPLAN
         FROM   GIROS_FINANCIAMIENTO GF, COND_FINANCIAMIENTO CF
         WHERE  CF.IdePol = nIdePol
         AND    GF.NumFinanc = CF.NumFinanc
         AND    GF.StsGiro = 'COB';
   BEGIN
      cFactCob  := 'N';
      FOR GIR IN GIROS LOOP
         IF GIR.GNumFinanc IS NOT NULL THEN
            BEGIN
               SELECT FecVct
               INTO   dFecHastaGiro
               FROM   GIROS_FINANCIAMIENTO
               WHERE  NumFinanc = GIR.GNumFinanc
               AND    NumGiro = GIR.GNumGiro + 1
               AND    StsGiro IN ('REF', 'ACT');
            EXCEPTION
               WHEN NO_DATA_FOUND THEN
                  BEGIN
                     SELECT MAX (PLA.Frecuencia)
                     INTO   nFrecuencia
                     FROM   PLAN_FINANCIAMIENTO PLA
                     WHERE  PLA.CodPlan = GIR.GCodPlan
                     AND    PLA.ModPlan = GIR.GModPlan;
                     dFecHastaGiro  := ADD_MONTHS (GIR.GFecVct, nFrecuencia);
                  EXCEPTION
                     WHEN NO_DATA_FOUND THEN
                        RAISE_APPLICATION_ERROR (-20100,
                                                 'NO EXISTE EL PLAN DE FINANCIAMIENTO: ' || GIR.GCodPlan || ' ' || GIR.GModPlan || ' ' || nIdePol);
                  END;
            END;
            IF TRUNC (dFecAnul) <= TRUNC (dFecHastaGiro) THEN
               cFactCob  := 'S';
            ELSE
               cFactCob  := 'N';
            END IF;
         END IF;
      END LOOP;
      RETURN (cFactCob);
   END;
   PROCEDURE VENCE_POLIZA (nIdePol NUMBER, dFecAnul DATE, cCodMotvAnul VARCHAR2, cTextMotvAnul VARCHAR2) IS
      nNumCert         NUMBER (10);
      nNumOper         NUMBER (14);
      nTotAnuPol       NUMBER (14, 2);
      nTotAnuCert      NUMBER (14, 2);
      cCodOfiemi       VARCHAR2 (6);
      nPolIFactura     NUMBER (14);
      cIndAdELSusc     VARCHAR2 (1);
      nIndPolVida      NUMBER (1);
      nMontoPrestamo   NUMBER (14, 2);
      cCodProd         POLIZA.CodProd%TYPE;
      nIdeAseg         ASEGURADO.IdeAseg%TYPE;
      cGenRetiro       VARCHAR2 (1);
      cTipoPlan        CARACTERISTICA_PLAN_VIDA.TipoPlan%TYPE;
      dFecSts          DATE;
      CURSOR C_CERTIFICADO IS
         SELECT NumCert
         FROM   CERTIFICADO
         WHERE  IdePol = nIdePol
         AND    StsCert IN ('ACT', 'REN');
   BEGIN
      PR_OPERACION.INICIAR ('VENCE', 'POLIZA', TO_CHAR (nIdePol));
      nNumOper  := PR_OPERACION.nNumOper;
      PR_POLIZA.CARGAR (nIdePol);
      cCodProd  := PR_POLIZA.cCodProd;
      --Erick Zoque 20/05/2013 E_CON_20120821_1_7
      dFecSts   := PR_FECHAS_EQUIVALENTES.FECHA_EMISION (SYSDATE, PR_POLIZA.cCodCia,'001');
      FOR X IN C_CERTIFICADO LOOP
         PR_CERTIFICADO.VENCE_CERT (nIdePol, X.NumCert);
         BEGIN
            --Erick Zoque 20/05/2013 E_CON_20120821_1_7
            IF dFecAnul < PR_FECHAS_EQUIVALENTES.FECHA_EMISION (SYSDATE, PR_POLIZA.cCodCia,'001') THEN
               PR_OPER_USUARIO.AUTORIZAR ('127', NULL, NULL, NULL, NULL, NULL, 0, NULL, cCodProd, 'VNC', dFecAnul);
            END IF;
            INSERT INTO OPER_POL
                        (TipoOp, IdePol, NumCert, NumOper, FecMov, MtoOper, IndAnul)
                 VALUES ('VNC', nIdePol, X.NumCert, nNumOper, dFecSts, 0, 'N');
         EXCEPTION
            WHEN OTHERS THEN
               RAISE_APPLICATION_ERROR (-20100, 'Error! InsertANDo Oper_Pol en VENCE_POLIZA.' || SQLERRM);
         END;
         UPDATE RECIBO SET TIPOOPE = 'VNC'
         WHERE  IdePol = nIdePol
         AND    NumCert = X.NumCert
         AND    NumOper = nNumOper;
      END LOOP;
      UPDATE POLIZA
      SET IndMovPolRen = 'N'
      WHERE  IdePol = nIdePol;
      PR_POLIZA.GENERAR_TRANSACCION (nIdePol, 'VENCE');
      BEGIN
         SELECT CodOfiemi
         INTO   cCodOfiemi
         FROM   POLIZA
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            RAISE_APPLICATION_ERROR (-20100, 'No Existe el Idepol = ' || nIdePol);
      END;
      IF cCodMotvAnul IS NOT NULL THEN
         BEGIN
            UPDATE POLIZA
            SET CodMotvAnul = cCodMotvAnul,
                TextMotvAnul = cTextMotvAnul,
                FecAnul = dFecAnul,
                FecOper = dFecAnul
            WHERE  IdePol = nIdePol;
         END;
      END IF;
      BEGIN
         SELECT 1, P.CodProd, P.CodOfiemi, A.IdeAseg, TipoPlan
         INTO   nIndPolVida, cCodProd, cCodOfiemi, nIdeAseg, cTipoPlan
         FROM   POLIZA P, CERT_RAMO CR, ASEGURADO A, COBERT_ASEG CA, VAL_GAR_PLAN_VIDA V, CARACTERISTICA_PLAN_VIDA CPP
         WHERE  P.IdePol = nIdePol
         AND    CR.IdePol = P.IdePol
         AND    A.IdePol = P.IdePol
         AND    A.NumCert = CR.NumCert
         AND    A.CodRamoCert = CR.CodRamoCert
         AND    A.CodPlan = CR.CodPlan
         AND    A.RevPlan = CR.RevPlan
         AND    CA.IdeAseg = A.IdeAseg
         AND    V.CodProd = P.CodProd
         AND    V.CodPlan = A.CodPlan
         AND    V.RevPlan = A.RevPlan
         AND    V.CodRamo = A.CodRamoCert
         AND    V.CodCobert = CA.CodCobert
         AND    CPP.CodProd = P.CodProd
         AND    CPP.CodPlan = A.CodPlan
         AND    CPP.RevPlan = A.RevPlan
         AND    CPP.CodRamo = A.CodRamoCert;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            nIndPolVida  := 0;
         WHEN TOO_MANY_ROWS THEN
            nIndPolVida  := 1;
      END;
--       Se elimina este proceso porque NO procede... de acuerdo a Test Director No. 5510
--       Conversado con Clara Galindo el 24/11/2005  - Edwin Cu�llar
      /*IF     nIndPolVida = 1
         AND cTipoPlan != 'TPC' THEN
         --  Generar la Liquidacion del fondo asociado a la poliza.
         BEGIN
            SELECT NVL (SUM (ValorMov), 0)
            INTO   nMontoPrestamo
            FROM   FONDO F, FONDO_POL FP
            WHERE  FP.IdePol = nIdePol
            AND    F.IdeFondo = FP.IdeFondo;
         EXCEPTION
            WHEN OTHERS THEN
               nMontoPrestamo  := 0;
         END;
         IF nMontoPrestamo > 0 THEN
            cGenRetiro  := PR_PRESTAMO_VIDA.GENERAR_RETIRO_TOTAL (cCodProd, nIdePol, nIdeAseg, nMontoPrestamo, cCodOfiemi, dFecAnul);
         END IF;
      END IF;*/
      PR_OPERACION.TERMINAR;
   END;
   FUNCTION CREA_POLIZA_BLO_LIQ (nIdePol POLIZA.IDEPOL%TYPE)
      RETURN NUMBER IS
      nExiste          NUMBER (1);
      nIndError        NUMBER (5);
      cIndAutorizado   POLIZA_LIQ_BLO.IndAutorizado%TYPE;
   BEGIN
      BEGIN
         SELECT 1, IndAutorizado
         INTO   nExiste, cIndAutorizado
         FROM   POLIZA_LIQ_BLO
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            nExiste  := 0;
         WHEN TOO_MANY_ROWS THEN
            nExiste  := 1;
      END;
      IF nExiste = 0 THEN
         INSERT INTO POLIZA_LIQ_BLO
                     (IdePol, FecSts, IndAutorizado, FecAutorizado, IndRechazada, FecRechazada)
              VALUES (nIdePol, SYSDATE, 'N', NULL, NULL, NULL);
         nIndError  := 50028;
      ELSE
         IF cIndAutorizado = 'S' THEN
            nIndError  := 0;   -- No hay error
         ELSE
            nIndError  := 50029;
         END IF;
      END IF;
      RETURN (nIndError);
   END;
   --//
   /*-----------------------------------------------------------------------------
       Nombre : PRORROGA
       Proposito : Extender las condiciones de Asegurabilidad de una P�liza,
                  ampliar la vigencia final de una p�liza y todos sus componentes
               (Certificados, bienes, coberturas)
       Parametros
         nIdePol     Identificativo Poliza
        dFecProVig  Fecha de fin de Vigencia Prorrogada de la Poliza
       Log de Cambios
         Fecha        Autor              Descripcion
         06-2004      Angelica Domenack  Creacion
     ----------------------------------------------------------------------------*/
PROCEDURE PRORROGA (nIdePol POLIZA.IdePol%TYPE, dFecProVig POLIZA.FecFinVig%TYPE) IS
  --Poliza
  dFecFinVig    POLIZA.FecFinVig%TYPE;
  cTipoPdcion   POLIZA.TipoPdcion%TYPE;
  cStsPol       POLIZA.StsPol%TYPE;
  nNumPol       POLIZA.NumPol%TYPE;
  cCodProd      POLIZA.CodProd%TYPE;
  cTipoVig      POLIZA.TipoVig%TYPE;
  --<<COTECSA *23718* FM 04/08/2018 CREACION DE VARIABLE PARA LA INSERCI�N DE LA NUEVA TABLA
  dFECINIOLD    POLIZA.FECINIVIG%TYPE;
  dFECFINOLD    POLIZA.FECFINVIG%TYPE;
  -->>COTECSA *23718* FM 04/08/2018 CREACION DE VARIABLE PARA LA INSERCI�N DE LA NUEVA TABLA
BEGIN
   --Erick Zoque 20/05/2013 E_CON_20120821_1_7
   PR_FECHAS_EQUIVALENTES.CEVENTO := '001';
   --Cargar Datos de la Poliza
   PR_POLIZA.CARGAR (nIdepol);
   dFecFinVig   := PR_POLIZA.dFecFinVig;
   cTipoPdcion  := PR_POLIZA.cTipoPdcion;
   cStsPol      := PR_POLIZA.cStsPol;
   nNumPol      := PR_POLIZA.nNumPol;
   cCodProd     := PR_POLIZA.cCodProd;
   cTipoVig     := PR_POLIZA.cTipoVig;
   --  IF dFecFinVig > dFecProVig THEN
   IF cStsPol = 'ACT' THEN
      --
      IF cTipoVig != 'D' THEN
         --
         SELECT FECINIVIG , FECFINVIG
         INTO   dFECINIOLD, dFECFINOLD
         FROM   POLIZA 
         WHERE  IDEPOL = nIdePol;
         --POLIZA
         UPDATE POLIZA
         SET    FecFinVig = dFecProVig
               ,FecRen    = dFecProVig
         WHERE  IdePol    = nIdepol;
         --
         UPDATE canal_poliza
         SET    fecfinvig = dFecProVig
         WHERE  IdePol    = nIdepol;
         --CERTIFICADO
         PR_CERTIFICADO.PRORROGA (nIdePol, dFecProVig);
         --ANEXO_POL
         PR_ANEXO_POL.PRORROGA (nIdePol, dFecProVig);
         --Distribucion de Coaseguro
         IF cTipoPdcion IN ('A', 'C') THEN
            PR_MOD_DIST_COA.PRORROGA (nIdepol, dFecProVig);
         END IF;
         --Recargos y Descuentos de Poliza
         PR_RECA_DCTO_POL.PRORROGA (nIdePol, dFecProVig);
         --<<COTECSA *23718* FM 04/08/2018 INSERCI�N DE LA NUEVA TABLA PARA QUEDAR REGISTRO DE LA PRORROGA Y AS� PODER DEVOLVER LO CORRESPONDIENTE
         INSERT INTO POLIZA_PRORROGA
               (IDEPOL    , FECINIOLD , FECFINOLD , FECININEW
               ,FECFINNEW , FECSTS    , NUMOPER)
         VALUES(nIdePol   , dFECINIOLD, dFECFINOLD, dFECINIOLD
               ,dFECPROVIG, SYSDATE   ,NULL); 
         -->>COTECSA *23718* FM 04/08/2018 INSERCI�N DE LA NUEVA TABLA PARA QUEDAR REGISTRO DE LA PRORROGA Y AS� PODER DEVOLVER LO CORRESPONDIENTE
      ELSE
         RAISE_APPLICATION_ERROR (-20349, PR.MENSAJE ('ABD', 20348, 'Poliza (' || cCodProd || '-' || TO_CHAR (nNumPol) || ')', NULL, NULL));
      END IF;
      --
   ELSE
      RAISE_APPLICATION_ERROR (-20348, PR.MENSAJE ('ABD', 20348, 'Poliza (' || cCodProd || '-' || TO_CHAR (nNumPol) || ')', NULL, NULL));
   END IF;
   --Erick Zoque 20/05/2013 E_CON_20120821_1_7
   PR_FECHAS_EQUIVALENTES.CEVENTO := NULL;
   --
END PRORROGA;
--
   PROCEDURE CASTIGO_CARTERA (nIdePol IN NUMBER, nIdefact IN NUMBER) IS
      --* PROCEDIMIENTO PARA CAMBIO DE ESTADO A NIVEL DE POLIZA
      --* DURANTE EL PROCESO DE CASTIGO DE CARTERA
      nContador   NUMBER (22)      := 0;
      nValida     NUMBER (22)      := 0;
      CURSOR c_Cert IS
         SELECT DISTINCT CF.Numcert
         FROM   ACREENCIA A, GIROS_FINANCIAMIENTO GF, COND_FINANCIAMIENTO CF
         WHERE  a.Idefact = nIdefact
         AND    gf.numacre = a.numacre
         AND    cf.numfinanc = gf.Numfinanc;
      cc1         c_Cert%ROWTYPE;
   BEGIN
      BEGIN
         SELECT COUNT (*)
         INTO   nValida
         FROM   CERTIFICADO
         WHERE  Idepol = nIdepol;
      END;
      OPEN c_Cert;
      LOOP
         FETCH  c_Cert
         INTO   cc1;
         EXIT WHEN c_Cert%NOTFOUND;
         PR_CERTIFICADO.CASTIGO_CARTERA (nIdepol, cc1.Numcert);
         nContador  := nContador + 1;
      END LOOP;
      CLOSE c_Cert;
      IF nContador = nValida THEN
         PR_POLIZA.GENERAR_TRANSACCION (nIdepol, 'CASTI');
      END IF;
   END;
   PROCEDURE REVERSO_CASTIGO (nIdePol IN NUMBER, nIdefact IN NUMBER) IS
      --* PROCEDIMIENTO PARA CAMBIO DE ESTADO A NIVEL DE POLIZA
      --* DURANTE EL PROCESO DE REVERSION DEL CASTIGO DE CARTERA
      nContador   NUMBER (22)      := 0;
      nValida     NUMBER (22)      := 0;
      CURSOR c_Cert IS
         SELECT DISTINCT CF.Numcert
         FROM   ACREENCIA A, GIROS_FINANCIAMIENTO GF, COND_FINANCIAMIENTO CF
         WHERE  a.Idefact = nIdefact
         AND    gf.numacre = a.numacre
         AND    cf.numfinanc = gf.Numfinanc;
      cc1         c_Cert%ROWTYPE;
   BEGIN
      BEGIN
         SELECT COUNT (*)
         INTO   nValida
         FROM   CERTIFICADO
         WHERE  Idepol = nIdepol;
      END;
      OPEN c_Cert;
      LOOP
         FETCH  c_Cert
         INTO   cc1;
         EXIT WHEN c_Cert%NOTFOUND;
         PR_CERTIFICADO.REVERSO_CASTIGO (nIdepol, cc1.Numcert);
         nContador  := nContador + 1;
      END LOOP;
      CLOSE c_Cert;
      IF nContador = nValida THEN
         PR_POLIZA.GENERAR_TRANSACCION (nIdepol, 'REVC');
      END IF;
   END;
   FUNCTION CALC_CORTO_PLAZO (
      nPorcCtp        POLIZA.PorcCtp%TYPE,
      cIndTipoPro     POLIZA.IndTipoPro%TYPE,
      dFecIniVig      POLIZA.FecIniVig%TYPE,
      dFecFinVig      POLIZA.FecFinVig%TYPE)
      RETURN NUMBER IS
      nDiasAno     NUMBER (4);
      nDiasPro     NUMBER (4);
      nDiasNoCub   NUMBER (4);
      nPorcNoCub   POLIZA.PorcCtp%TYPE;
      nPorcCub     POLIZA.PorcCtp%TYPE;
      nPorcAplic   POLIZA.PorcCtp%TYPE;
   BEGIN
      IF cIndTipoPro = 4 THEN
         nDiasAno    := ADD_MONTHS (dFecIniVig, 12) - dFecIniVig;
         nDiasPro    := dFecFinVig - dFecIniVig;
         nDiasNoCub  := nDiasAno - nDiasPro;
         nPorcNoCub  := 100 / nDiasAno * nDiasNoCub;
         nPorcCub    := 100 - nPorcNoCub;
         IF nPorcCtp = 0 THEN
            nPorcAplic  := nPorcCub;
         ELSE
            nPorcAplic  := nPorcCub + (nPorcNoCub * nPorcCtp / 100);
         END IF;
      ELSIF cIndTipoPro = 3 THEN
         nPorcAplic  := nPorcCtp;
      ELSE
         nPorcAplic  := 100;
      END IF;
      RETURN (nPorcAplic);
   END;
   --
   PROCEDURE DECLINAR (nIdePol IN NUMBER) IS
      CURSOR c_Cert IS
         SELECT NUMCERT
         FROM   CERTIFICADO
         WHERE  IdePol = nIdePol;
      cc1   c_Cert%ROWTYPE;
   BEGIN
      OPEN c_Cert;
      LOOP
         FETCH  c_Cert
         INTO   cc1;
         EXIT WHEN c_Cert%NOTFOUND;
         PR_CERTIFICADO.DECLINAR (nIdepol, cc1.Numcert);
      END LOOP;
      CLOSE c_Cert;
      PR_ANEXO_POL.DECLINAR (nIdePol);
      PR_POLIZA.GENERAR_TRANSACCION (nIdepol, 'DECLI');
   END;
   --
   PROCEDURE DECLINAR_REVERSO (nIdePol IN NUMBER) IS
      CURSOR c_Cert IS
         SELECT NUMCERT
         FROM   CERTIFICADO
         WHERE  IdePol = nIdePol;
      cc1   c_Cert%ROWTYPE;
   BEGIN
      OPEN c_Cert;
      LOOP
         FETCH  c_Cert
         INTO   cc1;
         EXIT WHEN c_Cert%NOTFOUND;
         PR_CERTIFICADO.DECLINAR_REVERSO (nIdepol, cc1.Numcert);
      END LOOP;
      CLOSE c_Cert;
      PR_ANEXO_POL.DECLINAR_REVERSO (nIdePol);
      PR_POLIZA.GENERAR_TRANSACCION (nIdepol, 'REVD');
   END;
   PROCEDURE FACT_COMPLEMENTO_OBL (nIdePol IN NUMBER, nNumCert IN NUMBER, nNumOper IN NUMBER) IS
      --
      cCodFinanc            COND_FINANCIAMIENTO.CodFinanc%TYPE         := NULL;
      nNumFinanc            COND_FINANCIAMIENTO.NumFinanc%TYPE         := NULL;
      cCodPlanFracc         RESP_PAGO_MOV.CodPlanFracc%TYPE            := NULL;
      cNumModPlanFracc      RESP_PAGO_MOV.NumModPlanFracc%TYPE         := NULL;
      nPorIni               PLAN_FINANCIAMIENTO.PorIni%TYPE            := NULL;
      nNroGiros             PLAN_FINANCIAMIENTO.NroGiros%TYPE          := NULL;
      nMtoNetoFrac          COND_FINANCIAMIENTO.MtoNetoFrac%TYPE       := NULL;
      nMtoNetoCom           COND_FINANCIAMIENTO.MtoNetoCom%TYPE        := NULL;
      nMtoNetoComOrig       COND_FINANCIAMIENTO.MtoNetoComOrig%TYPE    := NULL;
      nMtoOper              OPER_POL.MtoOper%TYPE                      := NULL;
      dFecMov               OPER_POL.FecMov%TYPE                       := NULL;
      cTipoOp               OPER_POL.TipoOp%TYPE                       := NULL;
      nMtoNetoFracAnual     COND_FINANCIAMIENTO.MtoNetoFrac%TYPE       := NULL;
      nMtoNetoFracPeriodo   COND_FINANCIAMIENTO.MtoNetoFrac%TYPE       := NULL;
      nMtoNetoComAnual      COND_FINANCIAMIENTO.MtoNetoCom%TYPE        := NULL;
      nMtoNetoComPeriodo    COND_FINANCIAMIENTO.MtoNetoCom%TYPE        := NULL;
      nMtoGiroForaneo       GIROS_FINANCIAMIENTO.MtoGiroForaneo%TYPE   := NULL;
      nMtoTotGiroForaneo    GIROS_FINANCIAMIENTO.MtoGiroForaneo%TYPE   := NULL;
      nMtoComGiro           GIROS_FINANCIAMIENTO.MtoComGiro%TYPE       := NULL;
      nMtoTotComGiro        GIROS_FINANCIAMIENTO.MtoComGiro%TYPE       := NULL;
      nNumPreOblig          PRE_OBLIG.NumPreOblig%TYPE                 := NULL;
      nNumGiro              GIROS_FINANCIAMIENTO.NumGiro%TYPE          := NULL;
      nNumDetPreOblig       DET_PRE_OBLIG.NumDetPreoblig%TYPE          := NULL;
      nMtoObligacion        OBLIGACION.MtoNetoObligMoneda%TYPE         := NULL;
      nNumOblig             OBLIGACION.NumOblig%TYPE                   := NULL;
      cCodInter             PART_INTER_MOV.CodInter%TYPE               := NULL;
      nPORCX                REC_FINANCIAMIENTO.PorcRec%TYPE            := NULL;
      cTipo                 OPER_POL.TipoOp%TYPE                       := NULL;
      cTipoOpOrig           OPER_POL.TipoOp%TYPE                       := NULL;
      nIdeRecCont           RECIBO.IdeRec%TYPE                         := NULL;
      nNumObligDef          OBLIGACION.NumObligDef%TYPE                := NULL;
      cCodCia               OBLIGACION.CodCia%TYPE                     := NULL;
      cOblTipoId            OBLIGACION.tipoid%TYPE                     := NULL;
      nOblNumId             OBLIGACION.Numid%TYPE                      := NULL;
      cOblDvId              OBLIGACION.Dvid%TYPE                       := NULL;
      cStsGiro              OBLIGACION.sTsOblig%TYPE                   := NULL;
      nMtoOperAnuPro        OPER_POL.MtoOperAnuPro%TYPE                := NULL;
      nMtoAnu               OPER_POL.MtoOperAnuPro%TYPE                := NULL;
      dFecVctIniRec         DATE                                       := NULL;
      dFecVctFinRec         DATE                                       := NULL;
      dFecVencGiro          DATE                                       := NULL;
      dFecha                DATE                                       := NULL;
      cExclusiones          VARCHAR2 (1)                               := NULL;
      cExisteOblRel         VARCHAR2 (1)                               := NULL;
      cIndContVig           VARCHAR2 (1)                               := NULL;
      cExisteRel            VARCHAR2 (1)                               := NULL;
      cStsCertif            VARCHAR2 (3)                               := NULL;
      nCantFinanc           NUMBER (10)                                := 0;
      nCantFinancAux        NUMBER (10)                                := 0;
      nTotMtoNetoFrac       NUMBER (22, 2)                             := 0;
      nTotMtoNetoCom        NUMBER (22, 2)                             := 0;
      nTotMtoNetoComOrig    NUMBER (22, 2)                             := 0;
      nMtoOperCom           NUMBER (22, 2)                             := 0;
      nMtoOperComOrig       NUMBER (22, 2)                             := 0;
      nCuenta               NUMBER (1)                                 := 0;
      nMtoDetObligLocal     NUMBER (22, 2)                             := 0;
      cIndPolDeclarativa    VARCHAR2 (1)                               := NULL;
      nNrGiCo               NUMBER (10)                                := NULL;   -- Numero de giros que completan el periodo de fechas completamente
      NGI                   NUMBER (3)                                 := NULL;   -- Indice usado en los cursores para llevar el conteo y numero de los giros
      nPorCuo               NUMBER                                     := NULL;
      nPorcRec              NUMBER                                     := NULL;
      nPorcComRec           NUMBER                                     := NULL;
      nMto                  NUMBER (22, 2)                             := NULL;
      nDias                 NUMBER (14)                                := NULL;
      nExisteExc            NUMBER (1)                                 := NULL;
      -- Mod 10/03/2005
      cCodClaEgre           CPTO_EGRE.CodClaEgre%TYPE                  := NULL;
      cCodCptoEgre          CPTO_EGRE.CodCptoEgre%TYPE                 := NULL;
      cCodEvento            CPTO_CONFIG.CodEvento%TYPE                 := NULL;
      cTipoBenef            CPTO_CONFIG.CodBenefSin%TYPE               := NULL;
      cTipoOblig            CPTO_CONFIG.TipoMovimiento%TYPE            := NULL;
      nIdeMovimiento        CPTO_CONFIG.IdeMovimiento%TYPE             := NULL;
      nNumDetOblig          DET_OBLIG_RAMO.NumDetOblig%TYPE            := NULL;
      cCodBenefSin          CPTO_CONFIG.CodBenefSin%TYPE               := NULL;
      cCodRefPago           OBLIGACION.CodRefPago%TYPE                 := NULL;
      cCodRol               TIPO_ROL.CodRol%TYPE;
      cIndImpuesto          IMPUESTO_RAMO.indimpuesto%TYPE;
      cCodRamo              RAMO.CodRamo%TYPE;
      cCodAreaSeguro        RAMO.CodAreaSeguros%TYPE;
      --
      -- Mod 10/03/2005
      CURSOR C_DET_OBLIG_RAMO IS
         SELECT *
         FROM   DET_OBLIG_RAMO
         WHERE  NumOblig = nNumOblig
         AND    NumDetOblig = nNumDetOblig;
      --
      --
      CURSOR C_FINAN IS
         SELECT NumFinanc
         FROM   COND_FINANCIAMIENTO
         WHERE  Numoper = nNumOper
         AND    Idepol = nIdePol
         AND    Numcert = nNumCert;
      CURSOR C_OBLIG_RAMO IS   -- Contabilidad
         SELECT DISTINCT (CODRAMO) CodRamoObl
         FROM   DET_OBLIG_RAMO
         WHERE  NumOblig = nNumOblig;
      CURSOR OBLIGACIONES IS
         SELECT GIR.NumGiro, GIR.FecVct, GIR.MtoGiroForaneo, GIR.MtoComGiro, GIR.PorcGiro, FIN.TipoId, FIN.NumId, FIN.DvId, GIR.Fecsts,
                DET.MtoDetGiroMoneda, DET.MtoComDetGiro, GIR.NumFinanc
         FROM   GIROS_FINANCIAMIENTO GIR, COND_FINANCIAMIENTO FIN, DET_GIRO_FIN DET
         WHERE  FIN.NumFinanc = GIR.NumFinanc
         AND    GIR.NumFinanc = DET.NumFinanc
         AND    GIR.NumGiro = DET.NumGiro
         AND    FIN.NumFinanc = nNumFinanc
         AND    DET.CodGrupoAcre = 'PRIMAS'
         AND    DET.CodCptoAcre = 'RECIBO'
         ORDER BY GIR.NumGiro;
      --
      CURSOR RESPONSABLES_PAGO IS
         SELECT RES.CodCli, RES.PorcPago, RES.NumDoc, RES.CodPlanFracc, RES.NumModPlanFracc, TER.TipoId, TER.NumId, TER.DvId, PLA.Porini,
                PLA.NroGiros, PLA.Frecuencia, RES.CodViaCobro
         FROM   RESP_PAGO_MOV RES, CLIENTE CLI, TERCERO TER, PLAN_FINANCIAMIENTO PLA
         WHERE  RES.CodCli = CLI.CodCli
         AND    CLI.TipoId = TER.TipoId
         AND    CLI.NumId = TER.NumId
         AND    CLI.DvId = TER.DvId
         AND    RES.CodPlanFracc = PLA.CodPlan
         AND    RES.NumModPlanFracc = PLA.ModPlan
         AND    RES.IdePol = nIdePol
         AND    RES.NumCert = nNumCert
         AND    RES.NumOper = nNumOper;
      CURSOR DETALLE_OBLIGACIONES IS
         SELECT DECODE (DET.CodGrupoAcre, 'PRIMAS', cCodClaEgre, DET.CodGrupoAcre) CodGrupoAcre,
                DECODE (DET.CodCptoAcre, 'RECIBO', cCodCptoEgre, DET.CodCptoAcre) CodCptoAcre, DET.MtoDetGiroMoneda, DET.MtoComdetGiro, DET.NumGiro,
                DET.NumDetGiro, CPT.NatCptoAcre
         FROM   DET_GIRO_FIN DET, CPTO_ACRE CPT
         WHERE  DET.CodGrupoAcre = CPT.CodGrupoAcre
         AND    DET.CodCptoAcre = CPT.CodCptoAcre
         AND    DET.NumFinanc = nNumFinanc
         AND    DET.NumGiro = nNumGiro
         AND    DECODE (cTipoOp, 'ANU', 'S', CPT.IndDevolOblig) = 'S'
         ORDER BY DET.NumDetGiro;
   --
   BEGIN
      cCodCia  := PR_POLIZA.cCodcia;
      --Erick Zoque 20/05/2013 E_CON_20120821_1_7
      dFecMov  := PR_FECHAS_EQUIVALENTES.FECHA_EMISION (SYSDATE, cCodCia,'001');
      FOR FIN IN C_FINAN LOOP
         FOR RES IN RESPONSABLES_PAGO LOOP
            nNumFinanc        := FIN.NumFinanc;
            cCodPlanFracc     := RES.CodPlanFracc;
            cNumModPlanFracc  := RES.NumModPlanFracc;
            nCantFinancAux    := (NVL (nCantFinancAux, 0) + 1);
            nPorIni           := RES.PorIni;
            nNroGiros         := RES.NroGiros;
            --
            BEGIN
               BEGIN
                  SELECT MAX (CodInter), MAX (CodRamoCert)
                  INTO   cCodInter, cCodRamo
                  FROM   RECIBO REC, PART_INTER_MOV PAR
                  WHERE  REC.IdeMovPrima = PAR.IdeMovPrima
                  AND    REC.IdePol = nIdePol
                  AND    REC.Numcert = nNumcert
                  AND    REC.NumOper = nNumOper
                  AND    PAR.IndLider = 'S';
               EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                     cCodInter  := NULL;
               END;
               BEGIN
                  SELECT CodAreaSeguros
                  INTO   cCodAreaSeguro
                  FROM   RAMO
                  WHERE  CodRamo = cCodRamo;
               EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                     RAISE_APPLICATION_ERROR (-20205, PR.MENSAJE ('ABD', 20205, 'CodRamo: ' || cCodRamo, 'RAMO', NULL));
               END;
               FOR OBL IN OBLIGACIONES LOOP
                  IF NVL (PR_FINANCIACION_INTERFACE.cObligInterface, 'N') = 'S' THEN
                     cOblTipoid  := PR_FINANCIACION_INTERFACE.cFinanTipoId;
                     nOblNumid   := PR_FINANCIACION_INTERFACE.nFinanNumId;
                     cOblDvid    := PR_FINANCIACION_INTERFACE.cFinanDvId;
                  ELSE
                     cOblTipoid  := OBL.TipoId;
                     nOblNumid   := OBL.NumId;
                     cOblDvid    := OBL.DvId;
                  END IF;
                  --
                  nNumGiro         := OBL.NumGiro;
                  nNumOblig        := PR_OBLIGACION.NUMERO;
                  nNumObligDef     := PR_VINISUSC.CORRELATIVO_CIA (cCodCia, 'O', NULL);
                  PR_VINISUSC.UPD_CORRELATIVO_CIA (cCodCia, 'O', NULL);
                  -- Mod 10/03/2005
                  cCodEvento       := PR_FRACCIONAMIENTO.DEVOLU_EVENTO (nIdePol, cOblTipoid, nOblNumid, cOblDvid);
                  -- Mod 10/03/2005
                  BEGIN
                     SELECT CF.CodBenefSin, CF.TipoMovimiento, CF.IdeMovimiento, CF.CodBenefSin
                     INTO   cTipoBenef, cTipoOblig, nIdeMovimiento, cCodBenefSin
                     FROM   CPTO_CONFIG CF
                     WHERE  CF.CodEvento = cCodEvento
                     AND    CF.CodMovimiento = 'O';
                  EXCEPTION
                     WHEN NO_DATA_FOUND THEN
                        RAISE_APPLICATION_ERROR (-20205, PR.MENSAJE ('ABD', 20205, 'CodEvento: ' || cCodEvento, 'CPTO_CONFIG', NULL));
                  END;
                  -- Mod 10/03/2005
                  BEGIN
                     SELECT CCD.CODCLASE, CCD.CODCPTO
                     INTO   cCodClaEgre, cCodCptoEgre
                     FROM   CPTO_CONFIG_DET CCD
                     WHERE  CCD.IdeMovimiento = nIdeMovimiento;
                  EXCEPTION
                     WHEN NO_DATA_FOUND OR TOO_MANY_ROWS THEN
                        cCodClaEgre   := NULL;
                        cCodCptoEgre  := NULL;
                  END;
                  -- Mod 10/03/2005
                  cCodRefPago      := PR_PAGOS.OBTIENE_REFPAGO (cOblTipoid, nOblNumid, cOblDvid, cCodCia, cTipoBenef, 'PRI', cCodAreaSeguro, cCodRol);
                  --


                  BEGIN
                     INSERT INTO OBLIGACION
                                 (StsOblig, FecSts, CodMoneda, MtoNetoObligLocal, MtoNetoObligMoneda, MtoBrutoObligLocal, MtoBrutoObligMoneda,
                                  FecGtiaPago, DptoEmi, SldoObligLocal, SldoObligMoneda, TipoId, NumId, DvId, NumOblig, TipoOblig, CodInterLider,
                                  NumOper, TextOblig,
                                  CodCia, NumObligDef, TipoBenef, CodRol, CodRefPago, CodViaCobro,
                                  IndNoGirarNoCruzar)
                          VALUES ('INC', dFecMov, PR_POLIZA.cCodMoneda, 0, 0, 0, 0,
                                  OBL.FecVct, PR_POLIZA.cCodOfiSusc, 0, 0, cOblTipoid, nOblNumid, cOblDvid, nNumOblig, cTipoOblig, cCodInter,
                                  nNumOper, 'DEVOLUCION / DISMINUCION DE PRIMA - POLIZA: ' || PR_POLIZA.cCodProd || '-' || PR_POLIZA.nNumPol,
                                  cCodCia, nNumObligDef, cTipoBenef, cCodRol, cCodRefPago, RES.CodViaCobro,
                                  PR_VIA_COBRO.INDICADOR_VIA_COBRO (RES.CodViaCobro, 'NGC'));
                  EXCEPTION
                     WHEN OTHERS THEN
                        RAISE_APPLICATION_ERROR (-20100, PR.MENSAJE ('ABD', 20289, 'OBLIGACION' || SQLERRM, NULL, NULL));
                  END;
                  nNumPreOblig     := PR_PRE_OBLIG.NUMERO;
                  BEGIN
                     INSERT INTO PRE_OBLIG
                                 (NumPreOblig, IdePol, NumCert, NumOper, TipoId, NumId, DvId, CodMoneda,
                                  MtoPreOblig, NumOblig)
                          VALUES (nNumPreOblig, nIdePol, nNumCert, nNumOper, OBL.TipoId, OBL.NumId, OBL.DvId, PR_POLIZA.cCodMoneda,
                                  ABS (OBL.MtoDetGiroMoneda), nNumOblig);
                  EXCEPTION
                     WHEN OTHERS THEN
                        RAISE_APPLICATION_ERROR (-20100, PR.MENSAJE ('ABD', 20289, 'PRE_OBLIG' || SQLERRM, NULL, NULL));
                  END;
                  BEGIN
                     INSERT INTO REC_PRE_OBLIG
                                 (NumPreOblig, IdeRec, PorcRec, PorcComRec)
                        (SELECT nNumPreOblig, RFI.IdeRec, RFI.PorcRec, RFI.PorcComRec
                         FROM   REC_FINANCIAMIENTO RFI
                         WHERE  RFI.NumFinanc = nNumFinanc);
                  EXCEPTION
                     WHEN OTHERS THEN
                        RAISE_APPLICATION_ERROR (-20100, PR.MENSAJE ('ABD', 20289, 'REC_PRE_OBLIG' || SQLERRM, NULL, NULL));
                  END;
                  nNumDetPreOblig  := 1;
                  nMtoObligacion   := 0;
                  cTipoOpOrig      := cTipoOp;
                  IF NVL (nExisteExc, 0) = 1 THEN
                     cTipoOp  := 'ANU';
                  END IF;
                  --
                  FOR DET IN DETALLE_OBLIGACIONES LOOP
                     BEGIN   -- CAMBIO A PRUEBA MANUEL GONZALEZ
                        SELECT Fecsts, StsGiro
                        INTO   dFecha, cStsGiro
                        FROM   GIROS_FINANCIAMIENTO
                        WHERE  NumFinanc = nNumFinanc
                        AND    NumGiro = nNumGiro;
                     EXCEPTION
                        WHEN OTHERS THEN
                           RAISE_APPLICATION_ERROR (-20100, PR.MENSAJE ('ABD', 20205, 'BUSQUEDA DE FECHA DEL GIRO', 'GIROS_FINANCIAMIENTO', NULL));
                     END;
                     IF cStsGiro = 'COB' THEN
                        dFecha  := dFecMov;
                     END IF;
                     nMtoDetObligLocal  :=
                        PR_TASA_CAMBIO.CAMBIO_MONEDA (PR_VINISUSC.cCodCia,
                                                      PR_POLIZA.cCodMoneda,
                                                      dFecha,
                                                      ABS (NVL (DET.MtoDetGiroMoneda, 0)),
                                                      PR_POLIZA.nIdepol);
                     --
                     BEGIN
                        INSERT INTO DET_PRE_OBLIG
                                    (NumPreOblig, NumDetPreOblig, CodClaEgre, CodCptoEgre, MtoDetObligLocal,
                                     MtoDetObligMoneda)
                             VALUES (nNumPreOblig, nNumDetPreoblig, DET.CodGrupoAcre, DET.CodCptoAcre, ABS (nMtoDetObligLocal),
                                     ABS (NVL (DET.MtoDetGiroMoneda, 0)));
                     EXCEPTION
                        WHEN OTHERS THEN
                           RAISE_APPLICATION_ERROR (-20100, PR.MENSAJE ('ABD', 20289, 'DET_PRE_OBLIG' || SQLERRM, NULL, NULL));
                     END;
                     nNumDetPreOblig    := ABS ((NVL (nNumDetPreOblig, 0) + 1));
                     nMtoObligacion     := ABS ((NVL (nMtoObligacion, 0) + DET.MtoDetGiroMoneda));
                     BEGIN
                        INSERT INTO DET_OBLIG
                                    (NumOblig, NumDetOblig, CodClaEgre, CodCptoEgre, CodMoneda, MtoDetObligLocal,
                                     MtoDetObligMoneda, NatCptoEgre, PorcCptoEgre, MtoCptoEgre)
                             VALUES (nNumOblig, DET.NumDetGiro, DET.CodGrupoAcre, DET.CodCptoAcre, PR_POLIZA.cCodMoneda, ABS (nMtoDetObligLocal),
                                     ABS (NVL (DET.MtoDetGiroMoneda, 0)), NVL (DET.NatCptoAcre, 0), 100, 0);
                     EXCEPTION
                        WHEN OTHERS THEN
                           RAISE_APPLICATION_ERROR (-20100, PR.MENSAJE ('ABD', 20289, 'DET_OBLIG' || SQLERRM, NULL, NULL));
                     END;
                     -- Mod 10/03/2005
                     INSERT INTO DET_OBLIG_RAMO
                                 (NumOblig, NumDetOblig, CodClaEgre, CodCptoEgre, CodRamo, MtoDetObligRamo, MtoComDetObligRamo)
                        (SELECT nNumOblig, DET.NumDetGiro, DECODE (CodGrupoAcre, 'PRIMAS', cCodClaEgre, CodGrupoAcre),
                                DECODE (CodCptoAcre, 'RECIBO', cCodCptoEgre, CodCptoAcre), CodRamo, NVL (MtoDetGiroRamo, 0),
                                NVL (MtoComDetGiroRamo, 0)
                         FROM   DET_GIRO_FIN_RAMO
                         WHERE  NumFinanc = nNumFinanc
                         AND    NumGiro = DET.NumGiro
                         AND    NumdetGiro = DET.NumDetGiro);
                     --
                     -- Mod 10/03/2005
                     nNumDetOblig       := DET.NumDetGiro;
                     FOR Y IN C_DET_OBLIG_RAMO LOOP
                        cIndImpuesto  :=
                           PR_PAGOS.VALIDA_REFPAGO_DET_OBLIG (cOblTipoid,
                                                              nOblNumid,
                                                              cOblDvid,
                                                              cCodCia,
                                                              cCodRol,
                                                              Y.CodRamo,
                                                              Y.CodClaEgre,
                                                              Y.CodCptoEgre,
                                                              cCodRefPago);
                        UPDATE DET_OBLIG
                        SET IndImpuesto = cIndImpuesto
                        WHERE  NumOblig = nNumOblig;
                     END LOOP;
                  END LOOP;
                  cTipoOp          := cTipoOpOrig;
                  -- Actualiza en Obligacion.
                  PR_DET_OBLIG.ACT_MTO_OBLIG (nNumOblig);
                  -- Mod 11/03/2005 -- Validacion de Activacion.
                  IF nNumOblig IS NOT NULL THEN
                     PR_OBLIGACION.EVALUA_OBLIG (nNumOblig);
                  END IF;
                  -- INSERT DE OBLIGACION VERSUS ACREENCIA
                  INSERT INTO REL_OBLACR_FRAC
                              (NumAcre, NumOblig, Idepol, NumCert, NumOper)
                     (SELECT GIR.NumAcre, nNumOblig, nIdePol, nNumCert, nNumOper
                      FROM   COND_FINANCIAMIENTO FIN, GIROS_FINANCIAMIENTO GIR, OPER_POL OP
                      WHERE  FIN.NumFinanc = GIR.NumFinanc
                      AND    FIN.IdePol = nIdePol
                      AND    FIN.NumCert = nNumCert
                      AND    FIN.NumOper < nNumOper
                      AND    FIN.TipoId = OBL.TipoId
                      AND    FIN.NumId = OBL.NumId
                      AND    FIN.DvId = OBL.DvId
                      AND    FIN.StsFin != 'ANU'
                      AND    (   cTipoOp = 'ANU'
                              OR (    cTipoOp != 'ANU'
                                  AND GIR.StsGiro IN ('ACT', 'COB')))
                      AND    TRUNC (GIR.FecVct) = TRUNC (OBL.FecVct)
                      AND    OP.NumOper = FIN.NumOper
                      AND    OP.NumCert = FIN.NumCert
                      AND    OP.TipoOp != 'REV'
                      AND    (   (    cTipoOp = 'ANU'
                                  AND OP.NumOperAnu = nNumOper)
                              OR cTipoOp != 'ANU'));
                  --
                  BEGIN
                     SELECT 'S'
                     INTO   cExisteOblRel
                     FROM   REL_OBLACR_FRAC
                     WHERE  IdePol = nIdePol
                     AND    Numcert = nNumCert
                     AND    NumOper = nNumOper
                     AND    NumOblig = nNumOblig;
                  EXCEPTION
                     WHEN NO_DATA_FOUND THEN
                        INSERT INTO REL_OBLACR_FRAC
                                    (NumAcre, NumOblig, Idepol, NumCert, NumOper)
                           (SELECT MAX (GIR.NumAcre), nNumOblig, nIdePol, nNumCert, nNumOper
                            FROM   COND_FINANCIAMIENTO FIN, GIROS_FINANCIAMIENTO GIR, OPER_POL OP
                            WHERE  FIN.NumFinanc = GIR.NumFinanc
                            AND    FIN.IdePol = nIdePol
                            AND    FIN.NumCert = nNumCert
                            AND    FIN.NumOper < nNumOper
                            AND    FIN.TipoId = OBL.TipoId
                            AND    FIN.NumId = OBL.NumId
                            AND    FIN.DvId = OBL.DvId
                            AND    FIN.StsFin != 'ANU'
                            AND    (   cTipoOp = 'ANU'
                                    OR (    cTipoOp != 'ANU'
                                        AND GIR.StsGiro IN ('ACT', 'COB')))
                            AND    TRUNC (GIR.FecVct) < TRUNC (OBL.FecVct)
                            AND    OP.NumOper = FIN.NumOper
                            AND    OP.NumCert = FIN.NumCert
                            AND    OP.TipoOp != 'REV'
                            AND    (   (    cTipoOp = 'ANU'
                                        AND OP.NumOperAnu = nNumOper)
                                    OR cTipoOp != 'ANU')
                            GROUP BY FIN.NumFinanc);
                     WHEN TOO_MANY_ROWS THEN
                        NULL;
                  END;
               END LOOP;
               BEGIN
                  BEGIN
                     DELETE DET_GIRO_FIN_RAMO
                     WHERE  NumFinanc = nNumFinanc;
                  END;
                  BEGIN
                     DELETE DET_GIRO_FIN
                     WHERE  NumFinanc = nNumFinanc;
                  END;
                  BEGIN
                     DELETE GIROS_FINANCIAMIENTO
                     WHERE  NumFinanc = nNumFinanc;
                  END;
                  BEGIN
                     DELETE REC_FINANCIAMIENTO
                     WHERE  NUmFinanc = nNumFinanc;
                  END;
                  BEGIN
                     DELETE COND_FINANCIAMIENTO
                     WHERE  NumFinanc = nNumFinanc;
                  END;
               END;
            END;
         END LOOP;   -- RESPONSABLES_PAGO
      END LOOP;
   END;
   --
   FUNCTION DUPLICA_POLIZA (nIdePol POLIZA.IdePol%TYPE)
      RETURN NUMBER IS
      --
      dFecRen         POLIZA.FecRen%TYPE;
      nIndRenAuto     POLIZA.IndRenAuto%TYPE;
      nIdePolRen      POLIZA.IdePol%TYPE;
      dFecUltFact     POLIZA.FecUltFact%TYPE;
      cIndTipoPro     POLIZA.IndTipoPro%TYPE;
      nNumRen         POLIZA.NumRen%TYPE;
      cIndRenGar      POLIZA.IndRenGar%TYPE;
      cVigProxRen     POLIZA.VigProxRen%TYPE;
      --
      dFecFact        DATE;
      dFecIniVigRen   DATE;
      dFecFinVigRen   DATE;
      nMeses          NUMBER;
      nExiPol         NUMBER;
      cCodProd        POLIZA.CodProd%TYPE;
      cCodOfiSusc     POLIZA.CodOfiSusc%TYPE;
      cCodPlan        VARCHAR2 (3)             := NULL;
      cRevPlan        VARCHAR2 (3)             := NULL;
      cCodCia         POLIZA.CodCia%TYPE;
      dFecSts         DATE;
      --
      CURSOR CERTIFICADOS_C IS
         SELECT NumCert
         FROM   CERTIFICADO
         WHERE  IdePol = nIdePol;
   BEGIN
      BEGIN
         SELECT 1, CodProd, CodOfiSusc, FecIniVig, FecFinVig, CodCia
         INTO   nExiPol, cCodProd, cCodOfiSusc, dFecIniVigRen, dFecFinVigRen, cCodCia
         FROM   POLIZA
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            RAISE_APPLICATION_ERROR (-20100, 'No se encontro datos de Poliza con identIFicador ' || nIdePol);
         WHEN TOO_MANY_ROWS THEN
            RAISE_APPLICATION_ERROR (-20100, 'Datos de poliza se encuentran duplicados para identIFicador ' || nIdePol);
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, 'Error al Selecionar datos de Poliza. ' || SQLERRM);
      END;
      BEGIN
         SELECT SQ_IdePol.NEXTVAL
         INTO   nIdePolRen
         FROM   DUAL;
      END;   -- NUEVO IdePol DE LA POLIZA --
      nNumPol  := PR_POLIZA.NumERO_ENDOSO (cCodProd, cCodOfiSusc, 'A');
      --Erick Zoque 20/05/2013 E_CON_20120821_1_7
      dFecSts  := PR_FECHAS_EQUIVALENTES.FECHA_EMISION (SYSDATE, cCodCia,'001');
      -- INSERTAR --
      INSERT INTO POLIZA
                  (IDEPOL, CODPOL, NUMPOL, CODCLI, TIPOCOTPOL, FECOPER, OBSERVA, NUMREN, INDNUMPOL, CODPROD, STSPOL, FECREN, FECINIVIG, FECFINVIG,
                   TIPOVIG, TIPOPDCION, TIPOFACT, INDCOA, CODFORMFCION, CODOFIEMI, CODOFISUSC, CODMONEDA, INDMULTIINTER, INDPOLADHESION, INDRENAUTO,
                   FECANUL, CODMOTVANUL, TEXTMOTVANUL, TIPOSUSC, CODFORMPAGO, TIPOANUL, FECULTFACT, CODPLANFRAC, MODPLANFRAC, INDCOBPROV, INDTIPOPRO,
                   CLASEPOL, PORCCTP, INDFACT, INDMOVPOLREN, INDRESPPAGO, FECINIPOL, IDECOT, INDRECUPREA, TIPOCOBRO, INDRENGAR, VIGPROXREN, NUMSOLIC,
                   INDCANCELA, INDIMPFACT, CODGRPPOL, EXPDIVS, INDDEVCONSIN, INDRENOVLOTE, NUMVECESIMP, CODFORMCAN, FECINIVIG1ERANO, CODCIA,
                   FECRESOLUCION, TIPTRM, FECTRMCONVENIDA)
         SELECT nIdePolRen, CodPol, nNumPol, CodCli, TipoCotPol, dFecSts, Observa, 0, IndNumPol, CodProd, 'VAL', FecFinVig, FecIniVig, FecFinVig,
                TipoVig, TipoPdcion, TipoFact, IndCoa, CodFormFcion, CodOfiemi, CodOfiSusc, CodMoneda, IndMultiInter, IndPolAdhesion, IndRenAuto,
                NULL, NULL, NULL, TipoSusc, CodFormPago, TipoAnul, FecIniVig, CodPlanFrac, ModPlanFrac, IndCobProv, IndTipoPro, ClasePol, PorcCtp,
                IndFact, IndMovPolRen, IndRespPago, FecIniVig, IdeCot, IndRecupRea, TipoCobro, IndRenGar, VigProxRen, NumSolic, IndCancela,
                IndImpFact, CodGrpPol, ExpDivs, IndDevConSin, IndRenovLote, NumVecesImp, CodFormCan, FecIniVig1erAno, CodCia, FecResolucion, TipTRM,
                FecTRMConvenida
         FROM   POLIZA
         WHERE  IdePol = nIdePol;
      --                    --
      FOR CP IN CERTIFICADOS_C LOOP
         PR_CERTIFICADO.RENOVAR (nIdePol, CP.NumCert, nIdePolRen, cCodPlan, cRevPlan);
      END LOOP;
      PR_DATOS_PART_POLIZA.RENOVAR (nIdePol, nIdePolRen);
      PR_PART_INTER_POL.RENOVAR (nIdePol, nIdePolRen);
      PR_POLIZA_CLIENTE.RENOVAR (nIdePol, nIdePolRen);
      PR_DIST_COA.RENOVAR (nIdePol, nIdePolRen, dFecIniVigRen, dFecFinVigRen);
      PR_RECA_DCTO_POL.RENOVAR (nIdePol, nIdePolRen, dFecIniVigRen, dFecFinVigRen);
      PR_ANEXO_POL.RENOVAR (nIdePol, nIdePolRen, dFecIniVigRen, dFecFinVigRen);
      PR_DEDUCIBLE_POLIZA.RENOVAR (nIdePol, nIdePolRen, dFecIniVigRen);
      PR_POLIZA.ACTUALIZA_ENDOSO (cCodProd, cCodOfiSusc, 'A');
      --
      RETURN (nIdePolRen);
   END DUPLICA_POLIZA;
   PROCEDURE T$_VALIDA_PRIMA_MINIMA (nIdePol T$_OPER_POL.IdePol%TYPE, nNumOper T$_OPER_POL.NumOper%TYPE) IS
      /*  Nombre   : T$_VALIDA_PRIMA_MINIMA
          Autor    : Edwin Cu�llar
          Fec.     : 25 de Enero 2005
          Objetivo : Verifica y Reajusta la P�liza para respetar la Prima M�nima Establecida
      */
      nMontoOper          T$_OPER_POL.MtoOper%TYPE;
      cTipoOp             T$_OPER_POL.TipoOp%TYPE;
      nPrimaAjustada      MOD_COBERT.PrimaFactMoneda%TYPE;
      nDiferencia         MOD_COBERT.PrimaFactMoneda%TYPE;
      nDifPrimaOper       MOD_COBERT.PrimaFactMoneda%TYPE;
      nMtoAjuste          MOD_COBERT.PrimaFactMoneda%TYPE;
      nMtoPrima           MOD_COBERT.PrimaFactMoneda%TYPE;
      nAjusteFinal        MOD_COBERT.PrimaFactMoneda%TYPE;
      nAjusteAcum         MOD_COBERT.PrimaFactMoneda%TYPE;
      nMtoAjusteOp        MOD_COBERT.PrimaFactMoneda%TYPE;
      nMtoPrimaPura       MOD_COBERT.PrimaFactMoneda%TYPE;
      nMtoRecaDcto        T$_RENG_MP.MtoReng%TYPE;
      nPrimaMinima        PRIMIN_PROD.Monto%TYPE            := 0;
      nPorcPrima          NUMBER (9, 6);
      nTotCobert          NUMBER (10);
      nCuentaCob          NUMBER (10)                       := 0;
      nPerioPago          NUMBER (2)                        := 1;
      cTipoOpConf         T$_OPER_POL.TipoOp%TYPE;
      --Defect # 2257 y Defect # 3434 Angelica Domenack 25/08/2005
      cTipCobro           DATOS_PART_POLIZA.TipCobro%TYPE;
      cAplicaPrimaMin     VARCHAR2 (1)                      := 'N';
      cAplicDifPerAnual   VARCHAR2 (1)                      := 'N';
      nOperAnt            NUMBER;
      nNumOperEmi         OPER_POL.NumOper%TYPE;
      nMontoOperEmi       OPER_POL.MtoOper%TYPE;
      cTipoOpI            T$_OPER_POL.TIPOOP%TYPE;
      nExistePrimaMinima  NUMBER;
      --
      CURSOR PRIMA_MIN_Q IS
         SELECT DISTINCT OP.TipoOp, P.CodMoneda, P.CodProd
         FROM   POLIZA P, T$_OPER_POL OP
         WHERE  OP.NumOper = nNumOper
         AND    OP.IdePol = P.IdePol
         AND    P.IdePol = nIdePol;
      CURSOR COBERT_Q IS
         SELECT MC.IdeCobert, MC.NumMod, MC.IdeMovPrimaT, MC.PrimaFactMoneda, MP.TasaCambio
         FROM   T$_OPER_POL OP, MOD_COBERT MC, T$_MOV_PRIMA MP
         WHERE  MP.IdeMovPrima = MC.IdeMovPrimaT
         AND    MC.PrimaFactMoneda > 0
         AND    MC.StsModCobert = 'INC'
         AND    MC.NumCert = OP.NumCert
         AND    MC.IdePol = OP.IdePol
         AND    OP.TipoOp = cTipoOp
         AND    OP.NumOper = nNumOper
         AND    OP.IdePol = nIdePol;
      CURSOR GEN_RENG_Q IS
         SELECT DISTINCT MC.IdeMovPrimaT
         FROM   T$_OPER_POL OP, MOD_COBERT MC, T$_MOV_PRIMA MP
         WHERE  MP.IdeMovPrima = MC.IdeMovPrimaT
         AND    MC.PrimaFactMoneda > 0
         AND    MC.StsModCobert = 'INC'
         AND    MC.NumCert = OP.NumCert
         AND    MC.IdePol = OP.IdePol
         AND    OP.TipoOp = cTipoOp
         AND    OP.NumOper = nNumOper
         AND    OP.IdePol = nIdePol;
      CURSOR ACT_OPER_Q IS
         SELECT IdePol, NumCert, NVL (SUM (NVL (MtoMoneda, 0)), 0) TotMtoMoneda
         FROM   T$_RECIBO
         WHERE  NumOper = nNumOper
         GROUP BY IdePol, NumCert;
   BEGIN
      PR_POLIZA.CARGAR (nIdePol);
      nPerioPago  := TO_NUMBER (PR.BUSCA_LVAL ('CANTREC', PR_POLIZA.cCodFormPago));
      -- Defect # 2257 y Defect # 3434
      -- Tipo de Cobro 001 Vencido y 002 Anticipado. Si es vencido no aplica Prima Minima
      --<<BBVA Consis 30/07/2014 - Se comenta Validacion para BBVA ya no utilizan la tabla
      -- Tipo de Cobro 001 Vencido y 002 Anticipado. Si es vencido no aplica Prima Minima
      /*BEGIN
         SELECT TipCobro
         INTO   cTipCobro
         FROM   DATOS_PART_POLIZA
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            cTipCobro  := NULL;
      END;*/
      BEGIN
         SELECT COUNT (*)
         INTO   nOperAnt
         FROM   OPER_POL
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            nOperAnt  := 0;
      END;
      --<<BBVA Consis 30/07/2014 - Valida si procuto maneja prima minima
      --<<BBVA Consis 19/03/2015 - Valiada deacuerdo al tipo operacion
      BEGIN
         SELECT DISTINCT OP.TipoOp
         INTO   cTipoOpI
         FROM   POLIZA P, T$_OPER_POL OP
         WHERE  OP.NumOper = nNumOper
         AND    OP.IdePol = P.IdePol
         AND    P.IdePol = nIdePol
         AND ROWNUM = 1;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
              cTipoOpI := 'XXX';
      END;

      BEGIN
         SELECT 1
         INTO   nExistePrimaMinima
         FROM   PRIMIN_PROD PP
         WHERE  PP.CodProd = PR_POLIZA.CCODPROD
         AND    PP.TIPOOP = cTipoOpI
         AND ROWNUM = 1;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            nExistePrimaMinima  := 0;
      END;

      IF nExistePrimaMinima = 1 THEN
         cAplicaPrimaMin  := 'S';
         --Documentos de Cobro cuando es una poliza que tiene diferente periodo.
         IF     nOperAnt > 1
            AND nPerioPago > 1
            AND PR_POLIZA.cIndFact = 'S' THEN
            cAplicaPrimaMin    := 'N';
            cAplicDifPerAnual  := 'S';
         ELSIF     nPerioPago > 1
               AND PR_POLIZA.cIndFact = 'N' THEN
            cAplicaPrimaMin  := 'N';
         END IF;
      ELSE
         cAplicaPrimaMin  := 'N';
      END IF;

      -->>BBVA Consis 30/07/2014
      IF cAplicaPrimaMin = 'S' THEN   -- Defect # 2257 y Defect # 3434 Angelica Domenack Aplica Prima Minima
         --
         FOR PM IN PRIMA_MIN_Q LOOP
            cTipoOp  := PM.TipoOp;
            BEGIN
               SELECT NVL (SUM (NVL (MtoOper, 0)), 0)
               INTO   nMontoOper
               FROM   T$_OPER_POL
               WHERE  TipoOp = cTipoOp
               AND    NumOper = nNumOper
               AND    IdePol = nIdePol;
            EXCEPTION
               WHEN OTHERS THEN
                  RAISE_APPLICATION_ERROR (-20288, PR.MENSAJE ('ABD', 20288, nNumOper, NULL, NULL));
            END;
            BEGIN
               SELECT NVL (SUM (NVL (MtoReng, 0)), 0)
               INTO   nMtoRecaDcto
               FROM   T$_OPER_POL OP, T$_RECIBO RE, T$_RENG_MP RM
               WHERE  RM.TipoReng = 'RDC'
               AND    RM.IdeMovPrima = RE.IdeMovPrima
               AND    RE.NumOper = OP.NumOper
               AND    RE.NumCert = OP.NumCert
               AND    RE.IdePol = OP.IdePol
               AND    OP.TipoOp = cTipoOp
               AND    OP.NumOper = nNumOper
               AND    OP.IdePol = nIdePol;
            EXCEPTION
               WHEN OTHERS THEN
                  RAISE_APPLICATION_ERROR (-20288, PR.MENSAJE ('ABD', 20288, nNumOper, NULL, NULL));
            END;
            IF PR_POLIZA.cIndFact = 'S' THEN
               cTipoOpConf  := 'EMI';
            ELSE
                -- Defect # 2257 y Defect # 3434 Angelica Domenack Aplica
               -- Cuando Existen mas operacion la configuracion de prima minima poliza
               -- es de tipo MOD Sino 'EMI'
               --<<BBVA COnsis EFVC 06/11/2014 Se cambia condicion para validar el tipo operacion enla configuracion
               IF cTipoOp = 'EMI' THEN
                  cTipoOpConf  := 'EMI';
               ELSE
                  cTipoOpConf  := 'MOD';
               END IF;
            END IF;
            BEGIN
               SELECT NVL (PP.Monto, 0) / nPerioPago
               INTO   nPrimaMinima
               FROM   PRIMIN_PROD PP
               WHERE  PP.TipoOp = cTipoOpConf
               AND    PP.CodMoneda = PM.CodMoneda
               AND    PP.CodProd = PM.CodProd;
            EXCEPTION
               WHEN NO_DATA_FOUND THEN
                  nPrimaMinima  := 0;
            END;
            IF nMontoOper < nPrimaMinima THEN
               nDiferencia  := nPrimaMinima - nMontoOper;
               BEGIN
                  SELECT NVL (COUNT (*), 0)
                  INTO   nTotCobert
                  FROM   T$_OPER_POL OP, MOD_COBERT MC
                  WHERE  MC.PrimaFactMoneda > 0
                  AND    MC.StsModCobert = 'INC'
                  AND    MC.NumCert = OP.NumCert
                  AND    MC.IdePol = OP.IdePol
                  AND    OP.TipoOp = cTipoOp
                  AND    OP.NumOper = nNumOper
                  AND    OP.IdePol = nIdePol;
               EXCEPTION
                  WHEN OTHERS THEN
                     RAISE_APPLICATION_ERROR (-20288, PR.MENSAJE ('ABD', 20288, nNumOper, NULL, NULL));
               END;
               IF NVL (nTotCobert, 0) > 0 THEN
                  nAjusteFinal  := 0;
                  FOR MC IN COBERT_Q LOOP
                     nMtoPrimaPura   := NVL (nMontoOper, 0) - NVL (nMtoRecaDcto, 0);
                     nPorcPrima      := MC.PrimaFactMoneda / NVL (nMtoPrimaPura, 0);
                     nMtoAjuste      := NVL (nDiferencia, 0) * NVL (nPorcPrima, 0);
                     nAjusteAcum     := NVL (nAjusteAcum, 0) + NVL (nMtoAjuste, 0);
                     nPrimaAjustada  := MC.PrimaFactMoneda + NVL (nMtoAjuste, 0);
                     nCuentaCob      := NVL (nCuentaCob, 0) + 1;
                     IF NVL (nCuentaCob, 0) = NVL (nTotCobert, 0) THEN
                        IF NVL (nAjusteAcum, 0) <> NVL (nDiferencia, 0) THEN
                           nAjusteFinal    := NVL (nDiferencia, 0) - NVL (nAjusteAcum, 0);
                           nPrimaAjustada  := NVL (nPrimaAjustada, 0) + NVL (nAjusteFinal, 0);
                        END IF;
                     END IF;
                     BEGIN
                        UPDATE MOD_COBERT
                        SET PriMinAjuste = NVL (nMtoAjuste, 0) + NVL (nAjusteFinal, 0),
                            PriFactOrig = PrimaFactMoneda,
                            PrimaFactMoneda = NVL (nPrimaAjustada, 0),
                            PrimaMovFactor = NVL (nPrimaAjustada, 0)
                        WHERE  IdeCobert = MC.IdeCobert
                        AND    NumMod = MC.NumMod;
                     EXCEPTION
                        WHEN OTHERS THEN
                           RAISE_APPLICATION_ERROR (-22052,
                                                    PR.MENSAJE ('ABD', 20288, 'No Actualiz� MOD_COBERT en Ajuste de Prima M�nima', NULL, NULL));
                     END;
                     BEGIN
                        UPDATE T$_TRANS_COBERT
                        SET MtoTransCobert = NVL (nPrimaAjustada, 0)
                        WHERE  IdeCobert = MC.IdeCobert
                        AND    NumMod = MC.NumMod;
                     EXCEPTION
                        WHEN OTHERS THEN
                           RAISE_APPLICATION_ERROR (-22052,
                                                    PR.MENSAJE ('ABD', 20288, 'No Actualiz� T$_TRANS_COBERT en Ajuste de Prima M�nima', NULL, NULL));
                     END;
                     BEGIN
                        UPDATE T$_MOV_PRIMA
                        SET MtoMoneda = MtoMoneda + NVL (nMtoAjuste, 0) + NVL (nAjusteFinal, 0),
                            MtoLocal = MtoLocal + ((NVL (nMtoAjuste, 0) + NVL (nAjusteFinal, 0)) * MC.TasaCambio),
                            MtoComMoneda = (MtoMoneda + NVL (nMtoAjuste, 0) + NVL (nAjusteFinal, 0)) * PorcCom / 100,
                            MtoComLocal = (MtoLocal + ((NVL (nMtoAjuste, 0) + NVL (nAjusteFinal, 0)) * MC.TasaCambio)) * PorcCom / 100
                        WHERE  IdeMovPrima = MC.IdeMovPrimaT;
                     EXCEPTION
                        WHEN OTHERS THEN
                           RAISE_APPLICATION_ERROR (-22052,
                                                    PR.MENSAJE ('ABD', 20288, 'No Actualiz� T$_MOV_PRIMA en Ajuste de Prima M�nima', NULL, NULL));
                     END;
                     BEGIN
                        UPDATE T$_RECIBO
                        SET MtoMoneda = MtoMoneda + NVL (nMtoAjuste, 0) + NVL (nAjusteFinal, 0),
                            MtoLocal = MtoLocal + ((NVL (nMtoAjuste, 0) + NVL (nAjusteFinal, 0)) * MC.TasaCambio),
                            MtoComMoneda = (MtoMoneda + NVL (nMtoAjuste, 0) + NVL (nAjusteFinal, 0)) * PorcCom / 100,
                            MtoComLocal = (MtoLocal + ((NVL (nMtoAjuste, 0) + NVL (nAjusteFinal, 0)) * MC.TasaCambio)) * PorcCom / 100
                        WHERE  IdeMovPrima = MC.IdeMovPrimaT;
                     EXCEPTION
                        WHEN OTHERS THEN
                           RAISE_APPLICATION_ERROR (-22052,
                                                    PR.MENSAJE ('ABD', 20288, 'No Actualiz� T$_RECIBO en Ajuste de Prima M�nima', NULL, NULL));
                     END;
                     nAjusteFinal    := 0;
                  END LOOP;
                  FOR GR IN GEN_RENG_Q LOOP
                     BEGIN
                        DELETE T$_RENG_MP
                        WHERE  IdeMovPrima = GR.IdeMovPrimaT;
                     EXCEPTION
                        WHEN OTHERS THEN
                           RAISE_APPLICATION_ERROR (-22052, PR.MENSAJE ('ABD', 20288, 'No Elimin� T$_RENG_MP en Ajuste de Prima M�nima', NULL, NULL));
                     END;
                     nMtoPrima  := PR_T$_MOV_PRIMA.GENERAR_RENGLONES (GR.IdeMovPrimaT);
                  END LOOP;
               END IF;
            END IF;
         END LOOP;
      END IF;
      IF cAplicDifPerAnual = 'S' THEN   --Aplicacion Diferente Anual (a partir del segundo cobro)
         FOR PM IN PRIMA_MIN_Q LOOP
            cTipoOp      := PM.TipoOp;
            -- Operacion de la Primera Emision de la Poliza
            BEGIN
               SELECT MIN (NumOper)
               INTO   nNumOperEmi
               FROM   OPER_POL
               WHERE  TipoOp = 'EMI'
               AND    IdePol = nIdePol;
            EXCEPTION
               WHEN OTHERS THEN
                  RAISE_APPLICATION_ERROR (-20288, PR.MENSAJE ('ABD', 20288, nNumOper, NULL, NULL));
            END;
            --  Monto de la Operacion de la Primera Emision de la Poliza
            BEGIN
               SELECT SUM (NVL (MC.PrimaFactMoneda, 0) - NVL (MC.PriMinAjuste, 0))
               INTO   nMontoOperEmi
               FROM   OPER_POL OP, RECIBO RE, MOD_COBERT MC, MOV_PRIMA MP
               WHERE  MP.IdeMovPrima = MC.IdeMovPrima
               AND    MC.PrimaFactMoneda > 0
               AND    MC.IdeMovPrima = RE.IdeMovPrima
               AND    RE.NumOper = OP.NumOper
               AND    RE.NumCert = OP.NumCert
               AND    RE.IdePol = OP.IdePol
               AND    OP.TipoOp = 'EMI'
               AND    OP.NumOper = nNumOperEmi
               AND    OP.IdePol = nIdePol;
            EXCEPTION
               WHEN NO_DATA_FOUND THEN
                  nMontoOperEmi  := 0;
               WHEN OTHERS THEN
                  RAISE_APPLICATION_ERROR (-20288, PR.MENSAJE ('ABD', 20288, nNumOperEmi, NULL, NULL));
            END;
            cTipoOpConf  := 'EMI';
            BEGIN
               SELECT NVL (PP.Monto, 0) / nPerioPago
               INTO   nPrimaMinima
               FROM   PRIMIN_PROD PP
               WHERE  PP.TipoOp = cTipoOpConf
               AND    PP.CodMoneda = PM.CodMoneda
               AND    PP.CodProd = PM.CodProd;
            EXCEPTION
               WHEN NO_DATA_FOUND THEN
                  nPrimaMinima  := 0;
            END;
            IF nMontoOperEmi < nPrimaMinima THEN
               FOR MC IN COBERT_Q LOOP
                  nMtoAjuste  := 0;
                  -- Ajuste Inicial
                  BEGIN
                     SELECT MC1.PriMinAjuste
                     INTO   nMtoAjuste
                     FROM   OPER_POL OP, RECIBO RE, MOD_COBERT MC1, MOV_PRIMA MP
                     WHERE  MP.IdeMovPrima = MC1.IdeMovPrima
                     AND    MC1.PrimaFactMoneda > 0
                     AND    MC1.IdeMovPrima = RE.IdeMovPrima
                     AND    RE.NumOper = OP.NumOper
                     AND    RE.NumCert = OP.NumCert
                     AND    RE.IdePol = OP.IdePol
                     AND    OP.TipoOp = 'EMI'
                     AND    OP.NumOper = nNumOperEmi
                     AND    OP.IdePol = nIdePol
                     AND    MC1.IdeCobert = MC.IdeCobert;
                  EXCEPTION
                     WHEN NO_DATA_FOUND THEN
                        nMtoAjuste  := 0;
                     WHEN OTHERS THEN
                        RAISE_APPLICATION_ERROR (-20288, PR.MENSAJE ('ABD', 20288, nNumOperEmi, NULL, NULL));
                  END;
                  BEGIN
                     UPDATE MOD_COBERT
                     SET PriMinAjuste = NVL (nMtoAjuste, 0),
                         PriFactOrig = MC.PrimaFactMoneda,
                         PrimaFactMoneda = MC.PrimaFactMoneda + NVL (nMtoAjuste, 0),
                         PrimaMovFactor = MC.PrimaFactMoneda + NVL (nMtoAjuste, 0)
                     WHERE  IdeCobert = MC.IdeCobert
                     AND    NumMod = MC.NumMod;
                  EXCEPTION
                     WHEN OTHERS THEN
                        RAISE_APPLICATION_ERROR (-22052, PR.MENSAJE ('ABD', 20288, 'No Actualiz� MOD_COBERT en Ajuste de Prima M�nima', NULL, NULL));
                  END;
                  BEGIN
                     UPDATE T$_TRANS_COBERT
                     SET MtoTransCobert = MC.PrimaFactMoneda + NVL (nMtoAjuste, 0)
                     WHERE  IdeCobert = MC.IdeCobert
                     AND    NumMod = MC.NumMod;
                  EXCEPTION
                     WHEN OTHERS THEN
                        RAISE_APPLICATION_ERROR (-22052,
                                                 PR.MENSAJE ('ABD', 20288, 'No Actualiz� T$_TRANS_COBERT en Ajuste de Prima M�nima', NULL, NULL));
                  END;
                  BEGIN
                     UPDATE T$_MOV_PRIMA
                     SET MtoMoneda = MtoMoneda + NVL (nMtoAjuste, 0),
                         MtoLocal = MtoLocal + (NVL (nMtoAjuste, 0) * MC.TasaCambio),
                         MtoComMoneda = (MtoMoneda + NVL (nMtoAjuste, 0)) * PorcCom / 100,
                         MtoComLocal = (MtoLocal + (NVL (nMtoAjuste, 0) * MC.TasaCambio)) * PorcCom / 100
                     WHERE  IdeMovPrima = MC.IdeMovPrimaT;
                  EXCEPTION
                     WHEN OTHERS THEN
                        RAISE_APPLICATION_ERROR (-22052,
                                                 PR.MENSAJE ('ABD', 20288, 'No Actualiz� T$_MOV_PRIMA en Ajuste de Prima M�nima', NULL, NULL));
                  END;
                  BEGIN
                     UPDATE T$_RECIBO
                     SET MtoMoneda = MtoMoneda + NVL (nMtoAjuste, 0),
                         MtoLocal = MtoLocal + (NVL (nMtoAjuste, 0) * MC.TasaCambio),
                         MtoComMoneda = (MtoMoneda + NVL (nMtoAjuste, 0)) * PorcCom / 100,
                         MtoComLocal = (MtoLocal + (NVL (nMtoAjuste, 0) * MC.TasaCambio)) * PorcCom / 100
                     WHERE  IdeMovPrima = MC.IdeMovPrimaT;
                  EXCEPTION
                     WHEN OTHERS THEN
                        RAISE_APPLICATION_ERROR (-22052, PR.MENSAJE ('ABD', 20288, 'No Actualiz� T$_RECIBO en Ajuste de Prima M�nima', NULL, NULL));
                  END;
               END LOOP;
               FOR GR IN GEN_RENG_Q LOOP
                  BEGIN
                     DELETE T$_RENG_MP
                     WHERE  IdeMovPrima = GR.IdeMovPrimaT;
                  EXCEPTION
                     WHEN OTHERS THEN
                        RAISE_APPLICATION_ERROR (-22052, PR.MENSAJE ('ABD', 20288, 'No Elimin� T$_RENG_MP en Ajuste de Prima M�nima', NULL, NULL));
                  END;
                  nMtoPrima  := PR_T$_MOV_PRIMA.GENERAR_RENGLONES (GR.IdeMovPrimaT);
               END LOOP;
            END IF;
         END LOOP;
      END IF;
      FOR AP IN ACT_OPER_Q LOOP
         BEGIN
            UPDATE T$_OPER_POL
            SET MtoOper = AP.TotMtoMoneda
            WHERE  IdePol = AP.IdePol
            AND    NumCert = AP.NumCert
            AND    NumOper = nNumOper;
         EXCEPTION
            WHEN OTHERS THEN
               RAISE_APPLICATION_ERROR (-22052, PR.MENSAJE ('ABD', 20288, 'No Actualiz� T$_OPER_POL en Ajuste de Prima M�nima', NULL, NULL));
         END;
      END LOOP;
   END T$_VALIDA_PRIMA_MINIMA;
   PROCEDURE VALIDA_PRIMA_MINIMA (nIdePol OPER_POL.IdePol%TYPE, nNumOper OPER_POL.NumOper%TYPE) IS
      /*  Nombre   : VALIDA_PRIMA_MINIMA
          Autor    : Edwin Cu�llar
          Fec.     : 25 de Enero 2005
          Objetivo : Verifica y Reajusta la P�liza para respetar la Prima M�nima Establecida
      */
      nMontoOper          OPER_POL.MtoOper%TYPE;
      cTipoOp             OPER_POL.TipoOp%TYPE;
      nPrimaAjustada      MOD_COBERT.PrimaFactMoneda%TYPE;
      nDiferencia         MOD_COBERT.PrimaFactMoneda%TYPE;
      nDifPrimaOper       MOD_COBERT.PrimaFactMoneda%TYPE;
      nMtoAjuste          MOD_COBERT.PrimaFactMoneda%TYPE;
      nMtoPrima           MOD_COBERT.PrimaFactMoneda%TYPE;
      nAjusteFinal        MOD_COBERT.PrimaFactMoneda%TYPE;
      nAjusteAcum         MOD_COBERT.PrimaFactMoneda%TYPE;
      nMtoAjusteOp        MOD_COBERT.PrimaFactMoneda%TYPE;
      nMtoPrimaPura       MOD_COBERT.PrimaFactMoneda%TYPE;
      nPrimaMinima        PRIMIN_PROD.Monto%TYPE            := 0;
      nMtoRecaDcto        RENG_MP.MtoReng%TYPE;
      nPorcPrima          NUMBER (9, 6);
      nTotCobert          NUMBER (10);
      nCuentaCob          NUMBER (10)                       := 0;
      nPerioPago          NUMBER (2)                        := 1;
      cTipoOpConf         T$_OPER_POL.TipoOp%TYPE;
      --Defect # 2257 y Defect # 3434 Angelica Domenack 25/08/2005
      cTipCobro           DATOS_PART_POLIZA.TipCobro%TYPE;
      cAplicaPrimaMin     VARCHAR2 (1)                      := 'N';
      cAplicDifPerAnual   VARCHAR2 (1)                      := 'N';
      nOperAnt            NUMBER;
      nNumOperEmi         OPER_POL.NumOper%TYPE;
      nMontoOperEmi       OPER_POL.MtoOper%TYPE;
      cTipoOpI             OPER_POL.TIPOOP%TYPE;
      --
      --<<BBVA Consis 30/07/2014 - Valida si procuto maneja prima minima
      nExistePrimaMinima NUMBER;

      CURSOR PRIMA_MIN_Q IS
         SELECT DISTINCT OP.TipoOp, P.CodMoneda, P.CodProd
         FROM   POLIZA P, OPER_POL OP
         WHERE  OP.NumOper = nNumOper
         AND    OP.IdePol = P.IdePol
         AND    P.IdePol = nIdePol;
      CURSOR COBERT_Q IS
         SELECT MC.IdeCobert, MC.NumMod, MC.IdeMovPrima, MC.PrimaFactMoneda, MP.TasaCambio
         FROM   OPER_POL OP, RECIBO RE, MOD_COBERT MC, MOV_PRIMA MP
         WHERE  MP.IdeMovPrima = MC.IdeMovPrima
         AND    MC.PrimaFactMoneda > 0
         AND    MC.IdeMovPrima = RE.IdeMovPrima
         AND    RE.NumOper = OP.NumOper
         AND    RE.NumCert = OP.NumCert
         AND    RE.IdePol = OP.IdePol
         AND    OP.TipoOp = cTipoOp
         AND    OP.NumOper = nNumOper
         AND    OP.IdePol = nIdePol;
      CURSOR GEN_RENG_Q IS
         SELECT DISTINCT MC.IdeMovPrima
         FROM   OPER_POL OP, RECIBO RE, MOD_COBERT MC
         WHERE  MC.PrimaFactMoneda > 0
         AND    MC.IdeMovPrima = RE.IdeMovPrima
         AND    RE.NumOper = OP.NumOper
         AND    RE.NumCert = OP.NumCert
         AND    RE.IdePol = OP.IdePol
         AND    OP.TipoOp = cTipoOp
         AND    OP.NumOper = nNumOper
         AND    OP.IdePol = nIdePol;
      CURSOR ACT_OPER_Q IS
         SELECT IdePol, NumCert, SUM (NVL (MtoMoneda, 0)) TotMtoMoneda
         FROM   RECIBO
         WHERE  NumOper = nNumOper
         GROUP BY IdePol, NumCert;
   BEGIN
      PR_POLIZA.CARGAR (nIdePol);
      nPerioPago  := TO_NUMBER (PR.BUSCA_LVAL ('CANTREC', PR_POLIZA.cCodFormPago));
      --<<BBVA Consis 30/07/2014 - Se comenta Validacion para BBVA ya no utilizan la tabla
      -- Tipo de Cobro 001 Vencido y 002 Anticipado. Si es vencido no aplica Prima Minima
      /*BEGIN
         SELECT TipCobro
         INTO   cTipCobro
         FROM   DATOS_PART_POLIZA
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            cTipCobro  := NULL;
      END;*/
      BEGIN
         SELECT COUNT (*)
         INTO   nOperAnt
         FROM   OPER_POL
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            nOperAnt  := 0;
      END;
      --<<BBVA Consis 30/07/2014 - Valida si procuto maneja prima minima
      --<<BBVA Consis 19/03/2015 - Valiada deacuerdo al tipo operacion
      -- INICIO BBVA 14/03/2018  REDMINE 23276 SII-GROUP : SE COMENTO PORQUE ESTOS QUERYS NO ERAN OPTIMOS Y ESTABAN DANDO ERROR
     /* BEGIN
         SELECT DISTINCT OP.TipoOp
         INTO   cTipoOpI
         FROM   POLIZA P, OPER_POL OP
         WHERE  OP.NumOper = nNumOper
         AND    OP.IdePol = P.IdePol
         AND    P.IdePol = nIdePol;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
              cTipoOpI := 'XXX';
         WHEN TOO_MANY_ROWS THEN
            BEGIN
               SELECT DISTINCT OP.TipoOp
               INTO   cTipoOpI
               FROM   POLIZA P, OPER_POL OP
               WHERE  OP.NumOper = nNumOper
               AND    OP.IdePol = P.IdePol
               AND    P.IdePol = nIdePol
               AND    OP.TipoOp = 'EMI';
            END;
      END;

      BEGIN
         SELECT 1
         INTO   nExistePrimaMinima
         FROM   PRIMIN_PROD PP
         WHERE  PP.CodProd = PR_POLIZA.CCODPROD
         AND    PP.TIPOOP = cTipoOpI
         AND ROWNUM = 1;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            nExistePrimaMinima  := 0;
      END;*/
      -- FIN BBVA 14/03/2018  REDMINE 23276 SII-GROUP
      -- INICIO BBVA 14/03/2018  REDMINE 23276 SII-GROUP
      FOR  C_TIPOOP1 IN (SELECT DISTINCT NVL(OP.TipoOp,'XXX') cTipoOpI
                        FROM   POLIZA P, OPER_POL OP
                        WHERE  OP.NumOper(+) = nNumOper
                        AND    OP.IdePol(+) = P.IdePol
                        AND    P.IdePol = nIdePol) LOOP

      BEGIN
         SELECT 1
         INTO   nExistePrimaMinima
         FROM   PRIMIN_PROD PP
         WHERE  PP.CodProd = PR_POLIZA.CCODPROD
         AND    PP.TIPOOP = C_TIPOOP1.cTipoOpI
         AND ROWNUM = 1;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            nExistePrimaMinima  := 0;
      END;
      -->>BBVA Consis 30/07/2014
      IF nExistePrimaMinima = 1 THEN
         cAplicaPrimaMin  := 'S';
         --Documentos de Cobro cuando es una poliza que tiene diferente periodo.
         IF     nOperAnt > 1
            AND nPerioPago > 1
            AND PR_POLIZA.cIndFact = 'S' THEN
            cAplicaPrimaMin    := 'N';
            cAplicDifPerAnual  := 'S';
         ELSIF     nPerioPago > 1
               AND PR_POLIZA.cIndFact = 'N' THEN
            cAplicaPrimaMin  := 'N';
         END IF;
      ELSE
         cAplicaPrimaMin  := 'N';
      END IF;
      IF cAplicaPrimaMin = 'S' THEN   -- Defect # 2257 y Defect # 3434 Angelica Domenack Aplica Prima Minima
         --
         FOR PM IN PRIMA_MIN_Q LOOP
            cTipoOp  := PM.TipoOp;
            BEGIN
               SELECT NVL (SUM (MtoOper), 0)
               INTO   nMontoOper
               FROM   OPER_POL
               WHERE  TipoOp = cTipoOp
               AND    NumOper = nNumOper
               AND    IdePol = nIdePol;
            EXCEPTION
               WHEN OTHERS THEN
                  RAISE_APPLICATION_ERROR (-20288, PR.MENSAJE ('ABD', 20288, nNumOper, NULL, NULL));
            END;
            BEGIN
               SELECT NVL (SUM (NVL (MtoReng, 0)), 0)
               INTO   nMtoRecaDcto
               FROM   OPER_POL OP, RECIBO RE, RENG_MP RM
               WHERE  RM.TipoReng = 'RDC'
               AND    RM.IdeMovPrima = RE.IdeMovPrima
               AND    RE.NumOper = OP.NumOper
               AND    RE.NumCert = OP.NumCert
               AND    RE.IdePol = OP.IdePol
               AND    OP.TipoOp = cTipoOp
               AND    OP.NumOper = nNumOper
               AND    OP.IdePol = nIdePol;
            EXCEPTION
               WHEN OTHERS THEN
                  RAISE_APPLICATION_ERROR (-20288, PR.MENSAJE ('ABD', 20288, nNumOper, NULL, NULL));
            END;
            IF PR_POLIZA.cIndFact = 'S' THEN
               cTipoOpConf  := 'EMI';
            ELSE
                -- Defect # 2257 y Defect # 3434 Angelica Domenack Aplica
               -- Cuando Existen mas operacion la configuracion de prima minima poliza
               -- es de tipo MOD Sino 'EMI'
               --<<BBVA COnsis EFVC 06/11/2014 Se cambia condicion para validar el tipo operacion enla configuracion
               IF cTipoOp = 'EMI' THEN
                  cTipoOpConf  := 'EMI';
               ELSE
                  cTipoOpConf  := 'MOD';
               END IF;
            END IF;
            BEGIN
               SELECT NVL (PP.Monto, 0) / nPerioPago
               INTO   nPrimaMinima
               FROM   PRIMIN_PROD PP
               WHERE  PP.TipoOp = cTipoOpConf
               AND    PP.CodMoneda = PM.CodMoneda
               AND    PP.CodProd = PM.CodProd;
            EXCEPTION
               WHEN NO_DATA_FOUND THEN
                  nPrimaMinima  := 0;
            END;
            IF nMontoOper < nPrimaMinima THEN
               nDiferencia  := nPrimaMinima - nMontoOper;
               BEGIN
                  SELECT NVL (COUNT (*), 0)
                  INTO   nTotCobert
                  FROM   RECIBO RE, OPER_POL OP, MOD_COBERT MC
                  WHERE  MC.PrimaFactMoneda > 0
                  AND    MC.IdeMovPrima = RE.IdeMovPrima
                  AND    RE.NumOper = OP.NumOper
                  AND    RE.numcert = OP.NumCert
                  AND    RE.IdePol = OP.IdePol
                  AND    OP.TipoOp = cTipoOp
                  AND    OP.NumOper = nNumOper
                  AND    OP.IdePol = nIdePol;
               EXCEPTION
                  WHEN OTHERS THEN
                     RAISE_APPLICATION_ERROR (-20288, PR.MENSAJE ('ABD', 20288, nNumOper, NULL, NULL));
               END;
               IF NVL (nTotCobert, 0) > 0 THEN
                  nAjusteFinal  := 0;
                  FOR MC IN COBERT_Q LOOP
                     nMtoPrimaPura   := NVL (nMontoOper, 0) - NVL (nMtoRecaDcto, 0);
                     nPorcPrima      := MC.PrimaFactMoneda / NVL (nMtoPrimaPura, 0);
                     nMtoAjuste      := NVL (nDiferencia, 0) * NVL (nPorcPrima, 0);
                     nAjusteAcum     := NVL (nAjusteAcum, 0) + NVL (nMtoAjuste, 0);
                     nPrimaAjustada  := MC.PrimaFactMoneda + NVL (nMtoAjuste, 0);
                     nCuentaCob      := NVL (nCuentaCob, 0) + 1;
                     IF NVL (nCuentaCob, 0) = NVL (nTotCobert, 0) THEN
                        IF NVL (nAjusteAcum, 0) <> NVL (nDiferencia, 0) THEN
                           nAjusteFinal    := NVL (nDiferencia, 0) - NVL (nAjusteAcum, 0);
                           nPrimaAjustada  := NVL (nPrimaAjustada, 0) + NVL (nAjusteFinal, 0);
                        END IF;
                     END IF;
                     BEGIN
                        UPDATE MOD_COBERT
                        SET PriMinAjuste = NVL (nMtoAjuste, 0) + NVL (nAjusteFinal, 0),
                            PriFactOrig = PrimaFactMoneda,
                            PrimaFactMoneda = NVL (nPrimaAjustada, 0),
                            PrimaMovFactor = NVL (nPrimaAjustada, 0)
                        WHERE  IdeCobert = MC.IdeCobert
                        AND    NumMod = MC.NumMod;
                     EXCEPTION
                        WHEN OTHERS THEN
                           RAISE_APPLICATION_ERROR (-22052,
                                                    PR.MENSAJE ('ABD', 20288, 'No Actualiz� MOD_COBERT en Ajuste de Prima M�nima', NULL, NULL));
                     END;
                     BEGIN
                        UPDATE TRANS_COBERT
                        SET MtoTransCobert = NVL (nPrimaAjustada, 0)
                        WHERE  IdeCobert = MC.IdeCobert
                        AND    NumMod = MC.NumMod;
                     EXCEPTION
                        WHEN OTHERS THEN
                           RAISE_APPLICATION_ERROR (-22052,
                                                    PR.MENSAJE ('ABD', 20288, 'No Actualiz� TRANS_COBERT en Ajuste de Prima M�nima', NULL, NULL));
                     END;
                     BEGIN
                        UPDATE MOV_PRIMA
                        SET MtoMoneda = MtoMoneda + NVL (nMtoAjuste, 0) + NVL (nAjusteFinal, 0),
                            MtoLocal = MtoLocal + ((NVL (nMtoAjuste, 0) + NVL (nAjusteFinal, 0)) * MC.TasaCambio),
                            MtoComMoneda = (MtoMoneda + NVL (nMtoAjuste, 0) + NVL (nAjusteFinal, 0)) * PorcCom / 100,
                            MtoComLocal = (MtoLocal + ((NVL (nMtoAjuste, 0) + NVL (nAjusteFinal, 0)) * MC.TasaCambio)) * PorcCom / 100
                        WHERE  IdeMovPrima = MC.IdeMovPrima;
                     EXCEPTION
                        WHEN OTHERS THEN
                           RAISE_APPLICATION_ERROR (-22052,
                                                    PR.MENSAJE ('ABD', 20288, 'No Actualiz� MOV_PRIMA en Ajuste de Prima M�nima', NULL, NULL));
                     END;
                     BEGIN
                        UPDATE RECIBO
                        SET MtoMoneda = MtoMoneda + NVL (nMtoAjuste, 0) + NVL (nAjusteFinal, 0),
                            MtoLocal = MtoLocal + ((NVL (nMtoAjuste, 0) + NVL (nAjusteFinal, 0)) * MC.TasaCambio),
                            MtoComMoneda = (MtoMoneda + NVL (nMtoAjuste, 0) + NVL (nAjusteFinal, 0)) * PorcCom / 100,
                            MtoComLocal = (MtoLocal + ((NVL (nMtoAjuste, 0) + NVL (nAjusteFinal, 0)) * MC.TasaCambio)) * PorcCom / 100
                        WHERE  IdeMovPrima = MC.IdeMovPrima;
                     EXCEPTION
                        WHEN OTHERS THEN
                           RAISE_APPLICATION_ERROR (-22052, PR.MENSAJE ('ABD', 20288, 'No Actualiz� RECIBO en Ajuste de Prima M�nima', NULL, NULL));
                     END;
                     nAjusteFinal    := 0;
                  END LOOP;
                  FOR GR IN GEN_RENG_Q LOOP
                     BEGIN
                        DELETE RENG_MP
                        WHERE  IdeMovPrima = GR.IdeMovPrima;
                     EXCEPTION
                        WHEN OTHERS THEN
                           RAISE_APPLICATION_ERROR (-22052, PR.MENSAJE ('ABD', 20288, 'No Elimin� RENG_MP en Ajuste de Prima M�nima', NULL, NULL));
                     END;
                     nMtoPrima  := PR_MOV_PRIMA.GENERAR_RENGLONES (GR.IdeMovPrima);
                  END LOOP;
               END IF;
            END IF;
         END LOOP;
      END IF;
      IF cAplicDifPerAnual = 'S' THEN   --Aplicacion Diferente Anual (a partir del segundo cobro)
         FOR PM IN PRIMA_MIN_Q LOOP
            cTipoOp      := PM.TipoOp;
            -- Operacion de la Primera Emision de la Poliza
            BEGIN
               SELECT MIN (NumOper)
               INTO   nNumOperEmi
               FROM   OPER_POL
               WHERE  TipoOp = 'EMI'
               AND    IdePol = nIdePol;
            EXCEPTION
               WHEN OTHERS THEN
                  RAISE_APPLICATION_ERROR (-20288, PR.MENSAJE ('ABD', 20288, nNumOper, NULL, NULL));
            END;
            --  Monto de la Operacion de la Primera Emision de la Poliza
            BEGIN
               SELECT SUM (NVL (MC.PrimaFactMoneda, 0) - NVL (MC.PriMinAjuste, 0))
               INTO   nMontoOperEmi
               FROM   OPER_POL OP, RECIBO RE, MOD_COBERT MC, MOV_PRIMA MP
               WHERE  MP.IdeMovPrima = MC.IdeMovPrima
               AND    MC.PrimaFactMoneda > 0
               AND    MC.IdeMovPrima = RE.IdeMovPrima
               AND    RE.NumOper = OP.NumOper
               AND    RE.NumCert = OP.NumCert
               AND    RE.IdePol = OP.IdePol
               AND    OP.TipoOp = 'EMI'
               AND    OP.NumOper = nNumOperEmi
               AND    OP.IdePol = nIdePol;
            EXCEPTION
               WHEN NO_DATA_FOUND THEN
                  nMontoOperEmi  := 0;
               WHEN OTHERS THEN
                  RAISE_APPLICATION_ERROR (-20288, PR.MENSAJE ('ABD', 20288, nNumOperEmi, NULL, NULL));
            END;
            cTipoOpConf  := 'EMI';
            BEGIN
               SELECT NVL (PP.Monto, 0) / nPerioPago
               INTO   nPrimaMinima
               FROM   PRIMIN_PROD PP
               WHERE  PP.TipoOp = cTipoOpConf
               AND    PP.CodMoneda = PM.CodMoneda
               AND    PP.CodProd = PM.CodProd;
            EXCEPTION
               WHEN NO_DATA_FOUND THEN
                  nPrimaMinima  := 0;
            END;
            IF nMontoOperEmi < nPrimaMinima THEN
               FOR MC IN COBERT_Q LOOP
                  nMtoAjuste  := 0;
                  -- Ajuste Inicial
                  BEGIN
                     SELECT MC1.PriMinAjuste
                     INTO   nMtoAjuste
                     FROM   OPER_POL OP, RECIBO RE, MOD_COBERT MC1, MOV_PRIMA MP
                     WHERE  MP.IdeMovPrima = MC1.IdeMovPrima
                     AND    MC1.PrimaFactMoneda > 0
                     AND    MC1.IdeMovPrima = RE.IdeMovPrima
                     AND    RE.NumOper = OP.NumOper
                     AND    RE.NumCert = OP.NumCert
                     AND    RE.IdePol = OP.IdePol
                     AND    OP.TipoOp = 'EMI'
                     AND    OP.NumOper = nNumOperEmi
                     AND    OP.IdePol = nIdePol
                     AND    MC1.IdeCobert = MC.IdeCobert;
                  EXCEPTION
                     WHEN NO_DATA_FOUND THEN
                        nMtoAjuste  := 0;
                     WHEN OTHERS THEN
                        RAISE_APPLICATION_ERROR (-20288, PR.MENSAJE ('ABD', 20288, nNumOperEmi, NULL, NULL));
                  END;
                  BEGIN
                     UPDATE MOD_COBERT
                     SET PriMinAjuste = NVL (nMtoAjuste, 0),
                         PriFactOrig = MC.PrimaFactMoneda,
                         PrimaFactMoneda = MC.PrimaFactMoneda + NVL (nMtoAjuste, 0),
                         PrimaMovFactor = MC.PrimaFactMoneda + NVL (nMtoAjuste, 0)
                     WHERE  IdeCobert = MC.IdeCobert
                     AND    NumMod = MC.NumMod;
                  EXCEPTION
                     WHEN OTHERS THEN
                        RAISE_APPLICATION_ERROR (-22052, PR.MENSAJE ('ABD', 20288, 'No Actualiz� MOD_COBERT en Ajuste de Prima M�nima', NULL, NULL));
                  END;
                  BEGIN
                     UPDATE TRANS_COBERT
                     SET MtoTransCobert = MC.PrimaFactMoneda + NVL (nMtoAjuste, 0)
                     WHERE  IdeCobert = MC.IdeCobert
                     AND    NumMod = MC.NumMod;
                  EXCEPTION
                     WHEN OTHERS THEN
                        RAISE_APPLICATION_ERROR (-22052,
                                                 PR.MENSAJE ('ABD', 20288, 'No Actualiz� TRANS_COBERT en Ajuste de Prima M�nima', NULL, NULL));
                  END;
                  BEGIN
                     UPDATE MOV_PRIMA
                     SET MtoMoneda = MtoMoneda + NVL (nMtoAjuste, 0),
                         MtoLocal = MtoLocal + (NVL (nMtoAjuste, 0) * MC.TasaCambio),
                         MtoComMoneda = (MtoMoneda + NVL (nMtoAjuste, 0)) * PorcCom / 100,
                         MtoComLocal = (MtoLocal + (NVL (nMtoAjuste, 0) * MC.TasaCambio)) * PorcCom / 100
                     WHERE  IdeMovPrima = MC.IdeMovPrima;
                  EXCEPTION
                     WHEN OTHERS THEN
                        RAISE_APPLICATION_ERROR (-22052, PR.MENSAJE ('ABD', 20288, 'No Actualiz� MOV_PRIMA en Ajuste de Prima M�nima', NULL, NULL));
                  END;
                  BEGIN
                     UPDATE RECIBO
                     SET MtoMoneda = MtoMoneda + NVL (nMtoAjuste, 0),
                         MtoLocal = MtoLocal + (NVL (nMtoAjuste, 0) * MC.TasaCambio),
                         MtoComMoneda = (MtoMoneda + NVL (nMtoAjuste, 0)) * PorcCom / 100,
                         MtoComLocal = (MtoLocal + (NVL (nMtoAjuste, 0) * MC.TasaCambio)) * PorcCom / 100
                     WHERE  IdeMovPrima = MC.IdeMovPrima;
                  EXCEPTION
                     WHEN OTHERS THEN
                        RAISE_APPLICATION_ERROR (-22052, PR.MENSAJE ('ABD', 20288, 'No Actualiz� RECIBO en Ajuste de Prima M�nima', NULL, NULL));
                  END;
               END LOOP;
               FOR GR IN GEN_RENG_Q LOOP
                  BEGIN
                     DELETE RENG_MP
                     WHERE  IdeMovPrima = GR.IdeMovPrima;
                  EXCEPTION
                     WHEN OTHERS THEN
                        RAISE_APPLICATION_ERROR (-22052, PR.MENSAJE ('ABD', 20288, 'No Elimin� RENG_MP en Ajuste de Prima M�nima', NULL, NULL));
                  END;
                  nMtoPrima  := PR_MOV_PRIMA.GENERAR_RENGLONES (GR.IdeMovPrima);
               END LOOP;
            END IF;
         END LOOP;
      END IF;
      FOR AP IN ACT_OPER_Q LOOP
         BEGIN
            UPDATE OPER_POL
            SET MtoOper = AP.TotMtoMoneda
            WHERE  IdePol = AP.IdePol
            AND    NumCert = AP.NumCert
            AND    NumOper = nNumOper;
         EXCEPTION
            WHEN OTHERS THEN
               RAISE_APPLICATION_ERROR (-22052, PR.MENSAJE ('ABD', 20288, 'No Actualiz� OPER_POL en Ajuste de Prima M�nima', NULL, NULL));
         END;
      END LOOP;
      END LOOP; --  BBVA 14/03/2018  REDMINE 23276 SII-GROUP
   END VALIDA_PRIMA_MINIMA;
   /*
    Nombre     : ES_FACTURACION_VENCIDA
    Objetivo   : Funci�n que determina si una p�liza esta en facturaci�n vencida o anticipada. Si retorna TRUE es facturaci�n
                 vencida, si retorna FALSE entonces es facturaci�n anticipada
    Parametros : nIdePol --> Identificador unico de p�liza
    Autor      : Javier Asmat
    Fecha      : 08.02.2005
    Especific. : Defect 1168
    Log de Cambios:
    Fecha        Autor             Descripci�n
    --------------------------------------------------------------------------------------------------------------------------------
    DD/MM/YYYY   XXXXXXXXXXXX      XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    --------------------------------------------------------------------------------------------------------------------------------
   */
   FUNCTION ES_FACTURACION_VENCIDA (nIdePol POLIZA.IdePol%TYPE)
      RETURN BOOLEAN IS
      --
      cIndPolAntVenc   POLIZA.IndPolAntVenc%TYPE;
   BEGIN
      SELECT NVL (IndPolAntVenc, 'A')
      INTO   cIndPolAntVenc
      FROM   POLIZA
      WHERE  IdePol = nIdePol;
      --
      RETURN (cIndPolAntVenc = 'V');
   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         RAISE_APPLICATION_ERROR (-20205, PR.MENSAJE ('ABD', 20205, 'p�liza ' || nIdePol, 'POLIZA', NULL));
      WHEN TOO_MANY_ROWS THEN
         RAISE_APPLICATION_ERROR (-20341, PR.MENSAJE ('ABD', 20341, 'p�liza ' || nIdePol, 'POLIZA', NULL));
      WHEN OTHERS THEN
         RAISE_APPLICATION_ERROR (-20288, PR.MENSAJE ('ABD', 20288, 'POLIZA:' || SQLERRM, NULL, NULL));
   END ES_FACTURACION_VENCIDA;
   /*
    Nombre     : ES_PRIMER_MOVIMIENTO
    Objetivo   : Funci�n que determina si la transacci�n actual es el primer movimiento en la p�liza. Retornara TRUE de ser
                 asi, en caso contrario retornara FALSE
    Parametros : nIdePol  --> Identificador unico de p�liza
    Autor      : Javier Asmat
    Fecha      : 08.02.2005
    Especific. : Defect 1168
    Log de Cambios:
    Fecha        Autor             Descripci�n
    --------------------------------------------------------------------------------------------------------------------------------
    15/03/2005   Javier Asmat      Se elimina el parametro nNumCert pues se debe validar para toda la p�liza y no por certificado
    --------------------------------------------------------------------------------------------------------------------------------
   */
   FUNCTION ES_PRIMER_MOVIMIENTO (nIdePol POLIZA.IdePol%TYPE)
      RETURN BOOLEAN IS
      --
      cEsPrimera     VARCHAR2 (2);
      nCantSinMov    NUMBER (5);
      nCantEmision   NUMBER (5);
   BEGIN
      --Determinamos si es la primera facturaci�n, esta sera evaluada junto con el valor de la variable cIndPolAntVenc
      BEGIN
         SELECT COUNT (1)
         INTO   nCantSinMov
         FROM   OPER_POL
         WHERE  IdePol = nIdePol
         AND    TipoOp = 'ESM';
      END;
      --
      BEGIN
         SELECT COUNT (1)
         INTO   nCantEmision
         FROM   OPER_POL
         WHERE  IdePol = nIdePol
         AND    TipoOp = 'EMI'
         AND    IndAnul = 'N'
         AND    FecAnul IS NULL
         AND    NumOperAnu IS NULL;
      END;
      --Evaluamos las cantidades, si ambas son cero, es el primer movimiento en ser generado
      IF     nCantSinMov = 0
         AND nCantEmision = 0 THEN
         cEsPrimera  := 'SI';
      ELSE
         cEsPrimera  := 'NO';
      END IF;
      --
      RETURN (cEsPrimera = 'SI');
   END ES_PRIMER_MOVIMIENTO;
   /*
    Nombre     : TIPO_OPER
    Objetivo   : Funci�n que determina el Tipo de operaci�n que se esta
                 Realizando para la Generaci�n de los Recargos y/o Descuentos
                 Autom�ticos, por los momentos ser� usada en EN PR_POLIZA.GENERAR_MOV
    Parametros : nIdePol  --> Identificador unico de p�liza
                 nNumCert --> Numero de certificado
                 nNumOper N�mero de Operaci�n que se esta Ejecutando
    Autor      : Sergio Sotelo
    Fecha      : 20.02.2005
    Especific. : Defect 1168
    Log de Cambios:
    Fecha        Autor             Descripci�n
    --------------------------------------------------------------------------------------------------------------------------------
    DD/MM/YYYY   XXXXXXXXXXXX      XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    --------------------------------------------------------------------------------------------------------------------------------
   */
   FUNCTION TIPO_OPER (nIdePol POLIZA.IdePol%TYPE, nNumCert CERTIFICADO.NumCert%TYPE, nNumOper OPER_POL.NumOper%TYPE)
      RETURN VARCHAR2 IS
      cTipoOper   VARCHAR2 (1)           := 'X';
      cTipoOp     OPER_POL.TipoOp%TYPE;
   BEGIN
      BEGIN
         SELECT 'MOD'
         INTO   cTipoOp
         FROM   OPER_POL
         WHERE  IndAnul = 'N'
         AND    NumOper =
                      (SELECT MAX (NumOper)
                       FROM   OPER_POL
                       WHERE  IdePol = nIdePol
                       AND    NumCert = nNumCert
                       AND    NumOPER < nNumOper
                       AND    FecAnul IS NULL
                       AND    IndAnul = 'N'
                       AND    TipoOP = 'EMI');
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            cTipoOp  := 'EMI';
         WHEN TOO_MANY_ROWS THEN
            cTipoOp  := 'MOD';
      END;
      IF NVL (PR_POLIZA.nNumRen, 0) > 1 THEN
         SELECT DECODE (cTipoOp, 'EMI', 'R', 'MOD', 'V', 'X')
         INTO   cTipoOper
         FROM   SYS.DUAL;
      ELSE
         SELECT DECODE (cTipoOp, 'EMI', 'E', 'MOD', 'M', 'X')
         INTO   cTipoOper
         FROM   SYS.DUAL;
      END IF;
      RETURN (cTipoOper);
   END TIPO_OPER;
   /*
    Nombre     : ASIGNA_TIPO_REHAB
    Objetivo   : Rutina que asignara a la variable del package cIndTipoRehab los valores T o A, si se recibe cualquier otro valor
                 como parametro, por default se asumira una rehabilitaci�n administrativa, por lo mismo se asignara el valor A
    Parametros : cValTipoRehab --> Valor a asignar a la variable del package
    Autor      : Javier Asmat
    Fecha      : 01.03.2005
    Especific. : ESVI032
    Log de Cambios:
    Fecha        Autor             Descripci�n
    --------------------------------------------------------------------------------------------------------------------------------
    DD/MM/YYYY   XXXXXXXXXXXX      XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    --------------------------------------------------------------------------------------------------------------------------------
   */
   PROCEDURE ASIGNA_TIPO_REHAB (cValTipoRehab VARCHAR2) IS
   BEGIN
      IF cValTipoRehab NOT IN ('T', 'A') THEN
         PR_POLIZA.cIndTipoRehab  := 'A';
      ELSE
         PR_POLIZA.cIndTipoRehab  := cValTipoRehab;
      END IF;
   END ASIGNA_TIPO_REHAB;
   /*
    Nombre     : ES_REHAB_TECNICA
    Objetivo   : Funci�n que retornara TRUE si la rehailitaci�n es T�cnica, en caso contrario retornara FALSE
    Parametros : Ninguno
    Autor      : Javier Asmat
    Fecha      : 01.03.2005
    Especific. : ESVI032
    Log de Cambios:
    Fecha        Autor             Descripci�n
    --------------------------------------------------------------------------------------------------------------------------------
    DD/MM/YYYY   XXXXXXXXXXXX      XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    --------------------------------------------------------------------------------------------------------------------------------
   */
   FUNCTION ES_REHAB_TECNICA
      RETURN BOOLEAN IS
   BEGIN
      RETURN (PR_POLIZA.cIndTipoRehab = 'T');
   END ES_REHAB_TECNICA;
   --
   PROCEDURE CAMBIO_FORMA_PAGO (
      nIdePol          POLIZA.IdePol%TYPE,
      cCodFormPago     POLIZA.CodFormPago%TYPE,
      dFecIniVig       POLIZA.FecIniVig%TYPE,
      dFecFinVig       POLIZA.FecFinVig%TYPE) IS
      nMeses   NUMBER;
      cCodFormaPago     POLIZA.CodFormPago%TYPE;
   BEGIN
      -- MOVIMIENTOS DE COBERTURAS --
      cCodFormaPago := cCodFormPago;
       IF     PR_POLIZA.ES_FACTURACION_VENCIDA (nIdePol)
         AND PR_POLIZA.ES_PRIMER_MOVIMIENTO (nIdePol) THEN
         cCodFormaPago := 'O';
      END IF;
      BEGIN
         nMeses  := PR.MESES_FORMA_PAGO (cCodFormPago);
         UPDATE MOD_COBERT
         SET FecIniValid = GREATEST (FecIniValid, dFecIniVig),
             FecFinValid = DECODE (cCodFormaPago, 'A', dFecFinVig, 'E', dFecFinVig, 'O', dFecIniVig,  LEAST (ADD_MONTHS (FecIniValid, nMeses), dFecFinVig))
         WHERE  StsModCobert IN ('INC', 'VAL')
         AND    IdePol = nIdePol;
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, 'Error actualizando MOD_COBERT: ' || SQLERRM);
      END;
   END CAMBIO_FORMA_PAGO;
   --
   --///****///-- {Mod 28/06/2005}
   PROCEDURE CAMBIAR_FEC_TRG (nIdePol POLIZA.IdePol%TYPE, dFecIniVig POLIZA.FecINIVIG%TYPE, dFecFinVig POLIZA.FecFINVIG%TYPE) IS
   BEGIN
      -- CERTIFICADOS --
      BEGIN
         UPDATE CERTIFICADO C
         --- Si no existia antes, tomara la nueva fecha de inicio
         SET C.FecIng = (SELECT NVL (MAX (CE.FecIng), dFecIniVig)
                         FROM   CERTIFICADO CE
                         WHERE  CE.NumCert = C.NumCert),
             C.FecFin = dFecFinVig
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, 'Error actualizando CERTIFICADO: ' || SQLERRM);
      END;
      -- CERTIFICADO DEL RAMO --
      BEGIN
         UPDATE CERT_RAMO
         SET FecIniValid = dFecIniVig,
             FecFinValid = dFecFinVig
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, 'Error actualizando CERT_RAMO: ' || SQLERRM);
      END;
      -- COBERTURAS DEL CERTIFICADO --
      BEGIN
         UPDATE COBERT_CERT
         SET FecIniValid = dFecIniVig,
             FecFinValid = dFecFinVig
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, 'Error actualizando COBERT_CERT: ' || SQLERRM);
      END;
      -- COBERTURAS DEL ASEGURADO --
      BEGIN
         UPDATE COBERT_ASEG
         SET FecIniValid = dFecIniVig,
             FecFinValid = dFecFinVig
         WHERE  IdeAseg IN (SELECT IdeAseg
                            FROM   ASEGURADO
                            WHERE  IdePol = nIdePol);
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, 'Error actualizando COBERT_ASEG: ' || SQLERRM);
      END;
      -- BIENES DEL CERTIFICADO --
      BEGIN
         UPDATE BIEN_CERT
         SET FecIniValid = dFecIniVig,
             FecFinValid = dFecFinVig
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, 'Error actualizando BIEN_CERT: ' || SQLERRM);
      END;
      -- COBERTURAS DEL BIEN  --
      BEGIN
         UPDATE COBERT_BIEN
         SET FecIniValid = dFecIniVig,
             FecFinValid = dFecFinVig
         WHERE  IdeBien IN (SELECT IdeBien
                            FROM   BIEN_CERT
                            WHERE  IdePol = nIdePol);
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, 'Error actualizando COBERT_BIEN: ' || SQLERRM);
      END;
      -- MOVIMIENTOS DE LAS COBERTURAS --
      BEGIN
         UPDATE MOD_COBERT
         SET FecIniValid = dFecIniVig,
             FecFinValid = dFecFinVig
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, 'Error actualizando MOD_COBERT: ' || SQLERRM);
      END;
      -- ESTADISTICAS DEL CERTIDFICADO RAMO --
      BEGIN
         UPDATE EST_CERT
         SET FecIniValid = dFecIniVig,
             FecFinVAlid = dFecFinVig
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, 'Error actualizando EST_CERT: ' || SQLERRM);
      END;
      -- ANEXO DEL CERTIDFICADO RAMO --
      BEGIN
         UPDATE ANEXO_CERT
         SET FecIniValid = dFecIniVig,
             FecFinValid = dFecFinVig
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, 'Error actualizando ANEXO_CERT: ' || SQLERRM);
      END;
      -- ANEXO POLIZA --
      BEGIN
         UPDATE ANEXO_POL
         SET FecIniValid = dFecIniVig,
             FecFinValid = dFecFinVig
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, 'Error actualizando ANEXO_POL: ' || SQLERRM);
      END;
      --CERTIFICADOS DEL VEHICULO --
      BEGIN
         UPDATE CERT_VEH
         SET FecIniVig = dFecIniVig,
             FecFinVig = dFecFinVig
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, 'Error actualizando CERT_VEH: ' || SQLERRM);
      END;
      --CLASULAS DEL CERTIFICADO --
      BEGIN
         UPDATE CLAU_CERT
         SET FecIniValid = dFecIniVig,
             FecFinValid = dFecFinVig
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, 'Error actualizando CLAU_CERT: ' || SQLERRM);
      END;
      --
      BEGIN
         UPDATE MOD_RECA_DCTO_CERTIF
         SET FecIniValid = dFecIniVig,
             FecFinValid = dFecFinVig
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, 'Error actualizando MOD_RECA_DCTO_CERTIF: ' || SQLERRM);
      END;
      -- COASEGURO --
      BEGIN
         UPDATE DIST_COA
         SET FecIniValid = dFecIniVig,
             FecFinValid = dFecFinVig
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, 'Error actualizando DIST_COA: ' || SQLERRM);
      END;
      -- MOVIMIENTO DEL ASEGURADO  --
      BEGIN
         UPDATE MOD_ASEG
         SET FecIniValid = dFecIniVig,
             FecFinValid = dFecFinVig
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, 'Error actualizando MOD_ASEG: ' || SQLERRM);
      END;
      -- RECAR Y DESC DE LA POLIZA --
      BEGIN
         UPDATE RECA_DCTO_POL
         SET FecIniValid = dFecIniVig,
             FecFinValid = dFecFinVig
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, 'Error actualizando RECA_DCTO_POL: ' || SQLERRM);
      END;
      -- RECA_DCTO_CERTIF --
      BEGIN
         UPDATE RECA_DCTO_CERTIF
         SET FecIniValid = dFecIniVig,
             FecFinValid = dFecFinVig
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, 'Error actualizando RECA_DCTO_CERTIF: ' || SQLERRM);
      END;
      -- RECA_DCTO_CERTIF_ASEG --
      BEGIN
         UPDATE RECA_DCTO_CERTIF_ASEG
         SET FecIniValid = dFecIniVig,
             FecFinValid = dFecFinVig
         WHERE  IdeAseg IN (SELECT IdeAseg
                            FROM   ASEGURADO
                            WHERE  IdePol = nIdePol
                            AND    TRUNC (FecING) < TRUNC (dFecIniVig));
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, 'Error actualizando RECA_DCTO_CERTIF_ASEG: ' || SQLERRM);
      END;
      -- RECA_DCTO_CERTIF_BIEN --
      BEGIN
         UPDATE RECA_DCTO_CERTIF_BIEN
         SET FecIniValid = dFecIniVig,
             FecFinValid = dFecFinVig
         WHERE  IdeBien IN (SELECT IdeBien
                            FROM   BIEN_CERT
                            WHERE  IdePol = nIdePol
                            AND    TRUNC (FecINIVALID) < TRUNC (dFecIniVig));
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, 'Error actualizando RECA_DCTO_CERTIF_BIEN: ' || SQLERRM);
      END;
      BEGIN
         UPDATE DEC_TRANSPORTE
         SET FecIniValid = dFecIniVig,
             FecFinValid = dFecFinVig
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, 'Error actualizando DEC_TRANSPORTE: ' || SQLERRM);
      END;
      --
      BEGIN
         UPDATE DEC_GEN
         SET FecIniValid = dFecIniVig,
             FecFinValid = dFecFinVig
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, 'Error actualizando DEC_GEN: ' || SQLERRM);
      END;
      -- ACTUALIZANDO FecHA DE INGRESO --
      BEGIN
         UPDATE ASEGURADO ASE
         SET FecIng =
                (SELECT NVL (MAX (AS1.FecIng), dFecIniVig)
                 FROM   ASEGURADO AS1, CERTIFICADO CE
                 WHERE  CE.IdePol = nIdePol
                 AND    CE.NumCert = ASE.NumCert
                 AND    AS1.IdePol = CE.IdePol
                 AND    AS1.NumCert = ASE.NumCert
                 AND    AS1.CodCli = ASE.CodCli
                 AND    AS1.CodRamoCert = ASE.CodRamoCert)
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, 'Error actualizando ASEGURADO: ' || SQLERRM);
      END;
      --
      BEGIN
         UPDATE BENEFICIARIO BENE
         SET FecING =
                (SELECT NVL (MAX (BE1.FecIng), dFecIniVig)
                 FROM   BENEFICIARIO BE1, CERTIFICADO CE
                 WHERE  CE.IdePol = nIdePol
                 AND    CE.NumCert = BENE.NumCert
                 AND    BE1.IdePol = CE.IdePol
                 AND    BE1.NumCert = BENE.NumCert
                 AND    BE1.TipoId = BENE.TipoId
                 AND    BE1.NumId = BENE.NumId
                 AND    BE1.DvId = BENE.DvId
                 AND    BE1.CodRamoCert = BENE.CodRamoCert)
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, 'Error actualizando BENEFICIARIO: ' || SQLERRM);
      END;
      --
      BEGIN
         UPDATE BENEF_ASEG BEA
         SET FecIng =
                (SELECT NVL (MAX (AS1.FecIng), dFecIniVig)
                 FROM   CERTIFICADO CE, ASEGURADO AS2, ASEGURADO AS1, BENEF_ASEG BE1
                 WHERE  CE.IdePol = nIdePol
                 AND    CE.NumCert = AS1.NumCert
                 AND    AS2.IdePol = AS1.IdePol
                 AND    AS2.NumCert = AS1.NumCert
                 AND    AS2.CodCli = AS1.CodCli
                 AND    AS1.IdeAseg = BEA.IdeAseg
                 AND    BE1.IdeAseg = AS2.IdeAseg
                 AND    BE1.NomBen = BEA.NomBen
                 AND    BE1.NumBen = BEA.NumBen)
         WHERE  IdeAseg IN (SELECT IdeAseg
                            FROM   ASEGURADO
                            WHERE  IdePol = nIdePol);
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, 'Error actualizando BENEF_ASEG: ' || SQLERRM);
      END;
      --
      BEGIN
         UPDATE COND_VEH COV
         SET FecIng =
                (SELECT NVL (MAX (CV1.FecIng), dFecIniVig)
                 FROM   COND_VEH CV1, CERTIFICADO CE
                 WHERE  CE.IdePol = nIdePol
                 AND    CE.NumCert = COV.NumCert
                 AND    CV1.NomCondVeh = COV.NomCondVeh
                 AND    CV1.NumIdCond = COV.NumIdCond
                 AND    CV1.NumCert = COV.NumCert)
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, 'Error actualizando COND_VEH: ' || SQLERRM);
      END;
   --
   END CAMBIAR_FEC_TRG;
   --///****///--
      /*
        Nombre     : ES_MOVIMIENTO_DEL_PERIODO
        Objetivo   : Funci�n que determina si el o los movimientos que se van a activar son producto de una facturaci�n o realmente
                     son movimientos sobre el periodo actualmente facturado. Retornara TRUE si es un movimiento del periodo, en caso
                     contrario retornara FALSE
        Parametros : nIdePol --> Identificador unico de p�liza
        Autor      : Javier Asmat
        Fecha      : 04.07.2005
        Especific. : Defect 3186
        Log de Cambios:
        Fecha        Autor             Descripci�n
      ------------------------------------------------------------------------------------------------------------------------------------
        06/07/2005   Javier Asmat      Se modifica rutina para considerar el movimiento anterior y asi comparar y determinar mejor si el
                                       movimiento realizado es por facturaci�n (Defect 3186)
     07/07/2005   Javier Asmat      Se condiciona para los casos de nuevas inclusiones
     08/07/2005   Javier Asmat      Se condiciona los casos de reversi�n de movimiento
      ------------------------------------------------------------------------------------------------------------------------------------
      */
   FUNCTION ES_MOVIMIENTO_DEL_PERIODO (nIdePol POLIZA.IdePol%TYPE)
      RETURN BOOLEAN IS
      CURSOR c_MovPol IS
         SELECT IdeCobert, NumMod, FecIniValid, FecFinValid
         FROM   MOD_COBERT
         WHERE  IdePol = nIdePol
         AND    StsModCobert = 'INC'
         AND    NVL(INDPRORROGA,'N') = 'N'; -- BBVA SII-GROUP Incidencia #23825 23/08/2018
      --
      nIdeCobert   MOD_COBERT.IdeCobert%TYPE;
      nNumMod      MOD_COBERT.NumMod%TYPE;
      --
      CURSOR c_MovAnt IS
         SELECT StsModCobert
         FROM   MOD_COBERT
         WHERE  IdeCobert = nIdeCobert
         AND    NumMod < nNumMod
         ORDER BY NumMod DESC;
      --
      rMovPol      c_MovPol%ROWTYPE;
      rMovAnt      c_MovAnt%ROWTYPE;
      bEsMovim     BOOLEAN;
   BEGIN
      PR_POLIZA.CARGAR (nIdePol);
      bEsMovim  := FALSE;
      --
      OPEN c_MovPol;
      --
      LOOP
         FETCH  c_MovPol
         INTO   rMovPol;
         EXIT WHEN c_MovPol%NOTFOUND;
         --Asignamos valores necesarios para levantar el nuevo cursor
         nIdeCobert            := rMovPol.IdeCobert;
         nNumMod               := rMovPol.NumMod;
         rMovAnt.StsModCobert  := NULL;
         --<I Defect 3516-3517> Javier Asmat/25.08.2005
         --Se pone en loop para buscar el movimiento anterior que no es REV
         WHILE TRUE LOOP
            --Si el movimiento anterior tiene estado diferente de LIQ, entonces estamos generando un movimiento
            OPEN c_MovAnt;
            FETCH  c_Movant
            INTO   rMovAnt;
            CLOSE c_MovAnt;
            --Si el estado es REV, se trata de una reversi�n por lo que buscamos el estado de el movimiento actual menos 2 para saber si fue
            --la generaci�n del proximo periodo de cobro o un movimiento
            IF rMovAnt.StsModCobert = 'REV' THEN
               nNumMod  := nNumMod - 2;
            ELSE
               EXIT;
            END IF;
         END LOOP;
         --<F Defect 3516-3517>
         --
         IF PR_POLIZA.cIndFact = 'S' THEN
            IF     rMovPol.FecIniValid BETWEEN ADD_MONTHS (PR_POLIZA.dFecUltFact, -1 * (PR.MESES_FORMA_PAGO (PR_POLIZA.cCodFormPago)))
                                           AND PR_POLIZA.dFecUltFact
               AND rMovPol.FecFinValid BETWEEN ADD_MONTHS (PR_POLIZA.dFecUltFact, -1 * (PR.MESES_FORMA_PAGO (PR_POLIZA.cCodFormPago)))
                                           AND PR_POLIZA.dFecUltFact THEN
               bEsMovim  := FALSE;
            ELSE
               bEsMovim  := TRUE;
            END IF;
         ELSE
            IF    rMovAnt.StsModCobert <> 'LIQ'
               OR (    rMovAnt.StsModCobert IS NULL
                   AND PR_POLIZA.cStsPol != 'INC') THEN
               bEsMovim  := TRUE;
            END IF;
         END IF;
      END LOOP;
      --
      CLOSE c_MovPol;
      --
      RETURN (bEsMovim);
   END ES_MOVIMIENTO_DEL_PERIODO;
   /*
     PROCEDURE: ACTUALIZA_MIGRA_REASEGURO
     Objetivo : Actualiza los status de los Reaseguros y Coaseguros
     Realizado: Eduardo Ocando
     Fechas   : 10/07/2005.
   */
   PROCEDURE ACTUALIZA_MIGRA_REASEGURO (nIdePol IN NUMBER, nNumOper NUMBER) IS
      cIndMigrada   RECIBO.IndMigrada%TYPE   := 'N';
      --
      CURSOR RECIBO_C IS
         SELECT Idepol, NumCert, CodRamoCert, IdeMovPrima, NumOper, IdeRec, IndMigrada
         FROM   RECIBO
         WHERE  Idepol = nIdepol
         AND    NumOper = nNumOper;
   BEGIN
      FOR I IN RECIBO_C LOOP
         IF I.IndMigrada = 'S' THEN
            -- Reaseguro Facultativo
            BEGIN
               UPDATE OPER_FACULT
               SET StsOperFacult = 'LIQ',
                   FecOper = SYSDATE
               WHERE  IdeRec = I.IdeRec;
            EXCEPTION
               WHEN OTHERS THEN
                  RAISE_APPLICATION_ERROR (-20288, PR.MENSAJE ('AIF', 22052, 'OPER_FACULT ' || SQLERRM, NULL, NULL));
            END;
            -- Coaseguro DIST_COA_MOV
            BEGIN
               UPDATE DIST_COA_MOV
               SET StsCoa = 'LIQ'
               WHERE  NumOper = I.NumOper
               AND    Idepol = I.IdePol;
            EXCEPTION
               WHEN OTHERS THEN
                  RAISE_APPLICATION_ERROR (-20288, PR.MENSAJE ('AIF', 22052, 'DIST_COA_MOV ' || SQLERRM, NULL, NULL));
            END;
            -- Coaseguro LIQ_COASEGURO
            BEGIN
               UPDATE LIQ_COASEGURO
               SET StsLiq = 'LIQ',
                   FecStsLiq = SYSDATE
               WHERE  NumOper = I.NumOper;
            EXCEPTION
               WHEN OTHERS THEN
                  RAISE_APPLICATION_ERROR (-20288, PR.MENSAJE ('AIF', 22052, 'LIQ_COASEGURO ' || SQLERRM, NULL, NULL));
            END;
         END IF;
      END LOOP;
   END;
   -- Fin del Pk ACTUALIZA_MIGRA_REASEGURO
   FUNCTION FechaUltSiniestro (nIdePol IN SINIESTRO.IdePol%TYPE, nNumCert IN SINIESTRO.NumCert%TYPE)
      RETURN SINIESTRO.FecOcurr%TYPE IS
      fOcurrSin   SINIESTRO.FecOcurr%TYPE;
   BEGIN
      SELECT MIN (FecOcurr)
      INTO   fOcurrSin
      FROM   SINIESTRO
      WHERE  IdePol = nIdePol
      AND    NumCert = NVL (nNumCert, NumCert)
      AND    StsSin NOT IN ('ANU','DEC','VAL');
      RETURN (fOcurrSin);
   EXCEPTION
      WHEN OTHERS THEN
         RETURN (NULL);
   END FechaUltSiniestro;
   FUNCTION FechaUltModifPoliza (nIdePol IN SINIESTRO.IdePol%TYPE, nNumCert IN SINIESTRO.NumCert%TYPE)
      RETURN SINIESTRO.FecOcurr%TYPE IS
      fUltModif   MOD_COBERT.FecIniValid%TYPE;
      nFactCob    NUMBER(8);
   BEGIN
      -- Debe Validarse la Fecha de �ltimo Movimiento solamente cuando NO existe
      -- Documentos Cobrados.  EC - 25/11/2005
      BEGIN
         SELECT NVL(COUNT(*),0)
           INTO nFactCob
           FROM FACTURA F, OPER_POL O
          WHERE F.StsFact = 'COB'
            AND F.NumOper = O.NumOper
            AND O.IdePol  = nIdePol
            AND O.NumCert = NVL(nNumCert,0);
      END;
      IF NVL(nFactCob,0) = 0 THEN
         SELECT MAX (M.FecIniValid)
         INTO   fUltModif
         FROM   MOD_COBERT M, RECIBO R, OPER_POL O, CERTIFICADO C
         WHERE  M.IdePol = nIdePol
         AND    M.NumCert = NVL (nNumCert, M.NumCert)
         AND    M.StsModCobert = 'ACT'
         AND    R.IdeMovPrima = M.IdeMovPrima
         AND    O.NumOper = R.NumOper
         AND    O.NumCert = R.NumCert
         AND    NVL (O.IndAnul, 'N') = 'N'
         AND    C.IdePol = M.IdePol
         AND    C.NumCert = M.NumCert
         AND    C.StsCert NOT IN ('EXC', 'ANU', 'CAD');
         RETURN (fUltModif);
      ELSE
         RETURN (PR_POLIZA.dFecIniVig);
      END IF;
   EXCEPTION
      WHEN OTHERS THEN
         RETURN (NULL);
   END FechaUltModifPoliza;
   FUNCTION NumSiniestrosPend (nIdePol IN SINIESTRO.IdePol%TYPE, nNumCert IN SINIESTRO.NumCert%TYPE)
      RETURN NUMBER IS
      nCantSin   NUMBER;
   BEGIN
      SELECT COUNT (*)
      INTO   nCantSin
      FROM   SINIESTRO
      WHERE  IdePol = nIdePol
      AND    NumCert = NVL (nNumCert, NumCert)
      AND    StsSin NOT IN ('ANU', 'PAG','DEC','VAL','INC'); --BBVA RGS 26/12/2017 redmine 22954 solo se valida que la cobertura tenga siniestro en estado activo o modificado
      RETURN (nCantSin);
   EXCEPTION
      WHEN OTHERS THEN
         RETURN (NULL);
   END NumSiniestrosPend;
   PROCEDURE TOT_POLIZA (
      nIdePol                    IN         POLIZA.IdePol%TYPE,
      nTotValAsegurable          OUT        NUMBER,
      nTotValAsegurado           OUT        NUMBER,
      nTotValAsegurableLucro     OUT        NUMBER,
      nTotValAseguradoLucro      OUT        NUMBER) IS
      nMtoAsegurable          DATOS_PART_POLIZA.ValAsegurable%TYPE   := 0;
      nMtoAsegurado           DATOS_PART_POLIZA.ValAsegurado%TYPE    := 0;
      nMtoLucro               DATOS_PART_LUCRO.UtilBruAnual%TYPE     := 0;
      nNumCert                CERTIFICADO.NumCert%TYPE;
      nIndVarAsegurableCert   NUMBER (22, 2);
      nIndVarAseguradoCert    NUMBER (22, 2);
      nIndVarAsegurablePol    NUMBER (22, 2);
      nIndVarAseguradoPol     NUMBER (22, 2);
      nValAsegurable          NUMBER (22, 2)                         := 0;
      nValAsegurado           NUMBER (22, 2)                         := 0;
      cCodProd                POLIZA.CodProd%TYPE;
      nTotAsegurableLucro     NUMBER (22, 2);
      nTotAseguradoLucro      NUMBER (22, 2);
      nTotSumaAsegCert        NUMBER (22, 2)                         := 0;
      nTotSumaAsegBien        NUMBER (22, 2)                         := 0;
      nSumaAsegCert           NUMBER (22, 2)                         := 0;
      nSumaAsegBien           NUMBER (22, 2)                         := 0;
      CURSOR C_CERTIFICADO IS
         SELECT NumCert
         FROM   CERTIFICADO
         WHERE  IdePol = nIdePol
         ORDER BY NumCert;
      CURSOR CERTRAMO IS
         SELECT CodRamoCert
         FROM   CERT_RAMO
         WHERE  IdePol = nIdePol
         AND    NumCert = nNumCert;
   BEGIN
      nTotValAsegurable       := 0;
      nTotValAsegurado        := 0;
      nTotValAsegurableLucro  := 0;
      nTotValAseguradoLucro   := 0;
      BEGIN
         SELECT CodProd
         INTO   cCodProd
         FROM   POLIZA
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20288, PR.MENSAJE ('ABD', 20288, ' POLIZA ' || SQLERRM, NULL, NULL));
      END;
---------------------------------------------
      BEGIN   --DATPAMUL
         SELECT SUM (ValAsegurable), SUM (ValAsegurado)
         INTO   nMtoAsegurable, nMtoAsegurado
         FROM   DAT_PART_MULTIRIESGO
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            nMtoAsegurable  := 0;
            nMtoAsegurado   := 0;
      END;
      nTotValAsegurable       := nTotValAsegurable + NVL (nMtoAsegurable, 0);
      nTotValAsegurado        := nTotValAsegurado + NVL (nMtoAsegurado, 0);
      nMtoAsegurable          := 0;
      nMtoAsegurado           := 0;
------------------------------------------------
      BEGIN   --DATPARTR
         SELECT SUM (MtoAsegurableTotal), SUM (MtoAseguradoTotal)
         INTO   nMtoAsegurable, nMtoAsegurado
         FROM   DATOS_PART_TR_MONTAJE
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            nMtoAsegurable  := 0;
            nMtoAsegurado   := 0;
      END;
      nTotValAsegurable       := nTotValAsegurable + NVL (nMtoAsegurable, 0);
      nTotValAsegurado        := nTotValAsegurado + NVL (nMtoAsegurado, 0);
      nMtoAsegurable          := 0;
      nMtoAsegurado           := 0;
-------------------------------------------------
      BEGIN   --DATPARCN
         SELECT SUM (MtoAsegurableTotal), SUM (MtoAseguradoTotal)
         INTO   nMtoAsegurable, nMtoAsegurado
         FROM   DATOS_PART_TR_CONSTRUCCION
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            nMtoAsegurable  := 0;
            nMtoAsegurado   := 0;
      END;
      nTotValAsegurable       := nTotValAsegurable + NVL (nMtoAsegurable, 0);
      nTotValAsegurado        := nTotValAsegurado + NVL (nMtoAsegurado, 0);
      nMtoAsegurable          := 0;
      nMtoAsegurado           := 0;
-------------------------------------------------
      BEGIN   --DATPAGEN
         SELECT SUM (ValAsegurable), SUM (ValAsegurado)
         INTO   nMtoAsegurable, nMtoAsegurado
         FROM   DAT_PART_SUMA
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            nMtoAsegurable  := 0;
            nMtoAsegurado   := 0;
      END;
      nTotValAsegurable       := nTotValAsegurable + NVL (nMtoAsegurable, 0);
      nTotValAsegurado        := nTotValAsegurado + NVL (nMtoAsegurado, 0);
      nMtoAsegurable          := 0;
      nMtoAsegurado           := 0;
---------------------------------------------------
      BEGIN   --DATEMBAR
         SELECT SUM (ValAsegurable), SUM (ValAsegurado)
         INTO   nMtoAsegurable, nMtoAsegurado
         FROM   DAT_PART_EMBARCACION
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            nMtoAsegurable  := 0;
            nMtoAsegurado   := 0;
      END;
      nTotValAsegurable       := nTotValAsegurable + NVL (nMtoAsegurable, 0);
      nTotValAsegurado        := nTotValAsegurado + NVL (nMtoAsegurado, 0);
      nMtoAsegurable          := 0;
      nMtoAsegurado           := 0;
---------------------------------------------------
      FOR C IN C_CERTIFICADO LOOP
-----------------
         BEGIN   -- DATMANEJ
            SELECT SUM (MtoAseguradoTotal)
            INTO   nMtoAsegurado
            FROM   DAT_PART_MANEJO
            WHERE  IdePol = nIdePol
            AND    NumCert = C.NumCert;
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               nMtoAsegurable  := 0;
               nMtoAsegurado   := 0;
         END;
         nTotValAsegurable  := nTotValAsegurable + NVL (nMtoAsegurable, 0);
         nTotValAsegurado   := nTotValAsegurado + NVL (nMtoAsegurado, 0);
         nMtoAsegurable     := 0;
         nMtoAsegurado      := 0;
-------------------
         BEGIN   -- DATBANCA
            SELECT SUM (MtoAseguradoTotal)
            INTO   nMtoAsegurado
            FROM   DAT_PART_BANCARIO
            WHERE  IdePol = nIdePol
            AND    NumCert = C.NumCert;
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               nMtoAsegurable  := 0;
               nMtoAsegurado   := 0;
         END;
         nTotValAsegurable  := nTotValAsegurable + NVL (nMtoAsegurable, 0);
         nTotValAsegurado   := nTotValAsegurado + NVL (nMtoAsegurado, 0);
         nMtoAsegurable     := 0;
         nMtoAsegurado      := 0;
-------------------
         BEGIN   -- DATRACLI
            SELECT SUM (Mtoaseguradoanual)
            INTO   nMtoAsegurado
            FROM   DAT_PART_CLINICA
            WHERE  IdePol = nIdePol
            AND    NumCert = C.NumCert;
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               nMtoAsegurable  := 0;
               nMtoAsegurado   := 0;
         END;
         nTotValAsegurable  := nTotValAsegurable + NVL (nMtoAsegurable, 0);
         nTotValAsegurado   := nTotValAsegurado + NVL (nMtoAsegurado, 0);
         nMtoAsegurable     := 0;
         nMtoAsegurado      := 0;
------------------
         BEGIN   -- DATPARPE
            SELECT SUM (LimiVigencia)
            INTO   nMtoAsegurado
            FROM   DAT_PART_PERSONAL
            WHERE  IdePol = nIdePol
            AND    NumCert = C.NumCert;
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               nMtoAsegurable  := 0;
               nMtoAsegurado   := 0;
         END;
         nTotValAsegurable  := nTotValAsegurable + NVL (nMtoAsegurable, 0);
         nTotValAsegurado   := nTotValAsegurado + NVL (nMtoAsegurado, 0);
         nMtoAsegurable     := 0;
         nMtoAsegurado      := 0;
-------------------
         BEGIN   --DATRAINT,DATRAPOR,DATRAGAS
            SELECT SUM (MtoAseguradoAnual)
            INTO   nMtoAsegurado
            FROM   DAT_PART_RC_PROFESIONAL
            WHERE  IdePol = nIdePol
            AND    NumCert = C.NumCert;
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               nMtoAsegurable  := 0;
               nMtoAsegurado   := 0;
         END;
         nTotValAsegurable  := nTotValAsegurable + NVL (nMtoAsegurable, 0);
         nTotValAsegurado   := nTotValAsegurado + NVL (nMtoAsegurado, 0);
         nMtoAsegurable     := 0;
         nMtoAsegurado      := 0;
--------------------
--LIMITE POR DESPACHO
         BEGIN   --DATRAVAL,DATRATER,DATRAMER
            SELECT MAX (LimDespacho)
            INTO   nMtoAsegurado
            FROM   DAT_PART_MERCANCIA_LIMITE
            WHERE  IdePol = nIdePol
            AND    NumCert = C.NumCert;
         EXCEPTION
            WHEN NO_DATA_FOUND THEN
               nMtoAsegurable  := 0;
               nMtoAsegurado   := 0;
         END;
         nTotValAsegurable  := nTotValAsegurable + NVL (nMtoAsegurable, 0);
         nTotValAsegurado   := nTotValAsegurado + NVL (nMtoAsegurado, 0);
         nMtoAsegurable     := 0;
         nMtoAsegurado      := 0;
         EXIT;   -- Solo es para calculo del primer Certificado
      END LOOP;
      -- Indice Variable
      nMtoAsegurable          := 0;
      nMtoAsegurado           := 0;
------------------------------
      nIndVarAsegurablePol    := 0;
      nIndVarAseguradoPol     := 0;
      nTotAsegurableLucro     := 0;
      nTotAseguradoLucro      := 0;
      FOR CERT IN C_CERTIFICADO LOOP
         nNumCert               := CERT.NumCert;
         -- Monto Asegurable y Asegurado para Indice Variable.
         nTotSumaAsegCert       := 0;
         nTotSumaAsegBien       := 0;
         nIndVarAsegurableCert  := 0;
         nIndVarAseguradoCert   := 0;
         FOR CRAM IN CERTRAMO LOOP
            nValAsegurable         := 0;
            nValAsegurado          := 0;
            nSumaAsegCert          := 0;
            nSumaAsegBien          := 0;
            PR_CERT_RAMO.TOT_INDICE_VARIABLE (cCodProd, nIdePol, nNumCert, CRAM.CodRamoCert, nValAsegurable, nValAsegurado);
            nIndVarAsegurableCert  := nIndVarAsegurableCert + nValAsegurable;
            nIndVarAseguradoCert   := nIndVarAseguradoCert + nValAsegurado;
            -- Lucro Cesante
            BEGIN
               SELECT NVL (SUM (CE.SumaAsegMoneda), 0)
               INTO   nSumaAsegCert
               FROM   COBERT_CERT CE, COBERT_PLAN_PROD CR
               WHERE  CE.CodRamoCert = CR.CodRamoPlan
               AND    CE.CodCobert = CR.CodCobert
               AND    CE.CodPlan = CR.CodPlan
               AND    CE.RevPlan = CR.RevPlan
               AND    CR.CodProd = cCodProd
               AND    CE.IdePol = nIdePol
               AND    CE.NumCert = nNumCert
               AND    CE.CodRamoCert = CRAM.CodRamoCert
               AND    CR.FormaTarifa = 'TARLUCRO'
               AND    CR.IndAcumulaSumaPoliza = 'S';
            EXCEPTION
               WHEN NO_DATA_FOUND THEN
                  nSumaAsegCert  := 0;
            END;
            BEGIN
               SELECT NVL (SUM (CB.SumaAsegMoneda), 0)
               INTO   nSumaAsegBien
               FROM   BIEN_CERT BI, COBERT_BIEN CB, COBERT_PLAN_PROD CR
               WHERE  BI.CodRamoCert = CR.CodRamoPlan
               AND    BI.IdeBien = CB.IdeBien
               AND    CB.CodCobert = CR.CodCobert
               AND    CB.CodPlan = CR.CodPlan
               AND    CB.RevPlan = CR.RevPlan
               AND    CR.CodProd = cCodProd
               AND    BI.IdePol = nIdePol
               AND    BI.NumCert = nNumCert
               AND    BI.CodRamoCert = CRAM.CodRamoCert
               AND    CR.FormaTarifa = 'TARLUCRO'
               AND    CR.IndAcumulaSumaPoliza = 'S';
            EXCEPTION
               WHEN NO_DATA_FOUND THEN
                  nSumaAsegBien  := 0;
            END;
            nTotSumaAsegCert       := nTotSumaAsegCert + nSumaAsegCert;
            nTotSumaAsegBien       := nTotSumaAsegBien + nSumaAsegBien;
         END LOOP;
         nIndVarAsegurablePol   := nIndVarAsegurablePol + nIndVarAsegurableCert;
         nIndVarAseguradoPol    := nIndVarAseguradoPol + nIndVarAseguradoCert;
         nTotAsegurableLucro    := nTotAsegurableLucro + (nTotSumaAsegCert + nTotSumaAsegBien);
         nTotAseguradoLucro     := nTotAsegurableLucro;
      END LOOP;
      nMtoAsegurable          := nIndVarAsegurablePol;
      nMtoAsegurado           := nIndVarAseguradoPol;
-----------------------------------------------
      nTotValAsegurable       := NVL (nTotValAsegurable, 0) + NVL (nMtoAsegurable, 0);
      nTotValAsegurado        := NVL (nTotValAsegurado, 0) + NVL (nMtoAsegurado, 0);
      nTotValAsegurableLucro  := nTotAsegurableLucro;
      nTotValAseguradoLucro   := nTotValAsegurableLucro;
   -- Lucro Cesante
   /*BEGIN
     SELECT SUM(NVL(UtilBruAnual,0))
     INTO   nMtoLucro
     FROM   DATOS_PART_LUCRO
     WHERE  Idepol      = nIdePol;
   EXCEPTION
     WHEN NO_DATA_FOUND THEN nMtoAsegurable:= 0;nMtoAsegurado := 0;
   END;
   IF nMtoLucro  > 0  THEN
    nTotValAsegurableLucro:= nMtoLucro;
    nTotValAseguradoLucro := nMtoLucro;
   END IF;*/
   END TOT_POLIZA;
   /*
    Nombre     : FACT_COMPLEMENTO
    Objetivo   : Rutina para generar facturaci�n complementaria en una p�liza determinada
    Parametros : nIdePol      --> Identificador unico de p�liza
                 nNumCert     --> Numero de certificado afectado, si fuera nulo se afectara a todos los certificados de la p�liza
                 dFecMov  --> Fecha a partir de la cual se generara el movimiento
                 nMtoFactcomp --> Monto a ser complementado
    Autor      : Javier Asmat
    Fecha      : 14.09.2005
    Especific. : Defect 4110
    Log de Cambios:
    Fecha        Autor             Descripci�n
    --------------------------------------------------------------------------------------------------------------------------------
    DD/MM/YYYY   XXXXXXXXXXXX      XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    --------------------------------------------------------------------------------------------------------------------------------
   */
   PROCEDURE FACT_COMPLEMENTO (
      nIdePol          POLIZA.IdePol%TYPE,
      nNumCert         CERTIFICADO.NumCert%TYPE,
      dFecMov          DATE,
      nMtoFactComp     MOD_COBERT.PrimaMoneda%TYPE) IS
      --Cursores
      CURSOR c_CertR IS
         SELECT NumCert, CodPlan, RevPlan, CodRamoCert
         FROM   CERT_RAMO
         WHERE  IdePol = nIdePol
         AND    NumCert = NVL (nNumCert, NumCert)
         ORDER BY NumCert;
      --
      rCertR              c_CertR%ROWTYPE;
      --
      CURSOR c_Cobert IS
         SELECT MC.IdeCobert, MAX (MC.NumMod) NumMod
         FROM   MOD_COBERT MC
         WHERE  MC.IdePol = nIdePol
         AND    MC.NumCert = rCertR.NumCert
         AND    MC.CodRamoCert = rCertR.CodRamoCert
         AND    MC.StsModCobert NOT IN ('ANU', 'EXC', 'REV')
         AND    MC.IndFactComp = 'N'
         AND    EXISTS (SELECT 1
                        FROM   POLIZA P
                        WHERE  P.IdePol = MC.IdePol
                        AND    NVL (P.IndAjusteBlanket, 'N') = 'N')
         GROUP BY IdeCobert
         UNION
         SELECT MC.IdeCobert, MAX (MC.NumMod) NumMod
         FROM   MOD_COBERT MC
         WHERE  MC.IdePol = nIdePol
         AND    MC.NumCert = rCertR.NumCert
         AND    MC.CodRamoCert = rCertR.CodRamoCert
         AND    MC.StsModCobert NOT IN ('ANU', 'EXC', 'REV')
         AND    MC.IndFactComp = 'N'
         AND    EXISTS (SELECT 1
                        FROM   POLIZA P
                        WHERE  P.IdePol = MC.IdePol
                        AND    NVL (P.IndAjusteBlanket, 'N') = 'S')
         HAVING SUM (MC.PrimaMoneda) > 0
         GROUP BY IdeCobert;
      --
      rCobert             c_Cobert%ROWTYPE;
      nPrimaTotMoneda     MOD_COBERT.PrimaMoneda%TYPE;
      nSumaAsegMoneda     MOD_COBERT.SumaAsegMoneda%TYPE;
      nTasa               MOD_COBERT.Tasa%TYPE;
      nPrimaMoneda        MOD_COBERT.PrimaMoneda%TYPE;
      nFactProp           NUMBER;
      nNumOper            OPER_POL.NumOper%TYPE;
      cIndAjusteBlanket   POLIZA.IndAjusteBlanket%TYPE;
   BEGIN
      OPEN c_CertR;
      LOOP
         FETCH  c_CertR
         INTO   rCertR;
         EXIT WHEN c_CertR%NOTFOUND;
         --Obtenemos el total de prima para en base a esto determinar el porcentaje de proporcion a distribuir entre coberturas
         BEGIN
            SELECT SUM (NVL (PrimaMoneda, 0))
            INTO   nPrimaTotMoneda
            FROM   MOD_COBERT
            WHERE  (IdeCobert, NumMod) IN (
                      SELECT MC.IdeCobert, MAX (MC.NumMod) NumMod
                      FROM   MOD_COBERT MC
                      WHERE  MC.IdePol = nIdePol
                      AND    MC.NumCert = NVL (nNumCert, NumCert)
                      AND    MC.StsModCobert NOT IN ('ANU', 'EXC', 'REV')
                      AND    MC.IndFactComp = 'N'
                      AND    EXISTS (SELECT 1
                                     FROM   POLIZA P
                                     WHERE  P.IdePol = MC.IdePol
                                     AND    NVL (P.IndAjusteBlanket, 'N') = 'N')
                      GROUP BY IdeCobert
                      UNION
                      SELECT MC.IdeCobert, MAX (MC.NumMod) NumMod
                      FROM   MOD_COBERT MC
                      WHERE  MC.IdePol = nIdePol
                      AND    MC.NumCert = NVL (nNumCert, NumCert)
                      AND    MC.StsModCobert NOT IN ('ANU', 'EXC', 'REV')
                      AND    MC.IndFactComp = 'N'
                      AND    EXISTS (SELECT 1
                                     FROM   POLIZA P
                                     WHERE  P.IdePol = MC.IdePol
                                     AND    NVL (P.IndAjusteBlanket, 'N') = 'S')
                      HAVING SUM (MC.PrimaMoneda) > 0
                      GROUP BY IdeCobert);
         END;
         --
         IF nPrimaTotMoneda = 0 THEN
            nPrimaTotMoneda  := 1;
         END IF;
         nFactProp  := 0;
         --
         OPEN c_Cobert;
         LOOP
            FETCH  c_Cobert
            INTO   rCobert;
            EXIT WHEN c_Cobert%NOTFOUND;
            --Obtengo los datos del movimiento actual para generar el movimiento de complemento
            BEGIN
               SELECT PrimaMoneda
               INTO   nPrimaMoneda
               FROM   MOD_COBERT
               WHERE  IdeCobert = rCobert.IdeCobert
               AND    NumMod = rCobert.NumMod;
            EXCEPTION
               WHEN NO_DATA_FOUND THEN
                  nPrimaMoneda  := 0;
               WHEN OTHERS THEN
                  RAISE_APPLICATION_ERROR (-20052, PR.MENSAJE ('ABD', 20052, 'Error en SELECT a MOD_COBERT:' || SQLERRM, NULL, NULL));
            END;
            --Obtenemos la proporci�n a aplicar sobre el nuevo valor para esta cobertura
            nFactProp  := nPrimaMoneda / nPrimaTotMoneda;
            --Generamos el movimiento de cobertura
            PR_MOD_COBERT.GENERAR_LIQ_COMPLEMENTO (rCobert.IdeCobert, rCobert.NumMod, nFactProp, nMtoFactComp, dFecMov);
         END LOOP;
         CLOSE c_Cobert;
      --
      END LOOP;
      CLOSE c_CertR;
      --Guardamos indicador de Ajuste blanket, para actualizarlo y poder activar la p�liza con monto
      BEGIN
         SELECT IndAjusteBlanket
         INTO   cIndAjusteBlanket
         FROM   POLIZA
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            cIndAjusteBlanket  := 'N';
      END;
      --
      UPDATE POLIZA
      SET IndAjusteBlanket = 'N'
      WHERE  IdePol = nIdePol;
      --Activamos la p�liza para generar el respectivo  movimiento
      nNumOper  := PR_POLIZA.ACTIVAR (nIdePol, 'D');
      --Generamos las acreencias, facturas y contabilidad
      PR_ACREENCIA.GENERAR (nNumOper);
      PR_FRACCIONAMIENTO.GENERAR (nNumOper);
      PR_FRACCIONAMIENTO.FACTUR_REHAB (nNumOper);
      --Restauramos el valor original del Ajuste Blanket
      UPDATE POLIZA
      SET IndAjusteBlanket = cIndAjusteBlanket
      WHERE  IdePol = nIdePol;
   EXCEPTION
      WHEN OTHERS THEN
         RAISE_APPLICATION_ERROR (-20052, PR.MENSAJE ('ABD', 20052, 'Error en FACT_COMPLEMENTO:' || SQLERRM, NULL, NULL));
   END FACT_COMPLEMENTO;
   --
   --BBVA-Consis 12/09/2012- Proceso Valida Prima Minima a nivel de Zona Sisimica
  FUNCTION VALAPLIC_PRIMIN_ZONSIS (nIdePol T$_OPER_POL.IdePol%TYPE)
      RETURN NUMBER IS
      nExiste NUMBER;
      cCodPlan  CERT_RAMO.CodPlan%TYPE;
      cRevPlan  CERT_RAMO.CodPlan%TYPE;
  BEGIN
    nExiste := 0;
    PR_POLIZA.CARGAR (nIdepol);
    BEGIN
      SELECT CodPlan, RevPlan
      INTO   cCodPlan, cRevPlan
      FROM   CERT_RAMO
      WHERE  IdePol = nIdepol
      AND    RowNum = 1;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
         cCodPlan := NULL;
         cRevPlan := NULL;
    END;
    BEGIN
      SELECT DISTINCT 1
      INTO   nExiste
      FROM   PRIMAMIN_LOCALIDAD
      WHERE  CodProd = PR_POLIZA.cCodProd
      AND    CodPlan = cCodPlan
      AND    RevPlan = cRevPlan;
    EXCEPTION
         WHEN OTHERS THEN
           nExiste := 0;
    END;
    Return(nExiste);
  END VALAPLIC_PRIMIN_ZONSIS;

   PROCEDURE T$_VALIDA_PRIMA_MINIMA_ZONASIS(nIdePol T$_OPER_POL.IdePol%TYPE, nNumOper T$_OPER_POL.NumOper%TYPE) IS
      /*  Nombre   : T$_VALIDA_PRIMA_MINIMA_CERTF
          Autor    :Consis
          Fec.     : 12/09/2012
          Objetivo : Verifica y Reajusta la P�liza para respetar la Prima M�nima Establecida a nivel de Certificado por Zona Sismica
      */
      nMontoOper          T$_OPER_POL.MtoOper%TYPE;
      cTipoOp             T$_OPER_POL.TipoOp%TYPE;
      nPrimaAjustada      MOD_COBERT.PrimaFactMoneda%TYPE;
      nDiferencia         MOD_COBERT.PrimaFactMoneda%TYPE;
      nDifPrimaOper       MOD_COBERT.PrimaFactMoneda%TYPE;
      nMtoAjuste          MOD_COBERT.PrimaFactMoneda%TYPE;
      nMtoPrima           MOD_COBERT.PrimaFactMoneda%TYPE;
      nAjusteFinal        MOD_COBERT.PrimaFactMoneda%TYPE;
      nAjusteAcum         MOD_COBERT.PrimaFactMoneda%TYPE;
      nMtoAjusteOp        MOD_COBERT.PrimaFactMoneda%TYPE;
      nMtoPrimaPura       MOD_COBERT.PrimaFactMoneda%TYPE;
      nMtoRecaDcto        T$_RENG_MP.MtoReng%TYPE;
      nPrimaMinima        PRIMIN_PROD.Monto%TYPE            := 0;
      nPorcPrima          NUMBER (9, 6);
      nTotCobert          NUMBER (10);
      nCuentaCob          NUMBER (10)                       := 0;
      nPerioPago          NUMBER (2)                        := 1;
      cTipoOpConf         T$_OPER_POL.TipoOp%TYPE;
      --Defect # 2257 y Defect # 3434 Angelica Domenack 25/08/2005
      cTipCobro           DATOS_PART_POLIZA.TipCobro%TYPE;
      cAplicaPrimaMin     VARCHAR2 (1)                      := 'N';
      cAplicDifPerAnual   VARCHAR2 (1)                      := 'N';
      nOperAnt            NUMBER;
      nNumOperEmi         OPER_POL.NumOper%TYPE;
      nMontoOperEmi       OPER_POL.MtoOper%TYPE;
      --<<BBVA-Consis 12/09/2012 - Se Agrega Variables nNumcert para que tenga en cuenta en calculo anivel de certificado
      nNumCert            CERTIFICADO.NUMCERT%TYPE;
      cCodPlan            CERT_RAMO.CodPlan%TYPE;
      cRevPlan            CERT_RAMO.RevPlan%TYPE;
      cCodRamoCert        CERT_RAMO.CodRamoCert%TYPE;
      cZonaSis            primamin_localidad.zonasis%TYPE;
      nPrimaMinimaRamo    PRIMIN_PROD.Monto%TYPE            := 0;
      -->>BBVA-Consis 12/09/2012
      --
      CURSOR PRIMA_MIN_Q IS
         SELECT DISTINCT op.NumCert, OP.TipoOp, P.CodMoneda, P.CodProd  --BBVA-Consis 12/09/2012 - Se Agrega Variable nNumcert para que tenga en cuenta en calculo anivel de certificado
         FROM   POLIZA P, T$_OPER_POL OP
         WHERE  OP.NumOper = nNumOper
         AND    OP.IdePol = P.IdePol
         AND    P.IdePol = nIdePol;
      CURSOR COBERT_Q IS
         SELECT MC.IdeCobert, MC.NumMod, MC.IdeMovPrimaT, MC.PrimaFactMoneda, MP.TasaCambio
         FROM   T$_OPER_POL OP, MOD_COBERT MC, T$_MOV_PRIMA MP
         WHERE  MP.IdeMovPrima = MC.IdeMovPrimaT
         AND    MC.PrimaFactMoneda > 0
         AND    MC.StsModCobert = 'INC'
         AND    MC.NumCert = OP.NumCert
         AND    MC.IdePol = OP.IdePol
         AND    OP.TipoOp = cTipoOp
         AND    OP.NumOper = nNumOper
         AND    OP.IdePol = nIdePol
         AND    OP.NumCert = nNumCert; --BBVA-Consis 12/09/2012 - Se Agrega Variable nNumcert para que tenga en cuenta en calculo anivel de certificado
      CURSOR GEN_RENG_Q IS
         SELECT DISTINCT MC.IdeMovPrimaT
         FROM   T$_OPER_POL OP, MOD_COBERT MC, T$_MOV_PRIMA MP
         WHERE  MP.IdeMovPrima = MC.IdeMovPrimaT
         AND    MC.PrimaFactMoneda > 0
         AND    MC.StsModCobert = 'INC'
         AND    MC.NumCert = OP.NumCert
         AND    MC.IdePol = OP.IdePol
         AND    OP.TipoOp = cTipoOp
         AND    OP.NumOper = nNumOper
         AND    OP.IdePol = nIdePol
         and    OP.NumCert = nNumCert; --BBVA-Consis 12/09/2012 - Se Agrega Variable nNumcert para que tenga en cuenta en calculo anivel de certificado
      CURSOR ACT_OPER_Q IS
         SELECT IdePol, NumCert, NVL (SUM (NVL (MtoMoneda, 0)), 0) TotMtoMoneda
         FROM   T$_RECIBO
         WHERE  NumOper = nNumOper
         GROUP BY IdePol, NumCert;

      --<<BBVA-Consis 12/09/2012 - Se Busca la Prima Minima por Zona Sismica del certificado y Ramo
      CURSOR PRIMA_MIN_ZONA_SIS IS
              select d.zonsis,ce.codplan, ce.revplan, ce.codramocert
              from direc_riesgo_cert d, cert_ramo ce
              where d.idepol = ce.idepol
              and d.numcert = ce.numcert
              and ce.idepol=nIdePol
              and ce.numcert=nNumCert;

   BEGIN
      PR_POLIZA.CARGAR (nIdePol);
      nPerioPago  := TO_NUMBER (PR.BUSCA_LVAL ('CANTREC', PR_POLIZA.cCodFormPago));
      -- Defect # 2257 y Defect # 3434
      -- Tipo de Cobro 001 Vencido y 002 Anticipado. Si es vencido no aplica Prima Minima
      BEGIN
         SELECT TipCobro
         INTO   cTipCobro
         FROM   DATOS_PART_POLIZA
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            cTipCobro  := NULL;
      END;

      IF cTipCobro = '002' THEN
         cAplicaPrimaMin  := 'S';
      ELSE
         cAplicaPrimaMin  := 'N';
      END IF;

      IF cAplicaPrimaMin = 'S' THEN   -- Defect # 2257 y Defect # 3434 Angelica Domenack Aplica Prima Minima
         --
         FOR PM IN PRIMA_MIN_Q LOOP
            cTipoOp  := PM.TipoOp;
            nNumCert := PM.NumCert; --BBVA-Consis 12/09/2012 - Se Agrega Variable nNumcert para que tenga en cuenta en calculo anivel de certificado
            BEGIN
               SELECT NVL (SUM (NVL (MtoOper, 0)), 0)
               INTO   nMontoOper
               FROM   T$_OPER_POL
               WHERE  TipoOp = cTipoOp
               AND    NumOper = nNumOper
               AND    IdePol = nIdePol
               AND    NumCert = nNumCert; --BBVA-Consis 12/09/2012 - Se Agrega Variable nNumcert para que tenga en cuenta en calculo anivel de certificado
            EXCEPTION
               WHEN OTHERS THEN
                  RAISE_APPLICATION_ERROR (-20288, PR.MENSAJE ('ABD', 20288, nNumOper, NULL, NULL));
            END;
            BEGIN
               SELECT NVL (SUM (NVL (MtoReng, 0)), 0)
               INTO   nMtoRecaDcto
               FROM   T$_OPER_POL OP, T$_RECIBO RE, T$_RENG_MP RM
               WHERE  RM.TipoReng = 'RDC'
               AND    RM.IdeMovPrima = RE.IdeMovPrima
               AND    RE.NumOper = OP.NumOper
               AND    RE.NumCert = OP.NumCert
               AND    RE.IdePol = OP.IdePol
               AND    OP.TipoOp = cTipoOp
               AND    OP.NumOper = nNumOper
               AND    OP.IdePol = nIdePol
               and    OP.NumCert = nNumCert; --BBVA-Consis 12/09/2012 - Se Agrega Variable nNumcert para que tenga en cuenta en calculo anivel de certificado
            EXCEPTION
               WHEN OTHERS THEN
                  RAISE_APPLICATION_ERROR (-20288, PR.MENSAJE ('ABD', 20288, nNumOper, NULL, NULL));
            END;
            --<<BBVA-Consis 12/09/2012- Se comenta proceso a nivel de ceetificado no se valida por tipo de operacion
            /*IF PR_POLIZA.cIndFact = 'S' THEN
               cTipoOpConf  := 'EMI';
            ELSE
                -- Defect # 2257 y Defect # 3434 Angelica Domenack Aplica
               -- Cuando Existen mas operacion la configuracion de prima minima poliza
               -- es de tipo MOD Sino 'EMI'
               IF nOperAnt > 0 THEN
                  cTipoOpConf  := 'MOD';
               ELSE
                  cTipoOpConf  := 'EMI';
               END IF;
            END IF;*/
            -->>BBVA-Consis 12/09/2012

            --<<BBVA-Consis 12/09/2012 - Se Busca la Prima Minima por Zona Sismica del certificado
            FOR PZ IN PRIMA_MIN_ZONA_SIS LOOP
              if pz.zonsis is not null then
                begin
                  select NVL(primamin,0) / nPerioPago
                  into nPrimaMinimaRamo
                  from primamin_localidad
                  where codprod=PM.CodProd
                  and codplan = pz.CodPlan
                  and revplan= pz.RevPlan
                  and codramoplan= pz.CodRamoCert
                  and zonasis = pz.zonsis;
                EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                      nPrimaMinima  := 0;
                end;
                nPrimaMinima := nvl(nPrimaMinima,0) + nPrimaMinimaRamo;
              end if;
            END LOOP;
            -->>BBVA-Consis 12/09/2012

            IF nMontoOper < nPrimaMinima THEN
               nDiferencia  := nPrimaMinima - nMontoOper;
               BEGIN
                  SELECT NVL (COUNT (*), 0)
                  INTO   nTotCobert
                  FROM   T$_OPER_POL OP, MOD_COBERT MC
                  WHERE  MC.PrimaFactMoneda > 0
                  AND    MC.StsModCobert = 'INC'
                  AND    MC.NumCert = OP.NumCert
                  AND    MC.IdePol = OP.IdePol
                  AND    OP.TipoOp = cTipoOp
                  AND    OP.NumOper = nNumOper
                  AND    OP.IdePol = nIdePol
                  and    OP.NumCert = nNumCert;  --BBVA-Consis 12/09/2012 - Se Agrega Variable nNumcert para que tenga en cuenta en calculo anivel de certificado
               EXCEPTION
                  WHEN OTHERS THEN
                     RAISE_APPLICATION_ERROR (-20288, PR.MENSAJE ('ABD', 20288, nNumOper, NULL, NULL));
               END;
               IF NVL (nTotCobert, 0) > 0 THEN
                  nAjusteFinal  := 0;
                  FOR MC IN COBERT_Q LOOP
                     nMtoPrimaPura   := NVL (nMontoOper, 0) - NVL (nMtoRecaDcto, 0);
                     nPorcPrima      := MC.PrimaFactMoneda / NVL (nMtoPrimaPura, 0);
                     nMtoAjuste      := NVL (nDiferencia, 0) * NVL (nPorcPrima, 0);
                     nAjusteAcum     := NVL (nAjusteAcum, 0) + NVL (nMtoAjuste, 0);
                     nPrimaAjustada  := MC.PrimaFactMoneda + NVL (nMtoAjuste, 0);
                     nCuentaCob      := NVL (nCuentaCob, 0) + 1;
                     IF NVL (nCuentaCob, 0) = NVL (nTotCobert, 0) THEN
                        IF NVL (nAjusteAcum, 0) <> NVL (nDiferencia, 0) THEN
                           nAjusteFinal    := NVL (nDiferencia, 0) - NVL (nAjusteAcum, 0);
                           nPrimaAjustada  := NVL (nPrimaAjustada, 0) + NVL (nAjusteFinal, 0);
                        END IF;
                     END IF;
                     BEGIN
                        UPDATE MOD_COBERT
                        SET PriMinAjuste = NVL (nMtoAjuste, 0) + NVL (nAjusteFinal, 0),
                            PriFactOrig = PrimaFactMoneda,
                            PrimaFactMoneda = NVL (nPrimaAjustada, 0),
                            PrimaMovFactor = NVL (nPrimaAjustada, 0)
                        WHERE  IdeCobert = MC.IdeCobert
                        AND    NumMod = MC.NumMod;
                     EXCEPTION
                        WHEN OTHERS THEN
                           RAISE_APPLICATION_ERROR (-22052,
                                                    PR.MENSAJE ('ABD', 20288, 'No Actualiz� MOD_COBERT en Ajuste de Prima M�nima', NULL, NULL));
                     END;
                     BEGIN
                        UPDATE T$_TRANS_COBERT
                        SET MtoTransCobert = NVL (nPrimaAjustada, 0)
                        WHERE  IdeCobert = MC.IdeCobert
                        AND    NumMod = MC.NumMod;
                     EXCEPTION
                        WHEN OTHERS THEN
                           RAISE_APPLICATION_ERROR (-22052,
                                                    PR.MENSAJE ('ABD', 20288, 'No Actualiz� T$_TRANS_COBERT en Ajuste de Prima M�nima', NULL, NULL));
                     END;
                     BEGIN
                        UPDATE T$_MOV_PRIMA
                        SET MtoMoneda = MtoMoneda + NVL (nMtoAjuste, 0) + NVL (nAjusteFinal, 0),
                            MtoLocal = MtoLocal + ((NVL (nMtoAjuste, 0) + NVL (nAjusteFinal, 0)) * MC.TasaCambio),
                            MtoComMoneda = (MtoMoneda + NVL (nMtoAjuste, 0) + NVL (nAjusteFinal, 0)) * PorcCom / 100,
                            MtoComLocal = (MtoLocal + ((NVL (nMtoAjuste, 0) + NVL (nAjusteFinal, 0)) * MC.TasaCambio)) * PorcCom / 100
                        WHERE  IdeMovPrima = MC.IdeMovPrimaT;
                     EXCEPTION
                        WHEN OTHERS THEN
                           RAISE_APPLICATION_ERROR (-22052,
                                                    PR.MENSAJE ('ABD', 20288, 'No Actualiz� T$_MOV_PRIMA en Ajuste de Prima M�nima', NULL, NULL));
                     END;
                     BEGIN
                        UPDATE T$_RECIBO
                        SET MtoMoneda = MtoMoneda + NVL (nMtoAjuste, 0) + NVL (nAjusteFinal, 0),
                            MtoLocal = MtoLocal + ((NVL (nMtoAjuste, 0) + NVL (nAjusteFinal, 0)) * MC.TasaCambio),
                            MtoComMoneda = (MtoMoneda + NVL (nMtoAjuste, 0) + NVL (nAjusteFinal, 0)) * PorcCom / 100,
                            MtoComLocal = (MtoLocal + ((NVL (nMtoAjuste, 0) + NVL (nAjusteFinal, 0)) * MC.TasaCambio)) * PorcCom / 100
                        WHERE  IdeMovPrima = MC.IdeMovPrimaT;
                     EXCEPTION
                        WHEN OTHERS THEN
                           RAISE_APPLICATION_ERROR (-22052,
                                                    PR.MENSAJE ('ABD', 20288, 'No Actualiz� T$_RECIBO en Ajuste de Prima M�nima', NULL, NULL));
                     END;
                     nAjusteFinal    := 0;
                  END LOOP;
                  FOR GR IN GEN_RENG_Q LOOP
                     BEGIN
                        DELETE T$_RENG_MP
                        WHERE  IdeMovPrima = GR.IdeMovPrimaT;
                     EXCEPTION
                        WHEN OTHERS THEN
                           RAISE_APPLICATION_ERROR (-22052, PR.MENSAJE ('ABD', 20288, 'No Elimin� T$_RENG_MP en Ajuste de Prima M�nima', NULL, NULL));
                     END;
                     nMtoPrima  := PR_T$_MOV_PRIMA.GENERAR_RENGLONES (GR.IdeMovPrimaT);
                  END LOOP;
               END IF;
            END IF;
         END LOOP;
      END IF;
      FOR AP IN ACT_OPER_Q LOOP
         BEGIN
            UPDATE T$_OPER_POL
            SET MtoOper = AP.TotMtoMoneda
            WHERE  IdePol = AP.IdePol
            AND    NumCert = AP.NumCert
            AND    NumOper = nNumOper;
         EXCEPTION
            WHEN OTHERS THEN
               RAISE_APPLICATION_ERROR (-22052, PR.MENSAJE ('ABD', 20288, 'No Actualiz� T$_OPER_POL en Ajuste de Prima M�nima', NULL, NULL));
         END;
      END LOOP;
   END T$_VALIDA_PRIMA_MINIMA_ZONASIS;

   PROCEDURE VALIDA_PRIMA_MINIMA_ZONASIS (nIdePol OPER_POL.IdePol%TYPE, nNumOper OPER_POL.NumOper%TYPE) IS
      /*  Nombre   : VALIDA_PRIMA_MINIMA
          Autor    :Consis
          Fec.     : 12/09/2012
          Objetivo : Verifica y Reajusta la P�liza para respetar la Prima M�nima Establecida a nivel de Certificado por Zona Sismica
      */
      nMontoOper          OPER_POL.MtoOper%TYPE;
      cTipoOp             OPER_POL.TipoOp%TYPE;
      nPrimaAjustada      MOD_COBERT.PrimaFactMoneda%TYPE;
      nDiferencia         MOD_COBERT.PrimaFactMoneda%TYPE;
      nDifPrimaOper       MOD_COBERT.PrimaFactMoneda%TYPE;
      nMtoAjuste          MOD_COBERT.PrimaFactMoneda%TYPE;
      nMtoPrima           MOD_COBERT.PrimaFactMoneda%TYPE;
      nAjusteFinal        MOD_COBERT.PrimaFactMoneda%TYPE;
      nAjusteAcum         MOD_COBERT.PrimaFactMoneda%TYPE;
      nMtoAjusteOp        MOD_COBERT.PrimaFactMoneda%TYPE;
      nMtoPrimaPura       MOD_COBERT.PrimaFactMoneda%TYPE;
      nPrimaMinima        PRIMIN_PROD.Monto%TYPE            := 0;
      nMtoRecaDcto        RENG_MP.MtoReng%TYPE;
      nPorcPrima          NUMBER (9, 6);
      nTotCobert          NUMBER (10);
      nCuentaCob          NUMBER (10)                       := 0;
      nPerioPago          NUMBER (2)                        := 1;
      cTipoOpConf         T$_OPER_POL.TipoOp%TYPE;
      --Defect # 2257 y Defect # 3434 Angelica Domenack 25/08/2005
      cTipCobro           DATOS_PART_POLIZA.TipCobro%TYPE;
      cAplicaPrimaMin     VARCHAR2 (1)                      := 'N';
      cAplicDifPerAnual   VARCHAR2 (1)                      := 'N';
      nOperAnt            NUMBER;
      nNumOperEmi         OPER_POL.NumOper%TYPE;
      nMontoOperEmi       OPER_POL.MtoOper%TYPE;
      --<<BBVA-Consis 12/09/2012 - Se Agrega Variables nNumcert para que tenga en cuenta en calculo anivel de certificado
      nNumCert            CERTIFICADO.NUMCERT%TYPE;
      cCodPlan            CERT_RAMO.CodPlan%TYPE;
      cRevPlan            CERT_RAMO.RevPlan%TYPE;
      cCodRamoCert        CERT_RAMO.CodRamoCert%TYPE;
      cZonaSis            primamin_localidad.zonasis%TYPE;
      nPrimaMinimaRamo    PRIMIN_PROD.Monto%TYPE            := 0;
      -->>BBVA-Consis 12/09/2012

      --
      CURSOR PRIMA_MIN_Q IS
         SELECT DISTINCT OP.NumCert, OP.TipoOp, P.CodMoneda, P.CodProd  --BBVA-Consis 12/09/2012 - Se Agrega Variable nNumcert para que tenga en cuenta en calculo anivel de certificado
         FROM   POLIZA P, OPER_POL OP
         WHERE  OP.NumOper = nNumOper
         AND    OP.IdePol = P.IdePol
         AND    P.IdePol = nIdePol;
      CURSOR COBERT_Q IS
         SELECT MC.IdeCobert, MC.NumMod, MC.IdeMovPrima, MC.PrimaFactMoneda, MP.TasaCambio
         FROM   OPER_POL OP, RECIBO RE, MOD_COBERT MC, MOV_PRIMA MP
         WHERE  MP.IdeMovPrima = MC.IdeMovPrima
         AND    MC.PrimaFactMoneda > 0
         AND    MC.IdeMovPrima = RE.IdeMovPrima
         AND    RE.NumOper = OP.NumOper
         AND    RE.NumCert = OP.NumCert
         AND    RE.IdePol = OP.IdePol
         AND    OP.TipoOp = cTipoOp
         AND    OP.NumOper = nNumOper
         AND    OP.IdePol = nIdePol
         AND    OP.NumCert = nNumCert; --BBVA-Consis 12/09/2012 - Se Agrega Variable nNumcert para que tenga en cuenta en calculo anivel de certificado
      CURSOR GEN_RENG_Q IS
         SELECT DISTINCT MC.IdeMovPrima
         FROM   OPER_POL OP, RECIBO RE, MOD_COBERT MC
         WHERE  MC.PrimaFactMoneda > 0
         AND    MC.IdeMovPrima = RE.IdeMovPrima
         AND    RE.NumOper = OP.NumOper
         AND    RE.NumCert = OP.NumCert
         AND    RE.IdePol = OP.IdePol
         AND    OP.TipoOp = cTipoOp
         AND    OP.NumOper = nNumOper
         AND    OP.IdePol = nIdePol
         and    OP.NumCert = nNumCert; --BBVA-Consis 12/09/2012 - Se Agrega Variable nNumcert para que tenga en cuenta en calculo anivel de certificado
      CURSOR ACT_OPER_Q IS
         SELECT IdePol, NumCert, SUM (NVL (MtoMoneda, 0)) TotMtoMoneda
         FROM   RECIBO
         WHERE  NumOper = nNumOper
         GROUP BY IdePol, NumCert;

      --<<BBVA-Consis 12/09/2012 - Se Busca la Prima Minima por Zona Sismica del certificado y Ramo
      CURSOR PRIMA_MIN_ZONA_SIS IS
              select d.zonsis,ce.codplan, ce.revplan, ce.codramocert
              from direc_riesgo_cert d, cert_ramo ce
              where d.idepol = ce.idepol
              and d.numcert = ce.numcert
              and ce.idepol=nIdePol
              and ce.numcert=nNumCert;

   BEGIN
      PR_POLIZA.CARGAR (nIdePol);
      nPerioPago  := TO_NUMBER (PR.BUSCA_LVAL ('CANTREC', PR_POLIZA.cCodFormPago));
      -- Defect # 2257 y Defect # 3434
      -- Tipo de Cobro 001 Vencido y 002 Anticipado. Si es vencido no aplica Prima Minima
      /*BEGIN
         SELECT TipCobro
         INTO   cTipCobro
         FROM   DATOS_PART_POLIZA
         WHERE  IdePol = nIdePol;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            cTipCobro  := NULL;
      END;

      IF cTipCobro = '002' THEN
         cAplicaPrimaMin  := 'S';
      ELSE
         cAplicaPrimaMin  := 'N';
      END IF;*/
      --<<BBVA Consis 29/08/2014 - Valida si procuto maneja prima minima por localidad
      BEGIN
         SELECT 'S'
         INTO   cAplicaPrimaMin
         FROM   primamin_localidad PP
         WHERE  PP.CodProd = PR_POLIZA.CCODPROD
         AND ROWNUM = 1;
      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            cAplicaPrimaMin  := 'N';
         WHEN TOO_MANY_ROWS THEN
            cAplicaPrimaMin  := 'S';
      END;
      -->>BBVA Consis 29/08/2014


      IF cAplicaPrimaMin = 'S' THEN   -- Defect # 2257 y Defect # 3434 Angelica Domenack Aplica Prima Minima
         --
         FOR PM IN PRIMA_MIN_Q LOOP
            cTipoOp  := PM.TipoOp;
            nNumCert := PM.NumCert; --BBVA-Consis 12/09/2012 - Se Agrega Variable nNumcert para que tenga en cuenta en calculo anivel de certificado
            BEGIN
               SELECT NVL (SUM (MtoOper), 0)
               INTO   nMontoOper
               FROM   OPER_POL
               WHERE  TipoOp = cTipoOp
               AND    NumOper = nNumOper
               AND    IdePol = nIdePol
               AND    NumCert = nNumCert; --BBVA-Consis 12/09/2012 - Se Agrega Variable nNumcert para que tenga en cuenta en calculo anivel de certificado
            EXCEPTION
               WHEN OTHERS THEN
                  RAISE_APPLICATION_ERROR (-20288, PR.MENSAJE ('ABD', 20288, nNumOper, NULL, NULL));
            END;
            BEGIN
               SELECT NVL (SUM (NVL (MtoReng, 0)), 0)
               INTO   nMtoRecaDcto
               FROM   OPER_POL OP, RECIBO RE, RENG_MP RM
               WHERE  RM.TipoReng = 'RDC'
               AND    RM.IdeMovPrima = RE.IdeMovPrima
               AND    RE.NumOper = OP.NumOper
               AND    RE.NumCert = OP.NumCert
               AND    RE.IdePol = OP.IdePol
               AND    OP.TipoOp = cTipoOp
               AND    OP.NumOper = nNumOper
               AND    OP.IdePol = nIdePol
               and    OP.NumCert = nNumCert; --BBVA-Consis 12/09/2012 - Se Agrega Variable nNumcert para que tenga en cuenta en calculo anivel de certificado
            EXCEPTION
               WHEN OTHERS THEN
                  RAISE_APPLICATION_ERROR (-20288, PR.MENSAJE ('ABD', 20288, nNumOper, NULL, NULL));
            END;
            --<<BBVA-Consis 12/09/2012- Se comenta proceso a nivel de ceetificado no se valida por tipo de operacion
/*            IF PR_POLIZA.cIndFact = 'S' THEN
               cTipoOpConf  := 'EMI';
            ELSE
                -- Defect # 2257 y Defect # 3434 Angelica Domenack Aplica
               -- Cuando Existen mas operacion la configuracion de prima minima poliza
               -- es de tipo MOD Sino 'EMI'
               IF nOperAnt > 1 THEN
                  cTipoOpConf  := 'MOD';
               ELSE
                  cTipoOpConf  := 'EMI';
               END IF;
            END IF;*/
            -->>BBVA-Consis 12/09/2012

            --<<BBVA-Consis 12/09/2012 - Se Busca la Prima Minima por Zona Sismica del certificado
            FOR PZ IN PRIMA_MIN_ZONA_SIS LOOP
              if pz.zonsis is not null then
                begin
                  select NVL(primamin,0) / nPerioPago
                  into nPrimaMinimaRamo
                  from primamin_localidad
                  where codprod=PM.CodProd
                  and codplan = pz.CodPlan
                  and revplan= pz.RevPlan
                  and codramoplan= pz.CodRamoCert
                  and zonasis = pz.zonsis;
                EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                      nPrimaMinima  := 0;
                end;
                nPrimaMinima := nvl(nPrimaMinima,0) + nPrimaMinimaRamo;
              end if;
            END LOOP;
            -->>BBVA-Consis 12/09/2012

            IF nMontoOper < nPrimaMinima THEN
               nDiferencia  := nPrimaMinima - nMontoOper;
               BEGIN
                  SELECT NVL (COUNT (*), 0)
                  INTO   nTotCobert
                  FROM   RECIBO RE, OPER_POL OP, MOD_COBERT MC
                  WHERE  MC.PrimaFactMoneda > 0
                  AND    MC.IdeMovPrima = RE.IdeMovPrima
                  AND    RE.NumOper = OP.NumOper
                  AND    RE.numcert = OP.NumCert
                  AND    RE.IdePol = OP.IdePol
                  AND    OP.TipoOp = cTipoOp
                  AND    OP.NumOper = nNumOper
                  AND    OP.IdePol = nIdePol
                  and    OP.NumCert = nNumCert;  --BBVA-Consis 12/09/2012 - Se Agrega Variable nNumcert para que tenga en cuenta en calculo anivel de certificado
               EXCEPTION
                  WHEN OTHERS THEN
                     RAISE_APPLICATION_ERROR (-20288, PR.MENSAJE ('ABD', 20288, nNumOper, NULL, NULL));
               END;
               IF NVL (nTotCobert, 0) > 0 THEN
                  nAjusteFinal  := 0;
                  FOR MC IN COBERT_Q LOOP
                     nMtoPrimaPura   := NVL (nMontoOper, 0) - NVL (nMtoRecaDcto, 0);
                     nPorcPrima      := MC.PrimaFactMoneda / NVL (nMtoPrimaPura, 0);
                     nMtoAjuste      := NVL (nDiferencia, 0) * NVL (nPorcPrima, 0);
                     nAjusteAcum     := NVL (nAjusteAcum, 0) + NVL (nMtoAjuste, 0);
                     nPrimaAjustada  := MC.PrimaFactMoneda + NVL (nMtoAjuste, 0);
                     nCuentaCob      := NVL (nCuentaCob, 0) + 1;
                     IF NVL (nCuentaCob, 0) = NVL (nTotCobert, 0) THEN
                        IF NVL (nAjusteAcum, 0) <> NVL (nDiferencia, 0) THEN
                           nAjusteFinal    := NVL (nDiferencia, 0) - NVL (nAjusteAcum, 0);
                           nPrimaAjustada  := NVL (nPrimaAjustada, 0) + NVL (nAjusteFinal, 0);
                        END IF;
                     END IF;
                     BEGIN
                        UPDATE MOD_COBERT
                        SET PriMinAjuste = NVL (nMtoAjuste, 0) + NVL (nAjusteFinal, 0),
                            PriFactOrig = PrimaFactMoneda,
                            PrimaFactMoneda = NVL (nPrimaAjustada, 0),
                            PrimaMovFactor = NVL (nPrimaAjustada, 0)
                        WHERE  IdeCobert = MC.IdeCobert
                        AND    NumMod = MC.NumMod;
                     EXCEPTION
                        WHEN OTHERS THEN
                           RAISE_APPLICATION_ERROR (-22052,
                                                    PR.MENSAJE ('ABD', 20288, 'No Actualiz� MOD_COBERT en Ajuste de Prima M�nima', NULL, NULL));
                     END;
                     BEGIN
                        UPDATE TRANS_COBERT
                        SET MtoTransCobert = NVL (nPrimaAjustada, 0)
                        WHERE  IdeCobert = MC.IdeCobert
                        AND    NumMod = MC.NumMod;
                     EXCEPTION
                        WHEN OTHERS THEN
                           RAISE_APPLICATION_ERROR (-22052,
                                                    PR.MENSAJE ('ABD', 20288, 'No Actualiz� TRANS_COBERT en Ajuste de Prima M�nima', NULL, NULL));
                     END;
                     BEGIN
                        UPDATE MOV_PRIMA
                        SET MtoMoneda = MtoMoneda + NVL (nMtoAjuste, 0) + NVL (nAjusteFinal, 0),
                            MtoLocal = MtoLocal + ((NVL (nMtoAjuste, 0) + NVL (nAjusteFinal, 0)) * MC.TasaCambio),
                            MtoComMoneda = (MtoMoneda + NVL (nMtoAjuste, 0) + NVL (nAjusteFinal, 0)) * PorcCom / 100,
                            MtoComLocal = (MtoLocal + ((NVL (nMtoAjuste, 0) + NVL (nAjusteFinal, 0)) * MC.TasaCambio)) * PorcCom / 100
                        WHERE  IdeMovPrima = MC.IdeMovPrima;
                     EXCEPTION
                        WHEN OTHERS THEN
                           RAISE_APPLICATION_ERROR (-22052,
                                                    PR.MENSAJE ('ABD', 20288, 'No Actualiz� MOV_PRIMA en Ajuste de Prima M�nima', NULL, NULL));
                     END;
                     BEGIN
                        UPDATE RECIBO
                        SET MtoMoneda = MtoMoneda + NVL (nMtoAjuste, 0) + NVL (nAjusteFinal, 0),
                            MtoLocal = MtoLocal + ((NVL (nMtoAjuste, 0) + NVL (nAjusteFinal, 0)) * MC.TasaCambio),
                            MtoComMoneda = (MtoMoneda + NVL (nMtoAjuste, 0) + NVL (nAjusteFinal, 0)) * PorcCom / 100,
                            MtoComLocal = (MtoLocal + ((NVL (nMtoAjuste, 0) + NVL (nAjusteFinal, 0)) * MC.TasaCambio)) * PorcCom / 100
                        WHERE  IdeMovPrima = MC.IdeMovPrima;
                     EXCEPTION
                        WHEN OTHERS THEN
                           RAISE_APPLICATION_ERROR (-22052, PR.MENSAJE ('ABD', 20288, 'No Actualiz� RECIBO en Ajuste de Prima M�nima', NULL, NULL));
                     END;
                     nAjusteFinal    := 0;
                  END LOOP;
                  FOR GR IN GEN_RENG_Q LOOP
                     BEGIN
                        DELETE RENG_MP
                        WHERE  IdeMovPrima = GR.IdeMovPrima;
                     EXCEPTION
                        WHEN OTHERS THEN
                           RAISE_APPLICATION_ERROR (-22052, PR.MENSAJE ('ABD', 20288, 'No Elimin� RENG_MP en Ajuste de Prima M�nima', NULL, NULL));
                     END;
                     nMtoPrima  := PR_MOV_PRIMA.GENERAR_RENGLONES (GR.IdeMovPrima);
                  END LOOP;
               END IF;
            END IF;
         END LOOP;
      END IF;
      FOR AP IN ACT_OPER_Q LOOP
         BEGIN
            UPDATE OPER_POL
            SET MtoOper = AP.TotMtoMoneda
            WHERE  IdePol = AP.IdePol
            AND    NumCert = AP.NumCert
            AND    NumOper = nNumOper;
         EXCEPTION
            WHEN OTHERS THEN
               RAISE_APPLICATION_ERROR (-22052, PR.MENSAJE ('ABD', 20288, 'No Actualiz� OPER_POL en Ajuste de Prima M�nima', NULL, NULL));
         END;
      END LOOP;
   END VALIDA_PRIMA_MINIMA_ZONASIS;
    --
   --<<BBVA - Consis 20/11/2012 - Facturaci�n Manual o Complemento por Ramo.
   --
   PROCEDURE FACT_COMPLEMENTO_RAMO (nIdePol POLIZA.IdePol%TYPE, nNumCert     CERTIFICADO.NumCert%TYPE
                                   ,dFecMov DATE              , nMtoFactComp MOD_COBERT.PrimaMoneda%TYPE
                                   ,cModApl VARCHAR2          , cObservaFC   VARCHAR2 DEFAULT NULL) IS
     --
     nMontoApl        NUMBER(22,2);
     dFecFact         DATE;
     dFecFactF        DATE;
     --Cursores
     CURSOR c_CertR IS
       SELECT NumCert, CodPlan, RevPlan, CodRamoCert
       FROM   CERT_RAMO
       WHERE  IdePol = nIdePol
       AND    NumCert = NVL (nNumCert, NumCert)
       ORDER BY NumCert;
     rCertR              c_CertR%ROWTYPE;
     --
     CURSOR c_Cobert IS
       SELECT MC.IdeCobert, MAX (MC.NumMod) NumMod
       FROM   MOD_COBERT MC
       WHERE  MC.IdePol       = nIdePol
       AND    MC.NumCert      = rCertR.NumCert
       AND    MC.CodRamoCert  = rCertR.CodRamoCert
       AND    MC.StsModCobert NOT IN ('ANU', 'EXC', 'REV')
       AND    MC.IndFactComp  = 'N'
       AND    EXISTS (SELECT 1
                      FROM   POLIZA P
                      WHERE  P.IdePol                    = MC.IdePol
                      AND    NVL(P.IndAjusteBlanket,'N') = 'N')
       ---<<BBVA Consis EFVC 17/10/2014 Se agrga condicion psra que no duplique valores al hacer el cobro de facturacion complmento
       AND    MC.IDECOBERT IN (SELECT MIN(IDECOBERT)
                               FROM MOD_COBERT
                               WHERE  IdePol       = nIdePol
                               AND    NumCert      = rCertR.NumCert
                               AND    CodRamoCert  = rCertR.CodRamoCert
                               AND    StsModCobert NOT IN ('ANU', 'EXC', 'REV')
                               AND    IndFactComp  = 'N'
                               AND    PRIMAMONEDA  > 0)-- COTECSA FM 09/02/2018 *23135* Se coloca esta sentencia para que busque el m�nimo idecobert que tenga primamomeda > 0
       --
       GROUP BY IdeCobert
       UNION
       SELECT MC.IdeCobert, MAX (MC.NumMod) NumMod
       FROM   MOD_COBERT MC
       WHERE  MC.IdePol       = nIdePol
       AND    MC.NumCert      = rCertR.NumCert
       AND    MC.CodRamoCert  = rCertR.CodRamoCert
       AND    MC.StsModCobert NOT IN ('ANU', 'EXC', 'REV')
       AND    MC.IndFactComp  = 'N'
       AND    EXISTS (SELECT 1
                      FROM   POLIZA P
                      WHERE  P.IdePol                    = MC.IdePol
                      AND    NVL(P.IndAjusteBlanket,'N') = 'S')
       ---<<BBVA Consis EFVC 17/10/2014 Se agrga condicion psra que no duplique valores al hacer el cobro de facturacion complmento
       AND    MC.IDECOBERT IN (SELECT MIN(IDECOBERT)
                               FROM MOD_COBERT
                               WHERE  IdePol       = nIdePol
                               AND    NumCert      = rCertR.NumCert
                               AND    CodRamoCert  = rCertR.CodRamoCert
                               AND    StsModCobert NOT IN ('ANU', 'EXC', 'REV')
                               AND    IndFactComp  = 'N'
                               AND    PRIMAMONEDA  > 0)-- COTECSA FM 09/02/2018 *23135* Se coloca esta sentencia para que busque el m�nimo idecobert que tenga primamomeda > 0
       HAVING SUM (MC.PrimaMoneda) > 0
       GROUP BY IdeCobert;
       rCobert             c_Cobert%ROWTYPE;
       nPrimaTotMoneda     MOD_COBERT.PrimaMoneda%TYPE;
       nSumaAsegMoneda     MOD_COBERT.SumaAsegMoneda%TYPE;
       nTasa               MOD_COBERT.Tasa%TYPE;
       nPrimaMoneda        MOD_COBERT.PrimaMoneda%TYPE;
       nFactProp           NUMBER;
       nNumOper            OPER_POL.NumOper%TYPE;
       cIndAjusteBlanket   POLIZA.IndAjusteBlanket%TYPE;
       cCodProd            POLIZA.CodProd%TYPE;
       nProdInval          NUMBER(1):=0;
       cTextoEndoso        VARCHAR2(1000);
       dFecIniVig          DATE;
       dFecFinVig          DATE;
       cCodMoneda          POLIZA.CODMONEDA%TYPE;
       cStsCert            CERTIFICADO.StsCert%TYPE;
       cStsPol             POLIZA.StsPol%TYPE;
       nnummodmax          MOD_COBERT.NumMod%TYPE; -- 28/04/2016 CAMM
   --
   BEGIN
      OPEN c_CertR;
      LOOP
        FETCH  c_CertR
        INTO   rCertR;
        EXIT WHEN c_CertR%NOTFOUND;
        --Obtenemos el total de prima para en base a esto determinar el porcentaje de proporcion a distribuir entre coberturas
        BEGIN
           SELECT SUM (NVL (PrimaMoneda, 0))
           INTO   nPrimaTotMoneda
           FROM   MOD_COBERT
           WHERE (IdeCobert, NumMod) IN (SELECT MC.IdeCobert, MAX (MC.NumMod) NumMod
                                         FROM   MOD_COBERT MC
                                         WHERE  MC.IdePol       = nIdePol
                                         AND    MC.NumCert      = NVL (nNumCert, NumCert)
                                         AND    MC.CODRAMOCERT  = rCertR.CodRamocert
                                         AND    MC.StsModCobert NOT IN ('ANU', 'EXC', 'REV')
                                         AND    MC.IndFactComp  = 'N'
                                         AND    EXISTS (SELECT 1
                                                        FROM   POLIZA P
                                                        WHERE  P.IdePol                    = MC.IdePol
                                                        AND    NVL(P.IndAjusteBlanket,'N') = 'N')
                                                        ---<<BBVA Consis EFVC 17/10/2014 Se agrga condicion psra que no duplique valores al hacer el cobro de facturacion complmento
                                         AND    MC.IDECOBERT IN (SELECT MIN(IDECOBERT)
                                                                 FROM MOD_COBERT
                                                                 WHERE  IdePol       = nIdePol
                                                                 AND    NumCert      = rCertR.NumCert
                                                                 AND    CodRamoCert  = rCertR.CodRamoCert
                                                                 AND    StsModCobert NOT IN ('ANU', 'EXC', 'REV')
                                                                 AND    IndFactComp  = 'N'
                                                                 AND    PRIMAMONEDA  > 0)-- COTECSA FM 09/02/2018 *23135* Se coloca esta sentencia para que busque el m�nimo idecobert que tenga primamomeda > 0
                                         GROUP BY IdeCobert
                                         UNION
                                         SELECT MC.IdeCobert, MAX (MC.NumMod) NumMod
                                         FROM   MOD_COBERT MC
                                         WHERE  MC.IdePol       = nIdePol
                                         AND    MC.NumCert      = NVL (nNumCert, NumCert)
                                         AND    MC.CODRAMOCERT  = rCertR.CodRamocert
                                         AND    MC.StsModCobert NOT IN ('ANU', 'EXC', 'REV')
                                         AND    MC.IndFactComp  = 'N'
                                         AND    EXISTS (SELECT 1
                                                        FROM   POLIZA P
                                                        WHERE  P.IdePol                     = MC.IdePol
                                                        AND    NVL (P.IndAjusteBlanket,'N') = 'S')
                                         ---<<BBVA Consis EFVC 17/10/2014 Se agrga condicion psra que no duplique valores al hacer el cobro de facturacion complmento
                                         AND    MC.IDECOBERT IN (SELECT MIN(IDECOBERT)
                                                                 FROM   MOD_COBERT
                                                                 WHERE  IdePol       = nIdePol
                                                                 AND    NumCert      = rCertR.NumCert
                                                                 AND    CodRamoCert  = rCertR.CodRamoCert
                                                                 AND    StsModCobert NOT IN ('ANU', 'EXC', 'REV')
                                                                 AND    IndFactComp  = 'N'
                                                                 AND    PRIMAMONEDA  > 0)-- COTECSA FM 09/02/2018 *23135* Se coloca esta sentencia para que busque el m�nimo idecobert que tenga primamomeda > 0
                                         HAVING SUM (MC.PrimaMoneda) > 0
                                         GROUP BY IdeCobert);
        END;
        --
        BEGIN
           SELECT MONTOAPL, FecFact, FecFactF
           INTO   nMontoApl, dFecFact, dFecFactF
           FROM   FACTCOMPLEMENTO
           WHERE  IDEPOL      = nidePol
           AND    NUMCERT     = nNumCert
           AND    CODPLAN     = rCertR.CodPlan
           AND    REVPLAN     = rCertR.RevPlan
           AND    CODRAMOPLAN = rCertR.CodRamoCert
           AND    FECHAPL     = dFecMov --TRUNC(SYSDATE) 21/01/2015 L.A
           AND    NUMOPER     IS NULL;
        EXCEPTION WHEN NO_DATA_FOUND THEN
           nMontoApl := 0;
           dFecFact  := TRUNC(SYSDATE);
           dFecFactF := TRUNC(SYSDATE);
        END;
        --
        IF cModApl = 'P' THEN
            nMontoApl := nMtoFactComp * nMontoApl /100;
        ELSE
           nMontoApl := nMontoApl;
        END IF;
        --
        IF nPrimaTotMoneda = 0 THEN
            nPrimaTotMoneda  := 1;
        END IF;
        --
        nFactProp  := 0;
        --
        OPEN c_Cobert;
        LOOP
          FETCH  c_Cobert
          INTO   rCobert;
          EXIT WHEN c_Cobert%NOTFOUND;
          --Obtengo los datos del movimiento actual para generar el movimiento de complemento
          BEGIN
             SELECT PrimaMoneda
             INTO   nPrimaMoneda
             FROM   MOD_COBERT
             WHERE  IdeCobert = rCobert.IdeCobert
             AND    NumMod = rCobert.NumMod;
          EXCEPTION WHEN NO_DATA_FOUND THEN
             nPrimaMoneda  := 0;
                    WHEN OTHERS THEN
             RAISE_APPLICATION_ERROR (-20052, PR.MENSAJE ('ABD', 20052, 'Error en SELECT a MOD_COBERT:' || SQLERRM, NULL, NULL));
          END;
          --Obtenemos la proporci�n a aplicar sobre el nuevo valor para esta cobertura
          nFactProp  := nPrimaMoneda / nPrimaTotMoneda;
          --DV. 17/10/2013. Ajuste para Previsionales
          IF nPrimaTotMoneda = 1 AND nFactProp = 0 THEN
             nFactProp := 1;
          END IF;
          --<< COTECSA-FM 02/05/2018 *23432* se coloca este if para que solo se haga la inserci�n a la tabla MOD_COBERT al ramo y la cobertura que se le esta colocando l monto a facturar
          IF nMontoApl != 0 THEN  -- COTECSA-FM 10/05/2018 *23432* se comenta esta linea debido a que esta mal el if IF nMontoApl > 0 THEN
             --Generamos el movimiento de cobertura
             PR_MOD_COBERT.GENERAR_LIQ_COMPLEMENTO (rCobert.IdeCobert, rCobert.NumMod, nFactProp, nMontoApl, dFecMov);
             -- << CAMM 28/04/2016 Se busca el numero de modificacion insertado anteriormente
             SELECT MAX (nummod)
             INTO   nnummodmax
             FROM   MOD_COBERT
             WHERE  IdeCobert = rCobert.IdeCobert;
             -- >> CAMM 28/04/2016
             UPDATE MOD_COBERT
             SET    FecIniValid = dFecFact,
                    FecFinValid = dFecFactF
             WHERE  IdeCobert   = rCobert.IdeCobert
             AND    NumMod      = nnummodmax;
             --
          END IF;
          -->> COTECSA-FM 02/05/2018 *23432* se coloca este if para que solo se haga la inserci�n a la tabla MOD_COBERT al ramo y la cobertura que se le esta colocando l monto a facturar
        END LOOP;
        CLOSE c_Cobert;
      --
      END LOOP;
      CLOSE c_CertR;
      --Guardamos indicador de Ajuste blanket, para actualizarlo y poder activar la p�liza con monto
      BEGIN
         SELECT IndAjusteBlanket, CodProd, FecIniVig, FecFinVig,
                CodMoneda
         INTO   cIndAjusteBlanket, cCodProd, dFecIniVig, dFecFinVig,
                cCodMoneda
         FROM   POLIZA
         WHERE  IdePol = nIdePol;
      EXCEPTION WHEN NO_DATA_FOUND THEN
         cIndAjusteBlanket  := 'N';
      END;
      --
      UPDATE POLIZA
      SET    IndAjusteBlanket = 'N'
      WHERE  IdePol = nIdePol
      AND    IndAjusteBlanket <> 'N';
      --Activamos la p�liza para generar el respectivo  movimiento
      nNumOper  := PR_POLIZA.ACTIVAR (nIdePol, 'D');
      --Generamos las acreencias, facturas y contabilidad
      PR_ACREENCIA.GENERAR (nNumOper);
      PR_FRACCIONAMIENTO.GENERAR (nNumOper);
      PR_FRACCIONAMIENTO.FACTUR_REHAB (nNumOper);
      --Restauramos el valor original del Ajuste Blanket
      UPDATE POLIZA
      SET    IndAjusteBlanket = cIndAjusteBlanket
      WHERE  IdePol           = nIdePol;
      --
      UPDATE FACTCOMPLEMENTO
      SET    NUMOPER = nNumOper
      WHERE  IDEPOL  = nidePol
      AND    NUMCERT = nNumCert
      AND    FECHAPL = dFecMov --TRUNC(SYSDATE) 21/01/2015 L.A
      AND    NUMOPER IS NULL;
      --DV. 29/10/2013. Ajuste para Previsionales
      BEGIN
         SELECT 1
         INTO   nProdInval
         FROM   PRODUCTO
         WHERE  CodProd = cCodProd
         AND    DescProd LIKE '%INVALIDEZ%';
      EXCEPTION WHEN NO_DATA_FOUND THEN
          nProdInval := 0;
      END;
      --
      IF nProdInval = 1 THEN
         cTextoEndoso := 'EN APLICACI�N A LAS CONDICIONES DE CONTRATACI�N DE LA P�LIZA, POR MEDIO DEL '
                       ||'PRESENTE CERTIFICADO SE EFECT�A EL COBRO MENSUAL CORRESPONDIENTE A LAS '
                       ||'ACREDITACIONES DEL MES DE '||TO_CHAR(dFecFact,'MONTH')||' '||TO_CHAR(dFecFact,'YYYY')
                       ||' RELATIVAS A COTIZACIONES DEL PER�ODO COMPRENDIDO ENTRE '||TO_CHAR(dFecIniVig,'DD/MM/YYYY')
                       ||' Y '||TO_CHAR(dFecFinVig,'DD/MM/YYYY')||chr(10)
                       ||'POR LO ANTERIOR COBRAMOS LA SUMA DE: '||chr(10)
                       ||LTRIM(RTRIM(PR_MONTO_ESCRITO.MTO_ESCRITO_MON(nMontoApl,NULL,cCodMoneda)))
                       ||' M/CTE'||chr(10)||chr(10)||TO_CHAR(dFecFact,'MONTH')||' '||TO_CHAR(dFecFact,'YYYY')||chr(10)
                       ||chr(10)||'VIGENCIA P�LIZA '||TO_CHAR(dFecIniVig,'DD/MM/YYYY')||' HASTA '||TO_CHAR(dFecFinVig,'DD/MM/YYYY');
      ELSE
         cTextoEndoso := 'Facturaci�n Complemento. Fecha: '||TO_CHAR(dFecMov,'DD/MM/YYYY')||chr(10)||chr(10)||cObservaFC;
      END IF;
      --
      INSERT INTO TEXTO_ENDOSOS
            (IdePol, NumCert, NumOper, Tipo_ENDoso, Fec_Endoso, Desc_Cambio)
      VALUES(nIdePol, nNumCert, nNumOper, 'MOD', dFecMov, cTextoEndoso);
      --
      BEGIN
         SELECT StsCert
         INTO   cStsCert
         FROM   certificado
         WHERE  idepol   = nIdePol
         AND    numcert  = nNumCert;
      EXCEPTION WHEN NO_DATA_FOUND THEN
         RAISE_APPLICATION_ERROR (-20052,'No existe el Certificado '||SQLERRM);
      END;
      --
      IF cStsPol <> 'ACT' AND cStsCert = 'ACT' THEN
         PR_POLIZA.GENERAR_TRANSACCION (nIdePol, 'ACTIV');
      END IF;
      --EXCEPTION
      -- WHEN OTHERS THEN
      --  RAISE_APPLICATION_ERROR (-20052, PR.MENSAJE ('ABD', 20052, 'Error en FACT_COMPLEMENTO:' || SQLERRM, NULL, NULL));
   END FACT_COMPLEMENTO_RAMO;
   --<<BBVA - COnsis 20/11/2012
--Erick Zoque 09/04/2013 E_CON_20120815_1_1
PROCEDURE COMP_SUMAASEG(nIdePol  NUMBER,nNumOper NUMBER,cTipMov VARCHAR2 DEFAULT NULL,nSumaAsegAnt NUMBER DEFAULT NULL) IS
nNumOperMax       OPER_POL.NUMOPER%TYPE;
nNumOperAnt       OPER_POL.NUMOPER%TYPE;
nSumaAsegAct      MOD_COBERT.SUMAASEGMONEDA%TYPE := 0;
nSumaAsegAntPol   MOD_COBERT.SUMAASEGMONEDA%TYPE := 0;
--nSumaAsegAnt      MOD_COBERT.SUMAASEGMONEDA%TYPE := 0;
cTipoOper         OPER_POL.TIPOOP%TYPE;
--nIdeCompRev       COMPROBANTE.IDECOMP%TYPE;
nSumaAsegTot      NUMBER := 0;
nIdeComp          NUMBER;
cTipOper          VARCHAR2(3);
cModo             USUARIO.Modo%TYPE;
cCodUsr           USUARIO.CodUsr%TYPE;
cCodUsrOper       USUARIO.CodUsr%TYPE;
nIdeMovPrima      RECIBO.IdeMovPrima  %TYPE;
nNumCert          RECIBO.NumCert      %TYPE;
cCodRamo          RECIBO.CodRamoCert  %TYPE;
cCodProd          POLIZA.CodProd      %TYPE;
cCodOper          COMPROBANTE.TIPCOMP %TYPE := '025';
  --<< 21858 Fase 3 ATCP  04.07.2018 RQ23708 
dFecEmision                 OPER_POL.FecMov%TYPE; 
nTasaCambioEmi              OPER_POL.ValorTrm%TYPE;
  -->> 21858 Fase 3 ATCP  04.07.2018 RQ23708

CURSOR SUMA_ANT
IS
  SELECT MC.IDECOBERT, MAX(NUMMOD) - 1 NUMMOD, MC.CODRAMOCERT
  FROM   COBERT_CERT CC, POLIZA P, COBERT_PLAN_PROD CPP, MOD_COBERT MC
  WHERE  P.IdePol = nIdePol
  AND    P.IdePol = CC.IdePol
  AND    CPP.CodProd = P.CodProd
  AND    CPP.CodPlan = CC.CodPlan
  AND    CPP.RevPLan = CC.RevPlan
  AND    CPP.CodRamoPlan = CC.CodRamoCert
  AND    CPP.CodCobert = CC.CodCobert
  AND    MC.IDECOBERT = CC.IDECOBERT
  AND    MC.IDEMOVPRIMA = nIdeMovPrima
  AND    NVL (CPP.IndAcumulaSumaPoliza, 'N') = 'S'
  --AND    CC.StsCobert NOT IN('VAL','ANU','EXC')
  GROUP BY MC.IDECOBERT,MC.CODRAMOCERT
  UNION
  SELECT MC.IDECOBERT, MAX(NUMMOD) - 1 NUMMOD, MC.CODRAMOCERT
  FROM   COBERT_BIEN CB, POLIZA P, COBERT_PLAN_PROD CPP, MOD_COBERT MC, BIEN_CERT BC
  WHERE  P.IdePol = nIdePol
  AND    CPP.CodProd = P.CodProd
  AND    CPP.CodPlan = CB.CodPlan
  AND    CPP.RevPLan = CB.RevPlan
  AND    CPP.CodRamoPlan = BC.CodRamoCert
  AND    CPP.CodCobert = CB.CodCobert
  AND    MC.IDECOBERT = CB.IDECOBERT
  AND    BC.IDEBIEN = CB.IDEBIEN
  AND    MC.IDEMOVPRIMA = nIdeMovPrima
  AND    NVL (CPP.IndAcumulaSumaPoliza, 'N') = 'S'
  --AND    CC.StsCobert NOT IN('VAL','ANU','EXC')
  GROUP BY MC.IDECOBERT,MC.CODRAMOCERT
  UNION
  SELECT MC.IDECOBERT, MAX(NUMMOD) - 1 NUMMOD, MC.CODRAMOCERT
  FROM   COBERT_ASEG CA, POLIZA P, COBERT_PLAN_PROD CPP, MOD_COBERT MC,ASEGURADO A
  WHERE  P.IdePol = nIdePol
  AND    CPP.CodProd = P.CodProd
  AND    CPP.CodPlan = CA.CodPlan
  AND    CPP.RevPLan = CA.RevPlan
  AND    CPP.CodRamoPlan = A.CodRamoCert
  AND    CPP.CodCobert = CA.CodCobert
  AND    CA.IDEASEG = A.IDEASEG
  AND    MC.IDECOBERT = CA.IDECOBERT
  AND    MC.IDEMOVPRIMA = nIdeMovPrima
  AND    NVL (CPP.IndAcumulaSumaPoliza, 'N') = 'S'
  --AND    CC.StsCobert NOT IN('VAL','ANU','EXC')
  GROUP BY MC.IDECOBERT,MC.CODRAMOCERT;

CURSOR SUMA_ACT
IS
  SELECT MC.IDECOBERT, MAX(NUMMOD) NUMMOD, MC.CODRAMOCERT
  FROM   MOD_COBERT MC
  WHERE MC.IdeMovPrima  = nIdeMovPrima
  AND   EXISTS
      ( SELECT 1
        FROM   COBERT_CERT CC, COBERT_PLAN_PROD CPP
        WHERE MC.IdeCobert    = CC.IdeCobert
        AND   CC.IdePol       = nIdePol
        AND   CC.NumCert      = nNumCert
        AND   CPP.CodProd     = cCodProd
        AND   CPP.CodPlan     = CC.CodPlan
        AND   CPP.RevPLan     = CC.RevPlan
        AND   CPP.CodRamoPlan = CC.CodRamoCert
        AND   CPP.CodCobert   = CC.CodCobert
        AND   NVL (CPP.IndAcumulaSumaPoliza, 'N') = 'S'
      )
  GROUP BY MC.IDECOBERT,MC.CODRAMOCERT
  UNION
  SELECT MC.IDECOBERT, MAX(NUMMOD) NUMMOD, MC.CODRAMOCERT
  FROM   MOD_COBERT MC
  WHERE MC.IdeMovPrima  = nIdeMovPrima
  AND   EXISTS
      ( SELECT 1
        FROM COBERT_PLAN_PROD CPP, COBERT_BIEN CB
        WHERE MC.IdeCobert    = CB.IdeCobert
        AND   CPP.CodProd     = cCodProd
        AND   CPP.CodPlan     = CB.CodPlan
        AND   CPP.RevPLan     = CB.RevPlan
        AND   CPP.CodRamoPlan = cCodRamo
        AND   CPP.CodCobert   = CB.CodCobert
        AND    NVL (CPP.IndAcumulaSumaPoliza, 'N') = 'S'
      )
  GROUP BY MC.IDECOBERT,MC.CODRAMOCERT
  UNION
  SELECT MC.IDECOBERT, MAX(NUMMOD) NUMMOD, MC.CODRAMOCERT
  FROM   MOD_COBERT MC
  WHERE MC.IDEMOVPRIMA  = nIdeMovPrima
  AND   EXISTS
        ( SELECT 1
          FROM COBERT_ASEG CA, COBERT_PLAN_PROD CPP
          WHERE CA.IDECOBERT    = MC.IDECOBERT
          AND   CPP.CodPlan     = CA.CodPlan
          AND   CPP.RevPLan     = CA.RevPlan
          AND   CPP.CodCobert   = CA.CodCobert
          AND   CPP.CodRamoPlan = cCodRamo
          AND   CPP.CodProd     = cCodProd
          AND   NVL (CPP.IndAcumulaSumaPoliza, 'N') = 'S'
        )
  GROUP BY MC.IDECOBERT,MC.CODRAMOCERT;

BEGIN
--se determina si el usuario es contable para que genere el comprobante
   cCodUsr := RTRIM(USER,' ');

   BEGIN
    SELECT RTRIM(CODUSR,' ')
    INTO cCodUsrOper
    FROM OPERACION
    WHERE NumOper = nNumOper;
   EXCEPTION
    WHEN NO_DATA_FOUND THEN
      NULL;
   END;

   IF NVL(cCodUsrOper,'XX') != 'XX' THEN
    IF cCodUsrOper != cCodUsr THEN
      cCodUsr:=cCodUsrOper;
    END IF;
   END IF;

   BEGIN
    SELECT  Modo
    INTO    cModo
    FROM    USUARIO
    WHERE   CodUsr = cCodUsr;
   EXCEPTION
      WHEN NO_DATA_FOUND THEN
        NULL;
   END;
   IF  cModo  <> '2' THEN
     PR_POLIZA.CARGAR(nIdePol);
     cCodProd := PR_POLIZA.cCodProd;

     SELECT MAX(NUMOPER)
     INTO   nNumOperMax
     FROM   OPER_POL
     WHERE  IDEPOL = nIdePol;

     FOR REG IN
      ( SELECT NUMCERT, IDEMOVPRIMA, CODRAMOCERT
        FROM RECIBO
        WHERE NUMOPER = nNumOperMax
      )
     LOOP
        nIdeMovPrima  :=  REG.IDEMOVPRIMA;
        nNumCert      :=  REG.NUMCERT;
        cCodRamo      :=  REG.CODRAMOCERT;

        --CURSOR PARA ACTUALIZAR LA SUMA ASEGURADA ANTERIOR Y ACTUAL
        FOR X IN SUMA_ACT
        LOOP

          --> Suma Anterior
          BEGIN
             SELECT SUMAASEGMONEDA
             INTO   nSumaAsegAntPol
             FROM   MOD_COBERT
             WHERE  IDECOBERT = X.IDECOBERT
             AND    NUMMOD    = X.NUMMOD - 1;
          EXCEPTION
             WHEN NO_DATA_FOUND THEN
              nSumaAsegAntPol := 0;
          END;
          --> Suma Actual
          BEGIN
             SELECT SUMAASEGMONEDA
             INTO   nSumaAsegAct
             FROM   MOD_COBERT
             WHERE  IDECOBERT = X.IDECOBERT
             AND    NUMMOD    = X.NUMMOD;
          EXCEPTION
             WHEN NO_DATA_FOUND THEN
                nSumaAsegAct := 0;
          END;

          INSERT INTO T$_SUMAASEG_COMP
            (IDEPOL,IDECOBERT,CODRAMO,SUMAANT,SUMAACT)
          VALUES
            (nIdePol,X.IDECOBERT,X.CODRAMOCERT,nSumaAsegAntPol,nSumaAsegAct);
        END LOOP;
     END LOOP;

     SELECT NVL(SUM(SUMAACT),0) - NVL(SUM(SUMAANT),0)
     INTO   nSumaAsegTot
     FROM   T$_SUMAASEG_COMP
     WHERE  IDEPOL = nIdePol;

     IF nSumaAsegTot != 0 THEN
       nIdeComp := PR_COMPROBANTE.ABRIR (cCodOper,PR_POLIZA.cCodOfiSusc,nNumOperMax,PR_POLIZA.cCodCia,PR_POLIZA.cTipoPdcion,PR_POLIZA.cCodMoneda);
       FOR REG IN (SELECT NVL(SUM(SUMAACT),0) - NVL(SUM(SUMAANT),0) SUMAFIN,
                          CODRAMO
                   FROM   T$_SUMAASEG_COMP
                   WHERE  IDEPOL = nIdePol
                   GROUP BY CODRAMO ) LOOP
          PR_MOV_DEFINITIVO.GEN_MOV_SUMAASEG(nIdeComp,PR_POLIZA.cCodCia,cCodOper,REG.SUMAFIN,PR_POLIZA.cCodOfiSusc,REG.CODRAMO,PR_POLIZA.CCODPROD);
       END LOOP;
       PR_COMPROBANTE.CERRAR(nIdeComp,'PRC');
        --Ini 04/07/2018 Fase 3 21858 Consis  RQ#22361
        BEGIN
              SELECT MIN(O.FECMOV)
                 INTO dFecEmision
                 FROM OPER_POL O
                WHERE O.IDEPOL = PR_POLIZA.NIDEPOL
                  AND O.TIPOOP = 'EMI';        
         END;  
        nTasaCambioEmi :=PR_TASA_CAMBIO.BUSCA_TASA (cCodMoneda, dFecEmision, PR_POLIZA.NIDEPOL);
        PR_COMPROBANTE.CERRAR(nIdeComp,'PRC');

        UPDATE MOV_DEFINITIVO
        SET    TASACAMBIO = nTasaCambioEmi
        WHERE  IDECOMP = nIdeComp;
       --FIN 04/07/2018 Fase 3 21858 Consis  RQ#22361 
     END IF;

     DELETE T$_SUMAASEG_COMP WHERE IDEPOL = nIdePol;
   END IF;
END COMP_SUMAASEG;
--
FUNCTION SUMA_ASEGURADA_POL (nIdePol NUMBER)  RETURN NUMBER IS
 nTotSumaAseg  COBERT_CERT.SumaAseg%TYPE:=0;
 nSumaAsegCert COBERT_CERT.SumaAseg%TYPE:=0;
 nSumaAsegBien COBERT_CERT.SumaAseg%TYPE:=0;
 nSumaAsegAseg COBERT_CERT.SumaAseg%TYPE:=0;
BEGIN
   -- SUMA ASEGURADA SOLO CON COBERTURAS DEL CERTIFICADO
   BEGIN
      SELECT NVL (SUM (CC.SumaAsegMoneda), 0)
      INTO   nSumaAsegCert
      FROM   COBERT_CERT CC, POLIZA P, COBERT_PLAN_PROD CPP
      WHERE  P.IdePol = nIdePol
      AND    P.IdePol = CC.IdePol
      AND    CPP.CodProd = P.CodProd
      AND    CPP.CodPlan = CC.CodPlan
      AND    CPP.RevPLan = CC.RevPlan
      AND    CPP.CodRamoPlan = CC.CodRamoCert
      AND    CPP.CodCobert = CC.CodCobert
      AND    NVL (CPP.IndAcumulaSumaPoliza, 'N') = 'S'
      AND    CC.StsCobert NOT IN('VAL','ANU','EXC');
   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         nSumaAsegCert  := 0;
   END;
   -- SUMA ASEGURADA SOLO CON COBERTURAS DEL BIEN
   BEGIN
      SELECT NVL (SUM (CC.SumaAsegMoneda), 0)
      INTO   nSumaAsegBien
      FROM   COBERT_BIEN CC, BIEN_CERT BC, POLIZA P, COBERT_PLAN_PROD CPP
      WHERE  P.IdePol = nIdePol
      AND    P.IdePol = BC.IdePol
      AND    BC.IdeBien = cc.IdeBien
      AND    CPP.CodProd = P.CodProd
      AND    CPP.CodPlan = CC.CodPlan
      AND    CPP.RevPLan = CC.RevPlan
      AND    CPP.CodRamoPlan = BC.CodRamoCert
      AND    CPP.CodCobert = CC.CodCobert
      AND    NVL (CPP.IndAcumulaSumaPoliza, 'N') = 'S'
      AND    CC.StsCobert NOT IN('VAL','ANU','EXC');
   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         nSumaAsegBien  := 0;
   END;
   BEGIN
      SELECT NVL (SUM (CC.SumaAsegMoneda), 0)
      INTO   nSumaAsegAseg
      FROM   COBERT_ASEG CC, ASEGURADO A, POLIZA P, COBERT_PLAN_PROD CPP
      WHERE  P.IdePol = nIdePol
      AND    P.IdePol = A.IdePol
      AND    A.IdeAseg = CC.IdeAseg
      AND    CPP.CodProd = P.CodProd
      AND    CPP.CodPlan = CC.CodPlan
      AND    CPP.RevPLan = CC.RevPlan
      AND    CPP.CodRamoPlan = A.CodRamoCert
      AND    CPP.CodCobert = CC.CodCobert
      AND    NVL (CPP.IndAcumulaSumaPoliza, 'N') = 'S'
      AND    CC.StsCobert NOT IN('VAL','ANU','EXC');
   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         nSumaAsegAseg  := 0;
   END;
   nTotSumaAseg  := nTotSumaAseg + NVL (nSumaAsegCert, 0) + NVL (nSumaAsegAseg, 0) + NVL (nSumaAsegBien, 0);
   RETURN (NVL (nTotSumaAseg, 0));
END SUMA_ASEGURADA_POL;
--
FUNCTION CRECIMIENTO_POL (nIdePol NUMBER)  RETURN NUMBER IS
 cCodProd     POLIZA.CodProd%TYPE;
 cCodPlan     CERT_RAMO.CodPlan%TYPE;
 cRevPlan     CERT_RAMO.RevPlan%TYPE;
 cCrecimiento COBERT_PLAN_PROD.PARAMAUMENTOCOBERTREN%TYPE;
BEGIN
 BEGIN
   SELECT p.CodProd, c.CodPlan, c.RevPlan
   INTO   cCodProd, cCodPlan, cRevPlan
   FROM   POLIZA p, CERT_RAMO c
   WHERE  c.IdePol = nIdePOL
   AND    p.IdePol = c.IdePol
   AND    RowNum = 1;
 EXCEPTION
   WHEN NO_DATA_FOUND THEN
     cCodProd := NULL;
 END;
 IF cCodProd IS NOT NULL THEN
   BEGIN
     SELECT PARAMAUMENTOCOBERTREN
     INTO   cCrecimiento
     FROM   COBERT_PLAN_PROD
     WHERE  CodProd = cCodProd
     AND    CodPlan = cCodPlan
     AND    RevPlan = cRevPlan
     AND    IndCobertBase = 'S'
     AND    PARAMAUMENTOCOBERTREN IS NOT NULL
     AND    RowNum = 1;
   EXCEPTION
     WHEN NO_DATA_FOUND THEN
       cCrecimiento := NULL;
   END;
 END IF;
 RETURN(cCrecimiento);
 --
END CRECIMIENTO_POL;
--
--<<BBVA Consis 06/09/2013. Procedimiento que Actualiza los datos de NumCredito y Origen de la Poliza que viene de bancaseguros
PROCEDURE ACT_DATOS_BANCASEGUROS_POL (nIdepol NUMBER, cNumCredBanco VARCHAR2,cOrigPol VARCHAR2) IS
   ctexto VARCHAR2(500);
   BEGIN
      BEGIN
         UPDATE POLIZA
         SET NumCredBanco = cNumCredBanco,
             OrigePol = cNumCredBanco
         WHERE  Idepol = nIdepol;
      EXCEPTION
         WHEN OTHERS THEN
            RAISE_APPLICATION_ERROR (-20100, PR.MENSAJE ('ABD', 22052, 'Error en Busqueda Producto ' || cCodprod, NULL, NULL));
      END;
      --
      ctexto :='Usu�rio: '||USER||' - Fecha : '||TO_CHAR(sysdate,'DD/MON/RRRR')||' - Hora: '||TO_CHAR(sysdate,'HH:MI:SS')||chr(10)||'N�mero Credito :'||cNumCredBanco||chr(10)||'Origen Poliza: '||cOrigPol||chr(10)||'                 ------ // -----';

      BEGIN
         INSERT INTO POLIZA_TEXTO (IDEPOL,TEXTPOL)
                                  VALUES(nIdepol,ctexto);
      EXCEPTION
        WHEN DUP_VAL_ON_INDEX THEN
             UPDATE     POLIZA_TEXTO
             SET TEXTPOL = TEXTPOL||chr(10)||ctexto
             WHERE IDEPOL = nIdepol;
      END;
      --
   END ACT_DATOS_BANCASEGUROS_POL;
   -->>BBVA Consis 06/09/2013. Procedimiento que Actualiza los datos de NumCredito y Origen de la Poliza que viene de bancaseguros
/*   PROCEDURE REVERSAR_MODIFICACION(nIdePol NUMBER) IS
   BEGIN
     REVERTIR_MODIFICACION(nIdePol);
   END REVERSAR_MODIFICACION;*/

  PROCEDURE REVERTIR_MODIFICACION(nIdePol NUMBER) IS
    nExiste NUMBER;

    CURSOR REG_CERTIFICADO IS
      SELECT NumCert, StsCert
        FROM CERTIFICADO
       WHERE IdePol = nIdePol
         AND StsCert IN ('MOD','INC');

  BEGIN
    FOR X IN REG_CERTIFICADO LOOP
      PR_CERTIFICADO.REVERTIR_MODIFICACION(nIdePol, X.NumCert, NULL, NULL);
    END LOOP;
    UPDATE POLIZA
       SET CodMotvAnul  = NULL,
           FecAnul      = NULL,
           TipoAnul     = NULL
     WHERE IdePol = nIdePol;

     BEGIN
       SELECT count(*)
       INTO   nExiste
       FROM   Certificado a
       WHERE  a.IdePol   = nIdePol
       AND    a.StsCert IN ('MOD', 'INC');
     END;
     IF nExiste = 0 THEN
        PR_POLIZA.GENERAR_TRANSACCION(nIdePol, 'REVER');
     END IF;

  END;

-- BBVA Consis 21/01/2015 L.A
FUNCTION VALIDA_FECHA_CODINTER (nIdepol poliza.IdePol%TYPE) RETURN NUMBER IS

 dFecInivig_pol DATE;
 dfecfinvig_pol DATE;
 nExiste        NUMBER(1);
 dFecIniVig_Can_Pol DATE;
 dFecFinVig_Can_Pol DATE;


CURSOR canal_pol IS
 SELECT fecinivig, fecfinvig, codinter, idepol
 FROM  canal_poliza
 WHERE idepol  = nidepol;

/*CURSOR esquema_poliz is
 SELECT fecinivig, fecfinvig
 FROM   esquema_poliza
 WHERE  idepol = nidepol;*/

BEGIN

     BEGIN
      SELECT fecinivig, fecfinvig
      INTO   dFecInivig_pol, dfecfinvig_pol
      FROM   poliza
      WHERE  idepol  = nidepol;
   EXCEPTION
    WHEN NO_DATA_FOUND THEN
    RAISE_APPLICATION_ERROR(-20100,'No Existe la poliza'||nidepol);
   END;
 --Compara las fechas de vigencias de la poliza con las fechas de canales de la poliza o fechas de comisiones estan deben ser iguales
  IF dfecfinvig_pol IS NOT NULL THEN
     FOR X IN canal_pol LOOP
 -- Busca que el intermediario de que esta en canal poliza sea el mismo que esta en part_inter_pol
       BEGIN
        SELECT 0
        INTO  nExiste
        FROM  part_inter_pol
        WHERE idepol   = X.idepol
        AND   codinter = X.codinter;
       EXCEPTION
        WHEN NO_DATA_FOUND THEN
         nExiste:=1;
       END;

       BEGIN
        SELECT MIN(fecinivig), MAX(fecfinvig)
        INTO dFecIniVig_Can_Pol, dFecFinVig_Can_Pol
        FROM  canal_poliza
        WHERE idepol  = X.idepol;
       END;

       IF dFecIniVig_Can_Pol <> dFecInivig_pol OR  dFecFinVig_Can_Pol <> dfecfinvig_pol THEN
         nExiste:=2;
       END IF;

     END LOOP;

   /*--Compara las fechas de vigencias de la poliza contra las fechas de esquema poliza etan deben ser iguales o fechas de comisiones
     FOR Z IN esquema_poliz LOOP
      IF dFecInivig_pol <> Z.fecinivig OR dfecfinvig_pol <> Z.fecfinvig THEN
       nExiste:=3;
      END IF;
     END LOOP;*/
  END IF;
  RETURN(nExiste);
END;
-- BBVA Consis 21/01/2015 L.A
--EZ 08/07/2015
PROCEDURE COMPENSAR_REH(nIdePol NUMBER) IS
  nNumOperOblg         NUMBER;
  nNumOperFact         NUMBER;
  nNumSeq              NUMBER(14);
  nIdeDocIng           NUMBER;
  nCantFact            NUMBER;
  nMtoTotOblig         NUMBER;
  nNumero              NUMBER;
  cUserCob             VARCHAR2(20);
  nMtototNeto          NUMBER;
  nMtoTotDocIngMoneda  NUMBER;
  nMtoTotDif           NUMBER;
  nGenAcreObl          NUMBER;
  nNumAcre             ACREENCIA.NUMACRE%TYPE;
  cTextAcre            VARCHAR2(50);
  cTextOblig           VARCHAR2(50);
  cCodCanal            VARCHAR2(4);
  cCodSubCanal         VARCHAR2(4);
  nNumObligPaG         NUMBER;
  nNumRelIng           NUMBER;
  nSldFact             NUMBER;
  nDelta               NUMBER;
  nTasaCambio          NUMBER;
  cCodMonedaObl        VARCHAR2(3);
  cNumRelIng           VARCHAR2(20);
  cDescNumRefDoc       VARCHAR2(200);
  nExiste              NUMBER(10);
  nNumOblig            OBLIGACION.NUMOBLIG%TYPE;
  nSldoObligMoneda     OBLIGACION.SLDOOBLIGMONEDA%TYPE;
  dFecGtiaPago         OBLIGACION.FECGTIAPAGO%TYPE;
  cTipoOblig           OBLIGACION.TIPOOBLIG%TYPE;
  cStsOblig            OBLIGACION.STSOBLIG%TYPE;
  cMonOblig            OBLIGACION.CODMONEDA%TYPE;
  nNumObligDef         OBLIGACION.NUMOBLIGDEF%TYPE;
  nIdCarga             NUMBER := NULL;
  nExistFac            NUMBER(10);  --ATCP RM20548


CURSOR FACTURAS_Q IS
  SELECT F.IDEFACT ,F.CODFACT,F.NUMFACT,F.TIPOID,F.NUMID,F.DVID,F.CODMONEDA MONFACT,F.MTOFACTLOCAL,
         F.CODINTER,F.MTOFACTMONEDA,F.STSFACT,F.FECSTS,F.CODOFIFACT,
         F.FECVENCFACT,'OK' OK, 0, F.NUMFACTDEF, F.CODCIA, F.SLDOFACTMONEDA, F.SLDOFACTLOCAL, CF.NUMCERT
  FROM   FACTURA F,
         ACREENCIA A,
         GIROS_FINANCIAMIENTO G,
         COND_FINANCIAMIENTO CF
  WHERE  F.NumOper  =  nNumOperFact
  AND    F.StsFact  = 'ACT'
  AND    F.SldoFactMoneda > 0
  AND    F.IdeFact  =  A.IdeFact
  AND    A.TipoAcre =  'PRI'
  AND    G.NUMACRE  =  A.NUMACRE
  AND    G.NUMGIRO  =  1
  AND    CF.NUMFINANC = G.NUMFINANC
  ORDER BY F.IdeFact ASC;

CURSOR C_T$_OBLIGACIONES IS
  SELECT NUMOBLIG
  FROM T$_OBLIGACION
  WHERE NumSeq = nNumSeq;


BEGIN
  --SE VERIFICA SI EXISTE OBLIGACION TIPO DEVOLU GENERADO EN LA ANULACION
  BEGIN
    SELECT NVL(MAX(O.NUMOPER),0)
    INTO   nNumOperOblg
    FROM   OBLIGACION O, OPER_POL OP, DET_OBLIG DO
    WHERE  OP.IDEPOL = nIdePol
    AND    OP.TIPOOP = 'ANU'
    AND    OP.NUMOPER = O.NUMOPER
    AND    O.NUMOBLIG = DO.NUMOBLIG
    AND    DO.CODCPTOEGRE = 'DEVOLU';
  END;
  --SE DETERMINA EL NUMERO DE OPERACION GENERADO POR LA REH
  BEGIN
    SELECT NVL(MAX(NUMOPER),0)
    INTO   nNumOperFact
    FROM   OPER_POL
    WHERE  IDEPOL = nIdePol
    AND    TIPOOP = 'REH';--'MOD'; -->> BBVA - 1.0.1.1 EFBC RM22491 - La operaci�n de reversi�n es tipo REH
  END;

  --SI EXISTE UNA OBLIGACION DEVOLU SE REALIZA CRUCE CON LA FACTURA DE LA REHABILITACION
  IF nNumOperOblg != 0 THEN
    PR_VINISUSC.CARGAR;

    SELECT SQ_TEMP_COBRANZA.NEXTVAL
    INTO   nNumSeq
    FROM   SYS.DUAL;

    FOR X IN FACTURAS_Q LOOP
     --<<RM20548 ATCP 10/11/2016 Se valida que exista factura con la misma secuencia
       BEGIN
        SELECT 1
        INTO  nExistFac
        FROM T$_FACTURA T
        WHERE T.IDEFACT = X.IDEFACT
        AND   T.NUMSEQ = nNumSeq;
        EXCEPTION
        WHEN NO_DATA_FOUND THEN
         nExistFac:=0;
        WHEN OTHERS THEN
         nExistFac:=1;
       END;
    IF NVL(nExistFac,0) = 0 THEN

      INSERT INTO T$_FACTURA (IDEFACT,CODFACT,NUMFACT,TIPOID,
                         NUMID,DVID,CODMONEDA,MTOFACTLOCAL,CODINTER,MTOFACTMONEDA,STSFACT,
                         FECSTS,CODOFIFACT,FECVENCFACT,NUMSEQ,OK,MTODIFERENCIA,NUMFACTDEF,
                         CODCIA,TEXTO,MTOTRMRECAUDO,MTOABONOMONEDA,MTOABONOLOCAL,IDEREMESA)

       VALUES ( X.IDEFACT,X.CODFACT,X.NUMFACT,X.TIPOID,X.NUMID,X.DVID,X.MONFACT,X.MTOFACTLOCAL,
                X.CODINTER,X.MTOFACTMONEDA,X.STSFACT,X.FECSTS,X.CODOFIFACT,
                X.FECVENCFACT,nNumSeq,'OK', 0, X.NUMFACTDEF, X.CODCIA,NULL, 1,X.SLDOFACTMONEDA,
                X.SLDOFACTLOCAL,null);

      --se toma la obligacion devolu a devolver
      BEGIN
        SELECT O.NumOblig, O.SldoObligMoneda,O.FecGtiaPago,O.TipoOblig,O.StsOblig,
               O.CodMoneda, O.NumObligDef
        INTO   nNumOblig,nSldoObligMoneda,dFecGtiaPago,cTipoOblig,cStsOblig,
               cMonOblig,nNumObligDef
        FROM   OBLIGACION O, DET_OBLIG DO, PRE_OBLIG PO
        WHERE  O.NUMOPER = nNumOperOblg
        AND    O.SldoObligMoneda > 0
        AND    O.NUMOBLIG = DO.NUMOBLIG
        AND    DO.CODCPTOEGRE = 'DEVOLU'
        AND    PO.NUMOBLIG = O.NUMOBLIG
        AND    PO.NUMCERT = X.NUMCERT;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          nNumOblig := 0;
        WHEN TOO_MANY_ROWS THEN
          nNumOblig := 0;
      END;

      BEGIN
        SELECT 1
        INTO   nExiste
        FROM   T$_OBLIGACION
        WHERE  NumSeq  =nNumSeq
        AND    NumOblig=nNumOblig;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
           nExiste:=0;
      END;

      IF nExiste = 0 AND nNumOblig != 0 THEN
          INSERT INTO T$_OBLIGACION(NumOblig,TipoId,NumId,DvId,MtoOblig,IndSelec,NumSeq,FecGtiaPago,SldoObligMoneda,TipoOblig,StsOblig,CodMoneda,NumObligDef)
              VALUES (nNumOblig,X.TipoId,X.NumId,X.DvId,nSldoObligMoneda,'S',nNumSeq,dFecGtiaPago,nSldoObligMoneda,cTipoOblig,cStsOblig,cMonOblig,nNumObligDef);
      END IF;

      BEGIN
        SELECT COUNT(*)
        INTO   nCantFact
        FROM   T$_FACTURA
        WHERE  NumSeq=nNumSeq
        AND    Ok='OK';
      END;

      BEGIN
        SELECT SUM(MtoOblig)
        INTO   nMtoTotOblig
        FROM   T$_OBLIGACION
        WHERE  NumSeq=nNumSeq
        AND    IndSelec = 'S';

        IF nMtoTotOblig != 0 THEN

           SELECT NUMOBLIG, T.CODMONEDA
           INTO   nNumObligPag,cCodMonedaObl
           FROM   T$_OBLIGACION T
           WHERE  NumSeq = nNumSeq
           AND    IndSelec = 'S';

           nTasaCambio := PR_TASA_CAMBIO.BUSCA_TASA(cCodMonedaObl,SYSDATE);
        END IF;
      END;

      UPDATE T$_FACTURA
      SET    MtoAbonoMoneda = nMtoTotOblig,
             MtoAbonoLocal  = nMtoTotOblig * nTasaCambio
      WHERE IdeFact = X.IDEFACT;

      IF nCantFact <> 0 THEN
        IF nMtoTotOblig > 0 THEN
          BEGIN
            SELECT NVL(MAX(NumDocIng),0)+1
            INTO   nNumero
            FROM   T$_DOC_ING
            WHERE  NumSeq    = nNumSeq;
          EXCEPTION
            WHEN NO_DATA_FOUND THEN
              nNumero := 0;
          END;

          BEGIN
            SELECT SQ_DOCING.NEXTVAL
            INTO   nIdeDocIng
            FROM   SYS.DUAL;
          END;

          BEGIN
            SELECT USER
            INTO   cUserCob
            FROM   SYS.DUAL;
          END;

          FOR DC IN C_T$_OBLIGACIONES LOOP
            cDescNumRefDoc := DC.NUMOBLIG||'-';
          END LOOP;

          INSERT INTO T$_DOC_ING(FecSts,         TipoDocIng,               FecValDocIng,
                                 NumRefDoc,      MtoDocIngLocal,           MtoDocIngMoneda,
                                 CodMoneda,      NumSeq,                   StsDocIng,
                                 NumRelIng,      IdeDocIng,                NumDocIng,
                                 Usuario)
                          VALUES(SYSDATE,        'CAO',                    SYSDATE,
                                 SUBSTR(cDescNumRefDoc,1,20),nMtoTotOblig, nMtoTotOblig,
                                 PR_VINISUSC.CODIGO_MONEDA,  TO_NUMBER(nNumSeq),'VAL',
                                 NULL,          nIdeDocIng,                 nNumero,
                                 cUserCob);
        ELSE
          NULL;
        END IF;
      ELSE
        NULL;
      END IF;
    END IF; --RM20548 ATCP 10/11/2016
      SELECT NVL(SUM(MtoAbonoMoneda),0)
      INTO   nMtoTotNeto
      FROM   T$_FACTURA
      WHERE  NumSeq = nNumSeq;

      --inicio del proceso de cobro
      IF NVL(nMtoTotOblig,0) != 0 AND NVL(nMtoTotNeto,0) != 0 THEN
        PR_REL_ING.COBRAR_COMPEN(X.TipoId,X.NumId,X.DvId,nNumSeq,nMtoTotNeto);
        PR_POLIZA.CARGAR(nIdePol);

        BEGIN
           SELECT CODCANAL,CODSUBCANAL
           INTO   cCodCanal,cCodSubCanal
           FROM   POLIZA
           WHERE  IDEPOL = nIdePol;
        EXCEPTION
           WHEN NO_DATA_FOUND THEN
              RAISE_APPLICATION_ERROR (-20288, 'No se encontraron datos de la poliza');
        END;

        BEGIN
           SELECT NUMRELING, F.SLDOFACTLOCAL
           INTO   nNumRelIng, nSldFact
           FROM   FACTURA F
           WHERE  IDEFACT = X.IDEFACT;
        EXCEPTION
           WHEN NO_DATA_FOUND THEN
              RAISE_APPLICATION_ERROR (-20288, 'No se encontraron datos de la factura');
        END;

        BEGIN
           SELECT NUMRELINGDEF
           INTO   cNumRelIng
           FROM   REL_ING
           WHERE  NUMRELING = nNumRelIng;
        EXCEPTION
           WHEN NO_DATA_FOUND THEN
              RAISE_APPLICATION_ERROR (-20288, 'No se encontraron datos de la relacion de ingreso');
        END;

        BEGIN
          SELECT NVL(SUM(MONTO),0)
          INTO   nDelta
          FROM   SOBRANTE_FALTANTE_DELTA
          WHERE  NUMRELING = nNumRelIng;
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            nDelta := 0;
        END;

        INSERT INTO T$_CRUCE_ACTAS( CODCIA,CODCANAL,CODSUBCANAL,CODOFIEMI,CODPOL,NUMPOL,
                                    NUMCERT,NUMOBLIG,NUMFACTDEF,NUMRELINGDEF,MTOFACTLOCAL,
                                    MTODOCINGLOCAL,DELTA,idcarga)
        VALUES( PR_POLIZA.CCODCIA,cCodCanal,cCodSubCanal,PR_POLIZA.CCODOFIEMI,PR_POLIZA.CCODPROD,PR_POLIZA.NNUMPOL,
                X.NumCert,nNumObligPag,X.NUMFACTDEF,cNumRelIng,X.SLDOFACTLOCAL,
                nMtoTotOblig,nDelta,nIdCarga);

      END IF;
    --RM20548 ATCP 10/11/2016 Se traslada deletes fuera del if proceso de cobro
       DELETE T$_FACTURA    WHERE  NumSeq = nNumSeq;
       DELETE T$_DOC_ING    WHERE  NumSeq = nNumSeq;
       DELETE T$_OBLIGACION WHERE  NumSeq = nNumSeq;
       DELETE T$_COBRANZA   WHERE  NumSeq = nNumSeq;
       -->>RM20548 ATCP 10/11/2016
      --SI LA OBLIGACION ES MAYOR QUE LA FACTURA Y AL DIF ES DELTA SE EJECUTA ESTE ROC PARA QUE PAGUE LA OBLIG
      PR_PAGOS.PAGO_OBLIG_DELTA_REH(nNumOblig);
    END LOOP;--FIN FACTURAS_Q
  END IF;
END COMPENSAR_REH;
--BBVA Consis Redmine 13581 23/07/2015 L.A
PROCEDURE VALIDA_CANCELA_SALDO (nIdePol poliza.IdePol%TYPE) IS

cStsPol      poliza.StsPol%TYPE;
nNumCotiFlex poliza.NumCotiFlex%TYPE;
nExisteCSL   NUMBER(1):=0;
cTipoAcre    acreencia.TipoAcre%TYPE;

-- Cursor que busca Facturas con saldo pendiente para realizar el cruce con obligaciones
CURSOR c_Factura_Saldo IS
    SELECT f.indcancelsal, f.idefact, f.tipoid, f.numid, f.dvid, op.NumOper
    FROM   OPER_POL op, FACTURA f
    WHERE  op.IdePol        = nIdePol
    AND    f.NumOper        = op.NumOper
    AND    f.sldofactmoneda > 0
    AND    f.StsFact        = 'ACT';

BEGIN
  BEGIN
    SELECT P.StsPol, P.NumCotiFlex
    INTO   cStsPol,  nNumCotiFlex
    FROM   POLIZA P
    WHERE  IdePol = nIdePol;
  EXCEPTION
   WHEN NO_DATA_FOUND THEN
    RAISE_APPLICATION_ERROR (-20288, 'No existe la poliza: '||nIdePol||'...Por favor verifique');
  END;

  IF cStsPol = 'ANU' AND nNumCotiFlex IS NOT NULL THEN
    FOR X IN c_Factura_Saldo LOOP
     IF X.indcancelsal IS NULL OR X.indcancelsal <> 'N' THEN
      BEGIN
        UPDATE FACTURA
        SET    indcancelsal = 'S'
        WHERE  IdeFact      = X.IdeFact;
      END;
      --Busca el tipo de acreencia (PRI) para ejecutar la cancelacion de saldos
      BEGIN
        SELECT ac.TipoAcre
        INTO   cTipoAcre
        FROM   ACREENCIA ac
        WHERE  ac.IdeFact = X.IdeFact;
      EXCEPTION
       WHEN TOO_MANY_ROWS THEN
        cTipoAcre := 'XXX';
      END;
     END IF;
     BEGIN
       SELECT 1
       INTO   nExisteCSL
       FROM   CANCEL_SALDOS
       WHERE  IdeFact   = X.IdeFact
       AND    StsCancel = 'ACT';
     EXCEPTION
       WHEN NO_DATA_FOUND THEN
         nExisteCSL := 0;
     END;

     IF cTipoAcre = 'PRI' AND nExisteCSL = 0 THEN
      --dbms_output.put_line('va a CANCELA_SALDOS X.IdeFact: '||X.IdeFact);
      BEGIN
       PR_REL_ING.CANCELA_SALDOS (cTipoAcre,NULL,X.IdeFact,nIdePol,X.TipoId,X.NumId,X.DvId);
      END;
     END IF;
    END LOOP;
 END IF;
END VALIDA_CANCELA_SALDO;
--BBVA Consis Redmine 13581 23/07/2015 L.A
--<<BBVA 29/09/2016 #20756 Consis EDMV Se crea un nuevo procedimiento, para cumplir con el cruce de poliza AcselWeb con prorrata 5

PROCEDURE CRUCE_ACTAS_WEB (nIdePol poliza.IdePol%TYPE) IS

  nMtoTotFact  NUMBER(22,2) :=0;
  nMtoTotOblig NUMBER(22,2) :=0;
  nExistPol    NUMBER(1):=0;
  nIdCarga     NUMBER(12) := 0;

BEGIN

 DELETE a$_poliza where IdePol = nIdePol;

 BEGIN
   nIdCarga := PR_INT_FUNCIONES.TRAE_IDCARGA;
 END;

   nMtoTotFact :=0;
   nMtoTotOblig:=0;
     BEGIN
       SAVEPOINT COMPENSAR_AUTO;

         PR_REL_ING.COMPENSAR_MANUAL_INDIVIDUAL(nIdePol,nIdCarga);

       --BBVA Consis Redmine 13581 23/07/2015 L.A
        BEGIN
          SELECT 1
          INTO   nExistPol
          FROM   a$_poliza
          WHERE  IdePol = nIdePol;
        EXCEPTION
         WHEN NO_DATA_FOUND THEN
            nExistPol := 0;
        END;

        IF nExistPol = 0 THEN
          INSERT INTO  a$_poliza (IdePol)
          VALUES            (nIdePol);
        END IF;

        BEGIN
         SELECT NVL(SUM(F.SldoFactMoneda),0)
         INTO   nMtoTotFact
         FROM   ACREENCIA A,FACTURA F, OPER_POL OP
         WHERE  OP.NumOper = F.NumOper
         AND    F.IdeFact  = A.IdeFact
         AND    A.TipoAcre ='PRI'
         AND    OP.IdePol  = nIdePol
         AND    F.StsFact  != 'ANU';
        END;

        BEGIN
         SELECT NVL(SUM(OB.SldoObligMoneda),0)
         INTO   nMtoTotOblig
         FROM   DET_OBLIG DTO, OBLIGACION OB, OPER_POL OP
         WHERE  OP.NumOper = OB.NumOper
         AND    OP.IdePol  = nIdePol
         AND    OB.TipoOblig = 'PRI'
         AND    OB.StsOblig != 'ANU'
         AND    OB.NumOblig = DTO.Numoblig
         AND    DTO.CodCLaEgre = 'DEVOLU'
         AND    DTO.CodCptoEgre ='DISMIN';
        END;

       --BBVA Consis Redmine 13581 23/07/2015 L.A
        IF  ABS(nMtoTotFact - nMtoTotOblig) != 0 AND ABS(nMtoTotFact - nMtoTotOblig) <= PR.BUSCA_PARAMETRO('096') THEN
           BEGIN
             PR_POLIZA.VALIDA_CANCELA_SALDO (nIdePol);
           END;
        ELSIF (nMtoTotFact = 0 and nMtoTotOblig > 0 AND nMtoTotOblig > PR.BUSCA_PARAMETRO('096') ) THEN
           -- JR. 30/10/2015
           BEGIN
             PR_PAGOS.PAGO_OBLIG_PRI(nIdePol);
           END;
        END IF;
    COMMIT;
   END;
END CRUCE_ACTAS_WEB;

--<<COTECSA-MX #23183--
PROCEDURE VALIDAR_MOVIMIENTO ( npIdePol NUMBER ) IS

nExiste         NUMBER(1);
nExCERT         NUMBER(1);
nExRAMO         NUMBER(1);
nExBIEN         NUMBER(1);
nExASEG         NUMBER(1);
nExCOBASEG      NUMBER(1);
nExCOBBIEN      NUMBER(1);
nExCOBCERT      NUMBER(1);

cError          VARCHAR2 (2000);
cMsjAlerta      VARCHAR2 (2000);
nCantCert       NUMBER(20);
cNumCert        VARCHAR2 (2000);
cSeparador      VARCHAR2 (10);

CURSOR C_MOD_COBERT IS
    SELECT  MC.NumCert
    FROM    MOD_COBERT MC
    WHERE   MC.IdePol       = npIdePol
    AND     MC.StsModCobert IN ('VAL','INC','MOD');

BEGIN

    cError          := NULL;
    cMsjAlerta      := NULL;

    BEGIN
        SELECT  1
        INTO    nExiste
        FROM    MOD_COBERT MC
        WHERE   MC.IdePol       = npIdePol
        AND     MC.StsModCobert IN ('VAL','INC','MOD')
        AND     ROWNUM          = 1;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            nExiste := 0;
        WHEN OTHERS THEN
            nExiste := 1;
            cError  := SQLERRM;
    END;

    IF nExiste = 1 THEN

        IF cError IS NULL THEN

            nCantCert   := 0;
            cNumCert    := 'Nro. ';

            FOR CER IN C_MOD_COBERT LOOP

                nCantCert   := nCantCert + 1;

                IF nCantCert != 1 THEN
                    cSeparador    := ',';
                ELSE
                    cSeparador    := NULL;
                END IF;

                cNumCert    := cNumCert||cSeparador||CER.NumCert;

                IF nCantCert >= 10 THEN
                    EXIT;
                END IF;

            END LOOP;

            IF nCantCert >= 10 THEN
                cMsjAlerta      := 'Existen Movimientos pendientes en Varios Certificados ';
            ELSE
                cMsjAlerta      := 'Existen Movimientos pendientes por Activar en los Certificados '||cNumCert;
            END IF;

        ELSE
            cMsjAlerta      := 'Error al Validar Movimientos de Poliza. '||cError;

        END IF;

        RAISE_APPLICATION_ERROR (-20201,cMsjAlerta);

    ELSE
        cMsjAlerta          := NULL;
    END IF;

END VALIDAR_MOVIMIENTO;
-->>COTECSA-MX #23183--

END PR_POLIZA;
/
